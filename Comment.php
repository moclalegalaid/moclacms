<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Comment extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'comments';
    public $fillable = [
        'news_id',
        'comment',
        'username',
    ];

    public function news()
    {
       return $this->belongsTo('App\News', 'news_id');
    }
    
}