<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         // Create user Super Admin
        $user  = User::firstOrCreate([
            'fname' =>'Super',
            'lname' => 'Admin',
            'username' => 'admin',
            'name' => 'Super Admin',
            'email' => 'admin@mocla.com',
            'password' => bcrypt('12345678'),
            'verified' => 1,
        ]);
        $user->assignRole('super-admin');
    }
}
