-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2021 at 09:09 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mocla-main`
--

-- --------------------------------------------------------

--
-- Table structure for table `aidtypes`
--

CREATE TABLE `aidtypes` (
  `id` int(10) NOT NULL,
  `name` varchar(190) NOT NULL,
  `created_by` int(10) NOT NULL,
  `creator_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aidtypes`
--

INSERT INTO `aidtypes` (`id`, `name`, `created_by`, `creator_name`, `created_at`, `updated_at`) VALUES
(1, 'Legal Advice', 1, 'Super Admin', '2021-08-29 05:27:10', '2021-08-29 05:27:10'),
(2, 'Legal Guidelines', 1, 'Super Admin', '2021-08-29 05:28:06', '2021-08-29 05:28:06'),
(3, 'Legal Representation', 1, 'Super Admin', '2021-08-29 05:28:26', '2021-08-29 05:28:26'),
(4, 'Legal Documents', 1, 'Super Admin', '2021-08-29 05:28:43', '2021-08-29 05:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `event` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `auditable_id` int(10) UNSIGNED NOT NULL,
  `auditable_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `old_values` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_values` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `tags`, `created_at`, `updated_at`) VALUES
(1, 1, 'created', 2, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/users/managements/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-02 21:15:20', '2020-08-02 21:15:20'),
(2, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-02 21:34:47', '2020-08-02 21:34:47'),
(3, 2, 'updated', 2, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-03 04:12:24', '2020-08-03 04:12:24'),
(4, 2, 'updated', 2, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-03 04:14:38', '2020-08-03 04:14:38'),
(5, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 05:08:18', '2020-08-06 05:08:18'),
(6, 1, 'created', 1, 'App\\News', '[]', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"hjdkhkj\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:31:35', '2020-08-06 17:31:35'),
(7, 1, 'created', 2, 'App\\News', '[]', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"hjdkhkj\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:33:11', '2020-08-06 17:33:11'),
(8, 1, 'created', 3, 'App\\News', '[]', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"Waziri wa Katiba na Sheria Dkt. Mwingulu Nchemba amewataka watendaji wa Ofisi ya Wakili Mkuu wa Serikali kutekeleza majukumu yao kwa weledi ili kuhakikisha haki inapatikana kwa ajili ya maslahi ya Taifa.\\r\\n\\r\\nAkizungumza leo jijini Dar es Salaam wakati akifanya ziara ya kikazi katika Ofisi ya Wakili Mkuu wa Serikali Dkt. Nchemba, amesema kuwa hatapenda kuona uzembe unafanywa kwa mawakili wasomi wa serikali katika kusimamia haki.\\r\\n\\r\\n\\u2018Mtendaji yoyote akifanya kosa ambalo litaleta madhara kwa taifa kupitia utendaji wake, atachukuliwa hatua za kisheria kwa kuwa amehujumu uchumi\\u201d amesema Dkt. Nchemba\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:34:26', '2020-08-06 17:34:26'),
(9, 1, 'created', 4, 'App\\News', '[]', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"Waziri wa Katiba na Sheria Dkt. Mwingulu Nchemba amewataka watendaji wa Ofisi ya Wakili Mkuu wa Serikali kutekeleza majukumu yao kwa weledi ili kuhakikisha haki inapatikana kwa ajili ya maslahi ya Taifa.\\r\\n\\r\\nAkizungumza leo jijini Dar es Salaam wakati akifanya ziara ya kikazi katika Ofisi ya Wakili Mkuu wa Serikali Dkt. Nchemba, amesema kuwa hatapenda kuona uzembe unafanywa kwa mawakili wasomi wa serikali katika kusimamia haki.\\r\\n\\r\\n\\u2018Mtendaji yoyote akifanya kosa ambalo litaleta madhara kwa taifa kupitia utendaji wake, atachukuliwa hatua za kisheria kwa kuwa amehujumu uchumi\\u201d amesema Dkt. Nchemba\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:38:19', '2020-08-06 17:38:19'),
(10, 1, 'created', 1, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:38:19', '2020-08-06 17:38:19'),
(11, 1, 'created', 5, 'App\\News', '[]', '{\"title\":\"KATIBU MKUU KATIBA NA SHERIA AWATAKA POLISI, MAGEREZA KUHAKIKISHA HAKI ZA MTOTO ZINALINDWA NA KUHIFADHIWA\",\"content\":\"SERIKALI imewataka Polisi na Magereza nchini kuhakikisha haki na maslahi ya mtoto anayekinzana na sheria zinalindwa na kuhifadhiwa kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto Na. 21 ya 2009.\\r\\n\\r\\nHayo yamesemwa na Katibu Mkuu wa Wizara ya Katiba na Sheria Prof. Sifuni Mchome wakati akifungua Mafunzo ya siku nne kwa maafisa wa Jeshi la Polisi na Magereza kutoka katika mikoa ya Iringa na Mbeya kuhusu utoaji wa huduma ya msaada wa kisheria kwa watoto jana katika ukumbi wa Mahakama ya Tanzania Kanda ya Mbeya- jijini Mbeya.\\r\\n\\r\\nProf. Mchome amesema Wizara kwa kushirikiana na wadau, kama vile UNICEF na wengine, imeendelea kuzingatia maslahi ya mtoto anayekinzana na sheria kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:41:20', '2020-08-06 17:41:20'),
(12, 1, 'created', 2, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 17:41:20', '2020-08-06 17:41:20'),
(13, 1, 'created', 1, 'App\\News', '[]', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"SERIKALI imewataka Polisi na Magereza nchini kuhakikisha haki na maslahi ya mtoto anayekinzana na sheria zinalindwa na kuhifadhiwa kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto Na. 21 ya 2009.\\r\\n\\r\\nHayo yamesemwa na Katibu Mkuu wa Wizara ya Katiba na Sheria Prof. Sifuni Mchome wakati akifungua Mafunzo ya siku nne kwa maafisa wa Jeshi la Polisi na Magereza kutoka katika mikoa ya Iringa na Mbeya kuhusu utoaji wa huduma ya msaada wa kisheria kwa watoto jana katika ukumbi wa Mahakama ya Tanzania Kanda ya Mbeya- jijini Mbeya.\\r\\n\\r\\nProf. Mchome amesema Wizara kwa kushirikiana na wadau, kama vile UNICEF na wengine, imeendelea kuzingatia maslahi ya mtoto anayekinzana na sheria kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:07:34', '2020-08-06 18:07:34'),
(14, 1, 'created', 1, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:07:34', '2020-08-06 18:07:34'),
(15, 1, 'created', 2, 'App\\News', '[]', '{\"title\":\"KATIBU MKUU KATIBA NA SHERIA AWATAKA POLISI, MAGEREZA KUHAKIKISHA HAKI ZA MTOTO ZINALINDWA NA KUHIFADHIWA\",\"content\":\"Hivi karibuni, Mhe. Jaji Mkuu alitoa Kanuni za ushughulikiaji wa mashauri yanayohusu watu wenye mahitaji maalum, wakiwamo watoto. Kanuni hizo zinaweka masharti ya kuhakikisha upelelezi, uendeshaji na utoaji wa hukumu kwa mashauri ya watoto unafanyika kwa wakati kutokana na changamoto zilizopo katika mashauri hayo amesisitiza Prof. Mchome.\\r\\n\\r\\nAidha, Prof. Sifuni Mchome ametaja hatua za zilizochukuliwa na Serikali katika kuboresha mfumo wa haki jinai kuwa ni marekebisho ya sheria ya Mwenendo wa Makosa ya Jinai, Sura ya 20 kwa kuweka utaratibu wa kufifisha makosa na kuimarisha mfumo wa upatanishi katika makosa ya jinai kwa lengo la kupunguza mrundikano wa mashauri Mahakamani na kuondoa msongamano wa mahabusu na wafungwa katika vituo vya polisi na magereza, hivyo kuwataka mafisa wa polisi na Magereza kuzitumia njia hizo\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:12:28', '2020-08-06 18:12:28'),
(16, 1, 'created', 2, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:12:28', '2020-08-06 18:12:28'),
(17, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:15:50', '2020-08-06 18:15:50'),
(18, 1, 'created', 3, 'App\\News', '[]', '{\"title\":\"KIKAO CHA SADC SEKTA YA SHERIA CHAKUBALIANA UTAFITI UFANYWE KUGUNDUA KINACHOCHELEWESHA URIDHIWAJI WA ITIFAKI\",\"content\":\"Waziri wa Katiba na Sheria Mhe. Dkt. Mwigulu Nchemba ambaye ni Mwenyekiti wa SADC kwa sekta ya sheria asema kikao cha SADC kwa sekta ya sheria kimekubaliana utafiti ufanywe ili kugundua kinachochelewesha uridhiwaji wa itifaki.\\r\\n\\r\\nWaziri Nchemba ameyasema hayo baada ya kumalizika kwa kikao cha Mawaziri wa Sheria na Wanasheria Wakuu wa nchi za SADC kilichofanyika jijini Dar Es Salaam katika Kituo cha Mikutano cha Mwalimu Nyerere.\\r\\n\\r\\nWaziri Nchemba alisema katika kikao hicho wamekubaliana kutoweka ukomo wa uridhiwaji wa itifaki bali wafanye utafiti ili kujua sababu za ucheleweshwaji huo na japokuwa nchi zinatofautiana taratibu nyingine zinaenda Bunge na nyingine na Baraza la Mawaziri kwanza kujadiliwa kabla ya kuwasilishwa kikaoni, hivyo hiyo ni moja ya sababu ya ucheleweshwaji.\\r\\n\\r\\n\\u201c Tumekubaliana kutoweka ukomo wa uridhiwaji wa itifaki bali tujue tatizo la ucheleweshwaji\\u201d alisema Waziri Nchemba.\\r\\n\\r\\nKatika majadiliano yao Waziri Nchemba amesema wamekubaliana masuala mengine kama takwimu yasubirie misimamo ya ndani ya nchi halafu ndipo iwasilishwe katika kikao ngazi ya Mawaziri na Wanasheria Wakuu ili yaweze kutolewa maaamuzi\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:51:51', '2020-08-06 18:51:51'),
(19, 1, 'created', 3, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 18:51:51', '2020-08-06 18:51:51'),
(20, 1, 'created', 4, 'App\\News', '[]', '{\"title\":\"Mfano wa Post mpya\",\"content\":\"Content\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 19:06:14', '2020-08-06 19:06:14'),
(21, 1, 'created', 4, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 19:06:14', '2020-08-06 19:06:14'),
(22, 1, 'created', 5, 'App\\News', '[]', '{\"title\":\"Test Slider\",\"content\":\"Test\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 19:08:46', '2020-08-06 19:08:46'),
(23, 1, 'created', 5, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-06 19:08:46', '2020-08-06 19:08:46'),
(24, 1, 'deleted', 5, 'App\\News', '{\"title\":\"Test Slider edited\",\"content\":\"Test edit\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/5?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-07 04:36:45', '2020-08-07 04:36:45'),
(25, 1, 'created', 6, 'App\\News', '[]', '{\"title\":\"News\",\"content\":\"Another new\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-08 11:07:52', '2020-08-08 11:07:52'),
(26, 1, 'created', 6, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-08 11:07:53', '2020-08-08 11:07:53'),
(27, 1, 'deleted', 6, 'App\\News', '{\"title\":\"News\",\"content\":\"Another new\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/6?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 09:09:48', '2020-08-10 09:09:48'),
(28, 1, 'deleted', 4, 'App\\News', '{\"title\":\"Mfano wa Post mpya\",\"content\":\"Content\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/4?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 09:09:55', '2020-08-10 09:09:55'),
(29, 1, 'deleted', 3, 'App\\News', '{\"title\":\"KIKAO CHA SADC SEKTA YA SHERIA CHAKUBALIANA UTAFITI UFANYWE KUGUNDUA KINACHOCHELEWESHA URIDHIWAJI WA ITIFAKI\",\"content\":\"Waziri wa Katiba na Sheria Mhe. Dkt. Mwigulu Nchemba ambaye ni Mwenyekiti wa SADC kwa sekta ya sheria asema kikao cha SADC kwa sekta ya sheria kimekubaliana utafiti ufanywe ili kugundua kinachochelewesha uridhiwaji wa itifaki.\\r\\n\\r\\nWaziri Nchemba ameyasema hayo baada ya kumalizika kwa kikao cha Mawaziri wa Sheria na Wanasheria Wakuu wa nchi za SADC kilichofanyika jijini Dar Es Salaam katika Kituo cha Mikutano cha Mwalimu Nyerere.\\r\\n\\r\\nWaziri Nchemba alisema katika kikao hicho wamekubaliana kutoweka ukomo wa uridhiwaji wa itifaki bali wafanye utafiti ili kujua sababu za ucheleweshwaji huo na japokuwa nchi zinatofautiana taratibu nyingine zinaenda Bunge na nyingine na Baraza la Mawaziri kwanza kujadiliwa kabla ya kuwasilishwa kikaoni, hivyo hiyo ni moja ya sababu ya ucheleweshwaji.\\r\\n\\r\\n\\u201c Tumekubaliana kutoweka ukomo wa uridhiwaji wa itifaki bali tujue tatizo la ucheleweshwaji\\u201d alisema Waziri Nchemba.\\r\\n\\r\\nKatika majadiliano yao Waziri Nchemba amesema wamekubaliana masuala mengine kama takwimu yasubirie misimamo ya ndani ya nchi halafu ndipo iwasilishwe katika kikao ngazi ya Mawaziri na Wanasheria Wakuu ili yaweze kutolewa maaamuzi\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/3?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 09:10:01', '2020-08-10 09:10:01'),
(30, 1, 'deleted', 2, 'App\\News', '{\"title\":\"KATIBU MKUU KATIBA NA SHERIA AWATAKA POLISI, MAGEREZA KUHAKIKISHA HAKI ZA MTOTO ZINALINDWA NA KUHIFADHIWA\",\"content\":\"Hivi karibuni, Mhe. Jaji Mkuu alitoa Kanuni za ushughulikiaji wa mashauri yanayohusu watu wenye mahitaji maalum, wakiwamo watoto. Kanuni hizo zinaweka masharti ya kuhakikisha upelelezi, uendeshaji na utoaji wa hukumu kwa mashauri ya watoto unafanyika kwa wakati kutokana na changamoto zilizopo katika mashauri hayo amesisitiza Prof. Mchome.\\r\\n\\r\\nAidha, Prof. Sifuni Mchome ametaja hatua za zilizochukuliwa na Serikali katika kuboresha mfumo wa haki jinai kuwa ni marekebisho ya sheria ya Mwenendo wa Makosa ya Jinai, Sura ya 20 kwa kuweka utaratibu wa kufifisha makosa na kuimarisha mfumo wa upatanishi katika makosa ya jinai kwa lengo la kupunguza mrundikano wa mashauri Mahakamani na kuondoa msongamano wa mahabusu na wafungwa katika vituo vya polisi na magereza, hivyo kuwataka mafisa wa polisi na Magereza kuzitumia njia hizo\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/2?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 09:10:50', '2020-08-10 09:10:50'),
(31, 1, 'deleted', 1, 'App\\News', '{\"title\":\"WAZIRI WA KATIBA NA SHERIA DKT. NCHEMBA ATEMBELEA OFISI YA WAKILI MKUU WA SERIKALI\",\"content\":\"SERIKALI imewataka Polisi na Magereza nchini kuhakikisha haki na maslahi ya mtoto anayekinzana na sheria zinalindwa na kuhifadhiwa kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto Na. 21 ya 2009.\\r\\n\\r\\nHayo yamesemwa na Katibu Mkuu wa Wizara ya Katiba na Sheria Prof. Sifuni Mchome wakati akifungua Mafunzo ya siku nne kwa maafisa wa Jeshi la Polisi na Magereza kutoka katika mikoa ya Iringa na Mbeya kuhusu utoaji wa huduma ya msaada wa kisheria kwa watoto jana katika ukumbi wa Mahakama ya Tanzania Kanda ya Mbeya- jijini Mbeya.\\r\\n\\r\\nProf. Mchome amesema Wizara kwa kushirikiana na wadau, kama vile UNICEF na wengine, imeendelea kuzingatia maslahi ya mtoto anayekinzana na sheria kwa kuhakikisha kuwa shauri linalomhusu linapewa kipaumbele na linashughulikiwa kwa mujibu wa Sheria ya Mtoto\"}', '[]', 'http://localhost/mocla-legal-aid/public/news/delete/1?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 09:10:55', '2020-08-10 09:10:55'),
(32, 1, 'created', 7, 'App\\News', '[]', '{\"title\":\"MAFUNZO KWA MAAFISA WA JESHI LA POLISI\",\"content\":\"Mafunzo ya siku mbili kwa maafisa wa Jeshi la Polisi kutoka katika mikoa ya Iringa na Mbeya kuhusu utoaji wa huduma ya msaada wa kisheria kwa watoto katika ukumbi wa Mahakama ya Tanzania Kanda ya Mbeya- jijini Mbeya\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:33:18', '2020-08-10 10:33:18'),
(33, 1, 'created', 7, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:33:18', '2020-08-10 10:33:18'),
(34, 1, 'created', 8, 'App\\News', '[]', '{\"title\":\"SERIKALI KUFUTA ADA YA USAJILI KWA WATOA HUDUMA YA MSAADA WA KISHERIA\",\"content\":\"Naibu Katibu Mkuu wa Wizara ya Sheria na Katiba Amon Mpanju (kushoto) akibadilishana mawazo na Kaimu Katibu Tawala Mkoa wa Tabora (Utumishi na Utawala) Hamis Mkunga (kulia)  wakati wa  hafla fupi ya uzinduzi Kamati ya Uratibu wa Huduma ya Msaada wa Kisheria kwa ngazi ya Mkoa wa Tabora jana\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:36:25', '2020-08-10 10:36:25'),
(35, 1, 'created', 8, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:36:25', '2020-08-10 10:36:25'),
(36, 1, 'created', 9, 'App\\News', '[]', '{\"title\":\"ZIARA YA KAIM KATIBU WIZARA YA KATIBA NA SHERIA AFANYA ZIARA MKOA TABORA\",\"content\":\"Kaimu Katibu Tawala Mkoa wa Tabora (Utumishi na Utawala) Hamis Mkunga akitoa maelezo mafupi kuhusu migogoro mbalimbali inayowakabili wananchi wa wakati wa  hafla fupi ya uzinduzi Kamati ya Uratibu wa Huduma ya Msaada wa Kisheria kwa ngazi ya Mkoa wa Tabora jana\"}', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:39:11', '2020-08-10 10:39:11'),
(37, 1, 'created', 9, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla-legal-aid/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', NULL, '2020-08-10 10:39:11', '2020-08-10 10:39:11'),
(38, 1, 'created', 1, 'App\\Background', '[]', '{\"title\":\"Background\",\"content\":\"6.\\tAbout us\\/Kuhusu sisi: Idara,Vitengo, Historia ya watoa huduma ya msaada wa kisheria, Ofisi ya Msajili wa Watoa huduma ya msaada wa kisheria na majukumu yake, orodha ya wasajili wasaidizi wa mikoa na wilaya, orodha ya watoa msaada wa kisheria na mahali walipo, Huduma zitolewazo na wizara kwa umma kupitia DCJM,DPLS,DHR,DNWROU\"}', 'http://localhost/mocla-legal-aid/public/home-page/background?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-01 15:30:38', '2020-09-01 15:30:38'),
(39, 1, 'created', 2, 'App\\Background', '[]', '{\"title\":\"Mission\",\"content\":\"Kuwa na Mfumo Madhubuti wa Kikatiba na Kisheria Wenye Kufanikisha Mipango ya Maendeleo ya Taifa\"}', 'http://localhost/mocla-legal-aid/public/home-page/mission?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 06:30:41', '2020-09-02 06:30:41'),
(40, 1, 'created', 3, 'App\\Background', '[]', '{\"title\":\"Vission\",\"content\":\"Katiba na Sheria Wezeshi kwa Maendeleo ya Taifa.\"}', 'http://localhost/mocla-legal-aid/public/home-page/vission?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 06:33:14', '2020-09-02 06:33:14'),
(41, 1, 'created', 1, 'App\\Faq', '[]', '{\"title\":\"How\",\"answer\":\"By Visiting MocLA Offices or legal aid offices at your region\"}', 'http://localhost/mocla-legal-aid/public/home-page/how?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 07:23:52', '2020-09-02 07:23:52'),
(42, 1, 'created', 2, 'App\\Faq', '[]', '{\"title\":\"How\",\"answer\":\"Hklkasdklmkldlkajdmakldasd\"}', 'http://localhost/mocla-legal-aid/public/home-page/how?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 07:40:06', '2020-09-02 07:40:06'),
(43, 1, 'created', 3, 'App\\Faq', '[]', '{\"title\":\"What\",\"answer\":\"Is a service provided by Legal aid Services\"}', 'http://localhost/mocla-legal-aid/public/home-page/what?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 08:02:29', '2020-09-02 08:02:29'),
(44, 1, 'created', 4, 'App\\Faq', '[]', '{\"title\":\"What\",\"answer\":\"Is a week of ......\"}', 'http://localhost/mocla-legal-aid/public/home-page/what?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 08:05:37', '2020-09-02 08:05:37'),
(45, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-02 08:28:37', '2020-09-02 08:28:37'),
(46, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-06 08:25:22', '2020-09-06 08:25:22'),
(47, NULL, 'created', 1, 'App\\Comment', '[]', '[]', 'http://localhost/mocla-legal-aid/public/post-comment/post/8?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-06 08:40:18', '2020-09-06 08:40:18'),
(48, NULL, 'created', 2, 'App\\Comment', '[]', '[]', 'http://localhost/mocla-legal-aid/public/post-comment/post/9?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', NULL, '2020-09-06 08:41:29', '2020-09-06 08:41:29'),
(49, 1, 'created', 10, 'App\\News', '[]', '{\"title\":\"SAMPLE VIDEO\",\"content\":\"Sample video\"}', 'http://localhost/mocla-legal-aid/public/media/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', NULL, '2020-09-27 10:46:42', '2020-09-27 10:46:42'),
(50, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', NULL, '2020-09-27 11:55:25', '2020-09-27 11:55:25'),
(51, 1, 'created', 5, 'App\\Faq', '[]', '{\"title\":\"How\",\"answer\":\"Sample Answer\"}', 'http://localhost/mocla-legal-aid/public/home-page/how?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', NULL, '2020-10-05 02:10:06', '2020-10-05 02:10:06'),
(52, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla-legal-aid/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', NULL, '2020-10-17 03:00:47', '2020-10-17 03:00:47'),
(53, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/LEGAL-AID/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', NULL, '2020-11-13 04:24:25', '2020-11-13 04:24:25'),
(54, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/LEGAL-AID/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, '2020-12-21 06:32:51', '2020-12-21 06:32:51'),
(55, 1, 'created', 1, 'App\\Management', '[]', '{\"title\":\"Waziri\"}', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 07:34:58', '2021-05-19 07:34:58'),
(56, 1, 'created', 10, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 07:34:59', '2021-05-19 07:34:59'),
(57, 1, 'created', 2, 'App\\Management', '[]', '{\"title\":\"Waziri\"}', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 07:37:39', '2021-05-19 07:37:39'),
(58, 1, 'created', 11, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 07:37:39', '2021-05-19 07:37:39'),
(59, 1, 'created', 1, 'App\\Structure', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 09:15:00', '2021-05-19 09:15:00'),
(60, 1, 'created', 12, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 09:15:00', '2021-05-19 09:15:00'),
(61, 1, 'created', 1, 'App\\Service', '[]', '[]', 'http://localhost/mocla/public/service/add-srv?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 09:21:38', '2021-05-19 09:21:38'),
(62, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', NULL, '2021-05-19 09:34:26', '2021-05-19 09:34:26'),
(63, 1, 'created', 2, 'App\\Structure', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:54:38', '2021-05-20 09:54:38'),
(64, 1, 'created', 13, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:54:39', '2021-05-20 09:54:39'),
(65, 1, 'deleted', 1, 'App\\Structure', '[]', '[]', 'http://localhost/mocla/public/structure/delete-str/1?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:54:47', '2021-05-20 09:54:47'),
(66, 1, 'deleted', 2, 'App\\Structure', '[]', '[]', 'http://localhost/mocla/public/structure/delete-str/2?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:54:52', '2021-05-20 09:54:52'),
(67, 1, 'created', 3, 'App\\Structure', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:56:50', '2021-05-20 09:56:50'),
(68, 1, 'created', 14, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/structure/add-str?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 09:56:50', '2021-05-20 09:56:50'),
(69, 1, 'deleted', 10, 'App\\News', '{\"title\":\"SAMPLE VIDEO\",\"content\":\"Sample video\"}', '[]', 'http://localhost/mocla/public/news/delete/10?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 10:00:33', '2021-05-20 10:00:33'),
(70, 1, 'created', 11, 'App\\News', '[]', '{\"title\":\"Wakuu wa Mikoa kuapishwa leo\",\"content\":\"Rais wa Jamhuri ya Muungano wa Tanzania\"}', 'http://localhost/mocla/public/media/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363', NULL, '2021-05-20 10:06:53', '2021-05-20 10:06:53'),
(71, 1, 'created', 12, 'App\\News', '[]', '{\"title\":\"ToR Mapping Capacity needs Judicial 19.2.2018\",\"content\":\"ToR Mapping Capacity needs Judicial\"}', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-21 05:54:18', '2021-05-21 05:54:18'),
(72, 1, 'created', 15, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-21 05:54:18', '2021-05-21 05:54:18'),
(73, 1, 'created', 1, 'App\\SiteLink', '[]', '[]', 'http://localhost/mocla/public/links/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-23 07:08:59', '2021-05-23 07:08:59'),
(74, 1, 'created', 2, 'App\\SiteLink', '[]', '[]', 'http://localhost/mocla/public/links/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-23 07:10:46', '2021-05-23 07:10:46'),
(75, 1, 'deleted', 2, 'App\\Management', '{\"title\":\"Waziri\"}', '[]', 'http://localhost/mocla/public/home-page/delete-mgt/2?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-23 07:51:00', '2021-05-23 07:51:00'),
(76, 1, 'created', 3, 'App\\Management', '[]', '{\"title\":\"Waziri\"}', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-23 07:52:59', '2021-05-23 07:52:59'),
(77, 1, 'created', 16, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/home-page/leaders?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-23 07:53:00', '2021-05-23 07:53:00'),
(78, 1, 'created', 13, 'App\\News', '[]', '{\"title\":\"MTOTO AKABIDHIWA CHETI CHA KUZALIWA\",\"content\":\"Naibu Waziri wa Katiba na Sheria, Mhe Geophrey Mizengo Pinda akikabidhi cheti cha kuzaliwa cha mtoto wakati wa uzinduzi wa usajili wa watoto wadogo chini ya Miaka 5 katika Mikoa ya Arusha na Manyara, uliofanyika katika Hotel ya Mount Meru, mkoani Arusha leo hii tarehe 11.05.2021\"}', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-26 07:02:03', '2021-05-26 07:02:03'),
(79, 1, 'created', 17, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-26 07:02:04', '2021-05-26 07:02:04'),
(80, 1, 'created', 14, 'App\\News', '[]', '{\"title\":\"TANGAZO LA KUITWA KWENYE USAILI TUME YA UTUMISHI WA MAHAKAMA\",\"content\":\"TANGAZO LA KUITWA KWENYE USAILI TUME YA UTUMISHI WA MAHAKAMA\"}', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:27:30', '2021-05-28 07:27:30'),
(81, 1, 'created', 18, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:27:31', '2021-05-28 07:27:31'),
(82, 1, 'created', 15, 'App\\News', '[]', '{\"title\":\"Public Notice on Registration of Legal Aid Providers\",\"content\":\"Public Notice on Registration of Legal Aid Providers\"}', 'http://localhost/mocla/public/documents/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:46:55', '2021-05-28 07:46:55'),
(83, 1, 'created', 19, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/documents/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:46:55', '2021-05-28 07:46:55'),
(84, 1, 'created', 16, 'App\\News', '[]', '{\"title\":\"kikao kazi cha kutathmini zoezi la kutafsiri sheria mbalimbali katika lugha ya Kiswahili\",\"content\":\"Waziri wa Katiba na Sheria Profesa Palamagamba Kabudi akiwa katika picha ya pamoja na Wakurugenzi wa Idara na Vitengo vya Sheria vya Wizara zote za Serikali walipohudhuria kikao kazi cha kutathmini zoezi la kutafsiri sheria mbalimbali katika lugha ya Kiswahili mapema leo tarehe 27 Mei, 2021 mjini Dodoma.\"}', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:57:55', '2021-05-28 07:57:55'),
(85, 1, 'created', 20, 'App\\WebDocument', '[]', '[]', 'http://localhost/mocla/public/news/submit?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36 Edg/90.0.818.66', NULL, '2021-05-28 07:57:55', '2021-05-28 07:57:55'),
(86, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://localhost/mocla/public/logout?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-25 05:30:23', '2021-08-25 05:30:23'),
(87, 1, 'created', 3, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/users/managements/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-27 07:01:05', '2021-08-27 07:01:05'),
(88, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-27 07:03:08', '2021-08-27 07:03:08'),
(89, 3, 'updated', 3, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-27 07:09:47', '2021-08-27 07:09:47'),
(90, 1, 'updated', 391, 'App\\Court', '[]', '[]', 'http://127.0.0.1:8000/users/courts/activate/391?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-27 08:10:05', '2021-08-27 08:10:05'),
(91, 1, 'updated', 392, 'App\\Court', '[]', '[]', 'http://127.0.0.1:8000/users/courts/activate/392?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-27 08:10:50', '2021-08-27 08:10:50'),
(92, 1, 'created', 1, 'App\\AidType', '[]', '{\"name\":\"Legal Advice\"}', 'http://localhost/mocla/public/desk/aid-type/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 05:27:10', '2021-08-29 05:27:10'),
(93, 1, 'created', 2, 'App\\AidType', '[]', '{\"name\":\"Legal Guidelines\"}', 'http://localhost/mocla/public/desk/aid-type/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 05:28:06', '2021-08-29 05:28:06'),
(94, 1, 'created', 3, 'App\\AidType', '[]', '{\"name\":\"Legal Representation\"}', 'http://localhost/mocla/public/desk/aid-type/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 05:28:26', '2021-08-29 05:28:26'),
(95, 1, 'created', 4, 'App\\AidType', '[]', '{\"name\":\"Legal Documents\"}', 'http://localhost/mocla/public/desk/aid-type/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 05:28:43', '2021-08-29 05:28:43'),
(96, 1, 'created', 4, 'App\\User', '[]', '[]', 'http://localhost/mocla/public/users/managements/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 05:44:03', '2021-08-29 05:44:03'),
(97, 1, 'created', 1, 'App\\ProvidedAid', '[]', '{\"litigant_name\":\"Dax Loopy\"}', 'http://localhost/mocla/public/desk/register-aid/add?', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', NULL, '2021-08-29 12:18:40', '2021-08-29 12:18:40'),
(98, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-13 07:49:42', '2021-09-13 07:49:42'),
(99, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-16 03:41:07', '2021-09-16 03:41:07'),
(100, 1, 'created', 17, 'App\\News', '[]', '{\"title\":\"1.\\tMADAWATI YA MSAADA WA KISHERIA KUANZISHWA MAHAKAMA ZA MWANZO HADI MAHAKAMA KUU\",\"content\":\"Serikali kupitia Wizara ya katiba na sheria imesema upo mpango wa kuwa na madawati ya msaada wa kisheria kuanzia Mahakama za Mwanzo hadi Mahakama Kuu.\\r\\nHayo yamebainishwa jijini Arusha na Naibu Waziri wa Katiba na Sheria Mhe. Geophrey Mizengo Pinda katika ufunguzi wa kikao cha wadau wa huduma za msaada wa kisheria chenye lengo la lengo la kujadiliana juu ya masuala ya huduma za msaada wa kisheria ambapo amesema hii yote ni katika kuhakikisha huduma za msaada wa kisheria zinapatikana kwa wananchi katika maeneo yote na vyombo vyote vya upatikanaji haki.\\r\\n\\u201cKwa upande wa Mahakama, Wizara iliingia makubaliano na Mahakama katika kuhakikisha huduma za msaada wa kisheria zinapatikana. Upo mpango wa kuwa na madawati ya msaada wa kisheria kuanzia Mahakama za Mwanzo hadi Mahakama Kuu hii yote ni katika kuhakikisha huduma za msaada wa kisheria zinapatikana kwa wananchi katika maeneo yote na vyombo vyote vya upatikanaji haki\\u201damefafanua.\\r\\nKatika hatua nyingine amesema Masuala ya msaada wa kisheria ni masuala mtambuka hivyo wadau wote wanalo jukumu la kuhakikisha huduma zinapatikana kwa wakati na kwa kila mwenye uhitaji.\\r\\n\\u201cSerikali inatambua mchango wenu mkubwa unaofanywa katika kuhakikisha wananchi wanafikia haki kwa wakati. Ni jukumu letu sote kuwafikia wananchi wa ngazi za chini kabisa kwa wote waliopo mijini na vijijin, hata hivyo tuangalie zaidi wale walio pembezoni kwani ndiyo walio na uhitaji mkubwa zaidi. Hawa wa mjini wanafursa ya kusikia kupitia redio mbalimbali na hata kuona vipindi mbalimbali vya elimu ya kisheria kwa umma kupitia televisheni\\u201damesema.\\r\\nAidha,Naibu waziri huyo amesema Lugha ya Kiingereza imekuwa na kikwazo na manyanyaso kwa mwananchi wa hali ya chini kunyang\\u2019anywa haki zake hivyo Mahakama na vyombo vingine vya utoaji haki vitaanza kutumia lugha ya Kiswahili katika uendeshaji wa mashauri na nyaraka mbalimbali za mahakama ikiwemo hukumu na mienendo ya Mahakama ili kuweza kumsaidia mwananchi wa hali ya chini.\\r\\nHivyo, Serikali kwa kutambua suala hili imeanza kufanyia kazi kwa kuanza kutafsiri sheria zote kutoka lugha ya kiingereza kwenda Kiswahili kwa lengo la kueleweka kwa urahisi kwa wananchi na kuwataka wadau wa sheria kutumia Kiswahili bila woga.\\r\\n\\u201cWananchi wetu wengi hawana uelewa wa masuala ya kisheria jambo ambalo linapelekea wengi wao kupoteza haki kwa kukosa mwongozo sahihi wa namna ya kupata haki zao. Serikali kwa kutambua suala hili imeanza kufanyia kazi kwa kuanza kutafsiri sheria zote kutoka lugha ya kiingereza kwenda Kiswahili kwa lengo la kueleweka kwa urahisi kwa wananchi, Si hivyo tu, Mahakama na vyombo vingine vya utoaji haki vitaanza kutumia lugha ya Kiswahili katika uendeshaji wa mashauri na nyaraka mbalimbali za mahakama ikiwemo hukumu na mienendo ya Mahakama\\u201damesema.\\r\\nAmeendelea kufafanua kuwa watoa huduma ya msaada wa kisheria wapatao 182 na wasaidizi wa kisheria wapatao 657 wameshasajiliwa mpaka sasa ambapo ni katika mwaka wa nne wa utekelezaji wa Sheria ya msaada wa kisheria ambayo ni Sheria Na. 1 ya Mwaka 2017 na Kanuni zake za Mwaka 2018.\\r\\nHata hivyo,Naibu waziri huyo amewataka wasaidizi wa kisheria ambao hawajajisajili kufanya mchakato wa kujisajili ili kufanya kazi kwa pamoja na kuweza kutoa huduma za msaada wa kisheria katika maeneo yaliyosahaulika hususan vijijini .\\r\\n\\u201cNinafahamu wengi wa watoa huduma mliopo hapa mmeshajisajili na kutambuliwa na Serikali. Kwa wale ambao bado, niwaombe wajisajili chini ya matakwa ya sheria ya msaada wa kisheria. Kujisajili kunaipa Serikali kuwa na takwimu sahihi juu ya watoa huduma waliopo ili kuimarisha mifumo ya kiutendaji na kufanya maamuzi ya kisera kuhusiana na masuala ya msaada wa kisheria\\u201damesema.\\r\\nKwa upande wao baadhi ya wadau wa sheria akiwemo Felister Mushi Msajili wa watoa Huduma ya msaada Kisheria pamoja na Tolbert Mmasy-Mkurugenzi mtandao wa wasaidizi wa kisheria Tanzania [TAPANET]wamesema mkutano huo ni muhimu katika kujadili masuala mbalimbali ya sheria zenye changamoto na kuweza kuzifanyia maboresho.\"}', 'http://127.0.0.1:8000/news/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-16 04:20:16', '2021-09-16 04:20:16'),
(101, 1, 'created', 21, 'App\\WebDocument', '[]', '[]', 'http://127.0.0.1:8000/news/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-16 04:20:17', '2021-09-16 04:20:17'),
(102, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-16 04:20:35', '2021-09-16 04:20:35'),
(103, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-16 09:09:18', '2021-09-16 09:09:18'),
(104, 1, 'created', 18, 'App\\News', '[]', '{\"title\":\"ToR Analysis Discriminatory Laws 19.2.2018\",\"content\":\"ToR Analysis Discriminatory Laws 19.2.2018\"}', 'http://127.0.0.1:8000/news/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-18 10:03:20', '2021-09-18 10:03:20'),
(105, 1, 'created', 22, 'App\\WebDocument', '[]', '[]', 'http://127.0.0.1:8000/news/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-18 10:03:20', '2021-09-18 10:03:20'),
(106, 1, 'created', 19, 'App\\News', '[]', '{\"title\":\"ToR Coordination Mechanisms Assessment 19.2.2018\",\"content\":\"ToR Coordination Mechanisms Assessment 19.2.2018\"}', 'http://127.0.0.1:8000/adverts/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-18 10:13:33', '2021-09-18 10:13:33'),
(107, 1, 'created', 23, 'App\\WebDocument', '[]', '[]', 'http://127.0.0.1:8000/adverts/submit?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-18 10:13:33', '2021-09-18 10:13:33'),
(108, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-20 02:17:16', '2021-09-20 02:17:16'),
(109, 4, 'updated', 4, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-20 02:21:18', '2021-09-20 02:21:18'),
(110, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-20 03:35:30', '2021-09-20 03:35:30'),
(111, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-20 06:51:23', '2021-09-20 06:51:23'),
(112, 4, 'updated', 4, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-20 06:52:55', '2021-09-20 06:52:55'),
(113, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-21 02:44:45', '2021-09-21 02:44:45'),
(114, 1, 'updated', 1, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 02:39:32', '2021-09-22 02:39:32'),
(115, 3, 'updated', 3, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 02:41:22', '2021-09-22 02:41:22'),
(116, 3, 'created', 2, 'App\\ProvidedAid', '[]', '{\"litigant_name\":\"Hamidu Selemani\"}', 'http://127.0.0.1:8000/desk/register-aid/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 03:18:41', '2021-09-22 03:18:41'),
(117, 3, 'updated', 3, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/logout?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 03:58:02', '2021-09-22 03:58:02');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `tags`, `created_at`, `updated_at`) VALUES
(118, 1, 'created', 5, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/users/managements/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 04:05:06', '2021-09-22 04:05:06'),
(119, 1, 'created', 6, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/users/managements/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 04:06:15', '2021-09-22 04:06:15'),
(120, 1, 'created', 7, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/users/managements/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 04:07:20', '2021-09-22 04:07:20'),
(121, 1, 'created', 8, 'App\\User', '[]', '[]', 'http://127.0.0.1:8000/users/managements/add?', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84', NULL, '2021-09-22 04:08:36', '2021-09-22 04:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `backgrounds`
--

CREATE TABLE `backgrounds` (
  `id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `user` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `backgrounds`
--

INSERT INTO `backgrounds` (`id`, `title`, `content`, `user`, `created_at`, `updated_at`) VALUES
(1, 'Background', '6.	About us/Kuhusu sisi: Idara,Vitengo, \r\nHistoria ya watoa huduma ya msaada wa kisheria, \r\nOfisi ya Msajili wa Watoa huduma ya msaada wa kisheria na majukumu yake, orodha ya wasajili wasaidizi wa mikoa na wilaya, orodha ya watoa msaada wa kisheria na mahali walipo, Huduma zitolewazo na wizara kwa umma kupitia DCJM,DPLS,DHR,DNWROU', 'Super Admin', '2020-09-02 10:15:35', '2020-09-01 15:30:38'),
(2, 'Mission', 'Kuwa na Mfumo Madhubuti wa Kikatiba na Kisheria Wenye Kufanikisha Mipango ya Maendeleo ya Taifa', 'Super Admin', '2020-09-02 06:30:41', '2020-09-02 06:30:41'),
(3, 'Vission', 'Katiba na Sheria Wezeshi kwa Maendeleo ya Taifa.', 'Super Admin', '2020-09-02 06:33:14', '2020-09-02 06:33:14');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(100) NOT NULL,
  `news_id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `comment` longtext NOT NULL,
  `posted_at` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `news_id`, `username`, `comment`, `posted_at`, `created_at`, `updated_at`) VALUES
(1, 8, 'Sumuni', 'Test comment', '2020-09-06 11:40:18', '2020-09-06 08:40:18', '2020-09-06 08:40:18'),
(2, 9, 'Bwakila', 'My Comment', '2020-09-06 11:41:29', '2020-09-06 08:41:29', '2020-09-06 08:41:29');

-- --------------------------------------------------------

--
-- Table structure for table `courts`
--

CREATE TABLE `courts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `court_level_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'inactive',
  `sub_sp_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `judiciary` int(11) DEFAULT NULL,
  `zonal` int(11) DEFAULT NULL,
  `special` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courts`
--

INSERT INTO `courts` (`id`, `name`, `court_level_id`, `location_id`, `display_name`, `code`, `status`, `sub_sp_code`, `judiciary`, `zonal`, `special`, `created_at`, `updated_at`) VALUES
(1, 'Arusha Court of Appeal', 2, 2, 'In the Court of Appeal of Tanzania at Arusha', 'CAT', 'inactive', '2002', 1, 2, 1, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(2, 'Arusha High Court', 3, 2, 'In the High court of United Republic of Tanzania at Arusha', 'HCT', 'inactive', '2003', 1, 2, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(3, 'Arusha High Court Labour Division', 3, 2, 'In the High court of United Republic of Tanzania Labour Division at Arusha', 'LBR', 'inactive', '2301', 1, 2, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(4, 'Arusha High Court Land Division', 3, 2, 'In the High court of United Republic of Tanzania Land Division at Arusha', 'LND', 'inactive', '2006', 1, 2, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(5, 'Arusha High Court Commercial Division', 3, 2, 'In the High court of United Republic of Tanzania Commercial Division at Arusha', 'COM', 'inactive', '2005', 1, 3, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(6, 'Arusha High Court Economic and Corruption Division', 3, 2, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Arusha', 'ECC', 'inactive', '2326', 1, 2, 2, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(7, 'Arusha Resident Magistrate', 4, 16, 'In the Resident Magistrate Court at Arusha', 'ARS', 'inactive', '1000', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(8, 'Arusha Juvenile court At Arusha', 5, 17, 'In the Juvenile Court of Arusha at Arusha', 'JUVARS', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(9, 'Arusha Juvenile court At Arumeru', 5, 18, 'In the Juvenile Court of Arusha at Arumeru', 'JUVARM', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(10, 'Arusha Juvenile court At Karatu', 5, 19, 'In the Juvenile Court of Arusha at Karatu', 'JUVKRT', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(11, 'Arusha Juvenile court At Longido', 5, 20, 'In the Juvenile Court of Arusha at Longido', 'JUVLNG', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(12, 'Arusha Juvenile court At Monduli', 5, 21, 'In the Juvenile Court of Arusha at Monduli', 'JUVMDL', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(13, 'Arusha Juvenile court At Ngorongoro', 5, 22, 'In the Juvenile Court of Arusha at Ngorongoro', 'NGL', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(14, 'Arusha District Court', 5, 17, 'In the District Court of Arusha District at Arusha', 'ARS', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(15, 'Arumeru District Court', 5, 18, 'In the District Court of Arumeru District at Arumeru', 'ARM', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(16, 'Karatu  District Court', 5, 19, 'In the District Court of Karatu District at Karatu', 'KRT', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(17, 'Longido District Court', 5, 20, 'In the District Court of Longido District at Longido', 'LNG', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(18, 'Monduli District Court', 5, 21, 'In the District Court of Monduli District at Monduli', 'MDL', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(19, 'Ngorongoro District Court', 5, 22, 'In the District Court of Ngorongoro District at Ngorongoro', 'NGL', 'inactive', '2004', 1, 2, 3, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(20, 'Manyara Resident Magistrate', 4, 23, 'In the Resident Magistrate Court of Manyara at Manyara', 'MNR', 'inactive', '1000', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(21, 'Manyara Juvenile Court At Mbulu', 5, 24, 'In the Juvenile Court of Manyara at Mbulu', 'JUVMBL', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(22, 'Manyara Juvenile Court At Hanang\'', 5, 25, 'In the Juvenile Court of Manyara at Hanang\'', 'JUVHNG', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(23, 'Manyara Juvenile Court At Kiteto', 5, 26, 'In the Juvenile Court of Manyara at Kiteto', 'JUVKTT', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(24, 'Manyara Juvenile Court At Simanjiro', 5, 27, 'In the Juvenile Court of Manyara at Simanjiro', 'JUVSMR', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(25, 'Mbulu District Court', 5, 24, 'In the District Court of Mbulu District at Mbulu', 'MBL', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(26, 'Hanang\' District Court', 5, 25, 'In the District Court of Hanang\' District at Hanang\'', 'HNG', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(27, 'Kiteto District Court', 5, 26, 'In the District Court of Kiteto District at Kiteto', 'KTT', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(28, 'Simanjiro District Court', 5, 27, 'In the District Court of Simanjiro District at Simanjiro', 'SMR', 'inactive', '2004', 1, 2, 4, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(29, 'Bukoba Court of Appeal', 2, 3, 'In The Court of Appeal of Tanzania at Bukoba', 'CAT', 'inactive', '2002', 1, 4, 1, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(30, 'Bukoba High Court', 3, 3, 'In the High court of United Republic of Tanzania at Bukoba', 'HCT', 'inactive', '2003', 1, 4, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(31, 'Bukoba High Court Labour Division', 3, 3, 'In the High court of United Republic of Tanzania Labour Division at Bukoba', 'LBR', 'inactive', '2301', 1, 4, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(32, 'Bukoba High Court Land Division', 3, 3, 'In the High court of United Republic of Tanzania Land Division at Bukoba', 'LND', 'inactive', '2006', 1, 4, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(33, 'Bukoba High Court Commercial Division', 3, 3, 'In the High court of United Republic of Tanzania Commercial Division at Bukoba', 'COM', 'inactive', '2005', 1, 4, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(34, 'Bukoba High Court Economic and Corruption Division', 3, 3, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Bukoba', 'ECC', 'inactive', '2326', 1, 4, 2, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(35, 'Bukoba Resident Magistrate', 4, 28, 'In the Resident Magistrate Court of Bukoba at Bukoba', 'BKB', 'inactive', '1000', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(36, 'Kagera Juvenile court At Bukoba', 5, 29, 'In the Juvenile Court of Kagera at Bukoba', 'JUVBKB', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(37, 'Kagera Juvenile court At Misenyi', 5, 30, 'In the Juvenile Court of Kagera at Misenyi', 'JUVMSN', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(38, 'Kagera Juvenile court At Muleba', 5, 31, 'In the Juvenile Court of Kagera at Muleba', 'JUVMLB', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(39, 'Kagera Juvenile court At Ngara', 5, 32, 'In the Juvenile Court of Kagera at Ngara', 'JUVNGR', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(40, 'Kagera Juvenile court At Karagwe', 5, 33, 'In the Juvenile Court of Kagera at Karagwe', 'JUVKRG', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(41, 'Kagera Juvenile court At Kyerwa', 5, 34, 'In the Juvenile Court of Kagera at Kyerwa', 'JUVKRW', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(42, 'Kagera Juvenile court At Biharamulo', 5, 35, 'In the Juvenile Court of Kagera at Biharamulo', 'JUVBML', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(43, 'Bukoba District Court', 5, 29, 'In the District Court of Bukoba District at Bukoba', 'BKB', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(44, 'Misenyi District Court', 5, 30, 'In the District Court of Misenyi District at Misenyi', 'MSN', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(45, 'Muleba  District Court', 5, 31, 'In the District Court of Muleba District at Muleba', 'MLB', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(46, 'Ngara District Court', 5, 32, 'In the District Court of Ngara District at Ngara', 'NGR', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(47, 'Karagwe District Court', 5, 33, 'In the District Court of Karagwe District at Karagwe', 'KRG', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(48, 'Kyerwa District Court', 5, 34, 'In the District Court of Kyerwa District at Kyerwa', 'KRW', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(49, 'Biharamulo District Court', 5, 35, 'In the District Court of Biharamulo District at Biharamulo', 'BML', 'inactive', '2004', 1, 4, 5, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(50, 'Dar es salaam Court of Appeal', 2, 4, 'In the Court of Appeal of Tanzania at Dar es Salaam', 'CAT', 'inactive', '2002', 1, 1, 1, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(51, 'Zanziba Court of Appeal', 2, 4, 'Court of Appeal of Tanzania at Zanzibar', 'CAT', 'inactive', '2002', 1, 1, 1, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(52, 'Dar es salaam High Court Main Registry', 3, 4, 'In the High court of United Republic of Tanzania Main Registry at Dar es Salaam', 'HCT', 'inactive', '2003', 1, 20, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(53, 'Dar es salaam High Court Dar es salaam zone', 3, 4, 'In the High court of United Republic of Tanzania at Dar es salaam', 'HCT', 'inactive', '2003', 1, 5, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(54, 'Dar es salaam High Court Labour Division', 3, 4, 'In the High court of United Republic of Tanzania Labour Division at Dar es salaam', 'LBR', 'inactive', '2301', 1, 18, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(55, 'Dar es salaam High Court Land Division', 3, 4, 'In the High court of United Republic of Tanzania Land Division at Dar es salaam', 'LND', 'inactive', '2006', 1, 19, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(56, 'Dar es salaam High Court Commercial Division', 3, 4, 'In the High court of United Republic of Tanzania Commercial Division at Dar es salaam', 'COM', 'inactive', '2005', 1, 3, NULL, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(57, 'Dar es salaam High Court Economic and Corruption Division', 3, 4, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Dar es salaam', 'ECC', 'inactive', '2326', 1, 5, 2, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(58, 'Kisutu Resident Magistrate', 4, 36, 'In the Resident Magistrate Court of Dar es Salaam at Kisutu', 'KST', 'inactive', '1000', 1, 5, 6, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(59, 'Kivukoni Resident Magistrate', 4, 37, 'In the Resident Magistrate Court of Kivukoni at Kinondoni', 'KVK', 'inactive', '1000', 1, 5, 6, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(60, 'Sokoine Drive Resident Magistrate', 4, 40, 'In the Resident Magistrate Court at Sokoine Drive', 'SKD', 'inactive', '1000', 1, 5, 6, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(61, 'Kisutu Juvenile court ', 4, 36, 'In the Juvenile Court of Dar es salaam at Kisutu', 'JUVKST', 'inactive', '1000', 1, 5, 6, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(62, 'Dar es salaam Juvenile court At Kinondoni', 5, 37, 'In the Juvenile Court of Dar es salaam at Kinondoni', 'JUVKND', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:25:59', '2018-07-01 10:25:59'),
(63, 'Dar es salaam Juvenile court At Ubungo', 5, 38, 'In the Juvenile Court of Dar es salaam at Ubungo', 'JUVUBN', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(64, 'Dar es salaam Juvenile court At Temeke', 5, 39, 'In the Juvenile Court of Dar es salaam at Temeke', 'JUVTMK', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(65, 'Dar es salaam Juvenile court At Kigamboni', 5, 40, 'In the Juvenile Court of Dar es salaam at Kigamboni', 'JUVKIG', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(66, 'Dar es salaam Juvenile court At Ilala', 5, 41, 'In the Juvenile Court of Dar es salaam at Ilala', 'JUVILA', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(67, 'Kinondoni District Court', 5, 37, 'In the District Court of Kinondoni District at Kinondoni', '', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(68, 'Ubungo District Court', 5, 38, 'In the District Court of Ubungo District at Ubungo', 'UBN', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(69, 'Temeke  District Court', 5, 39, 'In the District Court of Temeke District at Temeke', 'TMK', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(70, 'Kigamboni District Court', 5, 40, 'In the District Court of Kigamboni District at kigamboni', 'KIG', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(71, 'Ilala District Court', 5, 41, 'In the District Court of Ilala District at Ilala', 'ILA', 'inactive', '2004', 1, 5, 6, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(72, 'Kibaha Resident Magistrate', 4, 42, 'In the Resident Magistrate Court of Kibaha at Kibaha', 'KBH', 'inactive', '1000', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(73, 'Pwani Juvenile court At Kibaha', 5, 43, 'In the Juvenile Court of Pwani at Kibaha', 'JUVKBH', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(74, 'Pwani Juvenile court At Mkuranga', 5, 44, 'In the Juvenile Court of Pwani at Mkuranga', 'JUVMRG', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(75, 'Pwani Juvenile court At Kisarawe', 5, 45, 'In the Juvenile Court of Pwani at Kisarawe', 'JUVKSW', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(76, 'Kerege Juvenile Court At Bagamoyo', 6, 46, 'In the Juvenile Court of Kerege at Bagamoyo', 'JUVKRG', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2019-12-23 11:45:20'),
(77, 'Pwani Juvenile court At Mafia', 5, 47, 'In the Juvenile Court of Pwani at Mafia', 'JUVMFA', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(78, 'Pwani Juvenile court At Rufiji', 5, 48, 'In the Juvenile Court of Pwani at Rufiji', 'JUVRFJ', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(79, 'Pwani Juvenile court At Kibiti', 5, 49, 'In the Juvenile Court of Pwani at Kibiti', 'JUVKBT', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(80, 'Kibaha District Court', 5, 43, 'In the District Court of Kibaha District at Kibaha', 'KBH', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(81, 'Mkuranga District Court', 5, 44, 'In the District Court of Mkuranga District at Mkuranga', 'MRG', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(82, 'Kisarawe  District Court', 5, 45, 'In the District Court of Kisarawe District at Kisarawe', 'KSW', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(83, 'Bagamoyo District Court', 5, 46, 'In the District Court of Bagamoyo District at Bagamoyo', 'BGY', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(84, 'Mafia District Court', 5, 47, 'In the District Court of Mafia District at Mafia', 'MFA', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(85, 'Rufiji District Court', 5, 48, 'In the District Court of Rufiji District at Rufiji', 'RFJ', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(86, 'Kibiti District Court', 5, 49, 'In the District Court of Kibiti District at Kibiti', 'KBT', 'inactive', '2004', 1, 5, 7, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(87, 'Morogoro Resident Magistrate', 4, 50, 'In the Resident Magistrate Court of Morogoro at Morogoro', 'MOR', 'inactive', '1000', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(88, 'Morogoro Juvenile court At Kilosa', 5, 51, 'In the Juvenile Court of Morogoro at Kilosa', 'JUVKLS', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(89, 'Morogoro Juvenile court At Gairo', 5, 52, 'In the Juvenile Court of Morogoro at Gairo', 'JUVGAI', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(90, 'Kilombero Juvenile court At Ifakara', 5, 53, 'In the Juvenile Court of Kilombero at Ifakara', 'JUVIFKR', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(91, 'Morogoro Juvenile court At Morogoro', 5, 54, 'In the Juvenile Court of Morogoro at Morogoro', 'JUVMOR', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(92, 'Morogoro Juvenile court At Mvomero', 5, 55, 'In the Juvenile Court of Morogoro at Mvomero', 'JUVMVO', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(93, 'Morogoro Juvenile court At Ulanga', 5, 56, 'In the Juvenile Court of Morogoro at Ulanga', 'JUVULG', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(94, 'Kilosa District Court', 5, 51, 'In the District Court of Kilosa District at Kilosa', 'KLS', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(95, 'Gairo District Court', 5, 52, 'In the District Court of Gairo District at Gairo', 'GAI', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(96, 'Kilombero  District Court', 5, 53, 'In the District Court of Kilombero District at Kilombero', 'KLB', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(97, 'Morogoro District Court', 5, 54, 'In the District Court of Morogoro District at Morogoro', 'MOR', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(98, 'Mvomero District Court', 5, 55, 'In the District Court of Mvomelo District at Mvomero', 'MVO', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(99, 'Ulanga District Court', 5, 56, 'In the District Court of Ulanga District at Ulanga', 'ULG', 'inactive', '2004', 1, 5, 8, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(100, 'Dodoma Court of Appeal', 2, 5, 'In the Court of Appeal of Tanzania at Dodoma', 'CAT', 'inactive', '2002', 1, 6, 1, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(101, 'Dodoma High Court', 3, 5, 'In the High court of United Republic of Tanzania at Dodoma', 'HCT', 'inactive', '2003', 1, 6, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(102, 'Dodoma High Court Labour Division', 3, 5, 'In the High court of United republic of Tanzania Labour Division at Dodoma', 'LBR', 'inactive', '2301', 1, 6, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(103, 'Dodoma High Court Land Division', 3, 5, 'In the High court of United Republic of Tanzania Land Division at Dodoma', 'LND', 'inactive', '2006', 1, 6, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(104, 'Dodoma High Court Economic and Corruption Division', 3, 5, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Dodoma', 'ECC', 'inactive', '2326', 1, 6, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(105, 'Dodoma Resident Magistrate', 4, 57, 'In the Resident Magistrate Court of Dodoma at Dodoma', 'DDM', 'inactive', '1000', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(106, 'Dodoma Juvenile court At Bahi', 5, 58, 'In the Juvenile Court of Dodoma at Bahi', 'JUVBHI', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(107, 'Dodoma Juvenile court At Dodoma', 5, 59, 'In the Juvenile Court of Dodoma at Dodoma', 'JUVDDM', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(108, 'Dodoma Juvenile court At Chamwino', 5, 60, 'In the Juvenile Court of Dodoma at Chamwino', 'JUVCMN', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(109, 'Dodoma Juvenile court At Chemba', 5, 61, 'In the Juvenile Court of Dodoma at Chemba', 'JUVCMB', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(110, 'Dodoma Juvenile court At Kondoa', 5, 62, 'In the Juvenile Court of Dodoma at Kondoa', 'JUVKND', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(111, 'Dodoma Juvenile court At Kongwa', 5, 63, 'In the Juvenile Court of Dodoma at Kongwa', 'JUVKNG', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(112, 'Dodoma Juvenile court At Mpwapwa', 5, 64, 'In the Juvenile Court of Dodoma at Mpwapwa', 'JUVMPP', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(113, 'Bahi District Court', 5, 58, 'In the District Court of Bahi District at Bahi', 'BHI', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(114, 'Dodoma District Court', 5, 59, 'In the District Court of Dodoma District at Dodoma', 'DDM', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(115, 'Chamwino  District Court', 5, 60, 'In the District Court of Chamwino District at Chamwino', 'CMN', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(116, 'Chemba District Court', 5, 61, 'In the District Court of Chemba District at Chemba', 'CMB', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(117, 'Kondoa District Court', 5, 62, 'In the District Court of Kondoa District at kondoa', 'KND', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(118, 'Kongwa District Court', 5, 63, 'In the District Court of Kongwa District at Kongwa', 'KNG', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(119, 'Mpwapwa District Court', 5, 64, 'In the District Court of Mpwapwa District at Mpwapwa', 'MPP', 'inactive', '2004', 1, 6, 9, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(120, 'Singida Resident Magistrate', 4, 65, 'In the Resident Magistrate Court of Singida at Singida', 'SGD', 'inactive', '1000', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(121, 'Singida Juvenile court At Ikungi', 5, 66, 'In the Juvenile Court of Singida at Ikungi', 'JUVIKG', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(122, 'Singida Juvenile court At Singida', 5, 67, 'In the Juvenile Court of Singida at Singida', 'JUVSGD', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(123, 'Singida Juvenile court At Manyoni', 5, 68, 'In the Juvenile Court of Singida at Manyoni', 'JUVMNY', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(124, 'Singida Juvenile court At Iramba', 5, 69, 'In the Juvenile Court of Singida at Iramba', 'JUVIRM', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(125, 'Ikungi District Court', 5, 66, 'In the District Court of Ikungi District at Ikungi', 'IKG', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(126, 'Singida District Court', 5, 67, 'In the District Court of Singida District at Singida', 'SGD', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(127, 'Manyoni  District Court', 5, 68, 'In the District Court of Manyoni District at Manyoni', 'MNY', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(128, 'Iramba District Court', 5, 69, 'In the District Court of Iramba District at Iramba', 'IRM', 'inactive', '2004', 1, 6, 10, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(129, 'Iringa Court of Appeal', 2, 6, 'In the Court of Appeal of Tanzania at Iringa', 'CAT', 'inactive', '2002', 1, 8, 1, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(130, 'Iringa High Court', 3, 6, 'In the High court of united Republic of Tanzania at Iringa', 'HCT', 'inactive', '2003', 1, 8, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(131, 'Iringa High Court Labour Division', 3, 6, 'In the High court of united Republic of Tanzania Labour Division at Iringa', 'LBR', 'inactive', '2301', 1, 8, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(132, 'Iringa High Court Labour Division', 3, 6, 'In the High court of united Republic of Tanzania Labour Division at Iringa', 'LND', 'inactive', '2301', 1, 8, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(133, 'Iringa High Court Economic and Corruption Division', 3, 6, 'In the High court of united Republic of Tanzania Economic and Corruption Division at Iringa', 'ECC', 'inactive', '2326', 1, 8, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(134, 'Iringa Resident Magistrate', 4, 70, 'In the Resident Magistrate Court of Iringa at Iringa', 'IRG', 'inactive', '1000', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(135, 'Iringa Juvenile court At Iringa', 5, 71, 'In the Juvenile Court of Iringa at Iringa', 'JUVIRG', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(136, 'Iringa Juvenile court At Kilolo', 5, 72, 'In the Juvenile Court of Iringa at kilolo', 'JUVKLL', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(137, 'Iringa Juvenile court At Mufindi', 5, 73, 'In the Juvenile Court of Iringa at Mufindi', 'JUVMFN', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(138, 'Iringa District Court', 5, 71, 'In the District Court of Iringa District at Iringa', 'IRG', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(139, 'Kilolo District Court', 5, 72, 'In the District Court of Kilolo District at Kilolo', 'KLL', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(140, 'Mufindi  District Court', 5, 73, 'In the District Court of Mufindi District at Mufindi', 'MFN', 'inactive', '2004', 1, 8, 11, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(141, 'Njombe Resident Magistrate', 4, 74, 'In the Resident Magistrate Court of Njombe at Njombe', 'NJB', 'inactive', '1000', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(142, 'Njombe Juvenile court At Njombe', 5, 75, 'In the Juvenile Court of Njombe at Njombe', 'JUVNJB', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(143, 'Njombe Juvenile court At Wangimg\'ombe', 5, 76, 'In the Juvenile Court of Njombe at Wanging\'ombe', 'JUVWNG', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(144, 'Njombe Juvenile court At Ludewa', 5, 77, 'In the Juvenile Court of Njombe at Ludewa', 'JUVLDW', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(145, 'Njombe Juvenile court At Makete', 5, 78, 'In the Juvenile Court of Njombe at Makete', 'JUVMKT', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(146, 'Njombe District Court', 5, 75, 'In the District Court of Njombe District at Njombe', 'NJB', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(147, 'Wangimg\'ombe District Court', 5, 76, 'In the District Court of Wanging\'ombe District at Wanging\'ombe', 'WNG', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(148, 'Ludewa District Court', 5, 77, 'In the District Court of Ludewa District at Ludewa', 'LDW', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(149, 'Makete District Court', 5, 78, 'In the District Court of Makete Ditrict at Makete', 'MKT', 'inactive', '2004', 1, 8, 12, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(150, 'Mbeya Court of Appeal', 2, 7, 'In the Court of Appeal of Tanzania at Mbeya', 'CAT', 'inactive', '2002', 1, 9, 1, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(151, 'Mbeya High Court', 3, 7, 'In the High court of united Republic of Tanzania at Mbeya', 'HCT', 'inactive', '2003', 1, 9, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(152, 'Mbeya High Court Labour Division', 3, 7, 'In the High court of united Republic of Tanzania Labour Division at Mbeya', 'LBR', 'inactive', '2301', 1, 9, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(153, 'Mbeya High Court Land Division', 3, 7, 'In the High court of united Republic of Tanzania Land Division at Mbeya', 'LND', 'inactive', '2006', 1, 9, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(154, 'Mbeya High Court Economic and Corruption Division', 3, 7, 'In the High court of united Republic of Tanzania Economic and Corruption Division at Mbeya', 'ECC', 'inactive', '2326', 1, 9, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(155, 'Mbeya Resident Magistrate', 4, 79, 'In the Resident Magistrate Court of Mbeya at Mbeya', 'MBY', 'inactive', '1000', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(156, 'Mbeya Juvenile court At Mbeya', 5, 80, 'In the Juvenile Court of Mbeya at Mbeya', 'JUVMBY', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(157, 'Mbeya Juvenile court At Rungwe', 5, 81, 'In the Juvenile Court of Mbeya at Rungwe', 'JUVRGW', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(158, 'Mbeya Juvenile court At Kyela', 5, 82, 'In the Juvenile Court of Mbeya at Kyela', 'JUVKYL', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(159, 'Mbeya Juvenile court At Mbarali', 5, 83, 'In the Juvenile Court of Mbeya at Mbarali', 'JUVMRL', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(160, 'Mbeya Juvenile court At Chunya', 5, 84, 'In the Juvenile Court of Mbeya at Chunya', 'JUVCHY', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(161, 'Mbeya District Court', 5, 80, 'In the District Court of Mbeya District at Mbeya', 'MBY', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(162, 'Rungwe  District Court', 5, 81, 'In the District Court of Rungwe District at Rungwe', 'RGW', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(163, 'Kyela District Court', 5, 82, 'In the District Court of Kyela District at Kyela', 'KYL', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(164, 'Mbarali  District Court', 5, 83, 'In the District Court of Mbarali District at Mbarali', 'MRL', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(165, 'Chunya District Court', 5, 84, 'In the District Court of Chunya District at Chunya', 'CHY', 'inactive', '2004', 1, 9, 13, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(166, 'Songwe Resident Magistrate', 4, 85, 'In the Resident Magistrate Court of Songwe at Songwe', 'SGW', 'inactive', '1000', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(167, 'Songwe Juvenile court At Songwe', 5, 86, 'In the Juvenile Court of Songwe at Songwe', 'JUVSGW', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(168, 'Songwe Juvenile court At Momba', 5, 87, 'In the Juvenile Court of Songwe at Momba', 'JUVMMB', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(169, 'Songwe Juvenile court At Mbozi', 5, 88, 'In the Juvenile Court of Songwe at Mbozi', 'JUVMBZ', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(170, 'Songwe Juvenile court At Ileje', 5, 89, 'In the Juvenile Court of Songwe at Ileje', 'JUVILJ', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(171, 'Songwe District Court', 5, 86, 'In the District Court of Songwe at Songwe', 'SGW', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(172, 'Momba District Court', 5, 87, 'In the District Court of Momba District at Momba', 'MMB', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(173, 'Mbozi District Court', 5, 88, 'In the District Court of Mbozi District at Mbozi', 'MBZ', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(174, 'Ileje District Court', 5, 89, 'In the District Court of Ileje District at Ileje', 'ILJ', 'inactive', '2004', 1, 9, 14, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(175, 'Moshi High Court', 3, 8, 'In the High court of United Republic of Tanzania at Moshi', 'HCT', 'inactive', '2003', 1, 10, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(176, 'Moshi High Court Labour Division', 3, 8, 'In the High court of United Republic of Tanzania Labour Division at Moshi', 'LBR', 'inactive', '2301', 1, 10, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(177, 'Moshi High Court Land Division', 3, 8, 'In the High court of United Republic of Tanzania Land Division at Moshi', 'LND', 'inactive', '2006', 1, 10, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(178, 'Moshi High Court Economic and Corruption Division', 3, 8, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Moshi', 'ECC', 'inactive', '2326', 1, 10, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(179, 'Moshi Resident Magistrate', 4, 90, 'In the Resident Magistrate Court of Moshi at Moshi', 'MSH', 'inactive', '1000', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(180, 'Kilimanjaro Juvenile court At Moshi', 5, 91, 'In the Juvenile Court of Kilimanjaro at Moshi', 'JUVMSH', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(181, 'Kilimanjaro Juvenile court At Same', 5, 92, 'In the Juvenile Court of Kilimanjaro at Same', 'JUVSAM', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(182, 'Kilimanjaro Juvenile court At Mwanga', 5, 93, 'In the Juvenile Court of Kilimanjaro at Mwanga', 'JUVMWN', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(183, 'Kilimanjaro Juvenile court At Rombo', 5, 94, 'In the Juvenile Court of Kilimanjaro at Rombo', 'JUVRMB', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(184, 'Kilimanjaro Juvenile court At Hai', 5, 95, 'In the Juvenile Court of Kilimanjaro at Hai', 'JUVHAI', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(185, 'Kilimanjaro Juvenile court At Siha', 5, 96, 'In the Juvenile Court of Kilimanjaro at Siha', 'JUVSIH', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(186, 'Moshi District Court', 5, 91, 'In the District Court of Moshi Ditrict at Moshi', 'MSH', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(187, 'Same District Court', 5, 92, 'In the District Court of Same District at Same', 'SAM', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(188, 'Mwanga  District Court', 5, 93, 'In the District Court of Mwanga District at Mwanga', 'MWN', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(189, 'Rombo District Court', 5, 94, 'In the District Court of Rombo District at Rombo', 'RMB', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(190, 'Hai District Court', 5, 95, 'In the District Court of Hai District at Hai', 'HAI', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(191, 'Siha District Court', 5, 96, 'In the District Court of Siha Diistrict at Siha', 'SIH', 'inactive', '2004', 1, 10, 15, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(192, 'Mtwara Court of Appeal', 2, 9, 'In the Court of Appeal of Tanzania at Mtwara', 'CAT', 'inactive', '2002', 1, 11, 1, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(193, 'Mtwara High Court', 3, 9, 'In the High court of United Republic of Tanzania at Mtwara', 'HCT', 'inactive', '2003', 1, 11, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(194, 'Mtwara High Court Labour Division', 3, 9, 'In the High court of United Republic of Tanzania Labour Division at Mtwara', 'LBR', 'inactive', '2301', 1, 11, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(195, 'Mtwara High Court Land Division', 3, 9, 'In the High court of United Republic of Tanzania Land Division at Mtwara', 'LND', 'inactive', '2006', 1, 11, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(196, 'Mtwara High Court Economic and Corruption Division', 3, 9, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Mtwara', 'ECC', 'inactive', '2326', 1, 11, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(197, 'Mtwara Resident Magistrate', 4, 97, 'In the Resident Magistrate Court of Mtwara at Mtwara', 'MTR', 'inactive', '1000', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(198, 'Mtwara Juvenile court At Mtwara', 5, 98, 'In the Juvenile Court of Mtwara at Mtwara', 'JUVMTR', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(199, 'Mtwara Juvenile court At Masasi', 5, 99, 'In the Juvenile Court of Mtwara at Masasi', 'JUVMAS', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(200, 'Mtwara Juvenile court At Nanyumbu', 5, 100, 'In the Juvenile Court of Mtwara at Nanyumbu', 'JUVNAN', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(201, 'Mtwara Juvenile court At Tandahimba', 5, 101, 'In the Juvenile Court of Mtwara at Tandahimba', 'JUVTHM', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(202, 'Mtwara Juvenile court At Newala', 5, 102, 'In the Juvenile Court of Mtwara at Newala', 'JUVNEW', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(203, 'Mtwara District Court', 5, 98, 'In the District Court of Mtwara District at Mtwara', 'MTR', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(204, 'Masasi  District Court', 5, 99, 'In the District Court of Masasi District at Masasi', 'MAS', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(205, 'Nanyumbu District Court', 5, 100, 'In the District Court of Nanyumbu District at Nanyumbu', 'NAN', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(206, 'Tandahimba  District Court', 5, 101, 'In the District Court of Tandahimba District at Tandahimba', 'THM', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(207, 'Newala District Court', 5, 102, 'In the District Court of Newala District at Newala', 'NEW', 'inactive', '2004', 1, 11, 16, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(208, 'Lindi Resident Magistrate', 4, 103, 'In the Resident Magistrate Court of Lindi at Lindi', 'LND', 'inactive', '1000', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(209, 'Lindi Juvenile court At Lindi', 5, 104, 'In the Juvenile Court of Lindi at Lindi', 'JUVLND', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(210, 'Lindi Juvenile court At Kilwa Masoko', 5, 105, 'In the Juvenile Court of Lindi at Kilwa Masoko', 'JUVKIL', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(211, 'Lindi Juvenile court At Nachingwea', 5, 106, 'In the Juvenile Court of Lindi at Nachingwea', 'JUVNAC', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(212, 'Lindi Juvenile court At Liwale', 5, 107, 'In the Juvenile Court of Lindi at Liwale', 'JUVLWL', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(213, 'Lindi Juvenile court At Ruangwa', 5, 108, 'In the Juvenile Court of Lindi at Ruangwa', 'JUVRUA', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(214, 'Lindi District Court', 5, 104, 'In the District Court of Lindi Ditrict at Lindi', 'LND', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(215, 'Kilwa Masoko District Court', 5, 105, 'In the District Court of Kilwa Masoko District at Kilwa Masoko', 'KIL', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(216, 'Nachingwea District Court', 5, 106, 'In the District Court of Nachingwea District at Nachingwea', 'NAC', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(217, 'Liwale District Court', 5, 107, 'In the District Court of Newale Ditrict at Liwale', 'LWL', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(218, 'Ruangwa District Court', 5, 108, 'In the District Court of Ruangwa District at Ruangwa', 'RUA', 'inactive', '2004', 1, 11, 17, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(219, 'Mwanza Court of Appeal', 2, 10, 'In the Court of Appeal of Tanzania at Mwanza', 'CAT', 'inactive', '2002', 1, 12, 1, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(220, 'Mwanza High Court', 3, 10, 'In the High court of United Republic of Tanzania at Mwanza', 'HCT', 'inactive', '2003', 1, 12, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(221, 'Mwanza High Court Labour Division', 3, 10, 'In the High court of United Republic of Tanzania Labour Division at Mwanza', 'LBR', 'inactive', '2301', 1, 12, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(222, 'Mwanza High Court Land Division', 3, 10, 'In the High court of United Republic of Tanzania Land Division at Mwanza', 'LND', 'inactive', '2006', 1, 12, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(223, 'Mwanza High Court Commercial Division', 3, 10, 'In the High court of United Republic of Tanzania Commercial Division at Mwanza', 'COMM', 'inactive', '2005', 1, 3, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(224, 'Mwanza High Court Economic and Corruption Division', 3, 10, 'In the High court of United Republic of Tanzania Economic and Corruption Division at Mwanza', 'ECC', 'inactive', '2326', 1, 12, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(225, 'Mwanza Resident Magistrate', 4, 109, 'In the Resident Magistrate Court of Mwanza at Mwanza', 'MZA', 'inactive', '1000', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(226, 'Mwanza Juvenile court At Nyamagana', 5, 110, 'In the Juvenile Court of Mwanza at Nyamagana', 'JUVNYG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(227, 'Mwanza Juvenile court At Ilemela', 5, 111, 'In the Juvenile Court of Mwanza at Ilemela', 'JUVILE', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(228, 'Mwanza Juvenile court At Magu', 5, 112, 'In the Juvenile Court of Mwanza at Magu', 'JUVMAG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(229, 'Mwanza Juvenile court At Kwimba', 5, 113, 'In the Juvenile Court of Mwanza at Kwimba', 'JUVKWB', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(230, 'Mwanza Juvenile court At Misungwi', 5, 114, 'In the Juvenile Court of Mwanza at Misungwi', 'JUVMSG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(231, 'Mwanza Juvenile court At Sengerema', 5, 115, 'In the Juvenile Court of Mwanza at Sengerema', 'JUVSGM', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(232, 'Mwanza Juvenile court At Ukerewe', 5, 116, 'In the Juvenile Court of Mwanza at Ukerewe', 'JUVUKR', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(233, 'Nyamagana District Court', 5, 110, 'In the District Court of Nyamagana District at Nyamagana', 'NYG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(234, 'Ilemela  District Court', 5, 111, 'In the District Court of Ilemela District at Ilemela', 'ILE', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(235, 'Magu District Court', 5, 112, 'In the District Court of Magu District at Magu', 'MAG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(236, 'Kwimba District Court', 5, 113, 'In the District Court of Kwimba District at Kwimba', 'KWB', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(237, 'Misungwi District Court', 5, 114, 'In the District Court of Misungwi District at Misungwi', 'MSG', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(238, 'Sengerema District Court', 5, 115, 'In the District Court of Sengerema District at Sengerema', 'SGM', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(239, 'Ukerewe District Court', 5, 116, 'In the District Court of Ukerewe District at Ukerewe', 'UKR', 'inactive', '2004', 1, 12, 18, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(240, 'Musoma Resident Magistrate', 4, 117, 'In the Resident Magistrate Court at Musoma', 'MSM', 'inactive', '1000', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(241, 'Mara Juvenile court At Musoma', 5, 118, 'In the Juvenile Court of Mara at Musoma', 'JUVMSM', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(242, 'Mara Juvenile court At Butiama', 5, 119, 'In the Juvenile Court of Mara at Butiama', 'JUVBUT', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(243, 'Mara Juvenile court At Bunda', 5, 120, 'In the Juvenile Court of Mara at Bunda', 'JUVBND', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(244, 'Mara Juvenile court At Rolya', 5, 121, 'In the Juvenile Court of Mara at Rorya', 'JUVROR', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(245, 'Mara Juvenile court At Tarime', 5, 122, 'In the Juvenile Court of Mara at Tarime', 'JUVTRM', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(246, 'Mara Juvenile court At Serengeti', 5, 123, 'In the Juvenile Court of Mara at Serengeti', 'JUVSRT', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(247, 'Musoma District Court', 5, 118, 'In the District Court of Musoma District at Musoma', 'MSM', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(248, 'Butiama District Court', 5, 119, 'In the District Court of Butiama District at Butiama', 'BUT', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(249, 'Bunda District Court', 5, 120, 'In the District Court of Bunda District at Bunda', 'BND', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(250, 'Rolya District Court', 5, 121, 'In the District Court of Rorya District at Rorya', 'ROR', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(251, 'Tarime District Court', 5, 122, 'In the District Court of Tarime District at Tarime', 'TRM', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(252, 'Serengeti District Court', 5, 123, 'In the District Court of Serengeti District at Serengeti', 'SRT', 'inactive', '2004', 1, 21, 19, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(253, 'Geita Resident Magistrate', 4, 124, 'In the Resident Magistrate Court of Geita at Geita', 'GTA', 'inactive', '1000', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(254, 'Geita Juvenile court At Geita', 5, 125, 'In the Juvenile Court of Geita at Geita', 'JUVGTA', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(255, 'Geita Juvenile court At Nyangw\'ale', 5, 126, 'In the Juvenile Court of Geita at Nyang\'wale', 'JUVNYL', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(256, 'Geita Juvenile court At Chato', 5, 127, 'In the Juvenile Court of Geita at Chato', 'JUVCHT', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(257, 'Geita Juvenile court At Bukombe', 5, 128, 'In the Juvenile Court of Geita at Bukombe', 'JUVBUK', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(258, 'Geita Juvenile court At Mbogwe', 5, 129, 'In the Juvenile Court of Geita at Mbogwe', 'JUVMBG', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(259, 'Geita District Court', 5, 125, 'In the District Court of Geita Ditrict at Geita', 'GTA', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(260, 'Nyangw\'ale District Court', 5, 126, 'In the District Court of Nyangw\'ale District at Nyangw\'ale', 'NYL', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(261, 'Chato District Court', 5, 127, 'In the District Court of Chato District at Chato', 'CHT', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(262, 'Bukombe District Court', 5, 128, 'In the District Court of Bukombe District at Bukombe', 'BUK', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(263, 'Mbogwe District Court', 5, 129, 'In the District Court of Mbogwe District at Mbogwe', 'MBG', 'inactive', '2004', 1, 12, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(264, 'Shinyanga High Court', 3, 11, 'In the High Court of United Republic of Tanzania at Shinyanga', 'HCT', 'inactive', '2003', 1, 13, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(265, 'Shinyanga High Court Labour Division', 3, 11, 'In the High Court of United Republic of Tanzania Labour Division at Shinyanga', 'LBR', 'inactive', '2301', 1, 13, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(266, 'Shinyanga High Court Land Division', 3, 11, 'In the High Court of United Republic of Tanzania Land Division at Shinyanga', 'LND', 'inactive', '2006', 1, 13, 20, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(267, 'Shinyanga High Court Economic and Corruption Division', 3, 11, 'In the High Court of United Republic of Tanzania Economic and Corruption Division at Shinyanga', 'ECC', 'inactive', '2326', 1, 13, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(268, 'Shinyanga Resident Magistrate', 4, 130, 'In the Resident Magistrate Court of Shinyanga at Shinyanga', 'SHY', 'inactive', '1000', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(269, 'Shinyanga Juvenile court At Shinyanga', 5, 131, 'In the Juvenile Court of Shinyanga at Shinyanga', 'JUVSHY', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(270, 'Shinyanga Juvenile court At Kahama', 5, 132, 'In the Juvenile Court of Shinyanga at Kahama', 'JUVKHM', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(271, 'Shinyanga Juvenile court At Kishapu', 5, 133, 'In the Juvenile Court of Shinyanga at Kishapu', 'JUVKSP', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00');
INSERT INTO `courts` (`id`, `name`, `court_level_id`, `location_id`, `display_name`, `code`, `status`, `sub_sp_code`, `judiciary`, `zonal`, `special`, `created_at`, `updated_at`) VALUES
(272, 'Shinyanga District Court', 5, 131, 'In the District Court of Shinyanga District at Shinyanga', 'SHY', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(273, 'Kahama  District Court', 5, 132, 'In the District Court of Kahama District at Kahama', 'KHM', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(274, 'Kishapu District Court', 5, 133, 'In the District Court of Kishapu District at Kishapu', 'KSP', 'inactive', '2004', 1, 13, 21, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(275, 'Simiyu Resident Magistrate', 4, 134, 'In the Resident Magistrate Court of Simiyu at Simiyu', 'SMY', 'inactive', '1000', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(276, 'Simiyu Juvenile court At Bariadi', 5, 135, 'In the Juvenile Court of Simiyu at Bariadi', 'JUVBAR', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(277, 'Simiyu Juvenile court At Maswa', 5, 136, 'In the Juvenile Court of Simiyu at Maswa', 'JUVMAS', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(278, 'Simiyu Juvenile court At Meatu', 5, 137, 'In the Juvenile Court of Simiyu at Meatu', 'JUVMEA', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(279, 'Simiyu Juvenile court At Itilima', 5, 138, 'In the Juvenile Court of Simiyu at Itilima', 'JUVITL', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(280, 'Simiyu Juvenile court At Busega', 5, 139, 'In the Juvenile Court of Simiyu at Busega', 'JUVBSG', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(281, 'Bariadi District Court', 5, 135, 'In the District Court of Bariadi District at Bariadi', 'BAR', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(282, 'Maswa District Court', 5, 136, 'In the District Court of Maswa District at Maswa', 'MAS', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(283, 'Meatu District Court', 5, 137, 'In the District Court of Meatu District at Meatu', 'MEA', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(284, 'Itilima District Court', 5, 138, 'In the District Court of Itilima District at Itilima', 'ITL', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(285, 'Busega District Court', 5, 139, 'In the District Court of Busega District at Busega', 'BSG', 'inactive', '2004', 1, 13, 22, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(286, 'Songea High Court', 3, 12, 'In the High Court of United Republic of Tanzania at Songea', 'HCT', 'inactive', '2003', 1, 14, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(287, 'Songea  High Court Labour Division', 3, 12, 'In the High Court of United Republic of Tanzania Labour Division at Songea', 'LBR', 'inactive', '2301', 1, 14, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(288, 'Songea  High Court Land Division', 3, 12, 'In the High Court of United Republic of Tanzania Land Division at Songea', 'LND', 'inactive', '2006', 1, 14, NULL, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(289, 'Songea  High Court Economic and Corruption Division', 3, 12, 'In the High Court of United Republic of Tanzania Economic and Corruption Division at Songea', 'ECC', 'inactive', '2326', 1, 14, 2, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(290, 'Songea  Resident Magistrate', 4, 140, 'In the Resident Magistrate Court of Songea at Songea', 'SNG', 'inactive', '1000', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(291, 'Ruvuma Juvenile court At Songea ', 5, 141, 'In the Juvenile Court of Ruvuma at Songea', 'JUVSNG', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(292, 'Ruvuma Juvenile court At Mbinga', 5, 142, 'In the Juvenile Court of Ruvuma at Mbinga', 'JUVMBG', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(293, 'Ruvuma Juvenile court At Nyasa', 5, 143, 'In the Juvenile Court of Ruvuma at Nyasa', 'JUVNYS', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(294, 'Ruvuma Juvenile court At Tunduru', 5, 144, 'In the Juvenile Court of Ruvuma at Tunduru', 'JUVTDR', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(295, 'Ruvuma Juvenile court At Namtumbo', 5, 145, 'In the Juvenile Court of Ruvuma at Namtumbo', 'JUVNTB', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(296, 'Songea District Court', 5, 141, 'In the District Court of Songea District at Songea', 'SNG', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(297, 'Mbinga District Court', 5, 142, 'In the District Court of Mbinga District at Mbinga', 'MBG', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(298, 'Nyasa  District Court', 5, 143, 'In the District Court of Nyasa District at Nyasa', 'NYS', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:00', '2018-07-01 10:26:00'),
(299, 'Tunduru District Court', 5, 144, 'In the District Court of Tunduru District at Tunduru', 'TDR', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(300, 'Namtumbo District Court', 5, 145, 'In the District Court of Namtumbo District at Namtumbo', 'NTB', 'inactive', '2004', 1, 14, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(301, 'Sumbawanga Court of Appeal', 2, 13, 'In the Court of Appeal of Tanzania at Sumbawanga', 'CAT', 'inactive', '2002', 1, 15, 1, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(302, 'Sumbawanga High Court', 3, 13, 'In the High Court of United Republic of Tanzania at Sumbawanga', 'HCT', 'inactive', '2003', 1, 15, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(303, 'Sumbawanga High Court Labour Division', 3, 13, 'In the High Court of United Republic of Tanzania Labour Division at Sumbawanga', 'LBR', 'inactive', '2301', 1, 15, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(304, 'Sumbawanga High Court Land Division', 3, 13, 'In the High Court of United Republic of Tanzania Land Division at Sumbawanga', 'LND', 'inactive', '2006', 1, 15, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(305, 'Sumbawanga High Court Commercial Division', 3, 13, 'In the High Court of United Republic of Tanzania Commercial Division at Sumbawanga', 'COM', 'inactive', '2005', 1, 15, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(306, 'Sumbawanga High Court Economic and Corruption Division', 3, 13, 'In the High Court of United Republic of Tanzania Economic and Corruption Division at Sumbawanga', 'ECC', 'inactive', '2326', 1, 15, 2, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(307, 'Sumbawanga Resident Magistrate', 4, 146, 'In the Resident Magistrate Court of Sumbawanga at Sumbawanga', 'SBG', 'inactive', '1000', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(308, 'Rukwa Juvenile court At Sumbawanga', 5, 147, 'In the Juvenile Court of Rukwa at Sumbawanga', 'JUVSBG', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(309, 'Rukwa Juvenile court At Nkasi', 5, 148, 'In the Juvenile Court of Rukwa at Nkasi', 'JUVNKS', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(310, 'Rukwa Juvenile court At Kalambo', 5, 149, 'In the Juvenile Court of Rukwa at Kalambo', 'JUVKLB', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(311, 'Sumbawanga District Court', 5, 147, 'In the District Court of Sumbawanga District at Sumbawanga', 'SBG', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(312, 'Nkasi  District Court', 5, 148, 'In the District Court of Nkasi District at Nkasi', 'NKS', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(313, 'Kalambo District Court', 5, 149, 'In the District Court of Kalambo District at Kalambo', 'KLB', 'inactive', '2004', 1, 15, 23, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(314, 'Katavi Resident Magistrate', 4, 150, 'In the Resident Magistrate Court of Katavi at Katavi', 'KTV', 'inactive', '1000', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(315, 'Katavi Juvenile court At Mpanda', 5, 151, 'In the Juvenile Court of Katavi at Mpanda', 'JUVMPD', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(316, 'Katavi Juvenile court At Tanganyika', 5, 152, 'In the Juvenile Court of Katavi at Tanganyika', 'JUVTNG', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(317, 'Katavi Juvenile court At Mlele', 5, 153, 'In the Juvenile Court of Katavi at Mlele', 'JUVMLL', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(318, 'Mpanda District Court', 5, 151, 'In the District Court of Mpanda District at Mpanda', 'MPD', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(319, 'Tanganyika District Court', 5, 152, 'In the District Court of Tanganyika District at Tanganyika', 'TNG', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(320, 'Mlele District Court', 5, 153, 'In the District Court of Mlela District at Mlele', 'MLL', 'inactive', '2004', 1, 15, 24, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(321, 'Tabora High Court', 3, 14, 'In the High Court of United Republic of Tanzania at Tabora', 'HCT', 'inactive', '2003', 1, 16, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(322, 'Tabora High Court Labour Division', 3, 14, 'In the High Court of United Republic of Tanzania Labour Division at Tabora', 'LBR', 'inactive', '2301', 1, 16, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(323, 'Tabora High Court Land Division', 3, 14, 'In the High Court of United Republic of Tanzania Land Division at Tabora', 'LND', 'inactive', '2006', 1, 16, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(324, 'Tabora High Court Economic and Corruption Division', 3, 14, 'In the High Court of United Republic of Tanzania Economic and Corruption Division at Tabora', 'ECC', 'inactive', '2326', 1, 16, 2, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(325, 'Tabora Resident Magistrate', 4, 154, 'In the Resident Magistrate Court of Tabora at Tabora', 'TBR', 'inactive', '1000', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(326, 'Tabora Juvenile court At Tabora', 5, 155, 'In the Juvenile Court of Tabora at Tabora', 'JUVTBR', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(327, 'Tabora Juvenile court At Igunga', 5, 156, 'In the Juvenile Court of Tabora at Igunga', 'JUVIGN', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(328, 'Tabora Juvenile court At Nzega', 5, 157, 'In the Juvenile Court of Tabora at Nzega', 'JUVNZG', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(329, 'Tabora Juvenile court At Sikonge', 5, 158, 'In the Juvenile Court of Tabora at Sikonge', 'JUVSKN', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(330, 'Tabora Juvenile court At Uyui', 5, 159, 'In the Juvenile Court of Tabora at Uyui', 'JUVUYU', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(331, 'Tabora Juvenile court At Kaliua', 5, 160, 'In the Juvenile Court of Tabora at Kaliua', 'JUVKAL', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(332, 'Tabora Juvenile court At Urambo', 5, 161, 'In the Juvenile Court of Tabora at Urambo', 'JUVURB', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(333, 'Tabora District Court', 5, 155, 'In the District Court of Tabora District at Tabora', 'TBR', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(334, 'Igunga District Court', 5, 156, 'In the District Court of Igunga District at Igunga', 'IGN', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(335, 'Nzega District Court', 5, 157, 'In the District Court of Nzega District at Nzega', 'NZG', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(336, 'Sikonge District Court', 5, 158, 'In the District Court of Sikonge District at Sikonge', 'SKN', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(337, 'Uyui  District Court', 5, 159, 'In the District Court of Uyui District at uyui', 'UYU', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(338, 'Kaliua District Court', 5, 160, 'In the District Court of Kaliua District at Kaliua', 'KAL', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(339, 'Urambo District Court', 5, 161, 'In the District Court of Urambo District at Urambo', 'URB', 'inactive', '2004', 1, 16, 25, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(340, 'Kigoma Resident Magistrate', 4, 162, 'In the Resident Magistrate Court of Kigoma at Kigoma', 'KGM', 'inactive', '1000', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(341, 'Kigoma Juvenile court At Kigoma', 5, 163, 'In the Juvenile Court of Kigoma at Kigoma', 'JUVKGM', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(342, 'Kigoma Juvenile court At Kasulu', 5, 164, 'In the Juvenile Court of Kigoma at Kasulu', 'JUVKSL', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(343, 'Kigoma Juvenile court At Kibondo', 5, 165, 'In the Juvenile Court of Kigoma at Kibondo', 'JUVKBD', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(344, 'Kigoma Juvenile court At Buhigwe', 5, 166, 'In the Juvenile Court of Kigoma at Buhigwe', 'JUVBHG', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(345, 'Kigoma Juvenile court At Kakonko', 5, 167, 'In the Juvenile Court of Kigoma at Kakonko', 'JUVKNK', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(346, 'Kigoma Juvenile court At Uvinza', 5, 168, 'In the Juvenile Court of Kigoma at Uvinza', 'JUVUVZ', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(347, 'Kigoma District Court', 5, 163, 'In the District Court of Kigoma District at Kigoma', 'KGM', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(348, 'Kasulu District Court', 5, 164, 'In the District Court of Kasulu District at Kasulu', 'KSL', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(349, 'Kibondo District Court', 5, 165, 'In the District Court of Kibondo District at Kibondo', 'KBD', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(350, 'Buhigwe District Court', 5, 166, 'In the District Court of Buhigwe District at Buhigwe', 'BHG', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(351, 'Kakonko District Court', 5, 167, 'In the District Court of Kakonko District at Kakonko', 'KNK', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(352, 'Uvinza District Court', 5, 168, 'In the District Court of Uvinza District at Uvinza', 'UVZ', 'inactive', '2004', 1, 22, 26, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(353, 'Tanga Court of Appeal', 2, 15, 'In the Court of Appeal of Tanzania at Tanga', 'CAT', 'inactive', '2002', 1, 17, 1, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(354, 'Tanga High Court', 3, 15, 'In the High Court of United Republic of  Tanzania at Tanga', 'HCT', 'inactive', '2003', 1, 17, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(355, 'Tanga High Court Labour Division', 3, 15, 'In the High Court of United Republic of  Tanzania Labour Division at Tanga', 'LBR', 'inactive', '2301', 1, 17, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(356, 'Tanga High Court Land Division', 3, 15, 'In the High Court of United Republic of  Tanzania Land Division at Tanga', 'LND', 'inactive', '2006', 1, 17, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(357, 'Tanga High Court Economic and Corruption Division', 3, 15, 'In the High Court of United Republic of  Tanzania Economic and Corruption Division at Tanga', 'ECC', 'inactive', '2326', 1, 17, 2, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(358, 'Tanga Resident Magistrate', 4, 169, 'In the Resident Magistrate Court of Tanga at Tanga', 'TNG', 'inactive', '1000', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(359, 'Tanga Juvenile court At Tanga', 5, 170, 'In the Juvenile Court of Tanga at Tanga', 'JUVTNG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(360, 'Tanga Juvenile court At Lushoto', 5, 171, 'In the Juvenile Court of Tanga at Lushoto', 'JUVLST', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(361, 'Tanga Juvenile court At Handeni', 5, 172, 'In the Juvenile Court of Tanga at Handeni', 'JUVHDN', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(362, 'Tanga Juvenile court At Korogwe', 5, 173, 'In the Juvenile Court of Tanga at Korogwe', 'JUVKRG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(363, 'Tanga Juvenile court At Muheza', 5, 174, 'In the Juvenile Court of Tanga at Muheza', 'JUVMHZ', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(364, 'Tanga Juvenile court At Mkinga', 5, 175, 'In the Juvenile Court of Tanga at Mkinga', 'JUVMKG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(365, 'Tanga Juvenile court At Pangani', 5, 176, 'In the Juvenile Court of Tanga at Pangani', 'JUVPGN', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(366, 'Tanga District Court', 5, 170, 'In the District Court of Tanga District at Tanga', 'TNG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(367, 'Lushoto District Court', 5, 171, 'In the District Court of Lushoto District at Lushoto', 'LST', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(368, 'Handeni District Court', 5, 172, 'In the District Court of Handeni District at Handeni', 'HDN', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(369, 'Korogwe District Court', 5, 173, 'In the District Court of Korogwe District at Korogwe', 'KRG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(370, 'Muheza  District Court', 5, 174, 'In the District Court  of Muheza District at Muheza', 'MHZ', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(371, 'Mkinga District Court', 5, 175, 'In the District Court of Mkinga District at Mkinga', 'MKG', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(372, 'Pangani District Court', 5, 176, 'In the District Court of Pangani District at Pangani', 'PGN', 'inactive', '2004', 1, 17, 27, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(373, 'Judiciary of Tanzania', 1, 0, 'Judiciary of Tanzania', 'JOT', 'inactive', NULL, 1, 17, NULL, '2018-07-01 10:26:01', '2018-07-01 10:26:01'),
(377, 'Babati District Court', 5, 177, 'In the District Court of Babati at Babati', 'BBT', 'inactive', '2004', 1, 2, 3, '2018-07-04 11:08:21', '2018-07-04 11:08:21'),
(378, 'Manyara Juvenile Court at Babati', 5, 177, 'In the Juvenile Court of Manyara at Babati', 'JUVBBT', 'inactive', '2004', 1, 2, 4, '2018-07-04 11:10:42', '2018-07-04 11:10:42'),
(379, 'Moshi Court of Appeal', 2, 8, 'In the Court of Appeal of Tanzania at Moshi', 'CAT', 'inactive', '2002', 1, 10, 1, '2018-07-04 11:13:19', '2018-07-04 11:13:19'),
(380, 'Songea Court of Appeal', 2, 12, 'In the Court of Appeal of Tanzania at Songea', 'CAT', 'inactive', '2002', 1, 14, 1, '2018-07-04 11:15:37', '2018-07-04 11:15:37'),
(381, 'Tabora Court of Appeal', 2, 14, 'In the Court of Appeal of Tanzania at Tabora', 'CAT', 'inactive', '2002', 1, 16, 1, '2018-07-04 11:19:11', '2018-07-04 11:19:11'),
(382, 'Musoma High Court Labour Division', 3, 117, 'In the High Court of United Republic of Tanzania Labour Division at Musoma', 'LBR', 'inactive', '2301', 1, 12, NULL, '2018-07-04 11:23:48', '2018-07-04 11:36:28'),
(383, 'Morogoro High Court Labour Division', 3, 50, 'In the High Court of United Republic of Tanzania Labour Division at Morogoro', 'LBR', 'inactive', '2301', 1, 18, NULL, '2018-07-04 11:26:55', '2018-07-04 11:26:55'),
(384, 'Singida High Court Labour Division', 3, 65, 'In the High Court of United Republic of Tanzania Labour Division at Singida', 'LBR', 'inactive', '2301', 1, 6, NULL, '2018-07-04 11:35:19', '2018-07-04 11:35:19'),
(385, 'Lindi High Court Labour Division', 3, 103, 'In the High Court of United Republic of Tanzania Labour Division at Lindi', 'LBR', 'inactive', '2301', 1, 11, NULL, '2018-07-04 11:39:41', '2018-07-04 11:39:41'),
(386, 'Kigoma High Court Labour Division', 3, 162, 'In the High Court of United Republic of Tanzania Labour Division at Kigoma', 'LBR', 'inactive', '2301', 1, 16, NULL, '2018-07-04 11:41:42', '2018-07-04 11:41:42'),
(387, 'Tax Revenue Appeal Tribunal (TRAT)', 7, 1, 'Tax Revenue Appeal Tribunal (TRAT)', '000000', 'inactive', NULL, NULL, NULL, NULL, '2019-01-16 07:31:56', '2019-01-16 07:31:56'),
(388, 'Fair Competition  Tribunal (FCT)', 7, 1, 'Fair Competition  Tribunal (FCT)', '000000', 'inactive', NULL, NULL, NULL, NULL, '2019-01-16 07:34:29', '2019-01-16 07:34:29'),
(389, 'Commission For Mediation And Arbitration (CMA)', 7, 1, 'Commission For Mediation And Arbitration (CMA)', '000000', 'inactive', NULL, NULL, NULL, NULL, '2019-01-16 07:36:54', '2019-01-16 07:36:54'),
(390, 'District Land and Housing Tribunals (DLHT)', 7, 1, 'District Land and Housing Tribunals (DLHT)', '000000', 'inactive', '2006', NULL, NULL, NULL, '2019-01-16 07:38:27', '2019-01-16 07:38:27'),
(391, 'Kigoma High Court', 3, 182, 'In the High court of United Republic of Tanzania at Kigoma', 'KGM', 'active', '2003', 1, 22, NULL, '2019-04-01 12:08:04', '2021-08-27 08:10:05'),
(392, 'Musoma High Court', 3, 183, 'In the High court of United Republic of Tanzania at Musoma', 'MSM', 'active', '2003', 1, 21, NULL, '2019-04-01 12:12:47', '2021-08-27 08:10:50'),
(393, 'Musoma High Court Economic and Corruption Division', 3, 183, 'In the High court of United Republic of Tanzania at Musoma', 'ECC', 'inactive', '2326', 1, 21, NULL, '2019-04-29 10:24:54', '2019-04-29 10:24:54'),
(394, 'Kigoma High Court Economic and Corruption Division', 3, 182, 'In the High court of United Republic of Tanzania at Kigoma', 'ECC', 'inactive', '2326', NULL, NULL, NULL, '2019-04-29 10:26:42', '2019-04-29 10:26:42'),
(395, 'Lugoba Juvenile Court At Bagamoyo', 6, 46, 'In the Juvenile Court of Lugoba at Bagamoyo', 'JUVLGB', 'inactive', '2004', NULL, NULL, NULL, '2019-12-16 15:27:50', '2019-12-23 11:45:56'),
(396, 'Kilindi District Court', 5, 184, 'In The District Court of Kilindi at Kilindi', 'KIL', 'inactive', '2004', NULL, NULL, NULL, '2020-01-29 10:10:16', '2020-01-29 10:26:33'),
(397, 'Malinyi District Court', 5, 185, 'In the District Court of Malinyi District at Malinyi', 'MLY', 'inactive', '2004', NULL, NULL, NULL, '2020-05-07 10:44:25', '2020-05-07 10:44:25'),
(398, 'Malinyi juvenile court', 5, 50, 'Malinyi Juvenile Court at Malinyi', 'JUVSBG', 'inactive', '2004', NULL, NULL, NULL, '2020-07-29 12:03:12', '2020-07-29 12:03:12'),
(2463, 'Arusha Urban Primar Court', 6, 17, 'In the Primary Court of Arusha at Arusha Urban ', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2464, 'Kingori Primary Court', 6, 18, 'In the Primary Court of Arumeru at Kingori', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2465, 'Maji Ya Chai Primary Court', 6, 18, 'In the Primary Court of Arumeru at Maji ya Chai', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2466, 'Ngarenanyuki Primary Court', 6, 18, 'In the Primary Court of Arumeru at Ngarenanyuki', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2467, 'Nduruma Primary Court', 6, 18, 'In the Primary Court of Arumeru at Nduruma', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2468, 'Nkoaranga Primary Court', 6, 18, 'In the Primary Court of Arumeru at Nkoaranga', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2469, 'Oldonyosambu Primary Court', 6, 18, 'In the Primary Court of Arumeru at Oldonyosambu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2470, 'Poli Primary Court', 6, 18, 'In the Primary Court of Arumeru at Poli', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2471, 'Usa River Primary Court', 6, 18, 'In the Primary Court of Arumeru at Usa River', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2472, 'Emaoi Primary Court', 6, 18, 'In the Primary Court of Arumeru at Emaoi', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2473, 'Enaboishu Primary Court', 6, 18, 'In the Primary Court of Arumeru at Enaboishu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2474, 'Mbuguni Primary Court', 6, 18, 'In the Primary Court of Arumeru at Mbuguni', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2475, 'West Meru Primary Court', 6, 18, 'In the Primary Court of Arumeru at West Meru', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2476, 'Endabashi Primary Court', 6, 19, 'In the Primary Court of Karatu at Endabashi', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2477, 'Karatu Urban Primary Court', 6, 19, 'In the Primary Court of Karatu at Karatu urban', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2478, 'Mangola Primary Court', 6, 19, 'In the Primary Court of Karatu at Mangola', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2479, 'Oldieni Primary Court', 6, 19, 'In the Primary Court of Karatu at Oldieni', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2480, 'Mbulumbulu Primary Court', 6, 19, 'In the Primary Court of Karatu at Mbulumbulu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2481, 'Longido Urban Primary Court', 6, 20, 'In the Primary Court of Longido at Longido Urban', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2482, 'Namanga Primary Court', 6, 20, 'In the Primary Court of Longido at Namanga', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2483, 'Engaruka Primary Court', 6, 21, 'In the Primary Court of Monduli at Engaruka', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2484, 'Kissongo Primary Court', 6, 21, 'In the Primary Court of Monduli at Kissongo', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2485, 'Makuyuni Primary Court', 6, 21, 'In the Primary Court of Monduli at Makuyuni', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2486, 'Mto Wa Mbu Primary Court', 6, 21, 'In the Primary Court of Monduli at Mto wa mbu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2487, 'Digodigo Primary Court', 6, 22, 'In the Primary Court of Ngorongoro at Digodigo ', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2488, 'Enduleni Primary Court', 6, 22, 'In the Primary Court of Ngorongoro at Enduleni', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2489, 'Loliondo Primary Court', 6, 22, 'In the Primary Court of Ngorongoro at Loliondo', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2490, 'Ngorongoro Primary Court', 6, 22, 'In the Primary Court of Ngorongoro at Ngorongoro', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2491, 'Nainokanoka Primary Court', 6, 22, 'In the Primary Court of Ngorongoro at Nainokanoka', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2492, 'Babati Urban Primary Court', 6, 177, 'In the Primary Court of Babati at Babati urban', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2493, 'Bashnet Primary Court', 6, 177, 'In the Primary Court of Babati at Bashnet', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2494, 'Bonga Primary Court', 6, 177, 'In the Primary Court of Babati at Bonga', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2495, 'Dareda Primary Court', 6, 177, 'In the Primary Court of Babati at Dareda', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2496, 'Gallapo Primary Court', 6, 177, 'In the Primary Court of Babati at Gallapo', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2497, 'Magugu Primary Court', 6, 177, 'In the Primary Court of Babati at  Magugu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2498, 'Minjingu Primary Court', 6, 177, 'In the Primary Court of Babati at Minjingu', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2499, 'Gendi Primary Court', 6, 177, 'In the Primary Court of Babati at Gendi', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2500, 'Gidas Primary Court', 6, 177, 'In the Primary Court of Babati at Gidas', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2501, 'Ufana Primary Court', 6, 177, 'In the Primary Court of Babati at Ufana', 'PRM', 'inactive', '1001', 1, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2502, 'Endagikot Primary Court', 6, 24, 'In the Primary Court of Mbulu at Endagikot', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2503, 'Daudi Primary Court', 6, 24, 'In the Primary Court of Mbulu at Daudi', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2504, 'Dongobesh Primary Court', 6, 24, 'In the Primary Court of Mbulu at Dongobesh', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2505, 'Kainam Primary Court', 6, 24, 'In the Primary Court of Mbulu at Kainam', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2506, 'Tlawi Primary Court', 6, 24, 'In the Primary Court of Mbulu at Tlawi', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2507, 'Maghang Primary Court', 6, 24, 'In the Primary Court of Mbulu at Maghang', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2508, 'Murai Primary Court', 6, 24, 'In the Primary Court of Mbulu at Murai', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2509, 'Tumati Primary Court', 6, 24, 'In the Primary Court of Mbulu at Tumati', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2510, 'Katesh Primary Court', 6, 25, 'In the Primary Court of Hanang at Katesh', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2511, 'Endasak Primary Court', 6, 25, 'In the Primary Court of Hanang at Endasak', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2512, 'Bassotu Primary Court', 6, 25, 'In the Primary Court of Hanang at Bassotu', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2513, 'Kibaya Primary Court', 6, 26, 'In the Primary Court of Kiteto at Kibaya', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2514, 'Dosidosi Primary Court', 6, 26, 'In the Primary Court of Kiteto at Dosidosi', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2515, 'Kijungu Primary Court', 6, 26, 'In the Primary Court of Kiteto at Kijungu', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2516, 'Engasmet Primary Court', 6, 27, 'In the Primary Court of Simanjiro at Engasment', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2517, 'Shambarai Primary Court', 6, 27, 'In the Primary Court of Simanjiro at Shambarai', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2518, 'Terrat Primary Court', 6, 27, 'In the Primary Court of Simanjiro at Terrat', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2519, 'Nyumba Ya Mungu Primary Court', 6, 27, 'In the Primary Court of Simanjiro at Nyumba ya mungu', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2520, 'Msitu Wa Tembo Primary Court', 6, 27, 'In the Primary Court of Simanjiro at Msitu wa tembo', 'PRM', 'inactive', '1001', 1, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2521, 'Kolekelo Primary Court', 6, 28, 'In the Primary Court of Bukoba at Kolekelo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2522, 'Karabagaine Primary Court', 6, 28, 'In the Primary Court of Bukoba at Karabagaine', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2523, 'Katerelo Primary Court', 6, 28, 'In the Primary Court of Bukoba at Katerelo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2524, 'Ibwela Primary Court', 6, 28, 'In the Primary Court of Bukoba at Ibwela', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2525, 'Rubale Primary Court', 6, 28, 'In the Primary Court of Bukoba at Rubale', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2526, 'Izimbya Primary Court', 6, 28, 'In the Primary Court of Bukoba at Izimbya', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2527, 'Katoro Primary Court', 6, 28, 'In the Primary Court of Bukoba at Katoro', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2528, 'Buhendangabo Primary Court', 6, 28, 'In the Primary Court of Bukoba at Buhendangabo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2529, 'Kishanje Primary Court', 6, 28, 'In the Primary Court of Bukoba at Kishanje', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2530, 'Bukoba Urban Primary Court', 6, 28, 'In the Primary Court of Bukoba at Bukoba URBAN', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2531, 'Nyakato Primary Court', 6, 28, 'In the Primary Court of Bukoba at Nyakato', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2532, 'Kyaka Primary Court', 6, 30, 'In the Primary Court of Misenyi at Kyaka', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2533, 'Nsunga Primary Court', 6, 30, 'In the Primary Court of Misenyi at Nsunga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2534, 'Minziro Primary Court', 6, 30, 'In the Primary Court of Misenyi at Minziro', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2535, 'Kassambya Primary Court', 6, 30, 'In the Primary Court of Misenyi at Kassambya', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2536, 'Gera Primary Court', 6, 30, 'In the Primary Court of Misenyi at Gera', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2537, 'Ndwanilo Primary Court', 6, 30, 'In the Primary Court of Misenyi at Ndwanilo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2538, 'Kanyigo Primary Court', 6, 30, 'In the Primary Court of Misenyi at Kanyigo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2539, 'Kayanga Primary Court', 6, 30, 'In the Primary Court of Misenyi at Kayanga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2540, 'Muleba Urban Primary Court', 6, 31, 'In the Primary Court of Muleba at Muleba Urban', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2541, 'Nshamba Primary Court', 6, 31, 'In the Primary Court of Muleba at Nshamba', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2542, 'Rushwa Primary Court', 6, 31, 'In the Primary Court of Muleba at Rushwa', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2543, 'Mubunda Primary Court', 6, 31, 'In the Primary Court of Muleba at Mubunda', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2544, 'Nyamilanda Primary Court', 6, 31, 'In the Primary Court of Muleba at Nyamilanda', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2545, 'Muhutwe Primary Court', 6, 31, 'In the Primary Court of Muleba at Muhutwe', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2546, 'Kamachumu Primary Court', 6, 31, 'In the Primary Court of Muleba at Kamachumu', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2547, 'Kimwani Primary Court', 6, 31, 'In the Primary Court of Muleba at Kimwani', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2548, 'Bumbile Primary Court', 6, 31, 'In the Primary Court of Muleba at Bumbile', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2549, 'Kashasha Primary Court', 6, 31, 'In the Primary Court of Muleba at Kashasha', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2550, 'Nyamiaga Primary Court', 6, 32, 'In the Primary Court of Ngara at Nyamiaga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2551, 'Kanazi Primary Court', 6, 32, 'In the Primary Court of Ngara at Kanazi', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2552, 'Rusumo Primary Court', 6, 32, 'In the Primary Court of Ngara at Rusumo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2553, 'Shanga Primary Court', 6, 32, 'In the Primary Court of Ngara at Shanga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2554, 'Kabanga Primary Court', 6, 32, 'In the Primary Court of Ngara at Kabanga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2555, 'Rulenge Primary Court', 6, 32, 'In the Primary Court of Ngara at Rulenge', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2556, 'Murusagamba Primary Court', 6, 32, 'In the Primary Court of Ngara at Murusagamba', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2557, 'Muganza Primary Court', 6, 32, 'In the Primary Court of Ngara at Muganza', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2558, 'Mabawe Primary Court', 6, 32, 'In the Primary Court of Ngara at Mabawe', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2559, 'Nyakisasa Primary Court', 6, 32, 'In the Primary Court of Ngara at Nyakisasa', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2560, 'Bugarama Primary Court', 6, 32, 'In the Primary Court of Ngara at Bugarama', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2561, 'Bukirilo Primary Court', 6, 32, 'In the Primary Court of Ngara at Bukirilo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2562, 'Kirushya Primary Court', 6, 32, 'In the Primary Court of Ngara at Kirushya', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2563, 'Kayanga Primary Court', 6, 33, 'In the Primary Court of Karagwe at Kayanga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2564, 'Bugene Primary Court', 6, 33, 'In the Primary Court of Karagwe at Bugene', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2565, 'Kituntu Primary Court', 6, 33, 'In the Primary Court of Karagwe at Kituntu', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2566, 'Nyaishozi Primary Court', 6, 33, 'In the Primary Court of Karagwe at Nyaishozi', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2567, 'Nyabiyonza Primary Court', 6, 33, 'In the Primary Court of Karagwe at Nyabiyonza', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2568, 'Kaisho Primary Court', 6, 34, 'In the Primary Court of Kyerwa at kaisho', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2569, 'Mabira Primary Court', 6, 34, 'In the Primary Court of Kyerwa at Mabira', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2570, 'Murongo Primary Court', 6, 34, 'In the Primary Court of Kyerwa at Murongo', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2571, 'Nkwenda Primary Court', 6, 34, 'In the Primary Court of Kyerwa at Nkwenda', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2572, 'Biharamulo Urban Primary Court', 6, 35, 'In the Primary Court of Biharamulo at Biharamulo Urban', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2573, 'Runazi Primary Court', 6, 35, 'In the Primary Court of Biharamulo at Runazi', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2574, 'Lusahunga Primary Court', 6, 35, 'In the Primary Court of Biharamulo at Lusahunga', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2575, 'Kalenge Primary Court', 6, 35, 'In the Primary Court of Biharamulo at Kalenge', 'PRM', 'inactive', '1001', 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2576, 'Magomeni Primary Court', 6, 37, 'In the Primary Court of Kinondoni at Magomeni', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2577, 'Kawe Primary Court', 6, 37, 'In the Primary Court of Kinondoni at Kawe', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2578, 'Mwananyamala Primary Court', 6, 37, 'In the Primary Court of Kinondoni at Mwananyamala', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2579, 'Saranga Primary Court', 6, 38, 'In the Primary Court of Ubungo at Saranga', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2580, 'Sinza Primary Court', 6, 38, 'In the Primary Court of Ubungo at Sinza', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2581, 'Miburani Primary Court', 6, 39, 'In the Primary Court of Temeke at Miburani', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2582, 'Mbagala Primary Court', 6, 39, 'In the Primary Court of Temeke at Mbagala', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2583, 'Kigamboni Primary Court', 6, 40, 'In the Primary Court of Kigamboni at Kigamboni', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2584, 'Ilala Primary Court', 6, 41, 'In the Primary Court of Ilala at Ilala', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2585, 'Kariakoo Primary Court', 6, 41, 'In the Primary Court of Ilala at Kariakoo', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2586, 'Buguruni Primary Court', 6, 41, 'In the Primary Court of Ilala at Buguruni', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2587, 'Ukonga Primary Court', 6, 41, 'In the Primary Court of Ilala at Ukonga', 'PRM', 'inactive', '1001', 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2588, 'Mikumi Primary Court', 6, 51, 'In the Primary Court of Kilosa at Mikumi', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2589, 'Ruaha Kii Primary Court', 6, 51, 'In the Primary Court of Kilosa at Ruaha Kii', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2590, 'Malolo Primary Court', 6, 51, 'In the Primary Court of Kilosa at Malolo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2591, 'Kilosa Urban Primary Court', 6, 51, 'In the Primary Court of Kilosa at Kilosa Urban', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2592, 'Kimamba Primary Court', 6, 51, 'In the Primary Court of Kilosa at Kimamba', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2593, 'Kidete Primary Court', 6, 51, 'In the Primary Court of Kilosa at Kidete', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2594, 'Ulaya Primary Court', 6, 51, 'In the Primary Court of Kilosa at Ulaya', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2595, 'Masanze Primary Court', 6, 51, 'In the Primary Court of Kilosa at Masanze', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2596, 'Magole Primary Court', 6, 51, 'In the Primary Court of Kilosa at Magole', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2597, 'Msowero Primary Court', 6, 51, 'In the Primary Court of Kilosa at Msowero', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2598, 'Ludewa Primary Court', 6, 51, 'In the Primary Court of Kilosa at Ludewa', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2599, 'Kidodi Primary Court', 6, 51, 'In the Primary Court of Kilosa at Kidodi', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2600, 'Kisanga Primary Court', 6, 51, 'In the Primary Court of Kilosa at Kisanga', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2601, 'Mamboya Primary Court', 6, 51, 'In the Primary Court of Kilosa at Mamboya', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2602, 'Gairo Primary Court', 6, 52, 'In the Primary Court of Gairo at Gairo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2603, 'Chakwale Primary Court', 6, 52, 'In the Primary Court of Gairo at Chakwele', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2604, 'Idibo Primary Court', 6, 52, 'In the Primary Court of Gairo at Idibo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2605, 'Nongwe Primary Court', 6, 52, 'In the Primary Court of Gairo at Nongwe', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2606, 'Ifakara Urban Primary Court', 6, 53, 'In the Primary Court of Kilombero at Ifakara Urban', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2607, 'MangUla Primary Court', 6, 53, 'In the Primary Court of Kilombero at Mangula', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2608, 'Kiberege Primary Court', 6, 53, 'In the Primary Court of Kilombero at Kiberenge', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2609, 'Mngeta Primary Court', 6, 53, 'In the Primary Court of Kilombero at Mngeta', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2610, 'Mlimba Primary Court', 6, 53, 'In the Primary Court of Kilombero at Mlimba', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2611, 'Tanganyika Masagati Primary Court', 6, 53, 'In the Primary Court of Kilombero at Tanganyika masagati', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2612, 'Utengule Primary Court', 6, 53, 'In the Primary Court of Kilombero at Utengule', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2613, 'Mkamba Primary Court', 6, 53, 'In the Primary Court of Kilombero at Mkamba', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2614, 'Kilombero K I Primary Court', 6, 53, 'In the Primary Court of Kilombero at Kilombero k I', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2615, 'Chita Primary Court', 6, 53, 'In the Primary Court of Kilombero at Chita', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2616, 'Mbingu Primary Court', 6, 53, 'In the Primary Court of Kilombero at Mbingu', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2617, 'Morogoro Urban Primary Court', 6, 54, 'In the Primary Court of Morogoro at Morogoro Urban', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '2021-08-18 14:01:13'),
(2618, 'Chamwino Primary Court', 6, 54, 'In the Primary Court of Morogoro at Chamwino', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2619, 'Kingolwira Primary Court', 6, 54, 'In the Primary Court of Morogoro at Kingolwira', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2620, 'Matombo Primary Court', 6, 54, 'In the Primary Court of Morogoro at Matombo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2621, 'Tawa Primary Court', 6, 54, 'In the Primary Court of Morogoro at Tawa', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2622, 'Mkuyuni Primary Court', 6, 54, 'In the Primary Court of Morogoro at Mkuyuni', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `courts` (`id`, `name`, `court_level_id`, `location_id`, `display_name`, `code`, `status`, `sub_sp_code`, `judiciary`, `zonal`, `special`, `created_at`, `updated_at`) VALUES
(2623, 'Kisaki Primary Court', 6, 54, 'In the Primary Court of Morogoro at Kisaki', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2624, 'Bwakila Chini Primary Court', 6, 54, 'In the Primary Court of Morogoro at Bwakila chinu', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2625, 'Bwakila Juu Primary Court', 6, 54, 'In the Primary Court of Morogoro at Bwakila Juu', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2626, 'Mvuha Primary Court', 6, 54, 'In the Primary Court of Morogoro at Mvuha', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2627, 'Kolelo Primary Court', 6, 54, 'In the Primary Court of Morogoro at Kolelo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2628, 'Mikese Primary Court', 6, 54, 'In the Primary Court of Morogoro at Mikese', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2629, 'Ngerengere Primary Court', 6, 54, 'In the Primary Court of Morogoro at Ngerengere', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2630, 'Mgeta Primary Court', 6, 55, 'In the Primary Court of Mvomero at Mgeta', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2631, 'Mikongeni Primary Court', 6, 55, 'In the Primary Court of Mvomero at Mikongeni', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2632, 'Kibati Primary Court', 6, 55, 'In the Primary Court of Mvomero at Kibati', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2633, 'Turiani Primary Court', 6, 55, 'In the Primary Court of Mvomero at Turiani', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2634, 'Mtibwa Primary Court', 6, 55, 'In the Primary Court of Mvomero at Mtibwa', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2635, 'Mvomero Primary Court', 6, 55, 'In the Primary Court of Mvomero at Mvomero', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2636, 'Ilonga Primary Court', 6, 56, 'In the Primary Court of Ulanga at Ilonga', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2637, 'Ruaha Primary Court', 6, 56, 'In the Primary Court of Ulanga at Ruaha', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2638, 'Lupiro Primary Court', 6, 56, 'In the Primary Court of Ulanga at Lupiro', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2639, 'Iragua Primary Court', 6, 56, 'In the Primary Court of Ulanga at Iragua', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2640, 'Vigoi Primary Court', 6, 56, 'In the Primary Court of Ulanga at Vigoi', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2641, 'Mtimbira Primary Court', 6, 56, 'In the Primary Court of Ulanga at Mtimbira', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2642, 'Kilosa Mpepo Primary Court', 6, 56, 'In the Primary Court of Ulanga at Kilosa mpepo', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2643, 'Malinyi Primary Court', 6, 56, 'In the Primary Court of Ulanga at Malinyi', 'PRM', 'inactive', '1001', 1, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2644, 'Mailimoja Primary Court', 6, 43, 'In the Primary Court of Kibaha at Mailimoja', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2645, 'Mkuza Primary Court', 6, 43, 'In the Primary Court of Kibaha at Mkuza', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2646, 'Mlandizi Primary Court', 6, 43, 'In the Primary Court of Kibaha at Mlandizi', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2647, 'Ruvu Primary Court', 6, 43, 'In the Primary Court of Kibaha at Ruvu', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2648, 'Soga Primary Court', 6, 43, 'In the Primary Court of Kibaha at Soga', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2649, 'Magindu Primary Court', 6, 43, 'In the Primary Court of Kibaha at Magindu', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2650, 'Mkuranga Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Mkurunga', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2651, 'Mwarusembe Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Mwarusembe', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2652, 'Kimanzichana Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Kimanzichana', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2653, 'Kisiju Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Kisiju', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2654, 'Lukanga Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Lukanga', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2655, 'Shungubweni Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Shungubweni', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2656, 'Mkamba Primary Court', 6, 44, 'In the Primary Court of Mkurunga at Mkamba', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2657, 'Sungwi Primary Court', 6, 45, 'In the Primary Court of Kisarawe at Sungwi', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2658, 'Kisarawe Primary Court', 6, 45, 'In the Primary Court of Kisarawe at Kisarawe', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2659, 'Maneromango Primary Court', 6, 45, 'In the Primary Court of Kisarawe at Maneromango', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2660, 'Cholesamvula Primary Court', 6, 45, 'In the Primary Court of Kisarawe at Cholesamvula', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2661, 'Mzenga Primary Court', 6, 45, 'In the Primary Court of Kisarawe at Mzenga', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2662, 'Mwambao Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Mwambao', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2663, 'Kerege Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Kerege', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2664, 'Kiwangwa Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Kiwangwa', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2665, 'Chalinze Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at  Chalinze', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2666, 'Msoga/Lugoba Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Msoga/Lugoba', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2667, 'Miono Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at  Miono', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2668, 'Msata Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Msata', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2669, 'Yombo Primary Court', 6, 46, 'In the Primary Court of Bagamoyo at Yombo', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2670, 'Kirongwe Primary Court', 6, 47, 'In the Primary Court of Mafia at Kirongwe', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2671, 'Kilindoni Primary Court', 6, 47, 'In the Primary Court of Mafia at Kilindoni', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2672, 'Utete Primary Court', 6, 48, 'In the Primary Court of Rufiji at Utete', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2673, 'Mkongo Primary Court', 6, 48, 'In the Primary Court of Rufiji at Mkongo', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2674, 'Mwaseni Primary Court', 6, 48, 'In the Primary Court of Rufiji at Mwaseni', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2675, 'Mtanza Primary Court', 6, 48, 'In the Primary Court of Rufiji at Mtanza', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2676, 'Ngorongo Primary Court', 6, 48, 'In the Primary Court of Rufiji at Ngorongo', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2677, 'Ikwiriri Primary Court', 6, 48, 'In the Primary Court of Rufiji at Ikwiriri', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2678, 'Muhoro Primary Court', 6, 48, 'In the Primary Court of Rufiji at Muhoro', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2679, 'Chumbi Primary Court', 6, 48, 'In the Primary Court of Rufiji at Chimbi', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2680, 'Kibiti Primary Court', 6, 49, 'In the Primary Court of Kibiti at Kibiti', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2681, 'Bungu Primary Court', 6, 49, 'In the Primary Court of Kibiti at Bungu', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2682, 'Kikale Primary Court', 6, 49, 'In the Primary Court of Kibiti at Kikale', 'PRM', 'inactive', '1001', 1, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2683, 'Bahi Primary Court', 6, 58, 'In the Primary Court of Bahi at Bahi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2684, 'Kigwe Primary Court', 6, 58, 'In the Primary Court of Bahi at Kigwe', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2685, 'Chipanga Primary Court', 6, 58, 'In the Primary Court of Bahi at Chipanga', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2686, 'Kongwa Primary Court', 6, 58, 'In the Primary Court of Bahi at Kongwa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2687, 'Mwitikila Primary Court', 6, 58, 'In the Primary Court of Bahi at Mwitikila', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2688, 'Mundemu Primary Court', 6, 58, 'In the Primary Court of Bahi at Mundemu', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2689, 'Zanka Primary Court', 6, 58, 'In the Primary Court of Bahi at Zanka', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2690, 'Lamaiti Primary Court', 6, 58, 'In the Primary Court of Bahi at Lamaiti', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2691, 'Dodoma Urban Primary Court', 6, 59, 'In the Primary Court of Dodoma at Dodoma Urban', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2692, 'Makole Primary Court', 6, 59, 'In the Primary Court of Dodoma at Makole', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2693, 'Chamwino Urban Primary Court', 6, 59, 'In the Primary Court of Dodoma at Chamwino urban', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2694, 'Matumbulu Primary Court', 6, 59, 'In the Primary Court of Dodoma at Matumbulu', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2695, 'Nala Primary Court', 6, 59, 'In the Primary Court of Dodoma at Nala', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2696, 'Hombolo Primary Court', 6, 59, 'In the Primary Court of Dodoma at Hombolo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2697, 'Kikombo Primary Court', 6, 59, 'In the Primary Court of Dodoma at Kikombo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2698, 'Chamwino Ikulu Primary Court', 6, 60, 'In the Primary Court of Chamwino at Chamwino Ikulu', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2699, 'Chilonwa Primary Court', 6, 60, 'In the Primary Court of Chamwino at Chlonwa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2700, 'Dabalo Primary Court', 6, 60, 'In the Primary Court of Chamwino at Dabalo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2701, 'Handali Primary Court', 6, 60, 'In the Primary Court of Chamwino at Handali', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2702, 'Itiso Primary Court', 6, 60, 'In the Primary Court of Chamwino at Itiso', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2703, 'Haneti Primary Court', 6, 60, 'In the Primary Court of Chamwino at Haneti', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2704, 'Makangwa Primary Court', 6, 60, 'In the Primary Court of Chamwino at Makangwa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2705, 'Mvumi Primary Court', 6, 60, 'In the Primary Court of Chamwino at Mvumi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2706, 'Sanzawa Primary Court', 6, 61, 'In the Primary Court of Chemba at Sanzawa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2707, 'Mrijo Primary Court', 6, 61, 'In the Primary Court of Chemba at Mrijo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2708, 'Kwamtoro Primary Court', 6, 61, 'In the Primary Court of Chemba at Kwamtoro', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2709, 'Paranga Primary Court', 6, 61, 'In the Primary Court of Chemba at Paranga', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2710, 'Jangalo Primary Court', 6, 61, 'In the Primary Court of Chemba at Jangalo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2711, 'Goima Primary Court', 6, 61, 'In the Primary Court of Chemba at Goima', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2712, 'Mondo Primary Court', 6, 61, 'In the Primary Court of Chemba at Mondo', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2713, 'Farkwa Primary Court', 6, 61, 'In the Primary Court of Chemba at Farkwa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2714, 'Lalta Primary Court', 6, 61, 'In the Primary Court of Chemba at Lalta', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2715, 'Kondoa Primary Court', 6, 62, 'In the Primary Court of Kondoa at Kondoa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2716, 'Haubi Primary Court', 6, 62, 'In the Primary Court of Kondoa at Haubi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2717, 'Qorro Primary Court', 6, 62, 'In the Primary Court of Kondoa at Qorro', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2718, 'Masange Primary Court', 6, 62, 'In the Primary Court of Kondoa at Masange', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2719, 'Bereko Primary Court', 6, 62, 'In the Primary Court of Kondoa at Bereko', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2720, 'Bussi Primary Court', 6, 62, 'In the Primary Court of Kondoa at Bussi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2721, 'Pahi Primary Court', 6, 62, 'In the Primary Court of Kondoa at Pahi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2722, 'Kongwa Urban Primary Court', 6, 63, 'In the Primary Court of Kongwa at Kongwa urban', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2723, 'Sagara Primary Court', 6, 63, 'In the Primary Court of Kongwa at Sagara', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2724, 'Mlali Primary Court', 6, 63, 'In the Primary Court of Kongwa at Mlali', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2725, 'Pandambili Primary Court', 6, 63, 'In the Primary Court of Kongwa at Pandambili', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2726, 'Zoisa Primary Court', 6, 63, 'In the Primary Court of Kongwa at Zoisa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2727, 'Hogoro Primary Court', 6, 63, 'In the Primary Court of Kongwa at Hogoro', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2728, 'Chipogoro Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Chipogoro', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2729, 'Mlunduzi Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Mlunduzi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2730, 'Rudi Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Rudi', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2731, 'Ipera Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Ipera', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2732, 'Kibakwe Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Kibakwe', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2733, 'Lumuma Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Lumuma', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2734, 'Mima Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Mima', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2735, 'Mpwapwa Urban Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Mpwapwa', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2736, 'Mtera Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Mtera', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2737, 'Wotta Primary Court', 6, 64, 'In the Primary Court of Mpwapwa at Wotta', 'PRM', 'inactive', '1001', 1, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2738, 'Ikungi Primary Court', 6, 66, 'In the Primary Court of Ikungi at Ikungi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2739, 'Puma Primary Court', 6, 66, 'In the Primary Court of Ikungi at Puma', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2740, 'Sepuka Primary Court', 6, 66, 'In the Primary Court of Ikungi at Sepuka', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2741, 'Mungaa Primary Court', 6, 66, 'In the Primary Court of Ikungi at Mungaa', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2742, 'Ihanja Primary Court', 6, 66, 'In the Primary Court of Ikungi at Ihanja', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2743, 'Issuna Primary Court', 6, 66, 'In the Primary Court of Ikungi at Issuna', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2744, 'Misughaa Primary Court', 6, 66, 'In the Primary Court of Ikungi at Misughaa', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2745, 'Merya Primary Court', 6, 67, 'In the Primary Court of Singida at Merya', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2746, 'Mtinko Primary Court', 6, 67, 'In the Primary Court of Singida at Mtinko', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2747, 'Ilongero Primary Court', 6, 67, 'In the Primary Court of Singida at Ilongero', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2748, 'Ipembe Urban Primary Court', 6, 67, 'In the Primary Court of Singida at Ipembe urban', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2749, 'Utemini Primary Court', 6, 67, 'In the Primary Court of Singida at Utemini', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2750, 'Ngimu Primary Court', 6, 67, 'In the Primary Court of Singida at Ngimu', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2751, 'Mgori Primary Court', 6, 67, 'In the Primary Court of Singida at Mgori', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2752, 'Mtipa Primary Court', 6, 67, 'In the Primary Court of Singida at Mtipa', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2753, 'Manyoni Urban Primary Court', 6, 68, 'In the Primary Court of Manyoni at Manyoni Urban', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '2021-08-18 14:00:10'),
(2754, 'Kilimatinde Primary Court', 6, 68, 'In the Primary Court of Manyoni at Kilimatinde', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2755, 'Kintiku Primary Court', 6, 68, 'In the Primary Court of Manyoni at Kintiku', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2756, 'Makasuku Primary Court', 6, 68, 'In the Primary Court of Manyoni at Makasuku', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2757, 'Itigi Primary Court', 6, 68, 'In the Primary Court of Manyoni at Itigi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2758, 'Mgandu Primary Court', 6, 68, 'In the Primary Court of Manyoni at Mgandu', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2759, 'Mwamagembe Primary Court', 6, 68, 'In the Primary Court of Manyoni at Mwamagembe', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2760, 'Rungwa Primary Court', 6, 68, 'In the Primary Court of Manyoni at Rungwa', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2761, 'Chikola Primary Court', 6, 68, 'In the Primary Court of Manyoni at Chikola', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2762, 'Sanza Primary Court', 6, 68, 'In the Primary Court of Manyoni at Sanza', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2763, 'Nkonko Primary Court', 6, 68, 'In the Primary Court of Manyoni at Nkonko', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2764, 'Ikasi Primary Court', 6, 68, 'In the Primary Court of Manyoni at Ikasi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2765, 'Shelui Primary Court', 6, 68, 'In the Primary Court of Manyoni at Shelui', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2766, 'Ulemo Primary Court', 6, 69, 'In the Primary Court of Iramba at Ulemo', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2767, 'Kiomboi Primary Court', 6, 69, 'In the Primary Court of Iramba at Kiomboi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2768, 'Ndago Primary Court', 6, 69, 'In the Primary Court of Iramba at Ndago', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2769, 'Nduguti Primary Court', 6, 69, 'In the Primary Court of Iramba at Nduguti', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2770, 'Iguguno Primary Court', 6, 69, 'In the Primary Court of Iramba at Iguguno', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2771, 'Kinampanda Primary Court', 6, 69, 'In the Primary Court of Iramba at Kinampanda', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2772, 'Iambi Primary Court', 6, 69, 'In the Primary Court of Iramba at Iambi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2773, 'Kinyangiri Primary Court', 6, 69, 'In the Primary Court of Iramba at Kinyangiri', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2774, 'Mkalama Primary Court', 6, 69, 'In the Primary Court of Iramba at Mkalama', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2775, 'Kaselya Primary Court', 6, 69, 'In the Primary Court of Iramba at Kaselya', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2776, 'Ntwike Primary Court', 6, 69, 'In the Primary Court of Iramba at Ntwike', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2777, 'Urughu Primary Court', 6, 69, 'In the Primary Court of Iramba at Urughu', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2778, 'Kigaa Primary Court', 6, 69, 'In the Primary Court of Iramba at Kigaa', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2779, 'Gumanga Primary Court', 6, 69, 'In the Primary Court of Iramba at Gumanga', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2780, 'Kisiriri Primary Court', 6, 69, 'In the Primary Court of Iramba at Kisiriri', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2781, 'Kirumi Primary Court', 6, 69, 'In the Primary Court of Iramba at Kirumi', 'PRM', 'inactive', '1001', 1, 6, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2782, 'Iringa Urban Primary Court', 6, 71, 'In the Primary Court of Iringa at Iringa Urban', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '2021-08-18 13:56:52'),
(2783, 'Bomani Primary Court', 6, 71, 'In the Primary Court of Iringa at Bomani', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2784, 'Kalenga Primary Court', 6, 71, 'In the Primary Court of Iringa at Kalenga', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2785, 'Kiponzero Primary Court', 6, 71, 'In the Primary Court of Iringa at Kiponzero', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2786, 'Idodi Primary Court', 6, 71, 'In the Primary Court of Iringa at Idodi', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2787, 'Ismani Primary Court', 6, 71, 'In the Primary Court of Iringa at Ismani', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2788, 'Mtera Primary Court', 6, 71, 'In the Primary Court of Iringa at Mtera', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2789, 'Mlolo Primary Court', 6, 71, 'In the Primary Court of Iringa at Mlolo', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2790, 'Kimande Primary Court', 6, 71, 'In the Primary Court of Iringa at Kimande', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2791, 'Mahenge Primary Court', 6, 72, 'In the Primary Court of Kilolo at Mahenge', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2792, 'Mazombe Primary Court', 6, 72, 'In the Primary Court of Kilolo at Mazombe', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2793, 'Kilolo Primary Court', 6, 72, 'In the Primary Court of Kilolo at Kilolo', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2794, 'Mafinga Urban Primary Court', 6, 73, 'In the Primary Court of Mufindi at Mafinga urban', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2795, 'Ifwagi Primary Court', 6, 73, 'In the Primary Court of Mufindi at Ifwagi', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2796, 'Malangali Primary Court', 6, 73, 'In the Primary Court of Mufindi at Malangali', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2797, 'Mgololo Primary Court', 6, 73, 'In the Primary Court of Mufindi at Mgololo', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2798, 'Iramba Primary Court', 6, 73, 'In the Primary Court of Mufindi at Iramba', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2799, 'Mufindi Kibao Primary Court', 6, 73, 'In the Primary Court of Mufindi at Mufindi kibao', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2800, 'Igowole/Kasanga Primary Court', 6, 73, 'In the Primary Court of Mufindi at Igowole/Kasanga', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2801, 'Kibengu Primary Court', 6, 73, 'In the Primary Court of Mufindi at Kibengu', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2802, 'Mapanda Primary Court', 6, 73, 'In the Primary Court of Mufindi at Mapanda', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2803, 'Saadani Primary Court', 6, 73, 'In the Primary Court of Mufindi at Saadani', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2804, 'Ihanu Primary Court', 6, 73, 'In the Primary Court of Mufindi at Ihanu', 'PRM', 'inactive', '1001', 1, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2805, 'Njombe Urban Primary Court', 6, 75, 'In the Primary Court of Njombe at Njombe urban', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2806, 'Lupembe Primary Court', 6, 75, 'In the Primary Court of Njombe at Lupembe', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2807, 'Makambako Primary Court', 6, 75, 'In the Primary Court of Njombe at Makambako', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2808, 'Uwemba Primary Court', 6, 75, 'In the Primary Court of Njombe at Uwemba', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2809, 'Matola Primary Court', 6, 75, 'In the Primary Court of Njombe at Matola', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2810, 'Igominyi Primary Court', 6, 75, 'In the Primary Court of Njombe at Igominyi', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2811, 'Mtwango Primary Court', 6, 75, 'In the Primary Court of Njombe at Mtwango', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2812, 'WangingOmbe Primary Court', 6, 76, 'In the Primary Court of Wangingombe at Wangingombe', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2813, 'Kipengere Primary Court', 6, 76, 'In the Primary Court of Wangingombe at Kipengere', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2814, 'Mdandu Primary Court', 6, 76, 'In the Primary Court of Wangingombe at Mdandu', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2815, 'Imalinyi Primary Court', 6, 76, 'In the Primary Court of Wangingombe at Imalinyi', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2816, 'Mawengi Primary Court', 6, 77, 'In the Primary Court of Ludewa at Mawengi', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2817, 'Ludewa Urban Primary Court', 6, 77, 'In the Primary Court of Ludewa at Ludewa urban', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2818, 'Mlangali Primary Court', 6, 77, 'In the Primary Court of Ludewa at Mlangali', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2819, 'Manda Primary Court', 6, 77, 'In the Primary Court of Ludewa at  Manda', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2820, 'Mavanga Primary Court', 6, 77, 'In the Primary Court of Ludewa at Mavanga', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2821, 'Lugarawa Primary Court', 6, 77, 'In the Primary Court of Ludewa at Lugarawa', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2822, 'Lupalilo Primary Court', 6, 78, 'In the Primary Court of Makete at Lupalilo', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2823, 'Makete Urban Primary Court', 6, 78, 'In the Primary Court of Makete at Makete urban', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2824, 'Matamba Primary Court', 6, 78, 'In the Primary Court of Makete at Matamba', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2825, 'Bulongwa Primary Court', 6, 78, 'In the Primary Court of Makete at Bulongwa', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2826, 'Ukwama Primary Court', 6, 78, 'In the Primary Court of Makete at Ukwama', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2827, 'Lupila Primary Court', 6, 78, 'In the Primary Court of Makete at Lupila', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2828, 'Ipelele/Magoma Primary Court', 6, 78, 'In the Primary Court of Makete at Ipelele/Magoma', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2829, 'Ikuwo Primary Court', 6, 78, 'In the Primary Court of Makete at Ikuwo', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2830, 'Mfumbi Primary Court', 6, 78, 'In the Primary Court of Makete at Mfumbi', 'PRM', 'inactive', '1001', 1, 8, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2831, 'Mwanjelwa Primary Court', 6, 80, 'In the Primary Court of Mbeya at Mwanjelwa', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2832, 'Mbeya Urban Primary Court', 6, 80, 'In the Primary Court of Mbeya at Mbeya urban', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2833, 'Iyunga Primary Court', 6, 80, 'In the Primary Court of Mbeya at Iyunga', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2834, 'Uyole Primary Court', 6, 80, 'In the Primary Court of Mbeya at Uyole', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2835, 'Mbalizi Primary Court', 6, 80, 'In the Primary Court of Mbeya at Mbalizi', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2836, 'Igoma Primary Court', 6, 80, 'In the Primary Court of Mbeya at Igoma', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2837, 'Igale Primary Court', 6, 80, 'In the Primary Court of Mbeya at Igale', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2838, 'Ilembo Primary Court', 6, 80, 'In the Primary Court of Mbeya at Ilembo', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2839, 'Santilya Primary Court', 6, 80, 'In the Primary Court of Mbeya at Santilya', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2840, 'Masebe Primary Court', 6, 81, 'In the Primary Court of Rungwe at Masebe', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2841, 'Tukuyu Urban Primary Court', 6, 81, 'In the Primary Court of Rungwe at Tukuyu urban', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2842, 'Kiwira Primary Court', 6, 81, 'In the Primary Court of Rungwe at Kiwira', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2843, 'Isongole Primary Court', 6, 81, 'In the Primary Court of Rungwe at Isongole', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2844, 'Lupata Primary Court', 6, 81, 'In the Primary Court of Rungwe at Lupata', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2845, 'Ikama Primary Court', 6, 81, 'In the Primary Court of Rungwe at Ikama', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2846, 'Katumba Primary Court', 6, 81, 'In the Primary Court of Rungwe at Katumba', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2847, 'Kandete Primary Court', 6, 81, 'In the Primary Court of Rungwe at Kandete', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2848, 'Masoko Primary Court', 6, 81, 'In the Primary Court of Rungwe at Masoko', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2849, 'Kambasegela Primary Court', 6, 81, 'In the Primary Court of Rungwe at Kambasegela', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2850, 'Itagata Primary Court', 6, 81, 'In the Primary Court of Rungwe at Itagata', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2851, 'Masukulu Primary Court', 6, 81, 'In the Primary Court of Rungwe at Masukulu', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2852, 'Kibwe Primary Court', 6, 81, 'In the Primary Court of Rungwe at Kibwe', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2853, 'Kyela Urban Primary Court', 6, 82, 'In the Primary Court of Kyela at Kyela urban', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2854, 'Lusungo Primary Court', 6, 82, 'In the Primary Court of Kyela at Lusungo', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2855, 'Ndobo Primary Court', 6, 82, 'In the Primary Court of Kyela at Ndobo', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2856, 'Busale Primary Court', 6, 82, 'In the Primary Court of Kyela at Busale', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2857, 'Ikolo Primary Court', 6, 82, 'In the Primary Court of Kyela at Ikolo', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2858, 'Kajunjumele Primary Court', 6, 82, 'In the Primary Court of Kyela at Kajunjumele', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2859, 'Mwaya Primary Court', 6, 82, 'In the Primary Court of Kyela at Mwaya', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2860, 'Ngana Primary Court', 6, 82, 'In the Primary Court of Kyela at Ngana', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2861, 'Bujonde Primary Court', 6, 82, 'In the Primary Court of Kyela at Bujonde', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2862, 'Ipande Primary Court', 6, 82, 'In the Primary Court of Kyela at Ipande', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2863, 'Stamico/Lema Primary Court', 6, 82, 'In the Primary Court of Kyela at Stamico/lema', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2864, 'Rujewa Primary Court', 6, 83, 'In the Primary Court of Mbarali at Rujewa', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2865, 'Chimala Primary Court', 6, 83, 'In the Primary Court of Mbarali at Chimala', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2866, 'Igurusi Primary Court', 6, 83, 'In the Primary Court of Mbarali at Igurusi', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2867, 'Utengule Usangu Primary Court', 6, 83, 'In the Primary Court of Mbarali at Utengule usangu', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2868, 'Madibira Primary Court', 6, 83, 'In the Primary Court of Mbarali at Madibira', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2869, 'Ilongo Primary Court', 6, 83, 'In the Primary Court of Mbarali at Ilongo', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2870, 'Ruiwa Primary Court', 6, 83, 'In the Primary Court of Mbarali at Ruiwa', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2871, 'Mabadaga Primary Court', 6, 83, 'In the Primary Court of Mbarali at Mabandaga', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2872, 'Chunya Urban Primary Court', 6, 84, 'In the Primary Court of Chunya at Chunya urban', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2873, 'Makongolosi Primary Court', 6, 84, 'In the Primary Court of Chunya at Makongolosi', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2874, 'Lupatingatinga Primary Court', 6, 84, 'In the Primary Court of Chunya at Lipatingatinga', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2875, 'Sangambi Primary Court', 6, 84, 'In the Primary Court of Chunya at Sangambi', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2876, 'Itete Primary Court', 6, 84, 'In the Primary Court of Chunya at Itete', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2877, 'Ikumbukwa Primary Court', 6, 84, 'In the Primary Court of Chunya at Ikumbukwa', 'PRM', 'inactive', '1001', 1, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2878, 'Itumba Primary Court', 6, 89, 'In the Primary Court of Ileje at Itumba', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2879, 'Bupigu Primary Court', 6, 89, 'In the Primary Court of Ileje at Bupigu', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2880, 'Malangali Primary Court', 6, 89, 'In the Primary Court of Ileje at Malangali', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2881, 'Mtula Primary Court', 6, 89, 'In the Primary Court of Ileje at Mtula', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2882, 'Ikinga Primary Court', 6, 89, 'In the Primary Court of Ileje at Ikinga', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2883, 'Sange Primary Court', 6, 89, 'In the Primary Court of Ileje at Sange', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2884, 'Igamba Primary Court', 6, 88, 'In the Primary Court of Mbozi at Igamba', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2885, 'Vwawa Urban Primary Court', 6, 88, 'In the Primary Court of Mbozi at Viwawa urban', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2886, 'Iyula Primary Court', 6, 88, 'In the Primary Court of Mbozi at Iyula', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2887, 'Ruanda Primary Court', 6, 88, 'In the Primary Court of Mbozi at Ruanda', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2888, 'Itaka Primary Court', 6, 88, 'In the Primary Court of Mbozi at Itaka', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2889, 'Mlowo Primary Court', 6, 88, 'In the Primary Court of Mbozi at Mlowo', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2890, 'Msia Primary Court', 6, 88, 'In the Primary Court of Mbozi at Msia', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2891, 'Kamsamba Primary Court', 6, 87, 'In the Primary Court of Momba at Kamsamba', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2892, 'Tunduma Primary Court', 6, 87, 'In the Primary Court of Momba at Tunduma', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2893, 'Ndalambo Primary Court', 6, 87, 'In the Primary Court of Momba at Ndalambo', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2894, 'Mkulwe Primary Court', 6, 87, 'In the Primary Court of Momba at Mkulwe', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2895, 'Totowe Primary Court', 6, 86, 'In the Primary Court of Songwe at Totowe', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2896, 'Mwambani Primary Court', 6, 86, 'In the Primary Court of Songwe at Mwambani', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2897, 'Udinde Primary Court', 6, 86, 'In the Primary Court of Songwe at Udinde', 'PRM', 'inactive', '1001', 1, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2898, 'Mabokweni Primary Court', 6, 170, 'In the Primary Court of  Tanga at Mabokweni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2899, 'Mwakidila Primary Court', 6, 170, 'In the Primary Court of  Tanga at Mwakidila', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2900, 'Mwangombe Primary Court', 6, 170, 'In the Primary Court of  Tanga at Mwangombe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2901, 'Pongwe Primary Court', 6, 170, 'In the Primary Court of  Tanga at Pongwe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2902, 'Tanga Urban Primary Court', 6, 170, 'In the Primary Court of  Tanga at Tanga urban', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2903, 'Tongoni Primary Court', 6, 170, 'In the Primary Court of  Tanga at Tongoni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2904, 'Bumbuli Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Bumbili', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2905, 'Dochi Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Dochi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2906, 'Gare Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Gare', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2907, 'Lukozi Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Lukozi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2908, 'Mgwashi Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Mgwashi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2909, 'Mlalo Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Mlalo', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2910, 'Mlola Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Mola', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2911, 'Mnazi Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Mnazi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2912, 'Mtae Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Mtae', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2913, 'Soni Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Soni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2914, 'Vuga Primary Court', 6, 171, 'In the Primary Court of  Lushoto at Vuga', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2915, 'Chanika Primary Court', 6, 172, 'In the Primary Court of  Handeni at Chanika', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2916, 'Kabuku Primary Court', 6, 172, 'In the Primary Court of  Handeni at Kabuku', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2917, 'Kwamkono Primary Court', 6, 172, 'In the Primary Court of  Handeni at Kwamkono', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2918, 'Kwamsisi Primary Court', 6, 172, 'In the Primary Court of  Handeni at Kwamsasi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2919, 'Mzundu Primary Court', 6, 172, 'In the Primary Court of  Handeni at Mzundu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2920, 'Mazingara Primary Court', 6, 172, 'In the Primary Court of  Handeni at Mzingara', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2921, 'Mkumburu Primary Court', 6, 172, 'In the Primary Court of  Handeni at Mkumburu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `courts` (`id`, `name`, `court_level_id`, `location_id`, `display_name`, `code`, `status`, `sub_sp_code`, `judiciary`, `zonal`, `special`, `created_at`, `updated_at`) VALUES
(2922, 'Kimbe Primary Court', 6, 184, 'In the Primary Court of  Kilindi at Kimbe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2923, 'Kwediboma Primary Court', 6, 184, 'In the Primary Court of  Kilindi at Kwedibona', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2924, 'Kwekivu Primary Court', 6, 184, 'In the Primary Court of  Kilindi at Kwekivu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2925, 'Mswaki Primary Court', 6, 184, 'In the Primary Court of  Kilindi at Mswaki', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2926, 'Songe Primary Court', 6, 184, 'In the Primary Court of  Kilindi at Songe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2927, 'Bungu Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Bungu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2928, 'Hale Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Hale', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2929, 'Kalalani Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Kalalani', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2930, 'Magoma Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Magoma', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2931, 'Makuyuni Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Makuyuni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2932, 'Mashewa Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Mashewa', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2933, 'Mkomazi Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Komazi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2934, 'Mnyuzi Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Mnyuzi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2935, 'Mombo Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Mombo', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2936, 'Manundu Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Manundu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2937, 'Old Korogwe Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Old Korogwe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2938, 'Vugiri Primary Court', 6, 173, 'In the Primary Court of  Korogwe at Vugiri', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2939, 'Mbaramo Primary Court', 6, 174, 'In the Primary Court of  Muheza at Mbaramo', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2940, 'Kicheba Primary Court', 6, 174, 'In the Primary Court of  Muheza at Kicheba', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2941, 'Kigombe Primary Court', 6, 174, 'In the Primary Court of  Muheza at Kigombe', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2942, 'Kilulu Primary Court', 6, 174, 'In the Primary Court of  Muheza at Kilulu', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2943, 'Ngomeni Primary Court', 6, 174, 'In the Primary Court of  Muheza at Ngomeni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2944, 'Amani Primary Court', 6, 174, 'In the Primary Court of  Muheza at Amani', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2945, 'Chemka Primary Court', 6, 174, 'In the Primary Court of  Muheza at Chemka', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2946, 'Nkumba Primary Court', 6, 174, 'In the Primary Court of  Muheza at Mkumba', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2947, 'Mtindiro Primary Court', 6, 174, 'In the Primary Court of  Muheza at Mtindiro', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2948, 'Songa Primary Court', 6, 174, 'In the Primary Court of  Muheza at Songa', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2949, 'Bwembwera Primary Court', 6, 174, 'In the Primary Court of  Muheza at Mbembwera', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2950, 'Daluni Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Daluni', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2951, 'Duga Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Duga', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2952, 'Gombero Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Gombero', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2953, 'Maramba Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Maramba', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2954, 'Mjesani Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Mjesani', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2955, 'Mkinga Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Mkinga', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2956, 'Mtimbwani Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Mtibwani', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2957, 'Moa Primary Court', 6, 175, 'In the Primary Court of  Mkinga at Moa', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2958, 'Kipumbwi Primary Court', 6, 176, 'In the Primary Court of  Pangani at Kipumbwi', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2959, 'Madanga Primary Court', 6, 176, 'In the Primary Court of  Pangani at Madanga', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2960, 'Mkwaja Primary Court', 6, 176, 'In the Primary Court of  Pangani at Mkwaja', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2961, 'Pangani Urban Primary Court', 6, 176, 'In the Primary Court of  Pangani at Pangani Urban', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2962, 'Masaika Primary Court', 6, 176, 'In the Primary Court of  Pangani at Masaika', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2963, 'Mkalamo Primary Court', 6, 176, 'In the Primary Court of  Pangani at Mkalamo', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2964, 'Mwera Primary Court', 6, 176, 'In the Primary Court of  Pangani at Mwera', 'PRM', 'inactive', '1001', 1, 17, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2965, 'Tabora Urban Primary Court', 6, 155, 'In the Primary Court of  Tabora at Urban', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '2021-03-02 12:25:30'),
(2966, 'Isevya Primary Court', 6, 155, 'In the Primary Court of  Tabora at Isevya', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2967, 'Kalunde Primary Court', 6, 155, 'In the Primary Court of  Tabora at Kalunde', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2968, 'Ilolangulu Primary Court', 6, 155, 'In the Primary Court of  Tabora at Ilolangulu', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2969, 'Sikonge Primary Court', 6, 155, 'In the Primary Court of  Tabora at Sikonge', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2970, 'Kigwa Primary Court', 6, 155, 'In the Primary Court of  Tabora at Kigwa', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2971, 'Igalula Primary Court', 6, 155, 'In the Primary Court of  Tabora at Igalula', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2972, 'Imalakaseko Primary Court', 6, 155, 'In the Primary Court of  Tabora at Imalakaseko', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2973, 'Upuge Primary Court', 6, 155, 'In the Primary Court of  Tabora at Upuge', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2974, 'Goweko Primary Court', 6, 155, 'In the Primary Court of  Tabora at Goweko', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2977, 'Makomero Primary Court', 6, 156, 'In the Primary Court of  Igunga at Makomero', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2978, 'Igurubi Primary Court', 6, 156, 'In the Primary Court of  Igunga at Igurubi', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2979, 'Simbo Primary Court (Igunga)', 6, 156, 'In the Primary Court of  Igunga at Simbo', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '2021-03-12 06:43:50'),
(2980, 'Sungwizi Primary Court', 6, 156, 'In the Primary Court of  Igunga at Singwizi', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2981, 'Mwisi Primary Court', 6, 156, 'In the Primary Court of  Igunga at Mwisi', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2982, 'Choma Cha Nkola Primary Court', 6, 156, 'In the Primary Court of  Igunga at Choma cha Nkola', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2983, 'Ziba Primary Court', 6, 156, 'In the Primary Court of  Igunga at Ziba', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2985, 'Nzega Urban Primary Court', 6, 157, 'In the Primary Court of  Nzega at Urban', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '2021-03-02 12:22:34'),
(2986, 'Nyasa Primary Court', 6, 157, 'In the Primary Court of  Nzega at Nyasa', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2987, 'Utwigu Primary Court', 6, 157, 'In the Primary Court of  Nzega at Utwigu', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2988, 'Miguwa Primary Court', 6, 157, 'In the Primary Court of  Nzega at Miguwa', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2989, 'Bukene Primary Court', 6, 157, 'In the Primary Court of  Nzega at Bukene', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2990, 'Karitu Primary Court', 6, 157, 'In the Primary Court of  Nzega at Karitu', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2991, 'Mambali Primary Court', 6, 157, 'In the Primary Court of  Nzega at Mambali', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2992, 'Mwangoye Primary Court', 6, 157, 'In the Primary Court of  Nzega at Mwegoye', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2993, 'Itobo Primary Court', 6, 157, 'In the Primary Court of  Nzega at Itobo', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2994, 'Igusule Primary Court', 6, 157, 'In the Primary Court of  Nzega at Igusule', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2995, 'Ndala Primary Court', 6, 157, 'In the Primary Court of  Nzega at Ndala', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2996, 'Puge Primary Court', 6, 157, 'In the Primary Court of  Nzega at Puge', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2997, 'Urambo Urban Primary Court', 6, 161, 'In the Primary Court of  Urambo at Urban', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '2021-03-02 12:26:42'),
(2998, 'Ussoke Primary Court', 6, 161, 'In the Primary Court of  Urambo at Ussoke', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2999, 'Ichemba Primary Court', 6, 161, 'In the Primary Court of  Urambo at Ichemba', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3000, 'Ulyankulu Primary Court', 6, 161, 'In the Primary Court of  Urambo at Ulyankulu', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3001, 'Kashishi Primary Court', 6, 161, 'In the Primary Court of  Urambo at Kashishi', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3002, 'Uyowa Primary Court', 6, 161, 'In the Primary Court of  Urambo at Uyowa', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3003, 'Usinge Primary Court', 6, 161, 'In the Primary Court of  Urambo at Usinge', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3004, 'Kaliua Primary Court', 6, 161, 'In the Primary Court of  Urambo at Kaliua', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3005, 'Kazaroho Primary Court', 6, 161, 'In the Primary Court of  Urambo at Kazaroho', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3006, 'Ukumbisiganga Primary Court', 6, 161, 'In the Primary Court of  Urambo at Ukumbisiganga', 'PRM', 'inactive', '1001', 1, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3007, 'Kalinzi Primary Court', 6, 163, 'In the Primary Court of  Kigoma at Kalinzi', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3008, 'Mahembe Primary Court', 6, 163, 'In the Primary Court of  Kigoma at Mahembe', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3009, 'Mwandiga Primary Court', 6, 163, 'In the Primary Court of  Kigoma at Mwandiga', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3010, 'Simbo Primary Court (Kigoma)', 6, 163, 'In the Primary Court of  Kigoma at Simbo', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '2021-03-12 06:44:59'),
(3011, 'Ujiji Primary Court', 6, 163, 'In the Primary Court of  Kigoma at Ujiji', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3012, 'Kagunga Primary Court', 6, 163, 'In the Primary Court of  Kigoma at Kangunga', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3013, 'Kasulu Urban Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Urban', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3014, 'Makere Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Makere', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3015, 'Kasangezi Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Kasangezi', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3016, 'Nyakitonto Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Nyakitonto', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3017, 'Buhoro Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Buhoro', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3018, 'Heru Juu Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Heru juu', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3019, 'Muyama Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Muyama', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3020, 'Munyegera Primary Court', 6, 164, 'In the Primary Court of  Kasulu at Kasulu Munyegera', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3021, 'Itaba Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Itaba', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3022, 'Kibondo Urban Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Kibondo Urban', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3023, 'Kifura Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Kifura', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3024, 'Kumsenga Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Kumsenga', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3025, 'Mabamba Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Mabamba', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3026, 'Nyavyumbu Primary Court', 6, 165, 'In the Primary Court of  Kibondo at Nyavyumbu', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3027, 'Mwayaya Primary Court', 6, 166, 'In the Primary Court of  Buhigwe at Mwayaya', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3028, 'Manyovu Primary Court', 6, 166, 'In the Primary Court of  Buhigwe at Manyovu', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3029, 'Muyama Primary Court', 6, 166, 'In the Primary Court of  Buhigwe at Muyama', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3030, 'Buyenzi Primary Court', 6, 166, 'In the Primary Court of  Buhigwe at Buyenzi', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3031, 'Kakonko Primary Court', 6, 167, 'In the Primary Court of  Kakonko at Kakonko', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3032, 'Kasanda Primary Court', 6, 167, 'In the Primary Court of  Kakonko at Kasanda', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3033, 'Muhange Primary Court', 6, 167, 'In the Primary Court of  Kakonko at Muhenge', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3034, 'Mugunzu Primary Court', 6, 167, 'In the Primary Court of  Kakonko at Muzungu', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3035, 'Nyaronga Primary Court', 6, 167, 'In the Primary Court of  Kakonko at Nyaronga', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3036, 'Ilagala Primary Court', 6, 168, 'In the Primary Court of  Uvinza at Ilagala', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3037, 'Uvinza Primary Court', 6, 168, 'In the Primary Court of  Uvinza at Uvinza', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3038, 'Nguruka Primary Court', 6, 168, 'In the Primary Court of  Uvinza at Nzuruka', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3039, 'Kalya Primary Court', 6, 168, 'In the Primary Court of  Uvinza at Kalya', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3040, 'Mgambo Primary Court', 6, 168, 'In the Primary Court of  Uvinza at Mgambo', 'PRM', 'inactive', '1001', 1, 16, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3041, 'Sumbawanga Urban Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Sumbawanga urban', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3042, 'Mpui Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Mpui', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3043, 'Laela Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Laela', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3044, 'Kipeta Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Kipeta', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3045, 'Mtowisa Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Mtowisa', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3046, 'Muze Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Muze', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3047, 'Ilemba Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Ilemba', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3048, 'Kaengesa Primary Court', 6, 147, 'In the Primary Court of  Sumbawanga at Kaengesa', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3049, 'Namanyere Primary Court', 6, 148, 'In the Primary Court of  Nkasi at Namanyere', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3050, 'Chala Primary Court', 6, 148, 'In the Primary Court of  Nkasi at Chala', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3051, 'Kirando Primary Court', 6, 148, 'In the Primary Court of  Nkasi at Kirando', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3052, 'Kate Primary Court', 6, 148, 'In the Primary Court of  Nkasi at Kate', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3053, 'Kipande Primary Court', 6, 148, 'In the Primary Court of  Nkasi at Kipande', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3054, 'Matai Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Matai', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3055, 'Mwimbi Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Mwimbi', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3056, 'Kasanga Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Kasanga', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3057, 'Sopa Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Sopa', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3058, 'Mwazye Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Mwazye', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3059, 'Msanzi Primary Court', 6, 149, 'In the Primary Court of  Kalambo at Msanzi', 'PRM', 'inactive', '1001', 1, 15, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3060, 'Mpanda Urban Primary Court', 6, 151, 'In the Primary Court of  Mpanda at Mpanda Urban', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3061, 'Katumba Primary Court', 6, 151, 'In the Primary Court of  Mpanda at Katumba', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3062, 'Kabunga Primary Court', 6, 151, 'In the Primary Court of  Mpanda at Kabunga', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3063, 'Shanwe Primary Court', 6, 151, 'In the Primary Court of  Mpanda at Shanwe', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3064, 'Mishamo Primary Court', 6, 152, 'In the Primary Court of  Tanganyika at Mishamo', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3065, 'Mwese Primary Court', 6, 152, 'In the Primary Court of  Tanganyika at Mwese', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3066, 'Karema Primary Court', 6, 152, 'In the Primary Court of  Tanganyika at Karema', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3067, 'Inyonga Primary Court', 6, 153, 'In the Primary Court of  Mlele at Inyonga', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3068, 'Usevya Primary Court', 6, 153, 'In the Primary Court of  Mlele at Usevya', 'PRM', 'inactive', '1001', 1, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3069, 'Songea Urban Primary Court', 6, 141, 'In the Primary Court of  Songea at Songea Urban', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3070, 'Mfaranyaki Primary Court', 6, 141, 'In the Primary Court of  Songea at Mfaranyaki', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3071, 'Mpambano Primary Court', 6, 141, 'In the Primary Court of  Songea at Mpambano', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3072, 'Wabaki Primary Court', 6, 141, 'In the Primary Court of  Songea at Wabaki', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3073, 'Maposeni Primary Court', 6, 141, 'In the Primary Court of  Songea at Waposeni', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3074, 'Magagura Primary Court', 6, 141, 'In the Primary Court of  Songea at Magagura', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3075, 'Kilagano Primary Court', 6, 141, 'In the Primary Court of  Songea at Kalagano', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3076, 'Gumbiro Primary Court', 6, 141, 'In the Primary Court of  Songea at Gumbiro', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3077, 'Lumbingu/Mpitimbi Primary Court', 6, 141, 'In the Primary Court of  Songea at Lumbingu/Mpitimbi', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3078, 'Mahanje Primary Court', 6, 141, 'In the Primary Court of  Songea at Mahanje', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3079, 'Muhukuru Primary Court', 6, 141, 'In the Primary Court of  Songea at Muhukuru', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3080, 'Mbinga Urban Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Mbinga Urban', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3081, 'Kigonsera Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Kigonsera', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3082, 'Myangayanga Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Myangayanga', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3083, 'Ndengu Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Ndengu', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3084, 'Mpepai Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Mpepai', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3085, 'Langiro Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Langiro', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3086, 'Mkumbi Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Mkumbi', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3087, 'Litembo Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Litembo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3088, 'Matiri Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Matiri', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3089, 'Ruanda Primary Court', 6, 142, 'In the Primary Court of  Mbinga at Ruanda', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3090, 'Tingi Primary Court', 6, 143, 'In the Primary Court of  Nyasa at Tingi', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3091, 'Chiwanda Primary Court', 6, 143, 'In the Primary Court of  Nyasa at Chiwanda', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3092, 'Liuli Primary Court', 6, 143, 'In the Primary Court of  Nyasa at Liuli', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3093, 'Lituhi Primary Court', 6, 143, 'In the Primary Court of  Nyasa at Lituhi', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3094, 'Mbamba Bay Primary Court', 6, 143, 'In the Primary Court of  Nyasa at Mbambai', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3095, 'Mlingotini Urban Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Mlingotini Urban', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3096, 'Nandembo Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Nandembo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3097, 'Mbesa Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Mbesa', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3098, 'Matemanga Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Matemanga', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3099, 'Mtina Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Mtina', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3100, 'Nakapanya Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Nakapaya', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3101, 'Nalasi Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Nalasi', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3102, 'Namasakata Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Namasakata', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3103, 'Lukumbule Primary Court', 6, 144, 'In the Primary Court of  Tunduru at Lukumbule', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3104, 'Namtumbo Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Namtumbo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3105, 'Likuyusekamaganga Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Likuyusekamaganga', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3106, 'Mputa Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Mputa', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3107, 'Mbunga Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Buganga', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3108, 'Namabengo Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Namabengo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3109, 'Mkongo Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Mkongo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3110, 'Msindo Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Msindo', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3111, 'Luegu Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Luegu', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3112, 'Mawa Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Mawa', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3113, 'Ligera Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Ligera', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3114, 'Magazini Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Magazini', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3115, 'Sasawala Primary Court', 6, 145, 'In the Primary Court of  Namtumbo at Sasawala', 'PRM', 'inactive', '1001', 1, 14, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3116, 'Shinyanga Urban Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Shinyanga  Urban', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '2021-05-25 07:36:15'),
(3117, 'Kizumbi Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Kizumbi', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3118, 'Ibadakuli Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Ibadakuli', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3119, 'Chibe Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Chibe', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3120, 'Usanda Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Usanda', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3121, 'Tinde Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Tinde', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3122, 'Samuye Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Samuye', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3123, 'Nindo Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Nindo', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3124, 'Ilola Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Ilola', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3125, 'Salawe Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Salawe', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3126, 'Itwangi Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Itwangi', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3127, 'Mwantini Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Mwantini', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3128, 'Usule Primary Court', 6, 131, 'In the Primary Court of  Shinyanga at Usule', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3129, 'Kahama Urban Primary Court', 6, 132, 'In the Primary Court of  Kahama at Kahama Urban', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '2021-05-21 07:55:28'),
(3130, 'Kilago Primary Court', 6, 132, 'In the Primary Court of  Kahama at Kilango', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3131, 'Isagehe Primary Court', 6, 132, 'In the Primary Court of  Kahama at Isagehe', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3132, 'Zongomela Primary Court', 6, 132, 'In the Primary Court of  Kahama at Zongomela', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3133, 'Ngogwa Primary Court', 6, 132, 'In the Primary Court of  Kahama at Ngogwa', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3134, 'Lunguya Primary Court', 6, 132, 'In the Primary Court of  Kahama at Lunguya', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3135, 'Busangi Primary Court', 6, 132, 'In the Primary Court of  Kahama at Busangi', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3136, 'Isaka Primary Court', 6, 132, 'In the Primary Court of  Kahama at Isaka', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3137, 'Ngaya Primary Court', 6, 132, 'In the Primary Court of  Kahama at Ngaya', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3138, 'Isakajana Primary Court', 6, 132, 'In the Primary Court of  Kahama at Isakajana', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3139, 'Mpunze Primary Court', 6, 132, 'In the Primary Court of  Kahama at Mpunze', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3140, 'Ushetu Primary Court', 6, 132, 'In the Primary Court of  Kahama at Ushetu', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3141, 'Bulungwa Primary Court', 6, 132, 'In the Primary Court of  Kahama at Bulungwa', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3142, 'Ukune Primary Court', 6, 132, 'In the Primary Court of  Kahama at Ukune', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3143, 'Kishapu Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Kishapu', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3144, 'Negezi Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Negezi', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3145, 'Bubiki Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Bubiki', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3147, 'Mwadui Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Mwadui', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3148, 'Somagedi Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Somagedi', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3149, 'Mwamashele Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Mwamashele', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3150, 'Isungangolo Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Isungangolo', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3151, 'Kiloleli Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Kiloleli', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3152, 'Uchunga Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Uchunga', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3153, 'Itilima Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Itilima', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3154, 'Bugoro Primary Court', 6, 133, 'In the Primary Court of  Kishapu at Bugoro', 'PRM', 'inactive', '1001', 1, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3155, 'Somanda Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Somanda', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3156, 'Nyakabindi Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Nyakabindi', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3157, 'Dutwa Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Dutwa', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3158, 'Nkololo Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Nkololo', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3159, 'Bunamhala Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Bunamhala', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3160, 'Mhango Primary Court', 6, 135, 'In the Primary Court of  Bariadi at Mhango', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3161, 'Badi Primary Court', 6, 136, 'In the Primary Court of  Maswa at Badi', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3162, 'Buchambi Primary Court', 6, 136, 'In the Primary Court of  Maswa at Buchambi', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3163, 'Busilili Primary Court', 6, 136, 'In the Primary Court of  Maswa at Busilili', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3164, 'Isanga Primary Court', 6, 136, 'In the Primary Court of  Maswa at Isanga', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3165, 'Lalago Primary Court', 6, 136, 'In the Primary Court of  Maswa at Lalago', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3166, 'Malampaka Primary Court', 6, 136, 'In the Primary Court of  Maswa at Malampaka', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3167, 'Masela Primary Court', 6, 136, 'In the Primary Court of  Maswa at Masela', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3168, 'Mpindo Primary Court', 6, 136, 'In the Primary Court of  Maswa at Mpindo', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3169, 'Nyabubinza Primary Court', 6, 136, 'In the Primary Court of  Maswa at Nyabubinza', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3170, 'Nyalikungu Primary Court', 6, 136, 'In the Primary Court of  Maswa at Nyalikungu', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3171, 'Sukuma Primary Court', 6, 136, 'In the Primary Court of  Maswa at Sukuma', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3172, 'Mbaragane Primary Court', 6, 136, 'In the Primary Court of  Maswa at Mbaragane', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3173, 'Mwandoya Primary Court', 6, 137, 'In the Primary Court of  Meatu at Mwandoya', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3174, 'Kisesa Primary Court -Meatu', 6, 137, 'In the Primary Court of  Meatu at Kisesa', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '2021-06-02 07:05:02'),
(3175, 'Kimali Primary Court', 6, 137, 'In the Primary Court of  Meatu at Kimali', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3176, 'Bukundi Primary Court', 6, 137, 'In the Primary Court of  Meatu at Bukundi', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3177, 'Imalaseko Primary Court', 6, 137, 'In the Primary Court of  Meatu at Imalaseko', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3178, 'Itinje Primary Court', 6, 137, 'In the Primary Court of  Meatu at Itinje', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3179, 'Bumera Primary Court', 6, 138, 'In the Primary Court of  Itilima at Bumera', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3180, 'Chinamili Primary Court', 6, 138, 'In the Primary Court of  Itilima at Chinamili', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3181, 'Sagata Primary Court', 6, 138, 'In the Primary Court of  Itilima at Sagata', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3182, 'Luguru Primary Court', 6, 138, 'In the Primary Court of  Itilima at Luguru', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3183, 'Nkoma Primary Court', 6, 138, 'In the Primary Court of  Itilima at Nkoma', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3184, 'Zagayu Primary Court', 6, 138, 'In the Primary Court of  Itilima at Zagayu', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3185, 'Nyashimo Primary Court', 6, 139, 'In the Primary Court of  Busega at Nyashimo', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3186, 'Masanza Primary Court', 6, 139, 'In the Primary Court of  Busega at Nasanza', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3187, 'Kalemera Primary Court', 6, 139, 'In the Primary Court of  Busega at Kalemera', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3188, 'Mkula Primary Court', 6, 139, 'In the Primary Court of  Busega at Mkura', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3189, 'Igarukilo Primary Court', 6, 139, 'In the Primary Court of  Busega at Igarukilo', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3190, 'Nyaluhande Primary Court', 6, 139, 'In the Primary Court of  Busega at Nyalihande', 'PRM', 'inactive', '1001', 1, 13, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3191, 'Mtwara Urban Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Mtwara Urban', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3192, 'Mikindani Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Mikindani', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3193, 'Mpapura Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Mpapura', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3194, 'Msimbati Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Msimbati', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3195, 'Kitere Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Kitere', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3196, 'Dihimba Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Dihimba', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3197, 'Kitaya Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Kitaya', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3198, 'Nanyamba Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Nanyamba', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3199, 'Mahurunga Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Mahurunga', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3200, 'Mtawanya Primary Court', 6, 98, 'In the Primary Court of  Mtwara at Mtawanya', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3201, 'Chikundi Primary Court', 6, 99, 'In the Primary Court of  Masasi at Chikundi', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3202, 'Chiungutwa Primary Court', 6, 99, 'In the Primary Court of  Masasi at Chiungutwa', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3203, 'Lukuledi Primary Court', 6, 99, 'In the Primary Court of  Masasi at Likuledi', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3204, 'Lulindi Primary Court', 6, 99, 'In the Primary Court of  Masasi at Lulindi', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3205, 'Lisekese Primary Court', 6, 99, 'In the Primary Court of  Masasi at Lisekese', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3206, 'Mchauru Primary Court', 6, 99, 'In the Primary Court of  Masasi at Mchauru', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3207, 'Mangaka Primary Court', 6, 100, 'In the Primary Court of  Nanyumbu at Mangaka', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3208, 'Nanyumbu Primary Court', 6, 100, 'In the Primary Court of  Nanyumbu at Nanyumbu', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3209, 'Nakopi Primary Court', 6, 100, 'In the Primary Court of  Nanyumbu at Nakopi', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3210, 'Michiga Primary Court', 6, 100, 'In the Primary Court of  Nanyumbu at Michiga', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3211, 'Tandahimba Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Tandahimba', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3212, 'Namikupa Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Namikupa', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3213, 'Mihambwe Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Mihambe', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3214, 'Mahuta Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Mahuta', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3215, 'Mnyawa Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Mnyawa', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3216, 'Litehu Primary Court', 6, 101, 'In the Primary Court of  Tandahimba at Litehu', 'PRM', 'inactive', '1001', 1, 11, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3217, 'Chilangala Primary Court', 6, 102, 'In the Primary Court of  Newala at Chilangala', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `courts` (`id`, `name`, `court_level_id`, `location_id`, `display_name`, `code`, `status`, `sub_sp_code`, `judiciary`, `zonal`, `special`, `created_at`, `updated_at`) VALUES
(3218, 'Mnyambe Primary Court', 6, 102, 'In the Primary Court of  Newala at Mnyambe', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3219, 'Newala Urban Primary Court', 6, 102, 'In the Primary Court of  Newala at Newala Urban', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3220, 'Kitangali Primary Court', 6, 102, 'In the Primary Court of  Newala at Kitangali', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3221, 'Lindi Urban Primary Court', 6, 104, 'In the Primary Court of  Lindi at Lindi Urban', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3222, 'Mingoyo Primary Court', 6, 104, 'In the Primary Court of  Lindi at Mingoyo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3223, 'NgApa Primary Court', 6, 104, 'In the Primary Court of  Lindi at Ng?pa', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3224, 'Mchinga Primary Court', 6, 104, 'In the Primary Court of  Lindi at Mchinga', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3225, 'Mtama Primary Court', 6, 104, 'In the Primary Court of  Lindi at Mtama', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3226, 'Milola Primary Court', 6, 104, 'In the Primary Court of  Lindi at Milola', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3227, 'Kitomanga Primary Court', 6, 104, 'In the Primary Court of  Lindi at Kitomanga', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3228, 'Nyangamara Primary Court', 6, 104, 'In the Primary Court of  Lindi at Nyangamara', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3229, 'Mipingo Primary Court', 6, 104, 'In the Primary Court of  Lindi at Mipingo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3230, 'Nangaru Primary Court', 6, 104, 'In the Primary Court of  Lindi at Nangaru', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3231, 'Madangwa Primary Court', 6, 104, 'In the Primary Court of  Lindi at Madangwa', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3232, 'Rondo Primary Court', 6, 104, 'In the Primary Court of  Lindi at Rondo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3233, 'Rutamba Primary Court', 6, 104, 'In the Primary Court of  Lindi at Rutamba', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3234, 'Kilwa Masoko Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Kilwa masoko', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3235, 'Kilwa Kivinje Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Kilwa Kivinje', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3236, 'Kipatimu Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Kipatimu', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3237, 'Miteja Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Miteja', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3238, 'Nanjirinji Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Nanjirinji', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3239, 'Pande Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Pande', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3240, 'Kinjumbi Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Kunjubi', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3241, 'Mtandi Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Mtandi', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3242, 'Chumo Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Chumo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3243, 'Njinjo Primary Court', 6, 105, 'In the Primary Court of  Kilwa Masoko at Njinjo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3244, 'Nachinwea Urban Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Nachingwea Urban', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3245, 'Ruponda Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Ruponda', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3246, 'Lionja Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Lionja', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3247, 'Mbondo Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Mbondo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3248, 'Kilimarondo Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Kilimarondo', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3249, 'Mtua Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Mtua', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3250, 'Mnero Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Mnero', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3251, 'Ndomoni Primary Court', 6, 106, 'In the Primary Court of  Nachingwea at Ndomoni', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3252, 'Liwale Urban Primary Court', 6, 107, 'In the Primary Court of  Liwale at Liwale Urban', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3253, 'Kibutuka Primary Court', 6, 107, 'In the Primary Court of  Liwale at  Kibukuta', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3254, 'Ruangwa Urban Primary Court', 6, 108, 'In the Primary Court of  Ruangwa at Ruangwa Urban', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3255, 'Mnacho Primary Court', 6, 108, 'In the Primary Court of  Ruangwa at Mnacho', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3256, 'Mandawa Primary Court', 6, 108, 'In the Primary Court of  Ruangwa at Mandawa', 'PRM', 'inactive', '1001', 1, 11, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3257, 'Moshi - Urban Primary Court', 6, 91, 'In the Primary Court of  Moshi at Moshi Urban', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3258, 'Himo Primary Court', 6, 91, 'In the Primary Court of  Moshi at Himo', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3259, 'Uru Primary Court', 6, 91, 'In the Primary Court of  Moshi at Uru', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3260, 'Uchira Primary Court', 6, 91, 'In the Primary Court of  Moshi at Uchira', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3261, 'Mwika Primary Court', 6, 91, 'In the Primary Court of  Moshi at Mwika', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3262, 'Mamba Primary Court', 6, 91, 'In the Primary Court of  Moshi at Mamba', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3263, 'Kirua Vunjo Primary Court', 6, 91, 'In the Primary Court of  Moshi at Kirua Vunjo', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3264, 'Marangu Primary Court', 6, 91, 'In the Primary Court of  Moshi at Marangu', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3265, 'Kilema Primary Court', 6, 91, 'In the Primary Court of  Moshi at Kilema', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3266, 'Kibosho Primary Court', 6, 91, 'In the Primary Court of  Moshi at Kibosho', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3267, 'Kindi Primary Court', 6, 91, 'In the Primary Court of  Moshi at Kindi', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3268, 'Tpc - Arusha Chini Primary Court', 6, 91, 'In the Primary Court of  Moshi at TPC-Arusha Chini', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3269, 'Mabogini Primary Court', 6, 91, 'In the Primary Court of  Moshi at Mabogini', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3270, 'Kahe Primary Court', 6, 91, 'In the Primary Court of  Moshi at Kahe', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3271, 'Old - Moshi Primary Court', 6, 91, 'In the Primary Court of  Moshi at Old-Moshi', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3272, 'Same - Urban Primary Court', 6, 92, 'In the Primary Court of  Same at Same Urban', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3273, 'Kisiwani Primary Court', 6, 92, 'In the Primary Court of  Same at Kisiwani', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3274, 'Ndungu Primary Court', 6, 92, 'In the Primary Court of  Same at Ndungu', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3275, 'Gonja Primary Court', 6, 92, 'In the Primary Court of  Same at Ngonja', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3276, 'Hedaru Primary Court', 6, 92, 'In the Primary Court of  Same at Hedaru', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3277, 'Makanya Primary Court', 6, 92, 'In the Primary Court of  Same at Makanya', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3278, 'Kihurio Primary Court', 6, 92, 'In the Primary Court of  Same at Kihurio', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3279, 'Mamba- Miyamba Primary Court', 6, 92, 'In the Primary Court of  Same at Mamba-Miyamba', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3280, 'Mwanga- Urban Primary Court', 6, 93, 'In the Primary Court of  Mwanga at Mwanga Urban', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3281, 'Usangi Primary Court', 6, 93, 'In the Primary Court of  Mwanga at Usangi', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3282, 'Ugweno Primary Court', 6, 93, 'In the Primary Court of  Mwanga at Ugweno', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3283, 'Lembeni Primary Court', 6, 93, 'In the Primary Court of  Mwanga at Lembeni', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3284, 'Kagongo Primary Court', 6, 93, 'In the Primary Court of  Mwanga at Kagongo', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3285, 'Mkuu - Urban Primary Court', 6, 94, 'In the Primary Court of  Rombo at Mkuu Urban', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3286, 'Mashati Primary Court', 6, 94, 'In the Primary Court of  Rombo at Mashati', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3287, 'Usseri Primary Court', 6, 94, 'In the Primary Court of  Rombo at Usseri', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3288, 'Mengwe Primary Court', 6, 94, 'In the Primary Court of  Rombo at Mengwe', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3289, 'BomangOmbe Primary Court', 6, 95, 'In the Primary Court of  Hai at Mengwe Bomang?mbe', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3290, 'Masama Primary Court', 6, 95, 'In the Primary Court of  Hai at Masana', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3291, 'Hai Kati Primary Court', 6, 95, 'In the Primary Court of  Hai at Hai Kati', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3292, 'Sanya Juu Primary Court', 6, 96, 'In the Primary Court of  Siha at Sanya juu', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3293, 'Siha Primary Court', 6, 96, 'In the Primary Court of  Siha at Siha', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3294, 'Ngarenairobi Primary Court', 6, 96, 'In the Primary Court of  Siha at Ngarenairobi', 'PRM', 'inactive', '1001', 1, 10, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3295, 'Nyamagana Urban Primary Court', 6, 110, 'In the Primary Court of  Nyamagana at Nyamagana Urban', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '2021-08-18 13:54:47'),
(3296, 'Mkuyuni Primary Court', 6, 110, 'In the Primary Court of  Nyamagana at Mkuyuni', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3297, 'Ilemela Primary Court', 6, 111, 'In the Primary Court of  Ilemela at Ilemela', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3298, 'Sangabuye Primary Court', 6, 111, 'In the Primary Court of  Ilemela at Sangabuye', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3299, 'Magu Urban Primary Court', 6, 112, 'In the Primary Court of  Magu at Magu Urban', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3300, 'Ndagalu Primary Court', 6, 112, 'In the Primary Court of  Magu at Ndagalu', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3301, 'Kongolo Primary Court', 6, 112, 'In the Primary Court of  Magu at Kongolo', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3302, 'Bujashi Primary Court', 6, 112, 'In the Primary Court of  Magu at Bujashi', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3303, 'Kisesa Primary Court', 6, 112, 'In the Primary Court of  Magu at Kisesa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3304, 'Nyanguge Primary Court', 6, 112, 'In the Primary Court of  Magu at Nyanguge', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3305, 'Kahangara Primary Court', 6, 112, 'In the Primary Court of  Magu at Kahangara', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3306, 'Kabila Primary Court', 6, 112, 'In the Primary Court of  Magu at Kabila', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3307, 'Ngudu Urban Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Ndugu Urban', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3308, 'Nyambiti Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Nyambiti', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3309, 'Bungulwa Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Bungulwa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3310, 'Nyamikoma Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Nyamikoma', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3311, 'Malya Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Malya', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3312, 'Ngulla Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Ngulla', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3313, 'Nyamilama Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Nyamilama', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3314, 'Buyogo Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Buyogo', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3315, 'Mwamashimba Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Kwamashimba', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3316, 'Kikubiji Primary Court', 6, 113, 'In the Primary Court of  Kwimba at Kikubiji', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3317, 'Misungwi Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Misungwi', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3319, 'Igokelo Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Igokelo', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3320, 'Koromije Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Kolomije', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3321, 'Bukumbi Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Bukumbi', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3322, 'Bujingwa Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Bujingwa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3323, 'Inonelwa Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Inonelwa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3324, 'Busongo Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Busongo', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3325, 'Mbarika Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Barika', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3326, 'Ilujamate Primary Court', 6, 114, 'In the Primary Court of  Misungwi at Ilujamate', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3327, 'Sengerema Urban Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Sengerema Urban', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3328, 'Busisi Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Busisi', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3329, 'Sima Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Sima', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3330, 'Buzilasoga Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Buzilasoga', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3331, 'Nyakaliro Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Nyakaliro', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3332, 'Kazunzu Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Kazunzu', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3333, 'Kome Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Kome', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3334, 'Kasenyi Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Kasenyi', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3335, 'Kasungamile Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Kasungamile', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3336, 'Bupandwa Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Bupandwa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3337, 'Nyehunge Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Nyehunge', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3338, 'Buyagu Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Buyangu', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3339, 'Nyanchenche Primary Court', 6, 115, 'In the Primary Court of  Sengerema at Nyanchenche', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3340, 'Nansio Primary Court', 6, 116, 'In the Primary Court of  Ukerewe at Nansio', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3341, 'Bukindo Primary Court', 6, 116, 'In the Primary Court of  Ukerewe at Bukindo', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3342, 'Ilangala Primary Court', 6, 116, 'In the Primary Court of  Ukerewe at Ilangala', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3343, 'Ukara Primary Court', 6, 116, 'In the Primary Court of  Ukerewe at Ukara', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3344, 'Ilugwa Primary Court', 6, 116, 'In the Primary Court of  Ukerewe at Ilugwa', 'PRM', 'inactive', '1001', 1, 12, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3345, 'Musoma Urban Primary Court', 6, 118, 'In the Primary Court of  Musoma at Musoma Urban', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3346, 'Bwasi Primary Court', 6, 118, 'In the Primary Court of  Musoma at Bwasi', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3347, 'Mugango Primary Court', 6, 118, 'In the Primary Court of  Musoma at Mugango', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3348, 'Bukwaya Primary Court', 6, 118, 'In the Primary Court of  Musoma at Bukwaya', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3349, 'Nyambono Primary Court', 6, 118, 'In the Primary Court of  Musoma at Nyambono', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3350, 'Kukirango Primary Court', 6, 119, 'In the Primary Court of  Butiama at Kukirango', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3351, 'Kiagata Primary Court', 6, 119, 'In the Primary Court of  Butiama at Kiagata', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3352, 'Zanaki Primary Court', 6, 119, 'In the Primary Court of  Butiama at Zanaki', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3353, 'Bunda Urban Primary Court', 6, 120, 'In the Primary Court of  Bunda at Bunda Urban', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3354, 'Ikizu Primary Court', 6, 120, 'In the Primary Court of  Bunda at Ikizu', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3355, 'Kabasa Primary Court', 6, 120, 'In the Primary Court of  Bunda at Kabasa', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3356, 'Kenkombyo Primary Court', 6, 120, 'In the Primary Court of  Bunda at Kenkombyo', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3357, 'Nansimo Primary Court', 6, 120, 'In the Primary Court of  Bunda at Nansimo', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3358, 'Mcharo Primary Court', 6, 120, 'In the Primary Court of  Bunda at Mcharo', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3359, 'Mugeta Primary Court', 6, 120, 'In the Primary Court of  Bunda at Mugeta', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3360, 'Shirati Primary Court', 6, 121, 'In the Primary Court of  Rorya at Shirati', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3361, 'Riagoro Primary Court', 6, 121, 'In the Primary Court of  Rorya at Raigoro', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3362, 'Nyaburongo Primary Court', 6, 121, 'In the Primary Court of  Rorya at Nyaburongo', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3363, 'Kinesi Primary Court', 6, 121, 'In the Primary Court of  Rorya at Kinesi', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3364, 'Tarime Urban Primary Court', 6, 122, 'In the Primary Court of  Tarime at Tarime Urban', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3365, 'Sirari Primary Court', 6, 122, 'In the Primary Court of  Tarime at Sirari', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3366, 'Nyamwigura Primary Court', 6, 122, 'In the Primary Court of  Tarime at Nyamwigura', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3367, 'Nyamwaga Primary Court', 6, 122, 'In the Primary Court of  Tarime at Nyamwaga', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3368, 'Nyamongo Primary Court', 6, 122, 'In the Primary Court of  Tarime at Nyamongo', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3369, 'Mtana Primary Court', 6, 122, 'In the Primary Court of  Tarime at Mtana', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3370, 'Mugumu Urban Primary Court', 6, 123, 'In the Primary Court of  Serengeti at Mugumu Urban', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3371, 'Kenyana Primary Court', 6, 123, 'In the Primary Court of  Serengeti at Kenyana', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3372, 'Natta Primary Court', 6, 123, 'In the Primary Court of  Serengeti at Nata', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3373, 'Ngoreme Primary Court', 6, 123, 'In the Primary Court of  Serengeti at Ngoreme', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3374, 'Robanda Primary Court', 6, 123, 'In the Primary Court of  Serengeti at Robanda', 'PRM', 'inactive', '1001', 1, 12, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3375, 'Nyankumbu Primary Court', 6, 125, 'In the Primary Court of  Geita at Nyankumbu', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3376, 'Bulela Primary Court', 6, 125, 'In the Primary Court of  Geita at Bulela', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3377, 'Bugando Primary Court', 6, 125, 'In the Primary Court of  Geita at Bugando', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3378, 'Bukoli Primary Court', 6, 125, 'In the Primary Court of  Geita at Bukoli', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3379, 'Butundwe Primary Court', 6, 125, 'In the Primary Court of  Geita at Butundwe', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3380, 'Busanda Primary Court', 6, 125, 'In the Primary Court of  Geita at Busanda', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3381, 'Katoro Primary Court', 6, 125, 'In the Primary Court of  Geita at Katoro', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3382, 'Ihanamilo Primary Court', 6, 125, 'In the Primary Court of  Geita at Ihanamilo', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3383, 'Mwingiro Primary Court', 6, 126, 'In the Primary Court of  Nyangwale at Mwingiro', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3384, 'NyangwAle Primary Court', 6, 126, 'In the Primary Court of  Nyangwale at Nyangwale', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3385, 'Kharumwa Primary Court', 6, 126, 'In the Primary Court of  Nyangwale at Kharumwa', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3386, 'Bukwimba Primary Court', 6, 126, 'In the Primary Court of  Nyangwale at Bukwimba', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3387, 'Chato Urban Primary Court', 6, 127, 'In the Primary Court of  Chato at Chato Urban', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3388, 'Buseresere Primary Court', 6, 127, 'In the Primary Court of  Chato at Buseresere', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3389, 'Bwanga Primary Court', 6, 127, 'In the Primary Court of  Chato at Bwanga', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3390, 'Muganza Primary Court', 6, 127, 'In the Primary Court of  Chato at Muganza', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3391, 'Buzirayombo Primary Court', 6, 127, 'In the Primary Court of  Chato at Buzirayombo', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3392, 'Nyamirembe Primary Court', 6, 127, 'In the Primary Court of  Chato at Nyamirembe', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3393, 'Ichwankima Primary Court', 6, 127, 'In the Primary Court of  Chato at Ichwankima', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3394, 'Ushirombo Primary Court', 6, 128, 'In the Primary Court of  Bukombe at Ushirombo', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3395, 'Runzewe Primary Court', 6, 128, 'In the Primary Court of  Bukombe at Runzewe', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3396, 'Masumbwe Primary Court', 6, 129, 'In the Primary Court of  Mbogwe at Masumbwe', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3397, 'Mbogwe Primary Court', 6, 129, 'In the Primary Court of  Mbogwe at Mbogwe', 'PRM', 'inactive', '1001', 1, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3398, 'Mwanza Mjini Primary Court', 6, 110, 'In the Primary Court of Mwanza Mjiniat Mwanza', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-02-22 05:43:46', '2021-02-22 05:43:46'),
(3399, 'Kolo Primary Court', 6, 62, 'In the Primary Court of  Kolo at Kolo', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-02-22 05:46:37', '2021-02-22 05:46:37'),
(3400, 'Igunga Urban Primary Court', 6, 156, 'In the Primary Court Of Igunga at Igunga', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-03-02 12:14:07', '2021-03-02 12:14:07'),
(3401, 'Kimara Primary Court', 6, 38, 'In the Primary Court Of Kimara at Kimara', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-03-03 08:57:55', '2021-03-03 08:57:55'),
(3402, 'Nyamatongo Primary Court', 6, 116, 'In the Primary Court Of Nyamatongo at Nyamatongo', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-03-08 06:02:24', '2021-03-08 10:15:25'),
(3403, 'Tarakea Primary Court', 6, 94, 'In the Primary Court of  Tarakea at Tarakea', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-05-27 08:49:19', '2021-05-27 08:49:19'),
(3404, 'Morogoro High Court', 3, 50, 'In the High Court of  Morogoro at Morogoro', 'HCT', 'inactive', '2003', NULL, NULL, NULL, '2021-07-02 08:53:11', '2021-07-02 08:53:11'),
(3405, 'Temeke High Court', 3, 4, 'In the High Court of  Tanzania at Temeke', 'HCT', 'inactive', '2003', NULL, NULL, NULL, '2021-07-02 08:55:26', '2021-07-02 08:55:26'),
(3406, 'Temeke Primary Court', 6, 39, 'In the Primary Court of Temeke at Temeke', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-08-18 09:29:27', '2021-08-18 09:29:27'),
(3407, 'Majimoto Primary Court', 6, 153, 'In the Primary Court of Mlele at Majimoto', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-08-18 11:55:04', '2021-08-18 11:55:04'),
(3408, 'Wampembe Primary Court', 6, 148, 'In the Primary Court of Nkasi at Wampembe', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-08-18 13:43:59', '2021-08-18 13:43:59'),
(3409, 'Nondwa Primary Court', 6, 58, 'In the Primary Court of Bahi at Nondwa', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-08-18 14:02:58', '2021-08-18 14:02:58'),
(3410, 'Busi Primary Court', 6, 62, 'In the Primary Court of Kondoa at Busi', 'PRM', 'inactive', '1001', NULL, NULL, NULL, '2021-08-18 14:05:39', '2021-08-18 14:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `question` text NOT NULL,
  `answer` longtext NOT NULL,
  `user` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `question`, `answer`, `user`, `created_at`, `updated_at`) VALUES
(1, 'How', 'Get Legal Aid Services?', 'By Visiting MocLA Offices or legal aid offices at your region', 'Super Admin', '2020-09-02 07:23:52', '2020-09-02 07:23:52'),
(2, 'How', 'Apply extension of time', 'Hklkasdklmkldlkajdmakldasd', 'Super Admin', '2020-09-02 07:40:06', '2020-09-02 07:40:06'),
(3, 'What', 'Legal Aid Services?', 'Is a service provided by Legal aid Services', 'Super Admin', '2020-09-02 08:02:29', '2020-09-02 08:02:29'),
(4, 'What', 'Legal Aid Week?', 'Is a week of ......', 'Super Admin', '2020-09-02 08:05:37', '2020-09-02 08:05:37'),
(5, 'How', 'Sample Question', 'Sample Answer', 'Super Admin', '2020-10-05 02:10:06', '2020-10-05 02:10:06');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(100) NOT NULL,
  `news_id` int(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `count` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `managements`
--

CREATE TABLE `managements` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `profile` text NOT NULL,
  `seniority` varchar(10) NOT NULL,
  `reporter` varchar(100) NOT NULL,
  `publisher` varchar(100) DEFAULT NULL,
  `date_published` date DEFAULT NULL,
  `publish` varchar(10) NOT NULL,
  `file` varchar(250) DEFAULT NULL,
  `date_posted` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `managements`
--

INSERT INTO `managements` (`id`, `name`, `title`, `profile`, `seniority`, `reporter`, `publisher`, `date_published`, `publish`, `file`, `date_posted`, `created_at`, `updated_at`) VALUES
(4, 'Profesa Sifuni E. Mchome', 'Permanent Secretary', 'Prof. Sifuni Ernest Mchome, Katibu Mkuu Wizara ya Mambo ya Katiba na Sheria ana Shahada ya Uzamivu ya Sheria ya Chuo Kikuu cha Dar es salaam, Shahada ya Uzamili ya Sheria ya Chuo Kikuu cha Queens cha nchini Canada na Shahada ya Kwanza ya Sheria kutoka Chuo Kikuu cha Dar es salaam.\r\n\r\nKabla ya kuteuliwa kushika nafasi hii, Prof. Mchome alikuwa Katibu Mkuu Wizara ya Elimu na Mafunzo ya Ufundi kati ya 2013 -2015 na mwaka 2010 hadi 2013 alikuwa Naibu Katibu Mtendaji na baadae akawa Katibu Mtendaji katika Tume ya Vyuo Vikuu nchini (TCU).', '2', 'Super Admin', 'Super Admin', '2021-08-30', '1', 'secretary_1630327573.jpg', '2021-08-30 12:46:13', '2021-08-30 12:46:13', '2021-08-30 12:52:10'),
(5, 'Prof. Palamagamba Kabudi', 'The Minister', 'Utakujia punde', '1', 'Super Admin', 'Super Admin', '2021-08-30', '1', 'bio_1627910885-Prof Kabudi2_1630327710.jpg', '2021-08-30 12:48:30', '2021-08-30 12:48:30', '2021-08-30 12:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_28_111120_create_pages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(2, 6, 'App\\User'),
(3, 3, 'App\\User'),
(3, 7, 'App\\User'),
(4, 4, 'App\\User'),
(5, 8, 'App\\User'),
(6, 5, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(100) NOT NULL,
  `title` text NOT NULL,
  `content` longtext NOT NULL,
  `status` varchar(100) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `video_id` varchar(100) DEFAULT NULL,
  `publish` varchar(10) DEFAULT NULL,
  `reporter` varchar(100) NOT NULL,
  `date_posted` date NOT NULL,
  `editor` varchar(100) DEFAULT NULL,
  `date_edited` date DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `publisher` varchar(100) DEFAULT NULL,
  `date_published` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `status`, `category`, `video_id`, `publish`, `reporter`, `date_posted`, `editor`, `date_edited`, `file_name`, `publisher`, `date_published`, `created_at`, `updated_at`) VALUES
(7, 'MAFUNZO KWA MAAFISA WA JESHI LA POLISI', 'Mafunzo ya siku mbili kwa maafisa wa Jeshi la Polisi kutoka katika mikoa ya Iringa na Mbeya kuhusu utoaji wa huduma ya msaada wa kisheria kwa watoto katika ukumbi wa Mahakama ya Tanzania Kanda ya Mbeya- jijini Mbeya', 'General', 'News', NULL, '1', 'Super Admin', '2020-08-10', NULL, NULL, 'IMG-20200722-WA0026_1597066398.jpg', 'Super Admin', '2020-08-10', '2020-08-10 10:33:17', '2020-08-10 10:33:17'),
(8, 'SERIKALI KUFUTA ADA YA USAJILI KWA WATOA HUDUMA YA MSAADA WA KISHERIA', 'Naibu Katibu Mkuu wa Wizara ya Sheria na Katiba Amon Mpanju (kushoto) akibadilishana mawazo na Kaimu Katibu Tawala Mkoa wa Tabora (Utumishi na Utawala) Hamis Mkunga (kulia)  wakati wa  hafla fupi ya uzinduzi Kamati ya Uratibu wa Huduma ya Msaada wa Kisheria kwa ngazi ya Mkoa wa Tabora jana', 'General', 'News', NULL, '1', 'Super Admin', '2020-08-10', NULL, NULL, 'IMG_7587_1597066585.JPG', 'Super Admin', '2020-08-10', '2020-08-10 10:36:25', '2020-08-10 10:36:25'),
(9, 'ZIARA YA KAIM KATIBU WIZARA YA KATIBA NA SHERIA AFANYA ZIARA MKOA TABORA', 'Kaimu Katibu Tawala Mkoa wa Tabora (Utumishi na Utawala) Hamis Mkunga akitoa maelezo mafupi kuhusu migogoro mbalimbali inayowakabili wananchi wa wakati wa  hafla fupi ya uzinduzi Kamati ya Uratibu wa Huduma ya Msaada wa Kisheria kwa ngazi ya Mkoa wa Tabora jana', 'Important', 'News', NULL, '1', 'Super Admin', '2020-08-10', NULL, NULL, 'IMG_7508_1597066751.JPG', 'Super Admin', '2020-08-10', '2020-08-10 10:39:11', '2020-08-10 10:39:11'),
(11, 'Wakuu wa Mikoa kuapishwa leo', 'Rais wa Jamhuri ya Muungano wa Tanzania', 'video', 'Media', 'THZvlO3j-DE', '1', 'Super Admin', '2021-05-20', NULL, NULL, NULL, 'Super Admin', '2021-09-13', '2021-05-20 10:06:53', '2021-05-20 10:06:53'),
(12, 'ToR Mapping Capacity needs Judicial 19.2.2018', 'ToR Mapping Capacity needs Judicial', 'Tender', 'Adverts', NULL, '1', 'Super Admin', '2021-05-21', NULL, NULL, 'en1519104026-ToR Mapping Capacity needs Judicial 19.2.2018_1621587258.doc', NULL, NULL, '2021-05-21 05:54:18', '2021-05-21 05:54:18'),
(13, 'MTOTO AKABIDHIWA CHETI CHA KUZALIWA', 'Naibu Waziri wa Katiba na Sheria, Mhe Geophrey Mizengo Pinda akikabidhi cheti cha kuzaliwa cha mtoto wakati wa uzinduzi wa usajili wa watoto wadogo chini ya Miaka 5 katika Mikoa ya Arusha na Manyara, uliofanyika katika Hotel ya Mount Meru, mkoani Arusha leo hii tarehe 11.05.2021', 'Important', 'News', NULL, '1', 'Super Admin', '2021-05-26', NULL, NULL, '1620753110-WhatsApp Image 2021-05-11 at 19.01.47_1622023323.jpeg', 'Super Admin', '2021-05-26', '2021-05-26 07:02:03', '2021-05-26 07:02:03'),
(14, 'TANGAZO LA KUITWA KWENYE USAILI TUME YA UTUMISHI WA MAHAKAMA', 'TANGAZO LA KUITWA KWENYE USAILI TUME YA UTUMISHI WA MAHAKAMA', 'Anouncement', 'Docs', NULL, '1', 'Super Admin', '2021-05-28', NULL, NULL, 'sw1620828120-TANGAZO LA KUITWA KWENYE USAILI - MEI 2021_1622197650.pdf', 'Super Admin', '2021-09-18', '2021-05-28 07:27:30', '2021-05-28 07:27:30'),
(15, 'Public Notice on Registration of Legal Aid Providers', 'Public Notice on Registration of Legal Aid Providers', 'Anouncement', 'Docs', NULL, '1', 'Super Admin', '2021-05-28', NULL, NULL, 'case_proceedings_1622198815.pdf', 'Super Admin', '2021-05-28', '2021-05-28 07:46:55', '2021-05-28 07:46:55'),
(16, 'kikao kazi cha kutathmini zoezi la kutafsiri sheria mbalimbali katika lugha ya Kiswahili', 'Waziri wa Katiba na Sheria Profesa Palamagamba Kabudi akiwa katika picha ya pamoja na Wakurugenzi wa Idara na Vitengo vya Sheria vya Wizara zote za Serikali walipohudhuria kikao kazi cha kutathmini zoezi la kutafsiri sheria mbalimbali katika lugha ya Kiswahili mapema leo tarehe 27 Mei, 2021 mjini Dodoma.', 'Important', 'News', NULL, '1', 'Super Admin', '2021-05-28', NULL, NULL, '1622113899-2_1622199475.jpg', 'Super Admin', '2021-05-28', '2021-05-28 07:57:55', '2021-05-28 07:57:55'),
(17, 'MADAWATI YA MSAADA WA KISHERIA KUANZISHWA MAHAKAMA ZA MWANZO HADI MAHAKAMA KUU', 'Serikali kupitia Wizara ya katiba na sheria imesema upo mpango wa kuwa na madawati ya msaada wa kisheria kuanzia Mahakama za Mwanzo hadi Mahakama Kuu.\r\nHayo yamebainishwa jijini Arusha na Naibu Waziri wa Katiba na Sheria Mhe. Geophrey Mizengo Pinda katika ufunguzi wa kikao cha wadau wa huduma za msaada wa kisheria chenye lengo la lengo la kujadiliana juu ya masuala ya huduma za msaada wa kisheria ambapo amesema hii yote ni katika kuhakikisha huduma za msaada wa kisheria zinapatikana kwa wananchi katika maeneo yote na vyombo vyote vya upatikanaji haki.\r\n“Kwa upande wa Mahakama, Wizara iliingia makubaliano na Mahakama katika kuhakikisha huduma za msaada wa kisheria zinapatikana. Upo mpango wa kuwa na madawati ya msaada wa kisheria kuanzia Mahakama za Mwanzo hadi Mahakama Kuu hii yote ni katika kuhakikisha huduma za msaada wa kisheria zinapatikana kwa wananchi katika maeneo yote na vyombo vyote vya upatikanaji haki”amefafanua.\r\nKatika hatua nyingine amesema Masuala ya msaada wa kisheria ni masuala mtambuka hivyo wadau wote wanalo jukumu la kuhakikisha huduma zinapatikana kwa wakati na kwa kila mwenye uhitaji.\r\n“Serikali inatambua mchango wenu mkubwa unaofanywa katika kuhakikisha wananchi wanafikia haki kwa wakati. Ni jukumu letu sote kuwafikia wananchi wa ngazi za chini kabisa kwa wote waliopo mijini na vijijin, hata hivyo tuangalie zaidi wale walio pembezoni kwani ndiyo walio na uhitaji mkubwa zaidi. Hawa wa mjini wanafursa ya kusikia kupitia redio mbalimbali na hata kuona vipindi mbalimbali vya elimu ya kisheria kwa umma kupitia televisheni”amesema.\r\nAidha,Naibu waziri huyo amesema Lugha ya Kiingereza imekuwa na kikwazo na manyanyaso kwa mwananchi wa hali ya chini kunyang’anywa haki zake hivyo Mahakama na vyombo vingine vya utoaji haki vitaanza kutumia lugha ya Kiswahili katika uendeshaji wa mashauri na nyaraka mbalimbali za mahakama ikiwemo hukumu na mienendo ya Mahakama ili kuweza kumsaidia mwananchi wa hali ya chini.\r\nHivyo, Serikali kwa kutambua suala hili imeanza kufanyia kazi kwa kuanza kutafsiri sheria zote kutoka lugha ya kiingereza kwenda Kiswahili kwa lengo la kueleweka kwa urahisi kwa wananchi na kuwataka wadau wa sheria kutumia Kiswahili bila woga.\r\n“Wananchi wetu wengi hawana uelewa wa masuala ya kisheria jambo ambalo linapelekea wengi wao kupoteza haki kwa kukosa mwongozo sahihi wa namna ya kupata haki zao. Serikali kwa kutambua suala hili imeanza kufanyia kazi kwa kuanza kutafsiri sheria zote kutoka lugha ya kiingereza kwenda Kiswahili kwa lengo la kueleweka kwa urahisi kwa wananchi, Si hivyo tu, Mahakama na vyombo vingine vya utoaji haki vitaanza kutumia lugha ya Kiswahili katika uendeshaji wa mashauri na nyaraka mbalimbali za mahakama ikiwemo hukumu na mienendo ya Mahakama”amesema.\r\nAmeendelea kufafanua kuwa watoa huduma ya msaada wa kisheria wapatao 182 na wasaidizi wa kisheria wapatao 657 wameshasajiliwa mpaka sasa ambapo ni katika mwaka wa nne wa utekelezaji wa Sheria ya msaada wa kisheria ambayo ni Sheria Na. 1 ya Mwaka 2017 na Kanuni zake za Mwaka 2018.\r\nHata hivyo,Naibu waziri huyo amewataka wasaidizi wa kisheria ambao hawajajisajili kufanya mchakato wa kujisajili ili kufanya kazi kwa pamoja na kuweza kutoa huduma za msaada wa kisheria katika maeneo yaliyosahaulika hususan vijijini .\r\n“Ninafahamu wengi wa watoa huduma mliopo hapa mmeshajisajili na kutambuliwa na Serikali. Kwa wale ambao bado, niwaombe wajisajili chini ya matakwa ya sheria ya msaada wa kisheria. Kujisajili kunaipa Serikali kuwa na takwimu sahihi juu ya watoa huduma waliopo ili kuimarisha mifumo ya kiutendaji na kufanya maamuzi ya kisera kuhusiana na masuala ya msaada wa kisheria”amesema.\r\nKwa upande wao baadhi ya wadau wa sheria akiwemo Felister Mushi Msajili wa watoa Huduma ya msaada Kisheria pamoja na Tolbert Mmasy-Mkurugenzi mtandao wa wasaidizi wa kisheria Tanzania [TAPANET]wamesema mkutano huo ni muhimu katika kujadili masuala mbalimbali ya sheria zenye changamoto na kuweza kuzifanyia maboresho.', 'Important', 'News', NULL, '1', 'Super Admin', '2021-09-16', NULL, NULL, '1_1631776816.jpg', 'Super Admin', '2021-09-16', '2021-09-16 04:20:16', '2021-09-16 04:20:16'),
(18, 'ToR Analysis Discriminatory Laws 19.2.2018', 'ToR Analysis Discriminatory Laws 19.2.2018', 'Tender', 'Adverts', NULL, '0', 'Super Admin', '2021-09-18', NULL, NULL, 'en1519103809-ToR Analysis Discriminatory Laws 19.2.2018_1631970200.doc', NULL, NULL, '2021-09-18 10:03:20', '2021-09-18 10:03:20'),
(19, 'ToR Coordination Mechanisms Assessment 19.2.2018', 'ToR Coordination Mechanisms Assessment 19.2.2018', 'Tender', 'Adverts', NULL, '1', 'Super Admin', '2021-09-18', NULL, NULL, 'en1519103809-ToR Analysis Discriminatory Laws 19.2.2018_1631970813.doc', 'Super Admin', '2021-09-18', '2021-09-18 10:13:33', '2021-09-18 10:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_name`, `page_type`, `slug`, `status`, `content`, `parent_id`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'home', 'main_page', 'home', 'active', '<p align=\"justify\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', NULL, NULL, NULL, NULL, NULL),
(2, 'about us', 'main_page', 'about-us', 'active', '<h3>What is Lorem Ipsum?</h3>\n\n<p align=\"justify\"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<h3>Where does it come from?</h3>\n\n<p align=\"justify\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\n\n<p align=\"justify\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n\n<p>&nbsp;</p>\n\n<h3>Why do we use it?</h3>\n\n<p align=\"justify\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', NULL, NULL, NULL, NULL, NULL),
(3, 'profile', 'main_page', 'profile', 'active', '<hr />\n<p><img alt=\"An illustration for the story The Nightingale and the Rose by the author Oscar Wilde\" src=\"https://assets.americanliterature.com/al/images/story/the-nightingale-and-the-rose.jpg\" class=\"responsive-img\"/></p>\n\n<p>&quot;She said that she would dance with me if I brought her red roses,&quot; cried the young Student; &quot;but in all my garden there is no red rose.&quot;</p>\n\n<p>From her nest in the holm-oak tree the Nightingale heard him, and she looked out through the leaves, and wondered.</p>\n\n<p>&quot;No red rose in all my garden!&quot; he cried, and his beautiful eyes filled with tears. &quot;Ah, fon what little things does happiness depend! I have read all that the wise men have written, and all the secrets of philosophy are mine, yet for want of a red rose is my life made wretched.&quot;</p>\n\n<p>&quot;Here at last is a true lover,&quot; said the Nightingale. &quot;Night after night have I sung of him, though I knew him not: night after night have I told his story to the stars, and now I see him. His hair is dark as the hyacinth-blossom, and his lips are red as the rose of his desire; but passion has made his face like pale ivory, and sorrow has set her seal upon his brow.&quot;</p>\n\n<p>&quot;The Prince gives a ball to-morrow night,&quot; murmured the young Student, &quot;and my love will be of the company. If I bring her a red rose she will dance with me till dawn. If I bring her a red rose, I shall hold her in my arms, and she will lean her head upon my shoulder, and her hand will be clasped in mine. But there is no red rose in my garden, so I shall sit lonely, and she will pass me by. She will have no heed of me, and my heart will break.&quot;</p>\n\n<p>&quot;Here indeed is the true lover,&quot; said the Nightingale. &quot;What I sing of, he suffers - what is joy to me, to him is pain. Surely Love is a wonderful thing. It is more precious than emeralds, and dearer than fine opals. Pearls and pomegranates cannot buy it, nor is it set forth in the marketplace. It may not be purchased of the merchants, nor can it be weighed out in the balance for gold.&quot;</p>\n\n<p>&quot;The musicians will sit in their gallery,&quot; said the young Student, &quot;and play upon their stringed instruments, and my love will dance to the sound of the harp and the violin. She will dance so lightly that her feet will not touch the floor, and the courtiers in their gay dresses will throng round her. But with me she will not dance, for I have no red rose to give her&quot;; and he flung himself down on the grass, and buried his face in his hands, and wept.</p>\n\n<p>&quot;Why is he weeping?&quot; asked a little Green Lizard, as he ran past him with his tail in the air.</p>\n\n<p>&quot;Why, indeed?&quot; said a Butterfly, who was fluttering about after a sunbeam.</p>\n\n<p>&quot;Why, indeed?&quot; whispered a Daisy to his neighbour, in a soft, low voice.</p>\n\n<p>&quot;He is weeping for a red rose,&quot; said the Nightingale.</p>', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view roles', 'web', '2020-08-02 19:20:36', '2020-08-02 19:20:36'),
(2, 'add roles', 'web', '2020-08-02 19:20:36', '2020-08-02 19:20:36'),
(3, 'edit roles', 'web', '2020-08-02 19:21:21', '2020-08-02 19:21:21'),
(4, 'delete roles', 'web', '2020-08-02 19:21:21', '2020-08-02 19:21:21'),
(5, 'view permissions', 'web', '2020-08-02 19:22:10', '2020-08-02 19:22:10'),
(6, 'view users', 'web', '2020-08-02 19:23:23', '2020-08-02 19:23:23'),
(7, 'add users', 'web', '2020-08-02 19:23:23', '2020-08-02 19:23:23'),
(8, 'edit users', 'web', '2020-08-02 19:23:23', '2020-08-02 19:23:23'),
(9, 'delete users', 'web', '2020-08-02 19:23:23', '2020-08-02 19:23:23'),
(10, 'view profile', 'web', '2020-08-02 19:25:01', '2020-08-02 19:25:01'),
(11, 'edit profile', 'web', '2020-08-02 19:25:01', '2020-08-02 19:25:01'),
(12, 'view system logs', 'web', '2020-08-02 19:25:58', '2020-08-02 19:25:58'),
(13, 'view home', 'web', '2020-08-02 22:41:09', '2020-08-02 22:41:09'),
(14, 'add home', 'web', '2020-08-02 22:41:09', '2020-08-02 22:41:09'),
(15, 'edit home', 'web', '2020-08-02 22:41:09', '2020-08-02 22:41:09'),
(16, 'delete home', 'web', '2020-08-02 22:41:09', '2020-08-02 22:41:09'),
(17, 'view structure', 'web', '2020-08-02 22:44:05', '2020-08-02 22:44:05'),
(18, 'add structure', 'web', '2020-08-02 22:44:05', '2020-08-02 22:44:05'),
(19, 'edit structure', 'web', '2020-08-02 22:44:05', '2020-08-02 22:44:05'),
(20, 'delete structure', 'web', '2020-08-02 22:44:05', '2020-08-02 22:44:05'),
(21, 'view services', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(22, 'add services', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(23, 'edit services', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(24, 'delete services', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(25, 'view contact', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(26, 'add contact', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(27, 'edit contact', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(28, 'delete contact', 'web', '2020-08-02 22:46:21', '2020-08-02 22:46:21'),
(29, 'view news', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(30, 'add news', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(31, 'edit news', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(32, 'delete news', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(33, 'view posts', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(34, 'add posts', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(35, 'edit posts', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(36, 'delete posts', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(37, 'view media', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(38, 'add media', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(39, 'edit media', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(40, 'delete media', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(41, 'view links', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(42, 'add links', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(43, 'edit links', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(44, 'delete links', 'web', '2020-08-02 22:50:14', '2020-08-02 22:50:14'),
(45, 'view documents', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(46, 'add documents', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(47, 'edit documents', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(48, 'delete documents', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(49, 'view photo gallery', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(50, 'add photo gallery', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(51, 'edit photo gallery', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(52, 'delete photo gallery', 'web', '2020-08-02 23:54:07', '2020-08-02 23:54:07'),
(53, 'view paralegal', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(54, 'add paralegal', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(55, 'edit paralegal', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(56, 'delete paralegal', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(57, 'view provider', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(58, 'add provider', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(59, 'edit provider', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(60, 'delete provider', 'web', '2021-05-20 08:56:06', '2021-05-20 08:56:06'),
(61, 'view participant', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(62, 'add participant', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(63, 'edit participant', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(64, 'delete participant', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(65, 'view event', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(66, 'add event', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(67, 'edit event', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(68, 'delete event', 'web', '2021-05-20 09:02:12', '2021-05-20 09:02:12'),
(69, 'view query', 'web', '2021-05-20 09:06:06', '2021-05-20 09:06:06'),
(70, 'add query', 'web', '2021-05-20 09:06:06', '2021-05-20 09:06:06'),
(71, 'edit query', 'web', '2021-05-20 09:06:06', '2021-05-20 09:06:06'),
(72, 'delete query', 'web', '2021-05-20 09:06:06', '2021-05-20 09:06:06'),
(73, 'view comment', 'web', '2021-05-20 09:09:31', '2021-05-20 09:09:31'),
(74, 'add comment', 'web', '2021-05-20 09:09:31', '2021-05-20 09:09:31'),
(75, 'edit comment', 'web', '2021-05-20 09:09:31', '2021-05-20 09:09:31'),
(76, 'delete comment', 'web', '2021-05-20 09:09:31', '2021-05-20 09:09:31'),
(77, 'view desks', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(78, 'add desks', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(79, 'edit desks', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(80, 'delete desks', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(81, 'view comments', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(82, 'add comments', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(83, 'edit comments', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(84, 'delete comments', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(85, 'view aid type', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(86, 'add aid type', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(87, 'edit aid type', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(88, 'delete aid type', 'web', '2021-05-20 09:18:56', '2021-05-20 09:18:56'),
(89, 'view regions', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(90, 'add regions', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(91, 'edit regions', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(92, 'delete regions', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(93, 'view districts', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(94, 'add districts', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(95, 'edit districts', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(96, 'delete districts', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(97, 'view wards', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(98, 'add wards', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(99, 'edit wards', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(100, 'delete wards', 'web', '2021-05-20 11:28:13', '2021-05-20 11:28:13'),
(101, 'view legal weeks', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(102, 'add legal weeks', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(103, 'edit legal weeks', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(104, 'delete legal weeks', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(105, 'view legal aids', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(106, 'add legal aids', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(107, 'edit legal aids', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(108, 'delete legal aids', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(109, 'view claims', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(110, 'add claims', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(111, 'edit claims', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(112, 'delete claims', 'web', '2021-05-20 11:32:32', '2021-05-20 11:32:32'),
(113, 'view courts', 'web', '2021-08-27 07:15:30', '2021-08-27 07:15:30'),
(114, 'add courts', 'web', '2021-08-27 07:15:30', '2021-08-27 07:15:30'),
(115, 'edit courts', 'web', '2021-08-27 07:15:30', '2021-08-27 07:15:30'),
(116, 'delete courts', 'web', '2021-08-27 07:15:30', '2021-08-27 07:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `provided_aids`
--

CREATE TABLE `provided_aids` (
  `id` bigint(100) NOT NULL,
  `user_id` bigint(100) NOT NULL,
  `registered_by` varchar(100) NOT NULL,
  `court_id` bigint(100) NOT NULL,
  `litigant_name` varchar(100) NOT NULL,
  `litigant_address` varchar(100) NOT NULL,
  `litigant_phone` varchar(100) NOT NULL,
  `litigant_email` varchar(100) NOT NULL,
  `litigant_concern` text NOT NULL,
  `court_case_ref_number` varchar(100) DEFAULT NULL,
  `court_case_title` text DEFAULT NULL,
  `aid_type` int(100) NOT NULL,
  `description` text NOT NULL,
  `register_date` date NOT NULL,
  `court_name` varchar(100) DEFAULT NULL,
  `age` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `aid_name` varchar(100) DEFAULT NULL,
  `reg_month` varchar(100) DEFAULT NULL,
  `reg_year` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provided_aids`
--

INSERT INTO `provided_aids` (`id`, `user_id`, `registered_by`, `court_id`, `litigant_name`, `litigant_address`, `litigant_phone`, `litigant_email`, `litigant_concern`, `court_case_ref_number`, `court_case_title`, `aid_type`, `description`, `register_date`, `court_name`, `age`, `sex`, `aid_name`, `reg_month`, `reg_year`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super Admin', 0, 'Dax Loopy', 'KIGAMBONI', '0000000000', 'loopydax3@gmail.com', 'jdsjcwkmprekce', '2828109', NULL, 3, 'urocjrjlcjirojslclf', '2021-08-29', NULL, NULL, NULL, NULL, NULL, '2021', '2021-08-29 12:18:40', '2021-08-29 12:18:40'),
(2, 3, 'Omari Said', 391, 'Hamidu Selemani', 'KIGAMBONI', '0000000000', 'mwa@gmail.com', 'Naomba msaada wa kisheria', '334567', NULL, 1, 'Amepatiwa ushauri wa kisheria', '2021-09-22', 'Kigoma High Court', '37', 'Male', 'Legal Advice', 'September', '2021', '2021-09-22 03:18:41', '2021-09-22 03:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'web', '2020-08-02 19:14:09', '2020-08-02 19:14:09'),
(2, 'Web Master', 'web', '2020-08-02 21:11:46', '2020-08-02 21:11:46'),
(3, 'Desk Officer', 'web', '2021-08-26 04:06:24', '2021-08-26 04:06:24'),
(4, 'Legal Aid Admin', 'web', '2021-08-27 04:00:51', '2021-08-27 04:00:51'),
(5, 'management', 'web', '2021-09-22 04:02:20', '2021-09-22 04:02:20'),
(6, 'admin', 'web', '2021-09-22 04:03:47', '2021-09-22 04:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(10, 2),
(10, 3),
(10, 4),
(10, 5),
(11, 1),
(11, 2),
(11, 4),
(11, 5),
(12, 1),
(12, 6),
(13, 1),
(13, 2),
(13, 5),
(13, 6),
(14, 1),
(14, 2),
(14, 6),
(15, 1),
(15, 2),
(15, 6),
(16, 1),
(16, 6),
(17, 1),
(17, 2),
(17, 5),
(17, 6),
(18, 1),
(18, 2),
(18, 6),
(19, 1),
(19, 2),
(19, 6),
(20, 1),
(20, 6),
(21, 1),
(21, 2),
(21, 5),
(21, 6),
(22, 1),
(22, 2),
(22, 6),
(23, 1),
(23, 2),
(23, 6),
(24, 1),
(24, 6),
(25, 1),
(25, 2),
(25, 5),
(25, 6),
(26, 1),
(26, 2),
(26, 6),
(27, 1),
(27, 2),
(27, 6),
(28, 1),
(28, 6),
(29, 1),
(29, 2),
(29, 5),
(29, 6),
(30, 1),
(30, 2),
(30, 6),
(31, 1),
(31, 2),
(31, 6),
(32, 1),
(32, 6),
(33, 1),
(33, 2),
(33, 5),
(33, 6),
(34, 1),
(34, 2),
(34, 6),
(35, 1),
(35, 2),
(35, 6),
(36, 1),
(36, 6),
(37, 1),
(37, 2),
(37, 5),
(37, 6),
(38, 1),
(38, 2),
(38, 6),
(39, 1),
(39, 2),
(39, 6),
(40, 1),
(40, 6),
(41, 1),
(41, 2),
(41, 5),
(41, 6),
(42, 1),
(42, 2),
(42, 6),
(43, 1),
(43, 2),
(43, 6),
(44, 1),
(44, 6),
(45, 1),
(45, 2),
(45, 5),
(45, 6),
(46, 1),
(46, 2),
(46, 6),
(47, 1),
(47, 2),
(47, 6),
(48, 1),
(48, 6),
(49, 1),
(49, 2),
(49, 5),
(49, 6),
(50, 1),
(50, 2),
(50, 6),
(51, 1),
(51, 2),
(51, 6),
(52, 1),
(52, 6),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(77, 3),
(77, 5),
(77, 6),
(78, 1),
(78, 3),
(78, 5),
(78, 6),
(79, 1),
(79, 3),
(79, 6),
(80, 1),
(80, 6),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(85, 4),
(85, 5),
(85, 6),
(86, 1),
(86, 4),
(86, 5),
(86, 6),
(87, 1),
(87, 4),
(87, 6),
(88, 1),
(88, 4),
(88, 6),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(105, 3),
(105, 4),
(105, 5),
(105, 6),
(106, 1),
(106, 6),
(107, 1),
(107, 6),
(108, 1),
(108, 6),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(113, 4),
(114, 1),
(114, 4),
(115, 1),
(115, 4),
(116, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `reporter` varchar(100) NOT NULL,
  `publish` varchar(10) NOT NULL,
  `date_posted` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `reporter`, `publish`, `date_posted`, `created_at`, `updated_at`) VALUES
(1, 'Kusajili  Watoa Huduma za Msaada wa Sheria', 'Kusajili  Watoa Huduma za Msaada wa Sheria', 'Super Admin', '0', '2021-05-19 12:21:38', '2021-05-19 09:21:38', '2021-05-19 12:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Mahakama ya Tanzania', 'https://www.judiciary.go.tz', '2021-05-23 07:08:58', '2021-05-23 10:08:58'),
(2, 'Ofisi ya Wakili Mkuu wa Serikali', 'https://www.osg.go.tz', '2021-05-23 07:10:46', '2021-05-23 10:10:46');

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(10) NOT NULL,
  `reporter` varchar(100) NOT NULL,
  `publish` varchar(10) NOT NULL,
  `date_posted` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `structures`
--

INSERT INTO `structures` (`id`, `reporter`, `publish`, `date_posted`, `type`, `file`, `created_at`, `updated_at`) VALUES
(3, 'Super Admin', '0', '2021-05-20 12:56:50', 'Directorate', 'muundo_1621515410.PNG', '2021-05-20 12:56:50', '2021-05-20 12:56:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `salutation` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_salutation` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isExternal` tinyint(1) NOT NULL DEFAULT 0,
  `mobile` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `court_id` int(11) DEFAULT 0,
  `workplace` varchar(220) COLLATE utf8_unicode_ci DEFAULT NULL,
  `law_firm_id` int(11) DEFAULT NULL,
  `user_id` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_user` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `ver_code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activated',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `salutation`, `second_salutation`, `fname`, `oname`, `lname`, `name`, `address`, `telephone`, `isExternal`, `mobile`, `court_id`, `workplace`, `law_firm_id`, `user_id`, `username`, `is_user`, `email`, `password`, `ver_code`, `status`, `remember_token`, `created_at`, `updated_at`, `verified`) VALUES
(1, NULL, NULL, 'Super', NULL, 'Admin', 'Super Admin', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'admin', NULL, 'admin@mocla.com', '$2y$10$Z5We3yFuJU2O6mqcbKxRo.0lk4Tp26uuRLuxu/Hu2tPtFjeFdRXO2', NULL, 'activated', 'y4gprsSH3y90DgbimzQeFISD90P6OnSAduaKZetQ3oMZbMaVags3p8W4v8Jx', '2020-08-02 18:17:12', '2020-08-02 18:17:12', 1),
(2, 'Mr.', NULL, 'Othman', 'Juma', 'Kanyegezi', 'Othman Kanyegezi', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'othman.kanyegezi', NULL, 'athuju@gmail.com', '$2y$10$xrPltdh0ewvz004S44E4IOJgnhlX9kPhfzLuEp2Nb5syJsl7Hzcqm', NULL, 'activated', 'mqqYDP45aSn8IJY33acK3ID5XX1I5gKWF1SvBlaLT6RnYJF2cp9iulypMIb3', '2020-08-02 21:15:20', '2020-08-02 21:15:20', 1),
(3, 'Mr.', NULL, 'Omari', 'Juma', 'Said', 'Omari Said', NULL, NULL, 0, NULL, 391, NULL, NULL, NULL, 'omari.said', NULL, 'omary.said@email.com', '$2y$10$jWvQraTeavZm6yAqcVl7Ee8Dmpo9vjRQN10mHEBf05qQ0XnsS865.', NULL, 'activated', 'coZwpAyvkJyfBhYuTzkGcfGcS9zHyXNeH1jwntrS76JWvbWebC0inm6Vc6I2', '2021-08-27 07:01:05', '2021-08-27 07:01:05', 1),
(4, 'Mr.', NULL, 'Mwa', 'Juma', 'Shabani', 'Mwa Shabani', NULL, NULL, 0, NULL, 392, 'MoCLA', NULL, NULL, 'mwa.shabani', NULL, 'mwa@gmail.com', '$2y$10$/LEogiXSQZ8yrVIISg4djO5OZHcsVgdYsSP4HWHCPgtDW.ae/7wdS', NULL, 'activated', 'oQEXQCBCE5Wi2n21BmBlv7MneXYsV2IdsdRsS5kBFfiVpsFXm2W2K0eexr0h', '2021-08-29 05:44:03', '2021-08-29 05:44:03', 1),
(5, 'Mr.', NULL, 'Syatem', 'User', 'Administrator', 'Syatem Administrator', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'syatem.administrator', NULL, 'admin@mocla.go.tz', '$2y$10$lDkSmblN46w/pheYyg8cfOvLuWwfyWQ.dRazKKVidGnblms4oXC0G', NULL, 'activated', NULL, '2021-09-22 04:05:06', '2021-09-22 04:05:06', 1),
(6, 'Mr.', NULL, 'Web', 'Content', 'Manager', 'Web Manager', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'web.manager', NULL, 'webmaster@mocla.go.tz', '$2y$10$tjVI3T1Zd1EnZrkoGwW6V.UuqXPXLB0BDdsglT3uaCdTW.FWlLQIS', NULL, 'activated', NULL, '2021-09-22 04:06:15', '2021-09-22 04:06:15', 1),
(7, 'Mr.', NULL, 'Desk', 'User', 'Officer', 'Desk Officer', NULL, NULL, 0, NULL, 392, 'MoCLA', NULL, NULL, 'desk.officer', NULL, 'deskofficer@mocla.go.tz', '$2y$10$iAjxU0W5HtvsnJlEegMMfehrQyaFa6VDj6f26CP7.lWvtVV8e6a0K', NULL, 'activated', NULL, '2021-09-22 04:07:20', '2021-09-22 04:07:20', 1),
(8, 'Hon.', NULL, 'Registrar', 'Top', 'Management', 'Registrar Management', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, 'registrar.management', NULL, 'registrar@mocla.go.tz', '$2y$10$VitK5NrFxBpe3kDap.8GBuNlSxtPB025MeO2RtvRgZGmoextB.Gs6', NULL, 'activated', NULL, '2021-09-22 04:08:36', '2021-09-22 04:08:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `web_documents`
--

CREATE TABLE `web_documents` (
  `id` int(100) NOT NULL,
  `news_id` int(100) NOT NULL,
  `file` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `web_documents`
--

INSERT INTO `web_documents` (`id`, `news_id`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, 'waziri_1596748054.PNG', '2020-08-06 18:07:34', '2020-08-06 18:07:34'),
(2, 2, 'katibu_1596748348.PNG', '2020-08-06 18:12:28', '2020-08-06 18:12:28'),
(3, 3, 'slide-3_1596783160.jpg', '2020-08-06 18:51:51', '2020-08-06 18:51:51'),
(4, 4, 'IMG_7335_1596751574.JPG', '2020-08-06 19:06:14', '2020-08-06 19:06:14'),
(5, 5, 'slide-1_1596783253.jpg', '2020-08-06 19:08:46', '2020-08-06 19:08:46'),
(6, 6, 'IMG_7587_1596895672.JPG', '2020-08-08 11:07:53', '2020-08-08 11:07:53'),
(7, 7, 'IMG-20200722-WA0026_1597066398.jpg', '2020-08-10 10:33:18', '2020-08-10 10:33:18'),
(8, 8, 'IMG_7587_1597066585.JPG', '2020-08-10 10:36:25', '2020-08-10 10:36:25'),
(9, 9, 'IMG_7508_1597066751.JPG', '2020-08-10 10:39:11', '2020-08-10 10:39:11'),
(10, 1, 'minister_1621420499.jpeg', '2021-05-19 07:34:59', '2021-05-19 07:34:59'),
(11, 2, 'minister_1621420659.jpeg', '2021-05-19 07:37:39', '2021-05-19 07:37:39'),
(12, 1, 'muundo_1621426500.PNG', '2021-05-19 09:15:00', '2021-05-19 09:15:00'),
(13, 2, 'muundo_1621515279.PNG', '2021-05-20 09:54:39', '2021-05-20 09:54:39'),
(14, 3, 'muundo_1621515410.PNG', '2021-05-20 09:56:50', '2021-05-20 09:56:50'),
(15, 12, 'en1519104026-ToR Mapping Capacity needs Judicial 19.2.2018_1621587258.doc', '2021-05-21 05:54:18', '2021-05-21 05:54:18'),
(16, 3, 'bio_1618122358-Kabudi_1621767179.jpg', '2021-05-23 07:53:00', '2021-05-23 07:53:00'),
(17, 13, '1620753110-WhatsApp Image 2021-05-11 at 19.01.47_1622023323.jpeg', '2021-05-26 07:02:04', '2021-05-26 07:02:04'),
(18, 14, 'sw1620828120-TANGAZO LA KUITWA KWENYE USAILI - MEI 2021_1622197650.pdf', '2021-05-28 07:27:31', '2021-05-28 07:27:31'),
(19, 15, 'case_proceedings_1622198815.pdf', '2021-05-28 07:46:55', '2021-05-28 07:46:55'),
(20, 16, '1622113899-2_1622199475.jpg', '2021-05-28 07:57:55', '2021-05-28 07:57:55'),
(21, 17, '1_1631776816.jpg', '2021-09-16 04:20:17', '2021-09-16 04:20:17'),
(22, 18, 'en1519103809-ToR Analysis Discriminatory Laws 19.2.2018_1631970200.doc', '2021-09-18 10:03:20', '2021-09-18 10:03:20'),
(23, 19, 'en1519103809-ToR Analysis Discriminatory Laws 19.2.2018_1631970813.doc', '2021-09-18 10:13:33', '2021-09-18 10:13:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aidtypes`
--
ALTER TABLE `aidtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audits_auditable_id_auditable_type_index` (`auditable_id`,`auditable_type`);

--
-- Indexes for table `backgrounds`
--
ALTER TABLE `backgrounds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courts`
--
ALTER TABLE `courts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managements`
--
ALTER TABLE `managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provided_aids`
--
ALTER TABLE `provided_aids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `web_documents`
--
ALTER TABLE `web_documents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aidtypes`
--
ALTER TABLE `aidtypes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `backgrounds`
--
ALTER TABLE `backgrounds`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courts`
--
ALTER TABLE `courts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3411;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `managements`
--
ALTER TABLE `managements`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `provided_aids`
--
ALTER TABLE `provided_aids`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `web_documents`
--
ALTER TABLE `web_documents`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
