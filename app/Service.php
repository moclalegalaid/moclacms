<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'services';
    protected $fillable = [
        'name',
        'description',
    ];
   
}
