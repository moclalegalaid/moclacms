<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'structures';
    protected $fillable = [
        'file',
    ];
   
    
    public function webDocuments(){
        return $this->hasMany('App\WebDocument','structure_id');
    }
}
