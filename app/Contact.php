<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'contacts';
    protected $fillable = [
        'name',
        'address',
        'email',
        'phone',
        'telephone',
        'fax',
    ];
   
}
