<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'feedbacks';
    public $fillable = [
        'news_id',
        'count',
        'type',
    ];

    public function news()
    {
       return $this->belongsTo('App\News', 'news_id');
    }
    
}