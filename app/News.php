<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'news';
    protected $fillable = [
        'title',
        'status',
        'content',
        'reporter',
        'date_posted',
    ];

    public function coments(){
        return $this->hasMany('App\Comment','news_id');
    }
   
    
    public function webDocuments(){
        return $this->hasMany('App\WebDocument','news_id');
    }
}
