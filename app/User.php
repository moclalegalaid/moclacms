<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable 
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'users';
	 
    protected $guarded = [ 'id','created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 'password', 'remember_token'];

    public function court(){
        return $this->belongsTo('App\Court', 'court_id');
    }
    public function lawFirm(){
        return $this->belongsTo('App\LawFirm', 'law_firm_id');
    } 
	
	//email verification starts here
	
	 public function verificationToken()
    {
        return $this->hasOne(VerificationToken::class);
    }

    public function hasVerifiedEmail()
    {
        return $this->verified;
    }

    public static function byEmail($email)
    {
        return static::where('email', $email);
    }

    public function audit() {
        return $this->hasMany('App\Audit', 'user_id');
    }
    

}
