<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteLink extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'sites';
    protected $fillable = [
        'name',
        'link',
    ];

}
