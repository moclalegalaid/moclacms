<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Background extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'backgrounds';
    protected $fillable = [
        'title',
        'content',
    ];
}
