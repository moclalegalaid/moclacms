<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebDocument extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'web_documents';
    public $fillable = [
        'news_id',
        'name',
        'attachement',
    ];

    public function news()
    {
       return $this->belongsTo('App\News', 'news_id');
    }

    public function managements()
    {
       return $this->belongsTo('App\Management', 'management_id');
    }

    public function adverts()
    {
        return $this->belongsTo('App\Advert', 'advert_id');
    }
    
}
