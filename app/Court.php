<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{

    protected $auditInclude = [
        'title',
        'content',
        'code',
    ];

    protected $table = 'courts';

    public function users(){
        return $this->hasMany('App\User', 'court_id');
    }

   

    
}
