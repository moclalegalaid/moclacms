<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'managements';
    protected $fillable = [
        'name',
        'title',
        'profile',
    ];
   
    
    public function webDocuments(){
        return $this->hasMany('App\WebDocument','management_id');
    }
}
