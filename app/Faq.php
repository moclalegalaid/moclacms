<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    protected $auditInclude = [
        'title',
        'answer',
    ];

    protected $table = 'faqs';
    protected $fillable = [
        'title',
        'qustion',
        'answer',
    ];
}
