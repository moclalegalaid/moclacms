<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AidType extends Model
{

    protected $auditInclude = [
        'name',
    ];

    protected $table = 'aidtypes';
    protected $fillable = [
        'name',
        'created_by',
    ];
}
