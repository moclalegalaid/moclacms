<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvidedAid extends Model
{

    protected $auditInclude = [
        'litigant_name',
    ];

    protected $table = 'provided_aids';
    protected $fillable = [
        'court_id',
        'user_id',
        'litigant_name',
        'litigant_address',
        'litigant_phone',
        'litigant_email',
        'aid_type',
        'court_case_ref_number',
        'court_case_title',
        'description',
        'litigant_concern',
    ];
}
