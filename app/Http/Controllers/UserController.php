<?php
namespace App\Http\Controllers;

use App\CaseAssignment;
use App\Judge;
use ConsoleTVs\Charts\Facades\Charts;
use App\CaseProceeding;
use App\DocumentSubmission;
use App\AdmissionComplainant;
use App\EfilingNotification;
use App\AdmissionRespondent;
use App\Admission;
use App\Cases;
use App\AdmissionDocument;
use App\Classes\MsdgApi;
use App\AdmissionFee;
use App\CaseType;
use App\CourtFee;
use App\Court;
use App\CourtLevel;
use App\LawFirm;
use App\CaseAdvocate;
use App\Advocate;
use App\CaseNature;
use App\NatureCase;
use App\CaseScope;
use App\User;
use App\Bill;
use App\ExtUser;
use App\ReportCaseRegister;
use function GuzzleHttp\Psr7\str;
use Spatie\Permission\Models\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getHome()
    {
        
        $userId = Auth::user()->id;
		//echo $userId;exit;
        
        $admissionNotification = EfilingNotification::where('user_id','=',$userId)
                                                    ->orderBy('created_at','desc')->get();
        $admissionNotificationCount = EfilingNotification::where('user_id','=',$userId)
                                ->where('status','=','New')->count();
        
		$savedAdmissions = Admission::where('user_id','=',$userId)
                                ->where('status','=','Draft')->get();
        $savedAdmissionsCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Draft')->count();
            
        $admissions = Admission::where('user_id','=',$userId)
                                ->where('status','=','Submitted')->get();
        $admissionsCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Submitted')->count();
            
        $admitted = Admission::where('user_id','=',$userId)
                                ->where('status','=','Admitted')->get();
        $admittedCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Admitted')->count();
        $returned = Admission::where('user_id','=',$userId)
                                ->where('status','=','Returned')->get();
        $returnedCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Returned')->count();
        $rejected = Admission::where('user_id','=',$userId)
                                ->where('status','=','Rejected')->get();
        $rejectedCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Rejected')->count();
        
        $registered = Admission::where('user_id','=',$userId)
                                ->where('status','=','Registered')->get();
        $registeredCount = Admission::where('user_id','=',$userId)
                                ->where('status','=','Registered')->count();
        
        $submittedDocmt = DocumentSubmission::where('user_id','=',$userId)->orderBy('id', 'desc')->get();
        $submittedDocmtCount = DocumentSubmission::where('user_id','=',$userId)->count();
        
        $caseTypes = CaseType::where('case_type_id', '<>', 0)
            ->pluck('name', 'id');
        
        $crCaseTypes = CaseType::where('case_type_id', '<>', 0)
                ->whereType('Criminal')
                ->pluck('name', 'id');
        
        $caseProceedings = CaseProceeding::all()->first();
        
        
        $courtFee = CourtFee::all()->pluck('name', 'id');
		$caseAdvocate = CaseAdvocate::all();
        $advocates = Advocate::orderBy('name')->pluck('name', 'id')->toArray();
		$firmName = LawFirm::all()->pluck('name', 'id');
        $caseNature = NatureCase::getNatureCase("Civil")->pluck('nature', 'id');
        $crCaseNature = NatureCase::getNatureCase("Criminal")->pluck('nature', 'id');
		$courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        $listOfYears = array_combine(range(date("Y"), 2000), range(date("Y"), 2000));
		$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        
        
        if(auth()->user()->isExternal == 1){
            if(auth()->user()->verified == 1){
                
            return view('external',[
			'submittedDocmt' => $submittedDocmt,
            'submittedDocmtCount' => $submittedDocmtCount,
            'admissions' => $admissions,
            'listOfYears' => $listOfYears,
            'crCaseTypes' => $crCaseTypes,
            'crCaseNature' => $crCaseNature,
            'admitted' => $admitted,
            'returned' => $returned,
            'rejected' => $rejected,
            'registered' => $registered,
	        'registeredCount' => $registeredCount,
            'admissionsCount' => $admissionsCount,
            'savedAdmissions' => $savedAdmissions,
            'savedAdmissionsCount' => $savedAdmissionsCount,
            'admissionNotification' => $admissionNotification,
            'admissionNotificationCount' => $admissionNotificationCount,
            'admittedCount' => $admittedCount,
            'returnedCount' => $returnedCount,
            'rejectedCount' => $rejectedCount,
            'caseTypes' => $caseTypes,
			'courtLevels' => $courtLevels,
            'courts' => $courts,
            'courtFee'=> $courtFee,
            'caseTypes' => $caseTypes,
			'courtLevels' => $courtLevels,
            'courts' => $courts,
            'caseNature' => $caseNature,
			'caseAdvocate' => $caseAdvocate,
            'advocates' => $advocates,
			'firmName' => $firmName,
			'countries' => $countries,
            'caseProceedings' => $caseProceedings,
			'documentType' => [
                    'Counter Affidavit' => 'Counter Affidavit',
                    'Written Statement of Deffence' => 'Written Statement of Deffence',
                    'Rejoinder' => 'Rejoinder',
                    'Counter Petition' => 'Counter Petition',
					'Written Submission' => 'Written Submission',
                    'Counter Submission' => 'Counter Submission',
                    'Rejoinder Submission' => 'Rejoinder Submission',
                    'Third Party Notice' => 'Third Party Notice',
                    'Notice of Preliminary Objection' => 'Notice of Preliminary Objection',
                    'Notice to Produce' => 'Notice to Produce',
                ],
                'filedBy' => [
                    'Advocate' => 'Advocate',
                    'Prose Filer' => 'Prose Filer',
                    'Law Officer' => 'Law Officer',
                    'Agent' => 'Agent',
                    'Law Firm' => 'Law Firm',
                ],
            'savedTo' => [
                    'Plaintiff' => 'Plaintiff',
					'Petitioner' => 'Petitioner',
                    'Applicant' => 'Applicant',
                    'Appellant' => 'Appellant',
					'Respondent' => 'Respondent',
                    'Defendant' => 'Defendant',
                    'Objector' => 'Objector',
                    'Judgment Deptor' => 'Judgment Deptor',
                    'Decree Holder' => 'Decree Holder',
                    'Third Party' => 'Third Party',
                ],
            'qualification' => [
                    'Advocate' => 'Advocate',
					'State Attorney' => 'State Attorney',
                    'DPP' => 'DPP',
                    'Solicitor' => 'Solicitor',
                ],
            'caseStatus' => [
                    'Status1' => 'Status1',
                    'Status2' => 'Status2',
                    
                ],
            'urgency' => [
                    'Urgent' => 'Urgent',
                    'Ordinary' => 'Ordinary',
                    
                ],
                'complainantGender' => [
                    'Female' => 'Female',
                    'Male' => 'Male',
					'None' => 'None',
                ],
				'complainantPart' => [
                    'Plaintiff' => 'Plaintiff',
					'Petitioner' => 'Petitioner',
                    'Applicant' => 'Applicant',
                    'Appellant' => 'Appellant',
                ],
				'representing' => [
                    'Plaintiff' => 'Plaintiff',
					'Petitioner' => 'Petitioner',
                    'Applicant' => 'Applicant',
                    'Appellant' => 'Appellant',
					'Respondent' => 'Respondent',
                    'Defendant' => 'Defendant',
                ],
                'respondentGender' => [
                    'Female' => 'Female',
                    'Male' => 'Male',
					'None' => 'None',
                ],
				'appearedAs' => [
                    'Individual' => 'Individual',
                    'Firm' => 'Firm',
                ],
                'courtFeeExemp' => [
                    'Exempted By Order of The Court' => 'Exempted By Order of The Court',
                    'Exempted By Order of The Chief Justice' => 'Exempted By Order of The Chief Justice',
                    'Exempted By Law' => 'Exempted By Law',
                ],
                'currency' => [
                    'TZS' => 'TZS',
                    'USD' => 'USD',
					'EUR' => 'EUR',
                    'GBP' => 'GBP',
                ],
				'respondentPart' => [
                    'Respondent' => 'Respondent',
                    'Deffendant' => 'Deffendant',
                ]
			]); 
                
            }else{
                return redirect('login');
                 
                 }
            
        }else if(auth()->user()->isExternal == 2){
            
            
            return redirect("dashboard/stakeholder"); 
            
            
        }else{
            
            $user = Auth::user();
        
            $role = $user->getRoleNames()->first();
            
            $list = ['super-admin','chief-justice','judiciary-management','jaji-kiongozi','chief-registrar','chief-court-administrator','HJDU','DJSIE','DCM','judiciary'];
            
            if(in_array($role,$list)){  
                
                return redirect("dashboard/mgt");    
            } else { 
                
                return redirect("dashboard/station");       
            }
        }
    }
    
    public function external()
    { 
        $admissions = Admission::all();
        $caseTypes = CaseType::where('case_type_id', '<>', 0)
            ->whereType('Civil')
            ->pluck('name', 'id');
		$courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        
        $criminalCases = Cases::fetch()->get();
            $crCaseTypes = CaseType::where('case_type_id', '<>', 0)
                ->whereType('Criminal')
                ->pluck('name', 'id');
        return view('external',[
			'criminalCases' => $criminalCases,
            'crCaseTypes' => $crCaseTypes,
            'admissions' => $admissions,
            'caseTypes' => $caseTypes,
			'courtLevels' => $courtLevels,
            'courts' => $courts,
			]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(){
        
        
        $status = 1;
        $users = User::where('username','<>','admin')->get();
        $roles = Role::all()->pluck('name', 'id');
        return view('users.index2', [
            'users' => $users,
            'roles' => $roles,
        ]);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function addUser(Request $request)
    {
        if($request->isMethod('get')) {
            $user = User::all();
            $status = "active";
            $courts = Court::where('status', $status)->pluck('name', 'id');
            $roles = Role::where('name','<>','super-admin')->orderBy('name', 'asc')->pluck('name', 'id');
            return view('users.add', [
                'user' => $user,
                'courts' => $courts,
                'roles' => $roles,
                'salutation' => [
                    'Dr.' => 'Dr.',
                    'Eng.' => 'Eng.',
                    'Hon.' => 'Hon.',
                    'Mr.' => 'Mr.',
                    'Mrs.' => 'Mrs.',
                    'Miss.' => 'Miss.',
                    'Ms.' => 'Ms.',
                    'Prof.' => 'Prof.',
                ],
            ]);
        }

        $this->validate(request(), [
            'desk' => 'required',
            'roles' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

            $firstName = $request->input('fname');
            $lastName = $request->input('lname');
            $court = $request->input('court');
            $fullName = $firstName.' '.$lastName;
            $verification = $request->input('desk');
            $username = strtolower($firstName).'.'.strtolower($lastName);
            $status = "activated";
        
                
                  if($verification == 'desk') {
                        $user  = new User;
                        $user->salutation = $request->input('salutation');
                        $user->second_salutation = $request->input('second_salutation');
                        $user->fname = ucwords($firstName);
                        $user->oname = $request->input('oname');
                        $user->lname = ucwords($lastName);
                        $user->name = $fullName;
                        $user->email = $request->input('email');
                        $user->username = $username;
                        $user->court_id = $court;
                        $user->workplace = $request->input('workplace');
                        $user->password = bcrypt($request->input('password'));
                        $user->status = $status;
                        $user->verified = 1;
                        $user->is_user = $request->input('isJudge');
                        $user->save();

                    } else {
                        $user  = new User;
                        $user->salutation = $request->input('salutation');
                        $user->second_salutation = $request->input('second_salutation');
                        $user->fname = ucwords($firstName);
                        $user->oname = $request->input('oname');
                        $user->lname = ucwords($lastName);
                        $user->name = $fullName;
                        $user->email = $request->input('email');
                        $user->username = $username;
                        $user->password = bcrypt($request->input('password'));
                        $user->status = $status;
                        $user->verified = 1;
                        $user->is_user = $request->input('isJudge');
                        $user->save();

                    }
            


        if($user->wasRecentlyCreated){
            $user->assignRole($request->input('roles'));
           
            return redirect('users/managements')
                ->with('success', 'User with username '.$user->username.' has been created successfully.');
        }else{
            return back()
                ->withError('User with username '.$user->username.' already exists. Please try again!!');
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editUser(Request $request, $id)
    {
        if ($request->isMethod('get')) {
            $user = User::find($id);
            if(!$user){
                return back()->withError('User can not be found. Please check again.!');
            }

            $roles = Role::all()->pluck('name','name');
            $courtLevels = CourtLevel::all()->pluck('name', 'id');
            $courts = Court::all()->pluck('name', 'id');
            return view('users.edit', [
                'user' => $user,
                'roles' => $roles,
                'courtLevels' => $courtLevels,
                'courts' => $courts,
                'salutation' => [
                    'Dr.' => 'Dr.',
                    'Eng.' => 'Eng.',
                    'Hon.' => 'Hon.',
                    'Mr.' => 'Mr.',
                    'Mrs.' => 'Mrs.',
                    'Miss.' => 'Miss.',
                    'Ms.' => 'Ms.',
                    'Prof.' => 'Prof.',
                ],
            ]);
        }

        $this->validate(request(), [
            'fname' => 'required|max:80',
            'lname' => 'required|max:80',
            'courtId' => 'required',
            'roles' => 'required',
            'status' => 'required',
        ]);

        $firstName = $request->input('fname');
        $lastName = $request->input('lname');
        $fullName = $firstName.' '.$lastName;
        $verification = $request->input('status');
        $username = strtolower($firstName).'.'.strtolower($lastName);

        if($verification == 'activated') {
            $user = User::find($id);
            $user->salutation = $request->input('salutation');
            $user->second_salutation = $request->input('second_salutation');
            $user->fname = ucwords($firstName);
            $user->lname = ucwords($lastName);
            $user->name = $fullName;
            $user->court_id = $request->input('courtId');
            $user->username = $username;
            $user->status = $request->input('status');
            $user->verified = 1;
            $user->is_user = $request->input('isJudge');
            $user->save();
            $user->syncRoles($request->input('roles'));

        } else {
            $user = User::find($id);
            $user->salutation = $request->input('salutation');
            $user->second_salutation = $request->input('second_salutation');
            $user->fname = ucwords($firstName);
            $user->lname = ucwords($lastName);
            $user->name = $fullName;
            $user->court_id = $request->get('courtId');
            $user->username = $username;
            $user->status = $request->input('status');
            $user->verified = 0;
            $user->is_user = $request->input('isJudge');
            $user->save();
            $user->syncRoles($request->input('roles'));

        }
//        $query = "UPDATE case_scopes SET allowed_users = allowed_users &~ (1 << FIND_IN_SET($id, allowed_users) - 1)";
//         $allowed_use = DB::statement(DB::raw($query));
        
        $courtCasesIds = $user->court->cases->pluck('id');
        $allowed_user = DB::raw("CONCAT(case_scopes.allowed_users,',',$user->id)");
        CaseScope::whereIn('cases_id', $courtCasesIds)->update(['allowed_users' => $allowed_user]);

        if($user->wasRecentlyUpdated){
            $user->assignRole($request->input('roles'));
            $casesIds = Cases::whereCourtId($request->input('courtId'))->pluck('id')->toArray();
            $casesIds = implode(',', $casesIds );

            DB::table('case_scopes')
                        ->whereIn('cases_id', [$casesIds])
                        ->update([
                            'allowed_users' => DB::raw("CONCAT(allowed_users, ',".$user->id."')")
                        ]);

            return redirect('users/managements')
                ->with('success', 'User with username '.$user->username.' has been updated successfully.');
        }else{
          return redirect('users/managements')->with('success', 'User with username '.$user->username.' has been updated successfully.');
        }
    }

    public function changeUserStatus($id)
    {
        $userStatus  = User::whereId($id)->first();
        if($userStatus) {
            $userStatus->verified = ($userStatus->verified == 1)? 0:1;
            $userStatus->status = ($userStatus->status == 'activated')? 'deactivate':'activated';
            $userStatus->save();
        }

        if($userStatus->verified == 1) {
            return redirect('users/managements')->with('success', 'User status has been activated successfully.');
        }
        else {
            return redirect('users/managements')->with('success', 'User status has been deactivated successfully.');
        }

    }

    public function updateProfile(Request $request, $id)
    {
        if($request->isMethod('get')) {
            $user = User::find($id);
            if(!$user){
                return back()->withError('User can not be found. Please check again.!');
            }

            if(Auth::user()->id <> $id) {
                return back()->withWarning('You do not have permission to access this resource.');
            }

            $roles = Role::all()->pluck('name','id');
            return view('users.profile', [
                'user' => $user,
                'roles' => $roles,
            ]);
        }

        $user = User::find($id);
        try {
            $user->oname = $request->get('oname');
            $user->address = $request->get('address');
            $user->mobile = $request->get('mobile');
            $user->email = $request->get('email');

            $user->save();
        } catch (QueryException $exception) {
            return back()->withError($exception->getMessage());

        } catch (\ErrorException $exception) {
            return back()->withError($exception->getMessage());

        }

        return back()->with('success', 'User with username '.$user->username.' has been updated successfully.');
    }
    
    public function updateExtProfile(Request $request, $id)
    {
        if($request->isMethod('get')) {
            $user = User::find($id);
            if(!$user){
                return back()->withError('User can not be found. Please check again.!');
            }

            if(Auth::user()->id <> $id) {
                return back()->withWarning('You do not have permission to access this resource.');
            }

            $userId = Auth::user()->id;
            //echo $userId;exit;

            $admissionNotification = EfilingNotification::where('user_id','=',$userId)
                                                        ->orderBy('created_at','desc')->get();
            $admissionNotificationCount = EfilingNotification::where('user_id','=',$userId)
                                    ->where('status','=','New')->count();
            
            $roles = Role::all()->pluck('name','id');
            return view('users.extProfile', [
                'user' => $user,
                'admissionNotification' => $admissionNotification,
                'admissionNotificationCount' => $admissionNotificationCount,
                'roles' => $roles,
            ]);
        }

        $user = User::find($id);
        try {
            $user->oname = $request->get('oname');
            $user->address = $request->get('address');
            $user->mobile = $request->get('mobile');
            $user->email = $request->get('email');

            $user->save();
        } catch (QueryException $exception) {
            return back()->withError($exception->getMessage());

        } catch (\ErrorException $exception) {
            return back()->withError($exception->getMessage());

        }

        return back()->with('success', 'User with username '.$user->username.' has been updated successfully.');
    }

    public function changePassword(Request $request, $id)
    {
        if($request->isMethod('get')) {
            $user = User::find($id);
            if(!$user){
                return back()->withError('User not found');
            }

            if(Auth::user()->id <> $id) {
                return back()->withWarning('You do not have permission to access this resource.');
            }

            $roles = Role::all()->pluck('name','id');
            return view('users.change_password', [
                'user' => $user,
                'roles' => $roles,
            ]);
        }

        $user = User::find($id);
        $this->validate(request(), [
            'password' => 'required|min:6|confirmed',
        ]);

        try {
            $user->password = bcrypt($request->input('password'));
            $user->save();

        } catch (QueryException $exception) {
            return back()->withError($exception->getMessage());

        } catch (\ErrorException $exception) {
            return back()->withError($exception->getMessage());

        }

        return back()->with('success', 'Password has been changed successfully.');
    }
    

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteUser($id) 
    {
        $user = User::find($id);
        $user->delete();

        return redirect('users/managements')->with('success', 'User with username '.$user->username.' has been deleted successfully.');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getMgtDashboard(Request $request)
    {
        
    try {
        $userId = Auth::user()->id;
        $courtId = Auth::user()->court_id;
        $todatDate = date('Y-m-d');
        $thisMonth = date('F');
        $thisYear = date('Y');
        $outcome = "decided";
        $backlog = "Backlog";
        
        
        // ===== Case  Category ========
        
        $civil = "Civil Case";
        $land = "Land Case";
        $labour = "Labour Case";
        $com = "Commercial Case";
        $matrm = "Matrimonial Cause";
        $probate = "Probate and Administration";
        
        $juvenile = "Juvenile Civil Case";
        $application = "Application";
        $adoption = "Adoption";
        $miscellaneousCivilCause = "Miscellaneous Civil Cause";
        $bankruptcy = "Bankruptcy";
        
        $tax = "Taxation Cause";
        $criminal = "Criminal Case";
        $economic = "Economic Case";
        $traffic = "Traffic Case";
        $pi = "Preliminary Inquiry (PI)";
        $corruption = "Corruption Case";
        
        
        
        $courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        
        $courtsCount = Court::all()->count();
        
        //*********** MANAGEMENT VIEW STARTS**************
        
        //********* FILED CASES **********************
        
        // ======= Filed Cases Management View =========
        $filedCasesMgtViewTodayCount = ReportCaseRegister::where('filing_date','=',$todatDate)
                                ->count();
        
        $filedCasesMgtViewThisMonthCount = ReportCaseRegister::where('f_month','=',$thisMonth)
                                ->where('f_year','=',$thisYear)->count();
        
        $filedCasesMgtViewThisYearCount = ReportCaseRegister::where('f_year','=',$thisYear)
                                ->count();
        
        
       $filedCasesMgtViewEfilingCount = Admission::all()->count();
       
        // ======= Filed Cases Management View End =========
        
        // ======= Pending Cases Management View =========
        $nullRows =  ReportCaseRegister::whereNull('proceeding_outcome')->count();
        $notNullRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->count();
        
        $pendingCasesMgtViewTodayCount = $nullRows + $notNullRows ;
        // ======= Pending Cases Management View End =========
        
        // ======= Decided Cases Management View =========
        $decidedCasesMgtViewTodayCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('last_coram_date','=',$todatDate)->count();
        
        $decidedCasesMgtViewThisMonthCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)
         ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedCasesMgtViewThisYearCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('stage_year','=',$thisYear)->count();
        // ======= Decided Cases Management View End =========
        
        //************* CHARTS MGT VIEW ****************
        //========== Charts Stats===================
        // --------- Filed Cases -----------------
        
        
       
        
        //=======Charts Ends =========================
        
        // ========Summary of filed case Categories in a month starts=========
        
        
        $filedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        
        
        
        $filedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        
        
        
        
        $filedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedmurderCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        $filedcorruptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->count();
        
        // ========Summary of filed selected cases in a month end =========
        
        
        // *********** PENDING CASES *************************
        // ========Summary of pending  case Categories starts=========
        
        //----------Calculate Pending Case Categories --------------
        
        $civilNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$civil)->count();
        $civilNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$civil)->count();
        
        $landNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$land)->count();
        $landNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$land)->count();
        
        $labourNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$labour)->count();
        $labourNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$labour)->count();
        
        $comNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$com)->count();
        $comNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$com)->count();
        
        $matrimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$matrm)->count();
        $matrimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$matrm)->count();
        
        
        
        
        $juvenileNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$juvenile)->count();
        $juvenileNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$juvenile)->count();
        
        $applicationNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$application)->count();
        $applicationNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$application)->count();
        
        $adoptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$adoption)->count();
        $adoptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$adoption)->count();
        
        $miscNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->count();
        $miscNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->count();
        
        $bacraptNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$bankruptcy)->count();
        $bacraptNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$bankruptcy)->count();
        
        
        
        
        $probatNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$probate)->count();
        $probatNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$probate)->count();
        
        $crimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$criminal)->count();
        $crimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$criminal)->count();
        
        $econNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$economic)->count();
        $econNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$economic)->count();
        
        $traffNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$traffic)->count();
        $traffNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$traffic)->count();
        
        $taxNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$tax)->count();
        $taxNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$tax)->count();
        
        $piNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$pi)->count();
        $piNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$pi)->count();
        
        $corruptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$corruption)->count();
        $corruptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$corruption)->count();
        
        //-------- Pending Results for Mgt View---------
        
        $civilPendingCatMgtCount = $civilNullCatMgtRows + $civilNotNullCatMgtRows;
        
        $landPendingCatMgtCount = $landNullCatMgtRows + $landNotNullCatMgtRows;
        
        $labourPendingCatMgtCount = $labourNullCatMgtRows + $labourNotNullCatMgtRows;
        
        $comPendingCatMgtCount = $comNullCatMgtRows + $comNotNullCatMgtRows;
        
        $matrimPendingCatMgtCount = $matrimNullCatMgtRows + $matrimNotNullCatMgtRows;
        
        
        
        $juvenilePendingCatMgtCount = $juvenileNullCatMgtRows + $juvenileNotNullCatMgtRows;
        
        $applicationPendingCatMgtCount = $applicationNullCatMgtRows + $applicationNotNullCatMgtRows;
        
        $adoptionPendingCatMgtCount = $adoptionNullCatMgtRows + $adoptionNotNullCatMgtRows;
        
        $miscPendingCatMgtCount = $miscNullCatMgtRows + $miscNotNullCatMgtRows;
        
        $bancraptPendingCatMgtCount = $bacraptNullCatMgtRows + $bacraptNotNullCatMgtRows;
        
        
        
        
        $probatPendingCatMgtCount = $probatNullCatMgtRows + $probatNotNullCatMgtRows;
        
        $crimPendingCatMgtCount = $crimNullCatMgtRows + $crimNotNullCatMgtRows;
        
        $econPendingCatMgtCount = $econNullCatMgtRows + $econNotNullCatMgtRows;
        
        $traffPendingCatMgtCount = $traffNullCatMgtRows + $traffNotNullCatMgtRows;
        
        $taxPendingCatMgtCount = $taxNullCatMgtRows + $taxNotNullCatMgtRows;
        
        $piPendingCatMgtCount = $piNullCatMgtRows + $piNotNullCatMgtRows;
        
        $corruptionPendingCatMgtCount = $corruptionNullCatMgtRows + $corruptionNotNullCatMgtRows;
        
         // ========Summary of pending case categories in a month Ends =========
        
        
        //----------Calculate Backlog of Cases by Categories --------------
        
        $civilBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$civil)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        
        $landBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$land)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $labourBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$labour)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $comBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$com)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $matrimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$matrm)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        
        
        $juvenileBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$juvenile)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $applicationBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$application)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $adoptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$adoption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $miscBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $bancraptBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        
        
        
        $probatBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$probate)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $crimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$criminal)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $econBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$economic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $traffBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$traffic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $taxBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$tax)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $piBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$pi)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        $corruptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$corruption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->count();
        
        // --------- Unassigned Cases ----------------------------------------
        
        $civilUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$civil)->count();
        
        $landUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$land)->count();
        
        $labourUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$labour)->count();
        
        $comUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$com)->count();
        
        $matrimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$matrm)->count();
        
        
        
        
        $juvenileUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$juvenile)->count();
        
        $applicationUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$application)->count();
        
        $adoptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$adoption)->count();
        
        $miscUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->count();
        
        $bancraptUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$bankruptcy)->count();
        
        
        
        
        
        $probatUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$probate)->count();
        
        $crimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$criminal)->count();
        
        $econUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$economic)->count();
        
        $traffUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$traffic)->count();
        
        $taxUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$tax)->count();
        
        $piUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$pi)->count();
        
        $corruptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$corruption)->count();
        
        
        
         //******************* DECIDED CASES ********************************
        
        // ========Summary of decided case Categories in a month starts=========
        
        $decidedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        
        
        
        
        $decidedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        
        
        
        
        $decidedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedpiCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        $decidedcorruptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        
        // ========Summary of decided selected cases in a month end =========
        
        
        //********** CASES ASSIGNMENT STARTS*****************
        $assignedCasesCount =  ReportCaseRegister::where('user_id', '=',$userId)
                    ->where('assign_month','=',$thisMonth)->where('assign_year','=',$thisYear)->count();
        
        $pendingAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '!=',$outcome)->count();
        
        $decidedAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        // ======= Perfomance Indicator Count ==========
        $decidedAnualCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('f_year','=',$thisYear)->count();
        
        $percentageHcJudge = $decidedAnualCount/220;
        
        $judges = Judge::all()->count();
                
        $workload = $pendingCasesMgtViewTodayCount/$judges;
       
        

            return view('dashboard.mgt_home', [
                'thisMonth' =>$thisMonth,
                'thisYear' =>$thisYear,
                
                'courtLevels' => $courtLevels,
                'courts' => $courts,
                
                'judges' => $judges,
                
                'workload' => $workload,
                
                'courtsCount' => $courtsCount,
                
                //===== Filed Cases =========
                 //--- MGT View -----------
                'filedCasesMgtViewTodayCount' => $filedCasesMgtViewTodayCount,
                'filedCasesMgtViewThisMonthCount' => $filedCasesMgtViewThisMonthCount,
                'filedCasesMgtViewThisYearCount' => $filedCasesMgtViewThisYearCount,
                
                'filedCasesMgtViewEfilingCount' => $filedCasesMgtViewEfilingCount,
                
                
                 //======Pending Cases ===========
                //--- MGT View -----------
                'pendingCasesMgtViewTodayCount' => $pendingCasesMgtViewTodayCount,
                
                
                //====== Decided Cases ===========
                //--- MGT View -----------
                'decidedCasesMgtViewTodayCount' => $decidedCasesMgtViewTodayCount,
                'decidedCasesMgtViewThisMonthCount' => $decidedCasesMgtViewThisMonthCount,
                'decidedCasesMgtViewThisYearCount' => $decidedCasesMgtViewThisYearCount,
                
                
                //=========Summary of Filed Cases Categories  =========
                //--- MGT View -----------
                'filedcivilCatMgtCount' =>$filedcivilCatMgtCount,
                'filedlandCatMgtCount' =>$filedlandCatMgtCount,
                'filedlaborCatMgtCount' =>$filedlaborCatMgtCount,
                'filedcomCatMgtCount' =>$filedcomCatMgtCount,
                'filedmatrCatMgtCount' =>$filedmatrCatMgtCount,
                
                'filedjuvenileCatMgtCount' =>$filedjuvenileCatMgtCount,
                'filedapplicationCatMgtCount' =>$filedapplicationCatMgtCount,
                'filedadoptionCatMgtCount' =>$filedadoptionCatMgtCount,
                'filedmiscCatMgtCount' =>$filedmiscCatMgtCount,
                'filedbancraptCatMgtCount' =>$filedbancraptCatMgtCount,
                
                
                'filedprobCatMgtCount' =>$filedprobCatMgtCount,
                'filedcrimCatMgtCount' =>$filedcrimCatMgtCount,
                'filedecoCatMgtCount' =>$filedecoCatMgtCount,
                'filedtraffCatMgtCount' =>$filedtraffCatMgtCount,
                'filedtaxCatMgtCount' => $filedtaxCatMgtCount,
                'filedmurderCatMgtCount' => $filedmurderCatMgtCount,
                'filedcorruptCatMgtCount' => $filedcorruptCatMgtCount,
                
                
                //=========Summary of Selected Pending Cases =========
                //--- MGT View -----------
                'civilPendingCatMgtCount' =>$civilPendingCatMgtCount,
                'landPendingCatMgtCount' =>$landPendingCatMgtCount,
                'labourPendingCatMgtCount' =>$labourPendingCatMgtCount,
                'comPendingCatMgtCount' =>$comPendingCatMgtCount,
                'matrimPendingCatMgtCount' =>$matrimPendingCatMgtCount,
                
                
                'juvenilePendingCatMgtCount' =>$juvenilePendingCatMgtCount,
                'applicationPendingCatMgtCount' =>$applicationPendingCatMgtCount,
                'adoptionPendingCatMgtCount' =>$adoptionPendingCatMgtCount,
                'miscPendingCatMgtCount' =>$miscPendingCatMgtCount,
                'bancraptPendingCatMgtCount' =>$bancraptPendingCatMgtCount,
                
                
                'probatPendingCatMgtCount' =>$probatPendingCatMgtCount,
                'crimPendingCatMgtCount' =>$crimPendingCatMgtCount,
                'econPendingCatMgtCount' =>$econPendingCatMgtCount,
                'traffPendingCatMgtCount' =>$traffPendingCatMgtCount,
                'taxPendingCatMgtCount' =>$taxPendingCatMgtCount,
                'piPendingCatMgtCount' =>$piPendingCatMgtCount,
                'corruptionPendingCatMgtCount' =>$corruptionPendingCatMgtCount,
                
                //=========Summary of Selected Backlog Cases =========
                //--- MGT View -----------
                'civilBacklogCatMgtCount' =>$civilBacklogCatMgtCount,
                'landBacklogCatMgtCount' =>$landBacklogCatMgtCount,
                'labourBacklogCatMgtCount' =>$labourBacklogCatMgtCount,
                'comBacklogCatMgtCount' =>$comBacklogCatMgtCount,
                'matrimBacklogCatMgtCount' =>$matrimBacklogCatMgtCount,
                
                
                'juvenileBacklogCatMgtCount' =>$juvenileBacklogCatMgtCount,
                'applicationBacklogCatMgtCount' =>$applicationBacklogCatMgtCount,
                'adoptionBacklogCatMgtCount' =>$adoptionBacklogCatMgtCount,
                'miscBacklogCatMgtCount' =>$miscBacklogCatMgtCount,
                'bancraptBacklogCatMgtCount' =>$bancraptBacklogCatMgtCount,
                
                
                'probatBacklogCatMgtCount' =>$probatBacklogCatMgtCount,
                'crimBacklogCatMgtCount' =>$crimBacklogCatMgtCount,
                'econBacklogCatMgtCount' =>$econBacklogCatMgtCount,
                'traffBacklogCatMgtCount' =>$traffBacklogCatMgtCount,
                'taxBacklogCatMgtCount' =>$taxBacklogCatMgtCount,
                'piBacklogCatMgtCount' =>$piBacklogCatMgtCount,
                'corruptionBacklogCatMgtCount' =>$corruptionBacklogCatMgtCount,
                
                
                //=========Summary of Selected Unasaaigned Cases =========
                //--- MGT View -----------
                'civilUnassignedCatMgtCount' =>$civilUnassignedCatMgtCount,
                'landUnassignedCatMgtCount' =>$landUnassignedCatMgtCount,
                'labourUnassignedCatMgtCount' =>$labourUnassignedCatMgtCount,
                'comUnassignedCatMgtCount' =>$comUnassignedCatMgtCount,
                'matrimUnassignedCatMgtCount' =>$matrimUnassignedCatMgtCount,
                
                'juvenileUnassignedCatMgtCount' =>$juvenileUnassignedCatMgtCount,
                'applicationUnassignedCatMgtCount' =>$applicationUnassignedCatMgtCount,
                'adoptionUnassignedCatMgtCount' =>$adoptionUnassignedCatMgtCount,
                'miscUnassignedCatMgtCount' =>$miscUnassignedCatMgtCount,
                'bancraptUnassignedCatMgtCount' =>$bancraptUnassignedCatMgtCount,
                
                
                'probatUnassignedCatMgtCount' =>$probatUnassignedCatMgtCount,
                'crimUnassignedCatMgtCount' =>$crimUnassignedCatMgtCount,
                'econUnassignedCatMgtCount' =>$econUnassignedCatMgtCount,
                'traffUnassignedCatMgtCount' =>$traffUnassignedCatMgtCount,
                'taxUnassignedCatMgtCount' =>$taxUnassignedCatMgtCount,
                'piUnassignedCatMgtCount' =>$piUnassignedCatMgtCount,
                'corruptionUnassignedCatMgtCount' =>$corruptionUnassignedCatMgtCount,
            
                
                //=========Summary of Selected Decided Cases =========
                //--- MGT View -----------
                'decidedcivilCatMgtCount' =>$decidedcivilCatMgtCount,
                'decidedlandCatMgtCount' =>$decidedlandCatMgtCount,
                'decidedlaborCatMgtCount' =>$decidedlaborCatMgtCount,
                'decidedcomCatMgtCount' =>$decidedcomCatMgtCount,
                'decidedmatrCatMgtCount' =>$decidedmatrCatMgtCount,
                
                'decidedjuvenileCatMgtCount' =>$decidedjuvenileCatMgtCount,
                'decidedapplicationCatMgtCount' =>$decidedapplicationCatMgtCount,
                'decidedadoptionCatMgtCount' =>$decidedadoptionCatMgtCount,
                'decidedmiscCatMgtCount' =>$decidedmiscCatMgtCount,
                'decidedbancraptCatMgtCount' =>$decidedbancraptCatMgtCount,
                
                
                'decidedprobCatMgtCount' =>$decidedprobCatMgtCount,
                'decidedcrimCatMgtCount' =>$decidedcrimCatMgtCount,
                'decidedecoCatMgtCount' =>$decidedecoCatMgtCount,
                'decidedtraffCatMgtCount' =>$decidedtraffCatMgtCount,
                'decidedtaxCatMgtCount' =>$decidedtaxCatMgtCount,
                'decidedpiCatMgtCount' =>$decidedpiCatMgtCount,
                'decidedcorruptionCatMgtCount' =>$decidedcorruptionCatMgtCount,
                
                
                //==== Assignment Cases ======
                'assignedCasesCount' =>$assignedCasesCount,
                'pendingAssignedCasesCount' =>$pendingAssignedCasesCount,
                'decidedAssignedCasesCount' =>$decidedAssignedCasesCount
                
            ]);
        } catch (\Exception $exception) {
            return back()->withWarning('The system time out to load data. Please refresh the page or contact System Administrator.!');
        }
    }
    
      /**
     * @param Request $request
     * @return string
     */
    public function searchMgtDashboard(Request $request)
    {
        
        
        
    try {
        
        
        $userId = Auth::user()->id;
        $courtId = $request->input('courts');
        $courtName = Court::find($courtId)->name;
        $todatDate = date('Y-m-d');
        $thisMonth = date('F');
        $thisYear = date('Y');
        $outcome = "decided";
        $backlog = "Backlog";
        
        // ===== Case  Category ========
        
        $civil = "Civil Case";
        $land = "Land Case";
        $labour = "Labour Case";
        $com = "Commercial Case";
        $matrm = "Matrimonial Cause";
        $probate = "Probate and Administration";
        $tax = "Taxation Cause";
        $criminal = "Criminal Case";
        $economic = "Economic Case";
        $traffic = "Traffic Case";
        $pi = "Preliminary Inquiry (PI)";
        $corruption = "Corruption Case";
        
        $courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        
        
        
        //*********** STATION VIEW STARTS**************
        
        //********* FILED CASES **********************
        
        // ======= Filed Cases Management View =========
        $filedCasesMgtViewTodayCount = ReportCaseRegister::where('filing_date','=',$todatDate)
                                ->where('court_id','=',$courtId)->count();
        
        $filedCasesMgtViewThisMonthCount = ReportCaseRegister::where('f_month','=',$thisMonth)
                                ->where('court_id','=',$courtId)->where('f_year','=',$thisYear)->count();
        
        $filedCasesMgtViewThisYearCount = ReportCaseRegister::where('f_year','=',$thisYear)
                                ->where('court_id','=',$courtId)->count();
        
        
       $filedCasesMgtViewEfilingCount = Admission::all()->where('court_id','=',$courtId)->count();
       
        // ======= Filed Cases Management View End =========
        
        // ======= Pending Cases Management View =========
        $nullRows =  ReportCaseRegister::whereNull('proceeding_outcome')->where('court_id','=',$courtId)->count();
        $notNullRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('court_id','=',$courtId)->count();
        
        $pendingCasesMgtViewTodayCount = $nullRows + $notNullRows ;
        // ======= Pending Cases Management View End =========
        
        // ======= Decided Cases Management View =========
        $decidedCasesMgtViewTodayCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('last_coram_date','=',$todatDate)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisMonthCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)
         ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisYearCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        // ======= Decided Cases Management View End =========
        
        //************* CHARTS MGT VIEW ****************
        //========== Charts Stats===================
        // --------- Filed Cases -----------------
        
        
       
        
        //=======Charts Ends =========================
        
         // ========Summary of filed case Categories in a month starts=========
        
         
        $filedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmurderCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcorruptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        // ========Summary of filed selected cases in a month end =========
        
        
        // *********** PENDING CASES *************************
        // ========Summary of pending  case Categories starts=========
        
        //----------Calculate Pending Case Categories --------------
        
        $civilNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        $civilNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        $landNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        $labourNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        $comNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        $matrimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        $juvenileNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        $applicationNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        $adoptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        $miscNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bacraptNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        $bacraptNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        $probatNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        $probatNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        $crimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        $econNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        $traffNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        $taxNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        $piNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        $corruptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        //-------- Pending Results for Mgt View---------
        
        $civilPendingCatMgtCount = $civilNullCatMgtRows + $civilNotNullCatMgtRows;
        
        $landPendingCatMgtCount = $landNullCatMgtRows + $landNotNullCatMgtRows;
        
        $labourPendingCatMgtCount = $labourNullCatMgtRows + $labourNotNullCatMgtRows;
        
        $comPendingCatMgtCount = $comNullCatMgtRows + $comNotNullCatMgtRows;
        
        $matrimPendingCatMgtCount = $matrimNullCatMgtRows + $matrimNotNullCatMgtRows;
        
        
        
        $juvenilePendingCatMgtCount = $juvenileNullCatMgtRows + $juvenileNotNullCatMgtRows;
        
        $applicationPendingCatMgtCount = $applicationNullCatMgtRows + $applicationNotNullCatMgtRows;
        
        $adoptionPendingCatMgtCount = $adoptionNullCatMgtRows + $adoptionNotNullCatMgtRows;
        
        $miscPendingCatMgtCount = $miscNullCatMgtRows + $miscNotNullCatMgtRows;
        
        $bancraptPendingCatMgtCount = $bacraptNullCatMgtRows + $bacraptNotNullCatMgtRows;
        
        
        
        $probatPendingCatMgtCount = $probatNullCatMgtRows + $probatNotNullCatMgtRows;
        
        $crimPendingCatMgtCount = $crimNullCatMgtRows + $crimNotNullCatMgtRows;
        
        $econPendingCatMgtCount = $econNullCatMgtRows + $econNotNullCatMgtRows;
        
        $traffPendingCatMgtCount = $traffNullCatMgtRows + $traffNotNullCatMgtRows;
        
        $taxPendingCatMgtCount = $taxNullCatMgtRows + $taxNotNullCatMgtRows;
        
        $piPendingCatMgtCount = $piNullCatMgtRows + $piNotNullCatMgtRows;
        
        $corruptionPendingCatMgtCount = $corruptionNullCatMgtRows + $corruptionNotNullCatMgtRows;
        
         // ========Summary of pending case categories in a month Ends =========
        
        
        //----------Calculate Backlog of Cases by Categories --------------
        
        $civilBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$civil)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        $landBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$land)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $labourBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$labour)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $comBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$com)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $matrimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$matrm)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$juvenile)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $applicationBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$application)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $adoptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$adoption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $miscBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $bancraptBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        
        $probatBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$probate)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $crimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$criminal)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $econBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$economic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $traffBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$traffic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $taxBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$tax)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $piBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$pi)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $corruptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$corruption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        // --------- Unassigned Cases ----------------------------------------
        
        $civilUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        
        $juvenileUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bancraptUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        
        
        $probatUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        
        
         //******************* DECIDED CASES ********************************
        
        // ========Summary of decided case Categories in a month starts=========
        
        $decidedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedpiCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcorruptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        // ========Summary of decided selected cases in a month end =========
        
        //********** CASES ASSIGNMENT STARTS*****************
        $assignedCasesCount =  ReportCaseRegister::where('user_id', '=',$userId)
                    ->where('assign_month','=',$thisMonth)->where('assign_year','=',$thisYear)->count();
        
        $pendingAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '!=',$outcome)->count();
        
        $decidedAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        // ======= Perfomance Indicator Count ==========
        $decidedAnualCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_year','=',$thisYear)->count();
        
        $percentageHcJudge = $decidedAnualCount/220;
        
        
       $judges = Judge::where('court_id','=',$courtId)->count();
                
        $workload = $pendingCasesMgtViewTodayCount/$judges;
        

            return view('dashboard.mgt_search_home', [
                'thisMonth' =>$thisMonth,
                'thisYear' =>$thisYear,
                
                'courtName' => $courtName,
                
                'courtLevels' => $courtLevels,
                'courts' => $courts,
                
                'judges' => $judges,
                
                'workload' => $workload,
                
                //===== Filed Cases =========
                 //--- MGT View -----------
                'filedCasesMgtViewTodayCount' => $filedCasesMgtViewTodayCount,
                'filedCasesMgtViewThisMonthCount' => $filedCasesMgtViewThisMonthCount,
                'filedCasesMgtViewThisYearCount' => $filedCasesMgtViewThisYearCount,
                
                'filedCasesMgtViewEfilingCount' => $filedCasesMgtViewEfilingCount,
                
                
                 //======Pending Cases ===========
                //--- MGT View -----------
                'pendingCasesMgtViewTodayCount' => $pendingCasesMgtViewTodayCount,
                
                
                //====== Decided Cases ===========
                //--- MGT View -----------
                'decidedCasesMgtViewTodayCount' => $decidedCasesMgtViewTodayCount,
                'decidedCasesMgtViewThisMonthCount' => $decidedCasesMgtViewThisMonthCount,
                'decidedCasesMgtViewThisYearCount' => $decidedCasesMgtViewThisYearCount,
                
                
                //=========Summary of Filed Cases Categories  =========
                //--- MGT View -----------
                'filedcivilCatMgtCount' =>$filedcivilCatMgtCount,
                'filedlandCatMgtCount' =>$filedlandCatMgtCount,
                'filedlaborCatMgtCount' =>$filedlaborCatMgtCount,
                'filedcomCatMgtCount' =>$filedcomCatMgtCount,
                'filedmatrCatMgtCount' =>$filedmatrCatMgtCount,
                
                'filedjuvenileCatMgtCount' =>$filedjuvenileCatMgtCount,
                'filedapplicationCatMgtCount' =>$filedapplicationCatMgtCount,
                'filedadoptionCatMgtCount' =>$filedadoptionCatMgtCount,
                'filedmiscCatMgtCount' =>$filedmiscCatMgtCount,
                'filedbancraptCatMgtCount' =>$filedbancraptCatMgtCount,
                
                
                'filedprobCatMgtCount' =>$filedprobCatMgtCount,
                'filedcrimCatMgtCount' =>$filedcrimCatMgtCount,
                'filedecoCatMgtCount' =>$filedecoCatMgtCount,
                'filedtraffCatMgtCount' =>$filedtraffCatMgtCount,
                'filedtaxCatMgtCount' => $filedtaxCatMgtCount,
                'filedmurderCatMgtCount' => $filedmurderCatMgtCount,
                'filedcorruptCatMgtCount' => $filedcorruptCatMgtCount,
                
                
                //=========Summary of Selected Pending Cases =========
                //--- MGT View -----------
                'civilPendingCatMgtCount' =>$civilPendingCatMgtCount,
                'landPendingCatMgtCount' =>$landPendingCatMgtCount,
                'labourPendingCatMgtCount' =>$labourPendingCatMgtCount,
                'comPendingCatMgtCount' =>$comPendingCatMgtCount,
                'matrimPendingCatMgtCount' =>$matrimPendingCatMgtCount,
                
                
                'juvenilePendingCatMgtCount' =>$juvenilePendingCatMgtCount,
                'applicationPendingCatMgtCount' =>$applicationPendingCatMgtCount,
                'adoptionPendingCatMgtCount' =>$adoptionPendingCatMgtCount,
                'miscPendingCatMgtCount' =>$miscPendingCatMgtCount,
                'bancraptPendingCatMgtCount' =>$bancraptPendingCatMgtCount,
                
                
                'probatPendingCatMgtCount' =>$probatPendingCatMgtCount,
                'crimPendingCatMgtCount' =>$crimPendingCatMgtCount,
                'econPendingCatMgtCount' =>$econPendingCatMgtCount,
                'traffPendingCatMgtCount' =>$traffPendingCatMgtCount,
                'taxPendingCatMgtCount' =>$taxPendingCatMgtCount,
                'piPendingCatMgtCount' =>$piPendingCatMgtCount,
                'corruptionPendingCatMgtCount' =>$corruptionPendingCatMgtCount,
                
                //=========Summary of Selected Backlog Cases =========
                //--- MGT View -----------
                'civilBacklogCatMgtCount' =>$civilBacklogCatMgtCount,
                'landBacklogCatMgtCount' =>$landBacklogCatMgtCount,
                'labourBacklogCatMgtCount' =>$labourBacklogCatMgtCount,
                'comBacklogCatMgtCount' =>$comBacklogCatMgtCount,
                'matrimBacklogCatMgtCount' =>$matrimBacklogCatMgtCount,
                
                
                'juvenileBacklogCatMgtCount' =>$juvenileBacklogCatMgtCount,
                'applicationBacklogCatMgtCount' =>$applicationBacklogCatMgtCount,
                'adoptionBacklogCatMgtCount' =>$adoptionBacklogCatMgtCount,
                'miscBacklogCatMgtCount' =>$miscBacklogCatMgtCount,
                'bancraptBacklogCatMgtCount' =>$bancraptBacklogCatMgtCount,
                
                
                'probatBacklogCatMgtCount' =>$probatBacklogCatMgtCount,
                'crimBacklogCatMgtCount' =>$crimBacklogCatMgtCount,
                'econBacklogCatMgtCount' =>$econBacklogCatMgtCount,
                'traffBacklogCatMgtCount' =>$traffBacklogCatMgtCount,
                'taxBacklogCatMgtCount' =>$taxBacklogCatMgtCount,
                'piBacklogCatMgtCount' =>$piBacklogCatMgtCount,
                'corruptionBacklogCatMgtCount' =>$corruptionBacklogCatMgtCount,
                
                
                //=========Summary of Selected Unasaaigned Cases =========
                //--- MGT View -----------
                'civilUnassignedCatMgtCount' =>$civilUnassignedCatMgtCount,
                'landUnassignedCatMgtCount' =>$landUnassignedCatMgtCount,
                'labourUnassignedCatMgtCount' =>$labourUnassignedCatMgtCount,
                'comUnassignedCatMgtCount' =>$comUnassignedCatMgtCount,
                'matrimUnassignedCatMgtCount' =>$matrimUnassignedCatMgtCount,
                
                'juvenileUnassignedCatMgtCount' =>$juvenileUnassignedCatMgtCount,
                'applicationUnassignedCatMgtCount' =>$applicationUnassignedCatMgtCount,
                'adoptionUnassignedCatMgtCount' =>$adoptionUnassignedCatMgtCount,
                'miscUnassignedCatMgtCount' =>$miscUnassignedCatMgtCount,
                'bancraptUnassignedCatMgtCount' =>$bancraptUnassignedCatMgtCount,
                
                
                'probatUnassignedCatMgtCount' =>$probatUnassignedCatMgtCount,
                'crimUnassignedCatMgtCount' =>$crimUnassignedCatMgtCount,
                'econUnassignedCatMgtCount' =>$econUnassignedCatMgtCount,
                'traffUnassignedCatMgtCount' =>$traffUnassignedCatMgtCount,
                'taxUnassignedCatMgtCount' =>$taxUnassignedCatMgtCount,
                'piUnassignedCatMgtCount' =>$piUnassignedCatMgtCount,
                'corruptionUnassignedCatMgtCount' =>$corruptionUnassignedCatMgtCount,
            
                
                //=========Summary of Selected Decided Cases =========
                //--- MGT View -----------
                'decidedcivilCatMgtCount' =>$decidedcivilCatMgtCount,
                'decidedlandCatMgtCount' =>$decidedlandCatMgtCount,
                'decidedlaborCatMgtCount' =>$decidedlaborCatMgtCount,
                'decidedcomCatMgtCount' =>$decidedcomCatMgtCount,
                'decidedmatrCatMgtCount' =>$decidedmatrCatMgtCount,
                
                'decidedjuvenileCatMgtCount' =>$decidedjuvenileCatMgtCount,
                'decidedapplicationCatMgtCount' =>$decidedapplicationCatMgtCount,
                'decidedadoptionCatMgtCount' =>$decidedadoptionCatMgtCount,
                'decidedmiscCatMgtCount' =>$decidedmiscCatMgtCount,
                'decidedbancraptCatMgtCount' =>$decidedbancraptCatMgtCount,
                
                
                'decidedprobCatMgtCount' =>$decidedprobCatMgtCount,
                'decidedcrimCatMgtCount' =>$decidedcrimCatMgtCount,
                'decidedecoCatMgtCount' =>$decidedecoCatMgtCount,
                'decidedtraffCatMgtCount' =>$decidedtraffCatMgtCount,
                'decidedtaxCatMgtCount' =>$decidedtaxCatMgtCount,
                'decidedpiCatMgtCount' =>$decidedpiCatMgtCount,
                'decidedcorruptionCatMgtCount' =>$decidedcorruptionCatMgtCount,
                
                
                //==== Assignment Cases ======
                'assignedCasesCount' =>$assignedCasesCount,
                'pendingAssignedCasesCount' =>$pendingAssignedCasesCount,
                'decidedAssignedCasesCount' =>$decidedAssignedCasesCount
                
            ]);
        } catch (\Exception $exception) {
            return back()->withWarning('The system time out to load data. Please refresh the page or contact System Administrator.!');
        }
    }
    
     /**
     * @param Request $request
     * @return string
     */
    public function getStationDashboard(Request $request)
    {
         
    try {
        $userId = Auth::user()->id;
        $courtId = Auth::user()->court_id;
        $courtName = Court::find($courtId)->name;
        $todatDate = date('Y-m-d');
        $thisMonth = date('F');
        $thisYear = date('Y');
        $outcome = "decided";
        $backlog = "Backlog";
        
        $user = Auth::user();
        
        $role = $user->getRoleNames()->first();
        
        // ===== Case  Category ========
        
        $civil = "Civil Case";
        $land = "Land Case";
        $labour = "Labour Case";
        $com = "Commercial Case";
        $matrm = "Matrimonial Cause";
        
        $juvenile = "Juvenile Civil Case";
        $application = "Application";
        $adoption = "Adoption";
        $miscellaneousCivilCause = "Miscellaneous Civil Cause";
        $bankruptcy = "Bankruptcy";
        
        $probate = "Probate and Administration";
        $tax = "Taxation Cause";
        $criminal = "Criminal Case";
        $economic = "Economic Case";
        $traffic = "Traffic Case";
        $pi = "Preliminary Inquiry (PI)";
        $corruption = "Corruption Case";
        
        
        
        $courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        
        
        //*********** STATION VIEW STARTS**************
        
        //********* FILED CASES **********************
        
        // ======= Filed Cases Management View =========
        $filedCasesMgtViewTodayCount = ReportCaseRegister::where('filing_date','=',$todatDate)
                                ->where('court_id','=',$courtId)->count();
        
        $filedCasesMgtViewThisMonthCount = ReportCaseRegister::where('f_month','=',$thisMonth)
                                ->where('court_id','=',$courtId)->where('f_year','=',$thisYear)->count();
        
        $filedCasesMgtViewThisYearCount = ReportCaseRegister::where('f_year','=',$thisYear)
                                ->where('court_id','=',$courtId)->count();
        
        
       $filedCasesMgtViewEfilingCount = Admission::all()->where('court_id','=',$courtId)->count();
       
        // ======= Filed Cases Management View End =========
        
        // ======= Pending Cases Management View =========
        $nullRows =  ReportCaseRegister::whereNull('proceeding_outcome')->where('court_id','=',$courtId)->count();
        $notNullRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('court_id','=',$courtId)->count();
        
        $pendingCasesMgtViewTodayCount = $nullRows + $notNullRows ;
        // ======= Pending Cases Management View End =========
        
        // ======= Decided Cases Management View =========
        $decidedCasesMgtViewTodayCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('last_coram_date','=',$todatDate)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisMonthCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)
         ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisYearCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        // ======= Decided Cases Management View End =========
        
        //************* CHARTS MGT VIEW ****************
        //========== Charts Stats===================
        // --------- Filed Cases -----------------
        
        
       
        
        //=======Charts Ends =========================
        
        // ========Summary of filed case Categories in a month starts=========
        
         
        $filedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmurderCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcorruptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        // ========Summary of filed selected cases in a month end =========
        
        
        // *********** PENDING CASES *************************
        // ========Summary of pending  case Categories starts=========
        
        //----------Calculate Pending Case Categories --------------
        
        $civilNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        $civilNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        $landNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        $labourNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        $comNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        $matrimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        $juvenileNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        $applicationNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        $adoptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        $miscNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bacraptNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        $bacraptNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        $probatNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        $probatNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        $crimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        $econNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        $traffNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        $taxNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        $piNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        $corruptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        //-------- Pending Results for Mgt View---------
        
        $civilPendingCatMgtCount = $civilNullCatMgtRows + $civilNotNullCatMgtRows;
        
        $landPendingCatMgtCount = $landNullCatMgtRows + $landNotNullCatMgtRows;
        
        $labourPendingCatMgtCount = $labourNullCatMgtRows + $labourNotNullCatMgtRows;
        
        $comPendingCatMgtCount = $comNullCatMgtRows + $comNotNullCatMgtRows;
        
        $matrimPendingCatMgtCount = $matrimNullCatMgtRows + $matrimNotNullCatMgtRows;
        
        
        
        $juvenilePendingCatMgtCount = $juvenileNullCatMgtRows + $juvenileNotNullCatMgtRows;
        
        $applicationPendingCatMgtCount = $applicationNullCatMgtRows + $applicationNotNullCatMgtRows;
        
        $adoptionPendingCatMgtCount = $adoptionNullCatMgtRows + $adoptionNotNullCatMgtRows;
        
        $miscPendingCatMgtCount = $miscNullCatMgtRows + $miscNotNullCatMgtRows;
        
        $bancraptPendingCatMgtCount = $bacraptNullCatMgtRows + $bacraptNotNullCatMgtRows;
        
        
        
        $probatPendingCatMgtCount = $probatNullCatMgtRows + $probatNotNullCatMgtRows;
        
        $crimPendingCatMgtCount = $crimNullCatMgtRows + $crimNotNullCatMgtRows;
        
        $econPendingCatMgtCount = $econNullCatMgtRows + $econNotNullCatMgtRows;
        
        $traffPendingCatMgtCount = $traffNullCatMgtRows + $traffNotNullCatMgtRows;
        
        $taxPendingCatMgtCount = $taxNullCatMgtRows + $taxNotNullCatMgtRows;
        
        $piPendingCatMgtCount = $piNullCatMgtRows + $piNotNullCatMgtRows;
        
        $corruptionPendingCatMgtCount = $corruptionNullCatMgtRows + $corruptionNotNullCatMgtRows;
        
         // ========Summary of pending case categories in a month Ends =========
        
        
        //----------Calculate Backlog of Cases by Categories --------------
        
        $civilBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$civil)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        $landBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$land)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $labourBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$labour)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $comBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$com)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $matrimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$matrm)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$juvenile)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $applicationBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$application)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $adoptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$adoption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $miscBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $bancraptBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        
        $probatBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$probate)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $crimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$criminal)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $econBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$economic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $traffBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$traffic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $taxBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$tax)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $piBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$pi)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $corruptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$corruption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        // --------- Unassigned Cases ----------------------------------------
        
        $civilUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        
        $juvenileUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bancraptUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        
        
        $probatUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        
        
         //******************* DECIDED CASES ********************************
        
        // ========Summary of decided case Categories in a month starts=========
        
        $decidedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedpiCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcorruptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        // ========Summary of decided selected cases in a month end =========
        
        
        //********** CASES ASSIGNMENT STARTS*****************
        $assignedCasesCount =  ReportCaseRegister::where('user_id', '=',$userId)
                    ->where('assign_month','=',$thisMonth)->where('assign_year','=',$thisYear)->count();
        
        $pendingAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '!=',$outcome)->count();
        
        $decidedAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        // ======= Perfomance Indicator Count ==========
        $decidedAnualCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_year','=',$thisYear)->count();
        
        $percentageHcJudge = $decidedAnualCount/220;
        
        $judges = Judge::where('court_id','=',$courtId)->count();
        
        $workload = $pendingCasesMgtViewTodayCount/$judges;
       
        

            return view('dashboard.station_home', [
                'thisMonth' =>$thisMonth,
                'thisYear' =>$thisYear,
                
                'userId' =>$userId, 
                
                
                'courtLevels' => $courtLevels,
                'courts' => $courts,
                
                'role' => $role,
                
                'courtName' => $courtName,
                
                'judges' => $judges,
                
                'workload' => $workload,
                
                //===== Filed Cases =========
                 //--- MGT View -----------
                'filedCasesMgtViewTodayCount' => $filedCasesMgtViewTodayCount,
                'filedCasesMgtViewThisMonthCount' => $filedCasesMgtViewThisMonthCount,
                'filedCasesMgtViewThisYearCount' => $filedCasesMgtViewThisYearCount,
                
                'filedCasesMgtViewEfilingCount' => $filedCasesMgtViewEfilingCount,
                
                
                 //======Pending Cases ===========
                //--- MGT View -----------
                'pendingCasesMgtViewTodayCount' => $pendingCasesMgtViewTodayCount,
                
                
                //====== Decided Cases ===========
                //--- MGT View -----------
                'decidedCasesMgtViewTodayCount' => $decidedCasesMgtViewTodayCount,
                'decidedCasesMgtViewThisMonthCount' => $decidedCasesMgtViewThisMonthCount,
                'decidedCasesMgtViewThisYearCount' => $decidedCasesMgtViewThisYearCount,
                
                
                //=========Summary of Filed Cases Categories  =========
                //--- MGT View -----------
                'filedcivilCatMgtCount' =>$filedcivilCatMgtCount,
                'filedlandCatMgtCount' =>$filedlandCatMgtCount,
                'filedlaborCatMgtCount' =>$filedlaborCatMgtCount,
                'filedcomCatMgtCount' =>$filedcomCatMgtCount,
                'filedmatrCatMgtCount' =>$filedmatrCatMgtCount,
                
                'filedjuvenileCatMgtCount' =>$filedjuvenileCatMgtCount,
                'filedapplicationCatMgtCount' =>$filedapplicationCatMgtCount,
                'filedadoptionCatMgtCount' =>$filedadoptionCatMgtCount,
                'filedmiscCatMgtCount' =>$filedmiscCatMgtCount,
                'filedbancraptCatMgtCount' =>$filedbancraptCatMgtCount,
                
                
                'filedprobCatMgtCount' =>$filedprobCatMgtCount,
                'filedcrimCatMgtCount' =>$filedcrimCatMgtCount,
                'filedecoCatMgtCount' =>$filedecoCatMgtCount,
                'filedtraffCatMgtCount' =>$filedtraffCatMgtCount,
                'filedtaxCatMgtCount' => $filedtaxCatMgtCount,
                'filedmurderCatMgtCount' => $filedmurderCatMgtCount,
                'filedcorruptCatMgtCount' => $filedcorruptCatMgtCount,
                
                
                //=========Summary of Selected Pending Cases =========
                //--- MGT View -----------
                'civilPendingCatMgtCount' =>$civilPendingCatMgtCount,
                'landPendingCatMgtCount' =>$landPendingCatMgtCount,
                'labourPendingCatMgtCount' =>$labourPendingCatMgtCount,
                'comPendingCatMgtCount' =>$comPendingCatMgtCount,
                'matrimPendingCatMgtCount' =>$matrimPendingCatMgtCount,
                
                
                'juvenilePendingCatMgtCount' =>$juvenilePendingCatMgtCount,
                'applicationPendingCatMgtCount' =>$applicationPendingCatMgtCount,
                'adoptionPendingCatMgtCount' =>$adoptionPendingCatMgtCount,
                'miscPendingCatMgtCount' =>$miscPendingCatMgtCount,
                'bancraptPendingCatMgtCount' =>$bancraptPendingCatMgtCount,
                
                
                'probatPendingCatMgtCount' =>$probatPendingCatMgtCount,
                'crimPendingCatMgtCount' =>$crimPendingCatMgtCount,
                'econPendingCatMgtCount' =>$econPendingCatMgtCount,
                'traffPendingCatMgtCount' =>$traffPendingCatMgtCount,
                'taxPendingCatMgtCount' =>$taxPendingCatMgtCount,
                'piPendingCatMgtCount' =>$piPendingCatMgtCount,
                'corruptionPendingCatMgtCount' =>$corruptionPendingCatMgtCount,
                
                //=========Summary of Selected Backlog Cases =========
                //--- MGT View -----------
                'civilBacklogCatMgtCount' =>$civilBacklogCatMgtCount,
                'landBacklogCatMgtCount' =>$landBacklogCatMgtCount,
                'labourBacklogCatMgtCount' =>$labourBacklogCatMgtCount,
                'comBacklogCatMgtCount' =>$comBacklogCatMgtCount,
                'matrimBacklogCatMgtCount' =>$matrimBacklogCatMgtCount,
                
                
                'juvenileBacklogCatMgtCount' =>$juvenileBacklogCatMgtCount,
                'applicationBacklogCatMgtCount' =>$applicationBacklogCatMgtCount,
                'adoptionBacklogCatMgtCount' =>$adoptionBacklogCatMgtCount,
                'miscBacklogCatMgtCount' =>$miscBacklogCatMgtCount,
                'bancraptBacklogCatMgtCount' =>$bancraptBacklogCatMgtCount,
                
                
                'probatBacklogCatMgtCount' =>$probatBacklogCatMgtCount,
                'crimBacklogCatMgtCount' =>$crimBacklogCatMgtCount,
                'econBacklogCatMgtCount' =>$econBacklogCatMgtCount,
                'traffBacklogCatMgtCount' =>$traffBacklogCatMgtCount,
                'taxBacklogCatMgtCount' =>$taxBacklogCatMgtCount,
                'piBacklogCatMgtCount' =>$piBacklogCatMgtCount,
                'corruptionBacklogCatMgtCount' =>$corruptionBacklogCatMgtCount,
                
                
                //=========Summary of Selected Unasaaigned Cases =========
                //--- MGT View -----------
                'civilUnassignedCatMgtCount' =>$civilUnassignedCatMgtCount,
                'landUnassignedCatMgtCount' =>$landUnassignedCatMgtCount,
                'labourUnassignedCatMgtCount' =>$labourUnassignedCatMgtCount,
                'comUnassignedCatMgtCount' =>$comUnassignedCatMgtCount,
                'matrimUnassignedCatMgtCount' =>$matrimUnassignedCatMgtCount,
                
                'juvenileUnassignedCatMgtCount' =>$juvenileUnassignedCatMgtCount,
                'applicationUnassignedCatMgtCount' =>$applicationUnassignedCatMgtCount,
                'adoptionUnassignedCatMgtCount' =>$adoptionUnassignedCatMgtCount,
                'miscUnassignedCatMgtCount' =>$miscUnassignedCatMgtCount,
                'bancraptUnassignedCatMgtCount' =>$bancraptUnassignedCatMgtCount,
                
                
                'probatUnassignedCatMgtCount' =>$probatUnassignedCatMgtCount,
                'crimUnassignedCatMgtCount' =>$crimUnassignedCatMgtCount,
                'econUnassignedCatMgtCount' =>$econUnassignedCatMgtCount,
                'traffUnassignedCatMgtCount' =>$traffUnassignedCatMgtCount,
                'taxUnassignedCatMgtCount' =>$taxUnassignedCatMgtCount,
                'piUnassignedCatMgtCount' =>$piUnassignedCatMgtCount,
                'corruptionUnassignedCatMgtCount' =>$corruptionUnassignedCatMgtCount,
            
                
                //=========Summary of Selected Decided Cases =========
                //--- MGT View -----------
                'decidedcivilCatMgtCount' =>$decidedcivilCatMgtCount,
                'decidedlandCatMgtCount' =>$decidedlandCatMgtCount,
                'decidedlaborCatMgtCount' =>$decidedlaborCatMgtCount,
                'decidedcomCatMgtCount' =>$decidedcomCatMgtCount,
                'decidedmatrCatMgtCount' =>$decidedmatrCatMgtCount,
                
                'decidedjuvenileCatMgtCount' =>$decidedjuvenileCatMgtCount,
                'decidedapplicationCatMgtCount' =>$decidedapplicationCatMgtCount,
                'decidedadoptionCatMgtCount' =>$decidedadoptionCatMgtCount,
                'decidedmiscCatMgtCount' =>$decidedmiscCatMgtCount,
                'decidedbancraptCatMgtCount' =>$decidedbancraptCatMgtCount,
                
                
                'decidedprobCatMgtCount' =>$decidedprobCatMgtCount,
                'decidedcrimCatMgtCount' =>$decidedcrimCatMgtCount,
                'decidedecoCatMgtCount' =>$decidedecoCatMgtCount,
                'decidedtraffCatMgtCount' =>$decidedtraffCatMgtCount,
                'decidedtaxCatMgtCount' =>$decidedtaxCatMgtCount,
                'decidedpiCatMgtCount' =>$decidedpiCatMgtCount,
                'decidedcorruptionCatMgtCount' =>$decidedcorruptionCatMgtCount,
                
                
                //==== Assignment Cases ======
                'assignedCasesCount' =>$assignedCasesCount,
                'pendingAssignedCasesCount' =>$pendingAssignedCasesCount,
                'decidedAssignedCasesCount' =>$decidedAssignedCasesCount
                
            ]);
        } catch (\Exception $exception) {
            return back()->withWarning('The system time out to load data. Please refresh the page or contact System Administrator.!');
        }
    }
    
    
    
    /**
     * @param Request $request
     * @return string
     */
    public function getStakeholderDashboard(Request $request)
    {
   
            return view('dashboard.stakeholder_home');
    }
    
    
    
     public function searchStationDashboard(Request $request)
    {
         
    try {
        $userId = Auth::user()->id;
        $courtId = $request->input('courts');
        $courtName = Court::find($courtId)->name;
        $todatDate = date('Y-m-d');
        $thisMonth = date('F');
        $thisYear = date('Y');
        $outcome = "decided";
        $backlog = "Backlog";
        
        $user = Auth::user();
        
        $role = $user->getRoleNames()->first();
        
        // ===== Case  Category ========
        
        $civil = "Civil Case";
        $land = "Land Case";
        $labour = "Labour Case";
        $com = "Commercial Case";
        $matrm = "Matrimonial Cause";
        
        $juvenile = "Juvenile Civil Case";
        $application = "Application";
        $adoption = "Adoption";
        $miscellaneousCivilCause = "Miscellaneous Civil Cause";
        $bankruptcy = "Bankruptcy";
        
        $probate = "Probate and Administration";
        $tax = "Taxation Cause";
        $criminal = "Criminal Case";
        $economic = "Economic Case";
        $traffic = "Traffic Case";
        $pi = "Preliminary Inquiry (PI)";
        $corruption = "Corruption Case";
        
        
        
        $courtLevels = CourtLevel::all()->pluck('name', 'id');
        $courts = Court::all()->pluck('name', 'id');
        
        
        //*********** STATION VIEW STARTS**************
        
        //********* FILED CASES **********************
        
        // ======= Filed Cases Management View =========
        $filedCasesMgtViewTodayCount = ReportCaseRegister::where('filing_date','=',$todatDate)
                                ->where('court_id','=',$courtId)->count();
        
        $filedCasesMgtViewThisMonthCount = ReportCaseRegister::where('f_month','=',$thisMonth)
                                ->where('court_id','=',$courtId)->where('f_year','=',$thisYear)->count();
        
        $filedCasesMgtViewThisYearCount = ReportCaseRegister::where('f_year','=',$thisYear)
                                ->where('court_id','=',$courtId)->count();
        
        
       $filedCasesMgtViewEfilingCount = Admission::all()->where('court_id','=',$courtId)->count();
       
        // ======= Filed Cases Management View End =========
        
        // ======= Pending Cases Management View =========
        $nullRows =  ReportCaseRegister::whereNull('proceeding_outcome')->where('court_id','=',$courtId)->count();
        $notNullRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('court_id','=',$courtId)->count();
        
        $pendingCasesMgtViewTodayCount = $nullRows + $notNullRows ;
        // ======= Pending Cases Management View End =========
        
        // ======= Decided Cases Management View =========
        $decidedCasesMgtViewTodayCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('last_coram_date','=',$todatDate)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisMonthCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)
         ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedCasesMgtViewThisYearCount = ReportCaseRegister::where('proceeding_outcome', '=',$outcome)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        // ======= Decided Cases Management View End =========
        
        //************* CHARTS MGT VIEW ****************
        //========== Charts Stats===================
        // --------- Filed Cases -----------------
        
        
       
        
        //=======Charts Ends =========================
        
        // ========Summary of filed case Categories in a month starts=========
        
         
        $filedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $filedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedmurderCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $filedcorruptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('f_month','=',$thisMonth)->where('f_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        // ========Summary of filed selected cases in a month end =========
        
        
        // *********** PENDING CASES *************************
        // ========Summary of pending  case Categories starts=========
        
        //----------Calculate Pending Case Categories --------------
        
        $civilNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        $civilNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        $landNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        $labourNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        $comNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        $matrimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        $juvenileNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        $applicationNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        $adoptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        $miscNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bacraptNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        $bacraptNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        $probatNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        $probatNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        $crimNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        $econNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        $traffNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        $taxNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        $piNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionNullCatMgtRows =  ReportCaseRegister::whereNull('proceeding_outcome')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        $corruptionNotNullCatMgtRows = ReportCaseRegister::where('proceeding_outcome', '!=',$outcome)
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        //-------- Pending Results for Mgt View---------
        
        $civilPendingCatMgtCount = $civilNullCatMgtRows + $civilNotNullCatMgtRows;
        
        $landPendingCatMgtCount = $landNullCatMgtRows + $landNotNullCatMgtRows;
        
        $labourPendingCatMgtCount = $labourNullCatMgtRows + $labourNotNullCatMgtRows;
        
        $comPendingCatMgtCount = $comNullCatMgtRows + $comNotNullCatMgtRows;
        
        $matrimPendingCatMgtCount = $matrimNullCatMgtRows + $matrimNotNullCatMgtRows;
        
        
        
        $juvenilePendingCatMgtCount = $juvenileNullCatMgtRows + $juvenileNotNullCatMgtRows;
        
        $applicationPendingCatMgtCount = $applicationNullCatMgtRows + $applicationNotNullCatMgtRows;
        
        $adoptionPendingCatMgtCount = $adoptionNullCatMgtRows + $adoptionNotNullCatMgtRows;
        
        $miscPendingCatMgtCount = $miscNullCatMgtRows + $miscNotNullCatMgtRows;
        
        $bancraptPendingCatMgtCount = $bacraptNullCatMgtRows + $bacraptNotNullCatMgtRows;
        
        
        
        $probatPendingCatMgtCount = $probatNullCatMgtRows + $probatNotNullCatMgtRows;
        
        $crimPendingCatMgtCount = $crimNullCatMgtRows + $crimNotNullCatMgtRows;
        
        $econPendingCatMgtCount = $econNullCatMgtRows + $econNotNullCatMgtRows;
        
        $traffPendingCatMgtCount = $traffNullCatMgtRows + $traffNotNullCatMgtRows;
        
        $taxPendingCatMgtCount = $taxNullCatMgtRows + $taxNotNullCatMgtRows;
        
        $piPendingCatMgtCount = $piNullCatMgtRows + $piNotNullCatMgtRows;
        
        $corruptionPendingCatMgtCount = $corruptionNullCatMgtRows + $corruptionNotNullCatMgtRows;
        
         // ========Summary of pending case categories in a month Ends =========
        
        
        //----------Calculate Backlog of Cases by Categories --------------
        
        $civilBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$civil)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        $landBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$land)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $labourBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$labour)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $comBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$com)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $matrimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$matrm)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        $juvenileBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$juvenile)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $applicationBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$application)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $adoptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$adoption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $miscBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $bancraptBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        
        
        
        $probatBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$probate)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $crimBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$criminal)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $econBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$economic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $traffBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$traffic)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $taxBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$tax)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $piBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$pi)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        $corruptionBacklogCatMgtCount =  ReportCaseRegister::where('case_type_category', '=',$corruption)
                                    ->where('CaseAgeBacklog', '=',$backlog)->where('court_id','=',$courtId)->count();
        
        // --------- Unassigned Cases ----------------------------------------
        
        $civilUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$civil)->where('court_id','=',$courtId)->count();
        
        $landUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$land)->where('court_id','=',$courtId)->count();
        
        $labourUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$labour)->where('court_id','=',$courtId)->count();
        
        $comUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$com)->where('court_id','=',$courtId)->count();
        
        $matrimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$matrm)->where('court_id','=',$courtId)->count();
        
        
        
        
        $juvenileUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$juvenile)->where('court_id','=',$courtId)->count();
        
        $applicationUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$application)->where('court_id','=',$courtId)->count();
        
        $adoptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$adoption)->where('court_id','=',$courtId)->count();
        
        $miscUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$miscellaneousCivilCause)->where('court_id','=',$courtId)->count();
        
        $bancraptUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$bankruptcy)->where('court_id','=',$courtId)->count();
        
        
        
        
        
        $probatUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$probate)->where('court_id','=',$courtId)->count();
        
        $crimUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$criminal)->where('court_id','=',$courtId)->count();
        
        $econUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$economic)->where('court_id','=',$courtId)->count();
        
        $traffUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$traffic)->where('court_id','=',$courtId)->count();
        
        $taxUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$tax)->where('court_id','=',$courtId)->count();
        
        $piUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$pi)->where('court_id','=',$courtId)->count();
        
        $corruptionUnassignedCatMgtCount =  ReportCaseRegister::whereNull('judge_name')
                                ->where('case_type_category', '=',$corruption)->where('court_id','=',$courtId)->count();
        
        
        
         //******************* DECIDED CASES ********************************
        
        // ========Summary of decided case Categories in a month starts=========
        
        $decidedcivilCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$civil)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlandCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$land)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedlaborCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$labour)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcomCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$com)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmatrCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$matrm)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedjuvenileCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$juvenile)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedapplicationCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$application)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedadoptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$adoption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedmiscCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$miscellaneousCivilCause)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedbancraptCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$bankruptcy)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        
        $decidedprobCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$probate)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcrimCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$criminal)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedecoCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$economic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtraffCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$traffic)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedtaxCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$tax)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedpiCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$pi)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        $decidedcorruptionCatMgtCount = ReportCaseRegister::where('case_type_category', '=',$corruption)
         ->where('proceeding_outcome', '=',$outcome)->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->where('court_id','=',$courtId)->count();
        
        
        // ========Summary of decided selected cases in a month end =========
        
        
        //********** CASES ASSIGNMENT STARTS*****************
        $assignedCasesCount =  ReportCaseRegister::where('user_id', '=',$userId)
                    ->where('assign_month','=',$thisMonth)->where('assign_year','=',$thisYear)->count();
        
        $pendingAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '!=',$outcome)->count();
        
        $decidedAssignedCasesCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_month','=',$thisMonth)->where('stage_year','=',$thisYear)->count();
        
        // ======= Perfomance Indicator Count ==========
        $decidedAnualCount = ReportCaseRegister::where('user_id', '=',$userId)
            ->where('proceeding_outcome', '=',$outcome)
            ->where('stage_year','=',$thisYear)->count();
        
        $percentageHcJudge = $decidedAnualCount/220;
        
        
       $judges = Judge::where('court_id','=',$courtId)->count();
        
       $workload = $pendingCasesMgtViewTodayCount/$judges;
        
        

            return view('dashboard.station_search_home', [
                'thisMonth' =>$thisMonth,
                'thisYear' =>$thisYear,
                
                'courtLevels' => $courtLevels,
                'courts' => $courts,
                
                'role' => $role,
                
                'courtName' => $courtName,
                
                'judges' => $judges,
                
                'workload' => $workload,
                
                //===== Filed Cases =========
                 //--- MGT View -----------
                'filedCasesMgtViewTodayCount' => $filedCasesMgtViewTodayCount,
                'filedCasesMgtViewThisMonthCount' => $filedCasesMgtViewThisMonthCount,
                'filedCasesMgtViewThisYearCount' => $filedCasesMgtViewThisYearCount,
                
                'filedCasesMgtViewEfilingCount' => $filedCasesMgtViewEfilingCount,
                
                
                 //======Pending Cases ===========
                //--- MGT View -----------
                'pendingCasesMgtViewTodayCount' => $pendingCasesMgtViewTodayCount,
                
                
                //====== Decided Cases ===========
                //--- MGT View -----------
                'decidedCasesMgtViewTodayCount' => $decidedCasesMgtViewTodayCount,
                'decidedCasesMgtViewThisMonthCount' => $decidedCasesMgtViewThisMonthCount,
                'decidedCasesMgtViewThisYearCount' => $decidedCasesMgtViewThisYearCount,
                
                
                //=========Summary of Filed Cases Categories  =========
                //--- MGT View -----------
                'filedcivilCatMgtCount' =>$filedcivilCatMgtCount,
                'filedlandCatMgtCount' =>$filedlandCatMgtCount,
                'filedlaborCatMgtCount' =>$filedlaborCatMgtCount,
                'filedcomCatMgtCount' =>$filedcomCatMgtCount,
                'filedmatrCatMgtCount' =>$filedmatrCatMgtCount,
                
                'filedjuvenileCatMgtCount' =>$filedjuvenileCatMgtCount,
                'filedapplicationCatMgtCount' =>$filedapplicationCatMgtCount,
                'filedadoptionCatMgtCount' =>$filedadoptionCatMgtCount,
                'filedmiscCatMgtCount' =>$filedmiscCatMgtCount,
                'filedbancraptCatMgtCount' =>$filedbancraptCatMgtCount,
                
                
                'filedprobCatMgtCount' =>$filedprobCatMgtCount,
                'filedcrimCatMgtCount' =>$filedcrimCatMgtCount,
                'filedecoCatMgtCount' =>$filedecoCatMgtCount,
                'filedtraffCatMgtCount' =>$filedtraffCatMgtCount,
                'filedtaxCatMgtCount' => $filedtaxCatMgtCount,
                'filedmurderCatMgtCount' => $filedmurderCatMgtCount,
                'filedcorruptCatMgtCount' => $filedcorruptCatMgtCount,
                
                
                //=========Summary of Selected Pending Cases =========
                //--- MGT View -----------
                'civilPendingCatMgtCount' =>$civilPendingCatMgtCount,
                'landPendingCatMgtCount' =>$landPendingCatMgtCount,
                'labourPendingCatMgtCount' =>$labourPendingCatMgtCount,
                'comPendingCatMgtCount' =>$comPendingCatMgtCount,
                'matrimPendingCatMgtCount' =>$matrimPendingCatMgtCount,
                
                
                'juvenilePendingCatMgtCount' =>$juvenilePendingCatMgtCount,
                'applicationPendingCatMgtCount' =>$applicationPendingCatMgtCount,
                'adoptionPendingCatMgtCount' =>$adoptionPendingCatMgtCount,
                'miscPendingCatMgtCount' =>$miscPendingCatMgtCount,
                'bancraptPendingCatMgtCount' =>$bancraptPendingCatMgtCount,
                
                
                'probatPendingCatMgtCount' =>$probatPendingCatMgtCount,
                'crimPendingCatMgtCount' =>$crimPendingCatMgtCount,
                'econPendingCatMgtCount' =>$econPendingCatMgtCount,
                'traffPendingCatMgtCount' =>$traffPendingCatMgtCount,
                'taxPendingCatMgtCount' =>$taxPendingCatMgtCount,
                'piPendingCatMgtCount' =>$piPendingCatMgtCount,
                'corruptionPendingCatMgtCount' =>$corruptionPendingCatMgtCount,
                
                //=========Summary of Selected Backlog Cases =========
                //--- MGT View -----------
                'civilBacklogCatMgtCount' =>$civilBacklogCatMgtCount,
                'landBacklogCatMgtCount' =>$landBacklogCatMgtCount,
                'labourBacklogCatMgtCount' =>$labourBacklogCatMgtCount,
                'comBacklogCatMgtCount' =>$comBacklogCatMgtCount,
                'matrimBacklogCatMgtCount' =>$matrimBacklogCatMgtCount,
                
                
                'juvenileBacklogCatMgtCount' =>$juvenileBacklogCatMgtCount,
                'applicationBacklogCatMgtCount' =>$applicationBacklogCatMgtCount,
                'adoptionBacklogCatMgtCount' =>$adoptionBacklogCatMgtCount,
                'miscBacklogCatMgtCount' =>$miscBacklogCatMgtCount,
                'bancraptBacklogCatMgtCount' =>$bancraptBacklogCatMgtCount,
                
                
                'probatBacklogCatMgtCount' =>$probatBacklogCatMgtCount,
                'crimBacklogCatMgtCount' =>$crimBacklogCatMgtCount,
                'econBacklogCatMgtCount' =>$econBacklogCatMgtCount,
                'traffBacklogCatMgtCount' =>$traffBacklogCatMgtCount,
                'taxBacklogCatMgtCount' =>$taxBacklogCatMgtCount,
                'piBacklogCatMgtCount' =>$piBacklogCatMgtCount,
                'corruptionBacklogCatMgtCount' =>$corruptionBacklogCatMgtCount,
                
                
                //=========Summary of Selected Unasaaigned Cases =========
                //--- MGT View -----------
                'civilUnassignedCatMgtCount' =>$civilUnassignedCatMgtCount,
                'landUnassignedCatMgtCount' =>$landUnassignedCatMgtCount,
                'labourUnassignedCatMgtCount' =>$labourUnassignedCatMgtCount,
                'comUnassignedCatMgtCount' =>$comUnassignedCatMgtCount,
                'matrimUnassignedCatMgtCount' =>$matrimUnassignedCatMgtCount,
                
                'juvenileUnassignedCatMgtCount' =>$juvenileUnassignedCatMgtCount,
                'applicationUnassignedCatMgtCount' =>$applicationUnassignedCatMgtCount,
                'adoptionUnassignedCatMgtCount' =>$adoptionUnassignedCatMgtCount,
                'miscUnassignedCatMgtCount' =>$miscUnassignedCatMgtCount,
                'bancraptUnassignedCatMgtCount' =>$bancraptUnassignedCatMgtCount,
                
                
                'probatUnassignedCatMgtCount' =>$probatUnassignedCatMgtCount,
                'crimUnassignedCatMgtCount' =>$crimUnassignedCatMgtCount,
                'econUnassignedCatMgtCount' =>$econUnassignedCatMgtCount,
                'traffUnassignedCatMgtCount' =>$traffUnassignedCatMgtCount,
                'taxUnassignedCatMgtCount' =>$taxUnassignedCatMgtCount,
                'piUnassignedCatMgtCount' =>$piUnassignedCatMgtCount,
                'corruptionUnassignedCatMgtCount' =>$corruptionUnassignedCatMgtCount,
            
                
                //=========Summary of Selected Decided Cases =========
                //--- MGT View -----------
                'decidedcivilCatMgtCount' =>$decidedcivilCatMgtCount,
                'decidedlandCatMgtCount' =>$decidedlandCatMgtCount,
                'decidedlaborCatMgtCount' =>$decidedlaborCatMgtCount,
                'decidedcomCatMgtCount' =>$decidedcomCatMgtCount,
                'decidedmatrCatMgtCount' =>$decidedmatrCatMgtCount,
                
                'decidedjuvenileCatMgtCount' =>$decidedjuvenileCatMgtCount,
                'decidedapplicationCatMgtCount' =>$decidedapplicationCatMgtCount,
                'decidedadoptionCatMgtCount' =>$decidedadoptionCatMgtCount,
                'decidedmiscCatMgtCount' =>$decidedmiscCatMgtCount,
                'decidedbancraptCatMgtCount' =>$decidedbancraptCatMgtCount,
                
                
                'decidedprobCatMgtCount' =>$decidedprobCatMgtCount,
                'decidedcrimCatMgtCount' =>$decidedcrimCatMgtCount,
                'decidedecoCatMgtCount' =>$decidedecoCatMgtCount,
                'decidedtraffCatMgtCount' =>$decidedtraffCatMgtCount,
                'decidedtaxCatMgtCount' =>$decidedtaxCatMgtCount,
                'decidedpiCatMgtCount' =>$decidedpiCatMgtCount,
                'decidedcorruptionCatMgtCount' =>$decidedcorruptionCatMgtCount,
                
                
                //==== Assignment Cases ======
                'assignedCasesCount' =>$assignedCasesCount,
                'pendingAssignedCasesCount' =>$pendingAssignedCasesCount,
                'decidedAssignedCasesCount' =>$decidedAssignedCasesCount
                
            ]);
        } catch (\Exception $exception) {
            return back()->withWarning('The system time out to load data. Please refresh the page or contact System Administrator.!');
        }
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resetPassword(Request $request){
        if($request->isMethod('get')){
            return view('reset-password');
        }

        return redirect('login');
    }
    
     public function extrUserResetPassword(Request $request)
     {
         echo "Nafika";exit;
        if($request->isMethod('get')){
            return view('reset-password');
        }

        return redirect('login');
    }
}
