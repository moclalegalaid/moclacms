<?php

namespace App\Http\Controllers;

use App\News;
use App\User;
use App\WebDocument;
use App\Comment;
use App\SiteLink;
use App\Court;
use App\ProvidedAid;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WebContentController extends Controller
{

// ---- News Functions Starts------

    public function getNewsIndex()
    {  
        $category = "News";
    $news = News::where('category','=',$category)->orderBy('created_at', 'desc')->get();
        
        
        return view('admin.web_contents.news.index', [
            'news' => $news,
            'category' => [
                'General' => 'General',
                'Important' => 'Important',
            ]
            
        ]);
        
    }
    
    public function addNews()
    {   
        
            return view('admin.web_contents.news.add', [
                'category' => [
                'General' => 'General',
                'Important' => 'Important',
            ],
        ]);
 
    }
    
    
    
     public function submitNews(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
        $category = "News";
       
        
        $post  = new News;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->status = $request->input('category');
        $post->reporter = $name;
        $post->category = $category;
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->save();
         
        $news_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $news_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $newsUpdate = DB::table('news')
                        ->where('id','=',$news_id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('news')
                ->with('success', 'News posted successfully.');
    }
    
     public function editNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['title' => $request->input('title'), 
                                  'content' => $request->input('content'),
                                  'status' => $request->input('category'),
                                  'editor' => $name,
                                  'publish' => $publish,
                                  'date_edited' => $datetime
                                 ]);
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
            
            
             $attachementUpdate = DB::table('web_documents')
                        ->where('news_id','=',$id )
                        ->update(['file' => $fileNameToStore
                                 ]);
            
              $fileName = $fileNameToStore;
            
              // Update news table with end target year
             
             $fileUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('news')
                ->with('success', 'News updated successfully.');
      
     }
    
    
    
     public function publishNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['publisher' => $name,
                                  'publish' => $publish,
                                  'date_published' => $datetime
                                 ]);
        
        

         return redirect('news')
                ->with('success', 'News published successfully.');
      
     }
    
    
     public function deleteNews($id)
    {  
        $new = News::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('news')
                ->with('success', 'Post deleted successfully.');
      
     }
    
// ---- News Functions Ends------


// ---- Adverts Functions Starts------

public function getPostIndex()
{  
    $category = "Adverts";
    $news = News::where('category','=',$category)->orderBy('created_at', 'desc')->get();
    
    
    return view('admin.web_contents.adverts.index', [
        'news' => $news,
        'category' => [
            'Tender' => 'Tender',
            'Others' => 'Others',
        ]
        
    ]);
    
}

public function addPost()
{   
    
        return view('admin.web_contents.adverts.add', [
            'category' => [
                'Tender' => 'Tender',
                'Others' => 'Others',
        ],
    ]);

}



 public function submitPost(Request $request)
{   
    
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "0";
    $category = "Adverts";
   
    
    $post  = new News;
    $post->title = $request->input('title');
    $post->content = $request->input('content');
    $post->status = $request->input('category');
    $post->reporter = $name;
    $post->category = $category;
    $post->publish = $publish;
    $post->date_posted = $datetime;
    $post->save();
     
    $news_id = $post->id;
    
    // upload File
    
    if($request->hasFile('file')) {
             
          $attachement = new WebDocument;
          $filenameWithExt = $request->file('file')->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $request->file('file')->getClientOriginalExtension();
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          $request->file('file')->storeAs('public/downloads', $fileNameToStore);
          
          $attachement->news_id = $news_id;
          $attachement->file = $fileNameToStore;
          $attachement->save();
        
          $fileName = $attachement->file;
        
          // Update news table with file name
         
         $newsUpdate = DB::table('news')
                    ->where('id','=',$news_id )
                    ->update(['file_name' => $fileName]);
        }


     return redirect('adverts')
            ->with('success', 'Advert added successfully.');
}

 public function editPost(Request $request, $id)
{  
  
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "0";
      
     
    $newsUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['title' => $request->input('title'), 
                              'content' => $request->input('content'),
                              'status' => $request->input('category'),
                              'editor' => $name,
                              'publish' => $publish,
                              'date_edited' => $datetime
                             ]);
    
    // upload file
    
    if($request->hasFile('file')) {
             
          $filenameWithExt = $request->file('file')->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $request->file('file')->getClientOriginalExtension();
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          $request->file('file')->storeAs('public/downloads', $fileNameToStore);
        
        
         $attachementUpdate = DB::table('web_documents')
                    ->where('news_id','=',$id )
                    ->update(['file' => $fileNameToStore
                             ]);
        
          $fileName = $fileNameToStore;
        
          // Update news table with file name
         
         $fileUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['file_name' => $fileName]);
        }


     return redirect('adverts')
            ->with('success', 'Advert updated successfully.');
  
 }



 public function publishPost(Request $request, $id)
{  
  
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "1";
      
     
    $newsUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['publisher' => $name,
                              'publish' => $publish,
                              'date_published' => $datetime
                             ]);
    
    

     return redirect('adverts')
            ->with('success', 'Advert published successfully.');
  
 }


 public function deletePost($id)
{  
    $new = News::find($id);
    if(!$new) {
        return back()->withError("There is no post with ID $id in your scope");
    }

    $new->delete();

     return redirect('adverts')
            ->with('success', 'Advert deleted successfully.');
  
 }

// ---- Adverts Functions Ends------


// ---- Doduments Functions Starts------

public function getDocIndex()
{  
    $category = "Docs";
    $news = News::where('category','=',$category)->orderBy('created_at', 'desc')->get();
    
    
    return view('admin.web_contents.documents.index', [
        'news' => $news,
        'category' => [
            'Policy' => 'Policy',
            'Act' => 'Act',
            'Constitution' => 'Constitution',
            'Instrument' => 'Instrument',
            'Guideline' => 'Guideline',
            'Regulation' => 'Regulation',
            'Circular' => 'Circular',
            'Form' => 'Form',
            'Program' => 'Program',
            'Workplan' => 'Workplan',
            'Press' => 'Press',
            'Speach' => 'Speach',
            'Project' => 'Project',
            'Report' => 'Report',
            'Anouncement' => 'Anouncement',
        ]
        
    ]);
    
}

public function addDoc()
{   
    
        return view('admin.web_contents.documents.add', [
            'category' => [
                'Policy' => 'Policy',
                'Act' => 'Act',
                'Constitution' => 'Constitution',
                'Instrument' => 'Instrument',
                'Guideline' => 'Guideline',
                'Regulation' => 'Regulation',
                'Circular' => 'Circular',
                'Form' => 'Form',
                'Program' => 'Program',
                'Workplan' => 'Workplan',
                'Press' => 'Press',
                'Speach' => 'Speach',
                'Project' => 'Project',
                'Report' => 'Report',
                'Anouncement' => 'Anouncement',
        ],
    ]);

}



 public function submitDoc(Request $request)
{   
    
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "0";
    $category = "Docs";
   
    
    $post  = new News;
    $post->title = $request->input('title');
    $post->content = $request->input('content');
    $post->status = $request->input('category');
    $post->reporter = $name;
    $post->category = $category;
    $post->publish = $publish;
    $post->date_posted = $datetime;
    $post->save();
     
    $news_id = $post->id;
    
    // upload File
    
    if($request->hasFile('file')) {
             
          $attachement = new WebDocument;
          $filenameWithExt = $request->file('file')->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $request->file('file')->getClientOriginalExtension();
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          $request->file('file')->storeAs('public/downloads', $fileNameToStore);
          
          $attachement->news_id = $news_id;
          $attachement->file = $fileNameToStore;
          $attachement->save();
        
          $fileName = $attachement->file;
        
          // Update news table with file name
         
         $newsUpdate = DB::table('news')
                    ->where('id','=',$news_id )
                    ->update(['file_name' => $fileName]);
        }


     return redirect('documents')
            ->with('success', 'Document added successfully.');
}

 public function editDoc(Request $request, $id)
{  
  
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "0";
      
     
    $newsUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['title' => $request->input('title'), 
                              'content' => $request->input('content'),
                              'status' => $request->input('category'),
                              'editor' => $name,
                              'publish' => $publish,
                              'date_edited' => $datetime
                             ]);
    
    // upload file
    
    if($request->hasFile('file')) {
             
          $filenameWithExt = $request->file('file')->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
          $extension = $request->file('file')->getClientOriginalExtension();
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          $request->file('file')->storeAs('public/downloads', $fileNameToStore);
        
        
         $attachementUpdate = DB::table('web_documents')
                    ->where('news_id','=',$id )
                    ->update(['file' => $fileNameToStore
                             ]);
        
          $fileName = $fileNameToStore;
        
          // Update news table with file name
         
         $fileUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['file_name' => $fileName]);
        }


     return redirect('documents')
            ->with('success', 'Document updated successfully.');
  
 }



 public function publishDoc(Request $request, $id)
{  
  
    $datetime = date('Y-m-d H:i:s');
    $userId = Auth::user()->id;
    $name = User::find($userId)->name;
    $publish = "1";
      
     
    $newsUpdate = DB::table('news')
                    ->where('id','=',$id )
                    ->update(['publisher' => $name,
                              'publish' => $publish,
                              'date_published' => $datetime
                             ]);
    
    

     return redirect('documents')
            ->with('success', 'Document published successfully.');
  
 }


 public function deleteDoc($id)
{  
    $new = News::find($id);
    if(!$new) {
        return back()->withError("There is no post with ID $id in your scope");
    }

    $new->delete();

     return redirect('documents')
            ->with('success', 'Document deleted successfully.');
  
 }

// ---- Doduments Functions Ends------


// ---- Site links Functions Starts------

public function getSiteIndex()
{  
    
    $sites = SiteLink::orderBy('created_at', 'desc')->get();
    
    
    return view('admin.web_contents.links.index', [
        'sites' => $sites,
    ]);
    
}

public function addSite()
{   
    
     return view('admin.web_contents.links.add');

}

 public function submitSite(Request $request)
{   
   
    $site  = new SiteLink;
    $site->name = $request->input('name');
    $site->link = $request->input('link');
    $site->save();


     return redirect('links')
            ->with('success', 'Site added successfully.');
}

 public function deleteSite($id)
{  
    $site = SiteLink::find($id);
    $site->delete();

     return redirect('links')
            ->with('success', 'Site deleted successfully.');
  
 }

// ---- SIte Links Functions Ends------


// ---- Photo Gallery Functions Starts------

public function getGalleryIndex()
{  
    $category = "News";
    $galleries = News::where('category', '=', $category)->orderBy('created_at', 'desc')->get();

    return view('admin.web_contents.photo.index', [
        'galleries' => $galleries,
    ]);
    
}

// ---- Photo Gallery Functions Ends------


    
     public function postComment(Request $request, $id)
    {  
       $news_id = $id; 
       $datetime = date('Y-m-d H:i:s');
          
         
        $comment  = new Comment;
        $comment->news_id = $news_id;
        $comment->username = $request->input('username');
        $comment->comment = $request->input('comment');
        $comment->posted_at = $datetime;
        $comment->save();

         return redirect('/')
                ->with('success', 'Comment posted successfully.');
      
     }
    
    
    
    public function getMediaIndex()
    {  
        $category = "Media";
        $news = DB::table('news')->where('category','=',$category)->orderBy('created_at', 'desc')->get();
        
        
        return view('admin.web_contents.media.index', [
            'news' => $news,
            'category' => [
                'Audio' => 'Audio',
                'Video' => 'Video',
            ]
            
        ]);
        
    }
    
    
     public function submitMedia(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
        $category = "Media";
       
        
        $post  = new News;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->status = $request->input('category');
        $post->video_id = $request->input('video_id');
        $post->category = $category;
        $post->reporter = $name;
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->save();
         
        $news_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $news_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $newsUpdate = DB::table('news')
                        ->where('id','=',$news_id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('media')
                ->with('success', 'Media posted successfully.');
    }
    
    
}
