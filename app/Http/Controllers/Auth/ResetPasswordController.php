<?php

namespace App\Http\Controllers\Auth;
use App\Mail\ForgotPasswordMail;
use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function ResetPassword(Request $request)
    {
        request()->validate([
        'email' => 'required',
        ]);

        // check if email exists
        
        $checkEmail = User::where("email", $request->input('email'))->first();
        
          if ($checkEmail) {
            echo "Email ipo";exit();
            return redirect()->intended('auth/dashboard');
          }else{
            return redirect()->back()->withErrors('Email does not exist!');
          }
       
    }

}
