<?php

namespace App\Http\Controllers;

use App\Background;
use App\Faq;
use App\User;
use App\News;
use App\WebDocument;
use App\Management;
use App\Structure;
use App\Service;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AppSettingController extends Controller
{
    public function getHomeIndex()
    {  
        $bgtitle = "Background";
        $vstitle = "Vission";
        $mstitle = "Mission";
        $board = "Legal Aid Board";
        $reg = "Registrar";
        $areg = "Assistant Registrar";

        
        $how = "How";
        $what = "What";
        
        $background = Background::where('title','=',$bgtitle)->get();
        
        $vission = Background::where('title','=',$vstitle)->get();
        
        $mission = Background::where('title','=',$mstitle)->get();
        
        $howStatement = Faq::where('title','=',$how)->get();
        
        $whatStatement = Faq::where('title','=',$what)->get();

        $leaders = Management::all();

        $board = Background::where('title','=',$board)->get();

        $reg = Background::where('title','=',$reg)->get();

        $areg = Background::where('title','=',$areg)->get();
        
        
        return view('admin.app_settings.home_page.index', [
            'board' => $board,
            'reg' => $reg,
            'areg' => $areg,
            'background' => $background,
            'vission' => $vission,
            'mission' => $mission,
            'howStatement' => $howStatement,
            'whatStatement' => $whatStatement,
            'leaders' => $leaders,
            'category' => [
                'General' => 'General',
                'Important' => 'Important',
            ],

            'group' => [
                'Top Leader' => 'Top Leader',
                'Legal Aid Board' => 'Legal Aid Board',
                'Registrar' => 'Registrar',
                'Assistant Registrar' => 'Assistant Registrar',
            ],

            'seniority' => [
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
            ]
            
        ]);
        
    }
    
    
     public function saveBackground(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Background";
       
        $content = DB::table('backgrounds')
                        ->where('title','=',$title)->count();
         
        
         
         
        if($content > 0){
            
            return redirect('home-page')
                ->with('success', 'Background information exists.'); 
            
        }else{
            $background  = new Background;
            $background->title = $title;
            $background->content = $request->input('background');
            $background->user = $name;
            $background->save();
         

         return redirect('home-page')
                ->with('success', 'Background saved successfully.'); 
            
        }
        
        
    }
    
    
    public function saveMission(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Mission";
       
        $content = DB::table('backgrounds')
                        ->where('title','=',$title)->count();
         
        
         
         
        if($content > 0){
            
            return redirect('home-page')
                ->with('success', 'Mission statement exists.'); 
            
        }else{
            $background  = new Background;
            $background->title = $title;
            $background->content = $request->input('background');
            $background->user = $name;
            $background->save();
         

         return redirect('home-page')
                ->with('success', 'Mission statement saved successfully.'); 
            
        }
        
        
    }
    
    
    public function saveVission(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Vission";
       
        $content = DB::table('backgrounds')
                        ->where('title','=',$title)->count();
         
        
         
         
        if($content > 0){
            
            return redirect('home-page')
                ->with('success', 'Vission statement exists.'); 
            
        }else{
            $background  = new Background;
            $background->title = $title;
            $background->content = $request->input('background');
            $background->user = $name;
            $background->save();
         

         return redirect('home-page')
                ->with('success', 'Vission statement saved successfully.'); 
            
        }
        
        
    }


    public function saveAdmin(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = $request->input('group');
       
      
            $background  = new Background;
            $background->title = $title;
            $background->content = $request->input('content');
            $background->user = $name;
            $background->save();
         

         return redirect('home-page')
                ->with('success', 'Administration statement saved successfully.'); 
      
        
        
    }
    
     public function editBg(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Background";
          
         
        $newsUpdate = DB::table('backgrounds')
                        ->where('id','=',$id )
                        ->update(['title' => $title, 
                                  'content' => $request->input('background'),
                                  'user' => $name,
                                 ]);

         return redirect('home-page')
                ->with('success', 'Background information updated successfully.');
      
     }
    
    
     public function deleteBg($id)
    {  
        $bg = Background::find($id);
        
        $bg->delete();

         return redirect('home-page')
                ->with('success', 'Background information deleted successfully.');
      
     }
    
    
       public function editMs(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Mission";
          
         
        $newsUpdate = DB::table('backgrounds')
                        ->where('id','=',$id )
                        ->update(['title' => $title, 
                                  'content' => $request->input('background'),
                                  'user' => $name,
                                 ]);

         return redirect('home-page')
                ->with('success', 'Mission statement updated successfully.');
      
     }
    
    
     public function deleteMs($id)
    {  
        $bg = Background::find($id);
        
        $bg->delete();

         return redirect('home-page')
                ->with('success', 'Mission statement deleted successfully.');
      
     }
    
    
       public function editVs(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "Vission";
          
         
        $newsUpdate = DB::table('backgrounds')
                        ->where('id','=',$id )
                        ->update(['title' => $title, 
                                  'content' => $request->input('background'),
                                  'user' => $name,
                                 ]);

         return redirect('home-page')
                ->with('success', 'Vission statement updated successfully.');
      
     }
    
    
     public function deleteVs($id)
    {  
        $bg = Background::find($id);
        
        $bg->delete();

         return redirect('home-page')
                ->with('success', 'Vission statement deleted successfully.');
      
     }
    
    
    
        public function saveHow(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "How";
       
        
        $how  = new Faq;
        $how->title = $title;
        $how->question = $request->input('question');
        $how->answer = $request->input('answer');
        $how->user = $name;
        $how->save();
         

         return redirect('home-page')
                ->with('success', 'How I statement saved successfully.'); 
             
        
    }
    
    
   
        public function saveWhat(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $title = "What";
       
        
        $how  = new Faq;
        $how->title = $title;
        $how->question = $request->input('question');
        $how->answer = $request->input('answer');
        $how->user = $name;
        $how->save();
         

         return redirect('home-page')
                ->with('success', 'What Is statement saved successfully.'); 
             
        
    } 

    public function addLeader(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
       
        
        $post  = new Management;
        $post->name = $request->input('name');
        $post->title = $request->input('title');
        $post->profile = $request->input('profile');
        $post->seniority = $request->input('seniority');
        $post->groups = $request->input('group');
        $post->reporter = $name;
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->save();
         
        $leader_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $leader_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $leaderUpdate = DB::table('managements')
                        ->where('id','=',$leader_id )
                        ->update(['file' => $fileName]);
            }


         return redirect('home-page')
                ->with('success', 'Team member added successfully.');
    }


    public function publishMg(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
          
         
        $newsUpdate = DB::table('managements')
                        ->where('id','=',$id )
                        ->update(['publisher' => $name,
                                  'publish' => $publish,
                                  'date_published' => $datetime
                                 ]);
        
        

         return redirect('home-page')
                ->with('success', 'Image published successfully.');
      
     }
    
    
     public function deleteMg($id)
    {  
        $new = Management::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('home-page')
                ->with('success', 'Image deleted successfully.');
      
     }
    
    

     public function getStructureIndex()
    {  
    
        $structure = Structure::all();
        
        
        return view('admin.app_settings.org_structure.index', [
            'structure' => $structure,
            'type' => [
                'Directorate' => 'Directorate',
                'Registrar' => 'Registrar',
            ],
        ]);
        
    }



    public function addStr(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
       
        
        $post  = new Structure;
        $post->reporter = $name;
        $post->type = $request->input('type');
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->save();
         
        $str_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $str_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $strUpdate = DB::table('structures')
                        ->where('id','=',$str_id )
                        ->update(['file' => $fileName]);
            }


         return redirect('structure')
                ->with('success', 'Organisation structure added successfully.');
    }


    public function publishStr(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
          
         
        $newsUpdate = DB::table('structures')
                        ->where('id','=',$id )
                        ->update(['publisher' => $name,
                                  'publish' => $publish,
                                  'date_published' => $datetime
                                 ]);
        
        

         return redirect('structure')
                ->with('success', 'Structure published successfully.');
      
     }
    
    
     public function deleteStr($id)
    {  
        $new = Structure::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('structure')
                ->with('success', 'Structure deleted successfully.');
      
     }



     public function getServiceIndex()
    {  
    
        $service = News::where('title', '=', "Service")->get();
        
        //echo $service;exit();
        return view('admin.app_settings.services.index', [
            'service' => $service,
        ]);
        
    }



    public function addSrv(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
        $title = "Service";
       
        
        $post  = new News;
        $post->title = $title;
        $post->content = $request->input('content');
        $post->status = $request->input('subtype');
        $post->reporter = $name;
        $post->category = $request->input('type');;
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->date_published = $datetime;
        $post->publisher = $name;
        $post->save();
         
        $news_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $news_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $newsUpdate = DB::table('news')
                        ->where('id','=',$news_id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('service')
                ->with('success', 'Service added successfully.');
    }

    
     public function deleteSrv($id)
    {  
        $new = Service::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('service')
                ->with('success', 'Structure deleted successfully.');
      
     }
    
    
}
