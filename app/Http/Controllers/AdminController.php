<?php

namespace App\Http\Controllers;

use App\Court;
use App\ProvidedAid;

use Illuminate\Http\Request;
use KadekSystemInfo;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = Auth::user();
        $role = $user->getRoleNames()->first();

         //echo $role;exit;

        $list = ['Desk Officer'];

        if(in_array($role,$list)){  
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;
        
            $legalAidTodayCount = ProvidedAid::where('register_date','=',$todatDate)->where('court_id','=',$courtId)->count();
            $legalAidThisMonthCount = ProvidedAid::where('reg_month','=',$thisMonth)->where('court_id','=',$courtId)->count();
            $legalAidThisYearCount = ProvidedAid::where('reg_year','=',$thisYear)->where('court_id','=',$courtId)->count();
            $legalAidDeskCount = Court::where('status','=',$status)->count();

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('dashboard', [
                'legalAidTodayCount' => $legalAidTodayCount,
                'legalAidThisMonthCount' => $legalAidThisMonthCount,
                'legalAidThisYearCount' => $legalAidThisYearCount,
                'legalAidDeskCount' => $legalAidDeskCount,
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                ]
            );    
        } else { 
            
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;
        
            $legalAidTodayCount = ProvidedAid::where('register_date','=',$todatDate)->count();
            $legalAidThisMonthCount = ProvidedAid::where('reg_month','=',$thisMonth)->count();
            $legalAidThisYearCount = ProvidedAid::where('reg_year','=',$thisYear)->count();
            $legalAidDeskCount = Court::where('status','=',$status)->count();

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('home', [
                'legalAidTodayCount' => $legalAidTodayCount,
                'legalAidThisMonthCount' => $legalAidThisMonthCount,
                'legalAidThisYearCount' => $legalAidThisYearCount,
                'legalAidDeskCount' => $legalAidDeskCount,
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                ]
            );          
        }

    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminAidToday()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_today', [
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }


    
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserAidToday()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_today', [
                'legalAidToday' => $legalAidToday,
                ]
            );  

    }


     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminAidThisMonth()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_thismonth', [
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }


    
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserAidThisMonth()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_thismonth', [
                'legalAidThisMonth' => $legalAidThisMonth,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminAidThisYear()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_thisyear', [
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserAidThisYear()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->where('court_id','=',$courtId)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_thisyear', [
                'legalAidThisYear' => $legalAidThisYear,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAidDesk()
    {    
            $userId = Auth::user()->id;
            $courtId = Auth::user()->court_id;
            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            $status = "active";
            //echo $role;exit;

            $legalAidToday = ProvidedAid::where('register_date','=',$todatDate)->orderBy('created_at','desc')->get();
            $legalAidThisMonth = ProvidedAid::where('reg_month','=',$thisMonth)->orderBy('created_at','desc')->get();
            $legalAidThisYear = ProvidedAid::where('reg_year','=',$thisYear)->orderBy('created_at','desc')->get();
            $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();

            return view('admin.home.admin.aid_desk', [
                'legalAidToday' => $legalAidToday,
                'legalAidThisMonth' => $legalAidThisMonth,
                'legalAidThisYear' => $legalAidThisYear,
                'legalAidDesk' => $legalAidDesk,
                'thisMonth' => $thisMonth,
                'thisYear' => $thisYear,
                ]
            );  

    }

    /**
     * Show system information.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSysInfo()
    {
        $sysInfo = new KadekSystemInfo();
        $info = $sysInfo->getSystemInfo('array');

        return view('systeminfo')->with($info);
    }
}
