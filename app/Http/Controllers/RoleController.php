<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $roles = Role::where('name','<>','super-admin')->orderBy('name', 'asc')->get();
        return view('users.roles2.index', ['roles' => $roles ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function addRole(Request $request){
        if($request->isMethod("get")){
            return view("users.roles2.add");
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $role = Role::firstOrCreate([
            "name"=> $request->input("name"),
        ]);

        $permissions = $request->input("permissions");
        if(count($permissions)>0){
            $role->givePermissionTo($permissions);
        }
        return redirect("users/roles")->withSuccess('Role created successfully');
    }

    public function editRole(Request $request, $role_id){
        $role = Role::find($role_id);
        if(!$role){
            return redirect('users/roles')->withError('Role not found');
        }

        $currentPermissions = $role->permissions()->pluck('name')->toArray();
        if($request->isMethod("get")){
            return view("users.roles2.edit",[
                'role'=>$role,
                'permissions'=> $currentPermissions,
            ]);
        }

        $role->name = $request->input('name');
        $role->save();

        //Revoke all the current permissions
        foreach ($currentPermissions As $permission){
            $role->revokePermissionTo($permission);
        }

        $permissions = $request->input("permissions");

        if(count($permissions)>0){
            $role->givePermissionTo($permissions);
        }
        return redirect("users/roles")->withSuccess('Role edited successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteRole($id){
        $role = Role::find($id);
        if($role){
            $role->delete();
            return redirect("users/roles")->withSuccess('Role deleted successfully');
        }
        return redirect("users/roles")->withError('Role not found');
    }
}
