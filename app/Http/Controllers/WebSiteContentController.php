<?php

namespace App\Http\Controllers;

use App\News;
use App\User;
use App\WebDocument;
use App\Background;
use App\Management;
use App\Service;
use App\SiteLink;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WebSiteContentController extends Controller
{
    public function getBackground()
    {  
        $title = "Background";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.home.background', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }
    
    
    public function getVission()
    {  
        $title = "Vission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.home.vission', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }
    
    
    public function getMission()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.home.mission', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getBoard()
    {  
        $title = "Legal Aid Board";
        $board = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        //--- Get Management ----
        $publish = "1";
        $group = "Legal Aid Board";
        $management = Management::where('publish', '=', $publish)->where('groups', '=', $group)->orderBy('seniority', 'asc')->get();
        //--- Get News ----
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.administration.board', [
            'management' => $management,
            'board' => $board,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }
    

    public function getAregistrar()
    {  
        $title = "Assistant Registrar";
        $areg = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        //--- Get Management ----
        $publish = "1";
        $group = "Assistant Registrar";
        $mgt = Management::where('publish', '=', $publish)->where('groups', '=', $group)->orderBy('seniority', 'asc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.administration.a-registrar', [
            'areg' => $areg,
            'mgt' => $mgt,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getRegistrar()
    {  
        $title = "Registrar";
        $reg = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get Management ----
        $publish = "1";
        $group = "Registrar";
        $mgt = Management::where('publish', '=', $publish)->where('groups', '=', $group)->first();
        //echo $mgt;exit();
        //--- Get News ----
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.administration.registrar', [
            'reg' => $reg,
            'mgt' => $mgt,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }
    
    
    public function getProject()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Project";
        $project = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.funding.project', [
            'project' => $project,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getConstitution()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Constitution";
        $constitution = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.constitution', [
            'constitution' => $constitution,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }

    public function getPolicy()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Policy";
        $policy = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.policy', [
            'policy' => $policy,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getAct()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Act";
        $act = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.act', [
            'act' => $act,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getRegulation()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Regulation";
        $regulation = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.regulation', [
            'regulation' => $regulation,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getCircular()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Circular";
        $circular = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.circular', [
            'circular' => $circular,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getForm()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Form";
        $form = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.form', [
            'form' => $form,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getProgram()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Program";
        $program = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.program', [
            'program' => $program,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }

    public function getGuideline()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Guideline";
        $guideline = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.guideline', [
            'guideline' => $guideline,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getReport()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.report', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getWorkplan()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.document.workplan', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getRegOrganisation()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Registration of Organisation";
        $reg_org = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.reg_organisation', [
            'reg_org' => $reg_org,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getRegParalegal()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Registration of Individual Paralegal";
        $reg_para = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.reg_paralegal', [
            'reg_para' => $reg_para,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getDevTraining()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Development of Training Carriculum & Manuals";
        $dev_training = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.dev_training', [
            'dev_training' => $dev_training,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getSupTraining()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Supervision of Training of Paralegals";
        $sup_training = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.sup_training', [
            'sup_training' => $sup_training,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getTrainJustice()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Training to Justice Actors";
        $justice_training = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.justice_training', [
            'justice_training' => $justice_training,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getTrainProvider()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Training to Legal Aid Providers";
        $provider_training = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.provider_training', [
            'provider_training' => $provider_training,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getServiceLpa()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Legal Aid Services by LAPs";
        $serv_lap = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.serv_lap', [
            'serv_lap' => $serv_lap,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }

    public function getServiceCourt()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Legal Aid Service by Order of Court";
        $serv_court = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.serv_court', [
            'serv_court' => $serv_court,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getServiceDetention()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Legal Aid Service in Detention Services";
        $serv_detention = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.serv_detention', [
            'serv_detention' => $serv_detention,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getPress()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Press";
        $press = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.media_center.press', [
            'press' => $press,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }

    public function getSpeech()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Speach";
        $speach = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.media_center.speach', [
            'speach' => $speach,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }

    public function getVideo()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Video";
        $vide = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.media_center.video', [
            'video' => $video,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getMonitoring()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.m&e', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getProvider()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.provider', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getParalegal()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.paralegal', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getTraining()
    {  
        $title = "Mission";
        $background = Background::where('title', '=', $title)->orderBy('created_at', 'desc')->first();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.service.training', [
            'background' => $background,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getTender()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Tender";
        $tender = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.announcement.tender', [
            'tender' => $tender,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }


    public function getOther()
    {  
        //--- Get Tender ----
        $publish = "1";
        $status = "Other";
        $tender = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->get();
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

        
        
        return view('website.announcement.other', [
            'tender' => $tender,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
        
    }
     
     public function getNewsView($id)
    {  
         
        $newsView = News::find($id);
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

         return view('website.news.view', [
            'newsView' => $newsView,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
            
        ]);
      
    }
    

    public function getMgtView($id)
    {  
         
        $mgt = Management::find($id);
        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

         return view('website.management.view', [
            'mgt' => $mgt,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
            
        ]);
      
    }
    
    
     public function getBlog()
    {  
        //--- Get Blog ----
        $publish = "1";
        $category = "News";
        $blog = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->paginate(3); 
        //--- Get News ----
         $publish = "1";
         $category = "News";
         $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
         //--- Get Links ----
         $links = SiteLink::orderBy('created_at', 'desc')->get();
 
         //--- Get Announcements ----
         $publish = "1";
         $status = "Anouncement";
         $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();
 
         
         
         return view('website.blog.blog', [
             'news' => $news,
             'blog' => $blog,
             'links' => $links,
             'anounsment' => $anounsment,
         ]);
      
     }
    
    
    
     public function submitNews(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
       
        
        $post  = new News;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->status = $request->input('category');
        $post->reporter = $name;
        $post->publish = $publish;
        $post->date_posted = $datetime;
        $post->save();
         
        $news_id = $post->id;
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $attachement = new WebDocument;
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
              
              $attachement->news_id = $news_id;
              $attachement->file = $fileNameToStore;
              $attachement->save();
            
              $fileName = $attachement->file;
            
              // Update news table with end target year
             
             $newsUpdate = DB::table('news')
                        ->where('id','=',$news_id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('news')
                ->with('success', 'News posted successfully.');
    }
    
     public function editNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['title' => $request->input('title'), 
                                  'content' => $request->input('content'),
                                  'status' => $request->input('category'),
                                  'editor' => $name,
                                  'publish' => $publish,
                                  'date_edited' => $datetime
                                 ]);
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
            
            
             $attachementUpdate = DB::table('web_documents')
                        ->where('news_id','=',$id )
                        ->update(['file' => $fileNameToStore
                                 ]);
            
              $fileName = $fileNameToStore;
            
              // Update news table with end target year
             
             $fileUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('news')
                ->with('success', 'News updated successfully.');
      
     }
    
    
    
     public function publishNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['publisher' => $name,
                                  'publish' => $publish,
                                  'date_published' => $datetime
                                 ]);
        
        

         return redirect('news')
                ->with('success', 'News published successfully.');
      
     }
    
    
     public function deleteNews($id)
    {  
        $new = News::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('news')
                ->with('success', 'Post deleted successfully.');
      
     }
    
    
}
