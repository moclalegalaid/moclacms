<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Court;

class CourtController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        $court = Court::all();
        //dd($court);exit();
        return view('users.courts.index', ['court' => $court ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function addCourt(Request $request){
        if($request->isMethod("get")){
            return view("users.courts.add");
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $court = Court::firstOrCreate([
            "name"=> $request->input("name"),
        ]);

        return redirect("users/courts")->withSuccess('Court created successfully');
    }

    public function activateCourt(Request $request, $court_id){
        $court = Court::find($court_id);

        $court->status = "active";
        $court->save();

        return redirect("users/courts")->withSuccess('Court activated successfully');
    }


    public function editCourt(Request $request, $court_id){
        $court = Court::find($role_id);
        if(!$role){
            return redirect('users/roles')->withError('Role not found');
        }

        $currentPermissions = $role->permissions()->pluck('name')->toArray();
        if($request->isMethod("get")){
            return view("users.roles2.edit",[
                'role'=>$role,
                'permissions'=> $currentPermissions,
            ]);
        }

        $role->name = $request->input('name');
        $role->save();

        //Revoke all the current permissions
        foreach ($currentPermissions As $permission){
            $role->revokePermissionTo($permission);
        }

        $permissions = $request->input("permissions");

        if(count($permissions)>0){
            $role->givePermissionTo($permissions);
        }
        return redirect("users/roles")->withSuccess('Role edited successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteRole($id){
        $role = Role::find($id);
        if($role){
            $role->delete();
            return redirect("users/roles")->withSuccess('Role deleted successfully');
        }
        return redirect("users/roles")->withError('Role not found');
    }
}
