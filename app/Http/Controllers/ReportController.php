<?php

namespace App\Http\Controllers;

use App\AidType;
use App\ProvidedAid;
use App\User;
use App\Court;
use App\WebDocument;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;

class ReportController extends Controller
{
    public function getAidReportIndex()
    {  
        $status = "active";
        $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->pluck('name', 'id');
        
        return view('admin.report.legal_aid.index', [
            'legalAidDesk' => $legalAidDesk,
        ]);
        
    }


    public function getAidReportSpecific(Request $request)
    {
       
        
        $station = $request->input('station');
        
        $status = "active";
        $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->pluck('name', 'id');
        
        $dateFrom = $request->input('startDate');
        $dateTo = $request->input('endDate');

        
        
        $where = " WHERE 1=1 ";
        if($station !=""){
            $where.= " AND court_id =$station ";
        }

        $where.= " AND (register_date BETWEEN  '$dateFrom' AND '$dateTo' )";

        $query = "SELECT * FROM provided_aids $where";

        $data = DB::select(DB::raw($query));

        $aids = new Collection($data);
        

        return view('admin.report.legal_aid.index', [
            'aids' => $aids,
            'legalAidDesk' => $legalAidDesk,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'station' => $station,

        ])->withDetails($aids);
      
    }
    
    
    
    
    public function getAidReportAll(Request $request)
    {
        
        $dateFrom = $request->input('startDate');
        $dateTo = $request->input('endDate');

        $station = $request->input('station');

        //echo $dateTo;exit();

        $status = "active";
        $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->pluck('name', 'id');

        //echo $level;exit();
        $where = " WHERE 1=1 ";

        $where.= " AND (register_date BETWEEN  '$dateFrom' AND '$dateTo' )";

        $query = "SELECT * FROM provided_aids $where";

        $data = DB::select(DB::raw($query));

        $aids = new Collection($data);

        return view('admin.report.legal_aid.index', [
            'aids' => $aids,
            'legalAidDesk' => $legalAidDesk,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'station' => $station,

        ])->withDetails($aids);
    }
    


   
}
