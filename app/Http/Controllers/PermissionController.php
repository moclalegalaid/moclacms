<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function getIndex() {
        $permissions = Permission::orderBy('name')->get();
        $roles = Role::where('name','<>','super-admin')->orderBy('name')->get();
        return view('users.permissions2.index', [
            'permissions' => $permissions,
            'roles' => $roles,
        ]);
    }
}
