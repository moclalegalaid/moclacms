<?php

namespace App\Http\Controllers;

use App\AidType;
use App\ProvidedAid;
use App\User;
use App\Court;
use App\WebDocument;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LegalAidController extends Controller
{
    public function getAidTypeIndex()
    {  
        $aidtype = AidType::orderBy('created_at', 'desc')->get();
        
        
        return view('admin.legal_aid_desk.aid_types.index', [
            'aidtype' => $aidtype,
        ]);
        
    }


    public function getAidRegisterIndex()
    {  
        $userId = Auth::user()->id;
        $courtId = Auth::user()->court_id;
        $user = Auth::user();
        $role = $user->getRoleNames()->first();

        $aidtype = AidType::all()->pluck('name', 'id');
        $aidprovided = ProvidedAid::orderBy('created_at', 'desc')->get();
        $aidprovidedstation = ProvidedAid::where('court_id', '=', $courtId)->orderBy('created_at', 'desc')->get();
        
        
        return view('admin.legal_aid_desk.provided_aid.index', [
            'aidtype' => $aidtype,
            'aidprovided' => $aidprovided,
            'aidprovidedstation' => $aidprovidedstation,
            'role' => $role,
        ]);
        
    }


    public function getAidDeskIndex()
    {  
        $status = "active";
        
        $aidtype = AidType::all()->pluck('name', 'id');
        $aidprovided = ProvidedAid::orderBy('created_at', 'desc')->get();
        $legalAidDesk = Court::where('status','=',$status)->orderBy('created_at','desc')->get();
        
        
        return view('admin.legal_aid_desk.station.index', [
            'aidtype' => $aidtype,
            'aidprovided' => $aidprovided,
            'legalAidDesk' => $legalAidDesk,
        ]);
        
    }



     public function registerAid(Request $request)
    {   
        
        $datetime = date('Y-m-d');
        $thisMonth = date('F');
        $thisYear = date('Y');
        $aid = $request->input('aid_type');
        $userId = Auth::user()->id;
        $courtId = Auth::user()->court_id;
        $name = User::find($userId)->name;
        $courtName = Court::find($courtId)->name;
        $aidName = AidType::find($aid)->name;

        if($userId == 0){
            return redirect('desk/register-aid')
                    ->with('warning', 'You cant register aid provided because you are not registered to any legal aid desk office.');
        }else{
       
        if($request->input('case') == "Yes"){

            $legalaid  = new ProvidedAid;
            $legalaid->user_id = $userId;
            $legalaid->registered_by = $name;
            $legalaid->court_id = $courtId;
            $legalaid->court_name = $courtName;
            $legalaid->aid_name = $aidName;
            $legalaid->reg_month = $thisMonth;
            $legalaid->reg_year = $thisYear;
            $legalaid->age = $request->input('age');
            $legalaid->sex = $request->input('sex');
            $legalaid->litigant_name = $request->input('litigant_name');
            $legalaid->litigant_address = $request->input('litigant_address');
            $legalaid->litigant_phone = $request->input('litigant_phone');
            $legalaid->litigant_email = $request->input('litigant_email');
            $legalaid->litigant_concern = $request->input('litigant_concern');
            $legalaid->court_case_ref_number = $request->input('ref_number');
            $legalaid->	aid_type = $request->input('aid_type');
            $legalaid->description = $request->input('description');
            $legalaid->register_date = $datetime;
           
            $legalaid->save();


            return redirect('desk/register-aid')
                    ->with('success', 'Aid type added successfully.');
        }else{

            $legalaid  = new ProvidedAid;
            $legalaid->user_id = $userId;
            $legalaid->registered_by = $name;
            $legalaid->court_id = $courtId;
            $legalaid->court_name = $courtName;
            $legalaid->aid_name = $aidName;
            $legalaid->reg_month = $thisMonth;
            $legalaid->reg_year = $thisYear;
            $legalaid->age = $request->input('age');
            $legalaid->sex = $request->input('sex');
            $legalaid->litigant_name = $request->input('litigant_name');
            $legalaid->litigant_address = $request->input('litigant_address');
            $legalaid->litigant_phone = $request->input('litigant_phone');
            $legalaid->litigant_email = $request->input('litigant_email');
            $legalaid->litigant_concern = $request->input('litigant_concern');
            $legalaid->	aid_type = $request->input('aid_type');
            $legalaid->description = $request->input('description');
            $legalaid->register_date = $datetime;
            $legalaid->save();


            return redirect('desk/register-aid')
                    ->with('success', 'Aid type added successfully.');
        }
     }
    }


     public function addAidType(Request $request)
    {   
        
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
       
        
        $type  = new AidType;
        $type->name = $request->input('name');
        $type->created_by = $userId;
        $type->creator_name = $name;
        $type->save();


         return redirect('desk/aid-type')
                ->with('success', 'Aid type added successfully.');
    }
    
     public function editNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "0";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['title' => $request->input('title'), 
                                  'content' => $request->input('content'),
                                  'status' => $request->input('category'),
                                  'editor' => $name,
                                  'publish' => $publish,
                                  'date_edited' => $datetime
                                 ]);
        
        // upload Image
        
        if($request->hasFile('file')) {
                 
              $filenameWithExt = $request->file('file')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
              $extension = $request->file('file')->getClientOriginalExtension();
              $fileNameToStore = $filename.'_'.time().'.'.$extension;
              $request->file('file')->storeAs('public/downloads', $fileNameToStore);
            
            
             $attachementUpdate = DB::table('web_documents')
                        ->where('news_id','=',$id )
                        ->update(['file' => $fileNameToStore
                                 ]);
            
              $fileName = $fileNameToStore;
            
              // Update news table with end target year
             
             $fileUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['file_name' => $fileName]);
            }


         return redirect('news')
                ->with('success', 'News updated successfully.');
      
     }
    
    
    
     public function publishNews(Request $request, $id)
    {  
      
        $datetime = date('Y-m-d H:i:s');
        $userId = Auth::user()->id;
        $name = User::find($userId)->name;
        $publish = "1";
          
         
        $newsUpdate = DB::table('news')
                        ->where('id','=',$id )
                        ->update(['publisher' => $name,
                                  'publish' => $publish,
                                  'date_published' => $datetime
                                 ]);
        
        

         return redirect('news')
                ->with('success', 'News published successfully.');
      
     }
    
    
     public function deleteNews($id)
    {  
        $new = News::find($id);
        if(!$new) {
            return back()->withError("There is no post with ID $id in your scope");
        }

        $new->delete();

         return redirect('news')
                ->with('success', 'Post deleted successfully.');
      
     }
    
    
}
