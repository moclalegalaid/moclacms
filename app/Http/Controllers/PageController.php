<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\News;
use App\Management;
use App\SiteLink;
use App\Faq;
use App\Comment;
use App\Court;
use App\ProvidedAid;
use Illuminate\Support\Facades\Http;

use Spatie\Permission\Models\Role;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {
        $pages = Page::select('page_name','slug')
                     ->where('status', '=', 'active')
                     ->get();

        if($slug === null) {

            $todatDate = date('Y-m-d');
            $thisMonth = date('F');
            $thisYear = date('Y');
            
            //*** Returns the Website index page ****
            //--- Get News ----
            $publish = "1";
            $category = "News";
            $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();

            //--- Get very Current News ----
            $publish = "1";
            $category = "News";
            $newsCurrent = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(1)->get();

            //--- Get very Current Video ----
            $publish = "1";
            $status = "video";
            $videoCurrent = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(1)->get();
            //--- Get Announcements ----
            $publish = "1";
            $status = "Anouncement";
            $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();

            //--- Get Management ----
            $group = "Top Leader";
            $management = Management::where('publish', '=', $publish)->where('groups', '=', $group)->orderBy('seniority', 'desc')->get();

            //--- Get Trending ----
            $atatus = "Anouncement";
            $trendings = News::where('status', '=', $atatus)->where('publish', '=', $publish)->orderBy('created_at', 'desc')->get();

            //--- Get Publications ----
            $category = "Docs";
            $publication = News::where('category', '=', $category)->where('publish', '=', $publish)->orderBy('created_at', 'desc')->limit(3)->get();
            
            //--- Get Number of aid provided ----
            $providedAid = ProvidedAid::all()->count();

            //--- Get Number of aid providers ----
           


            //--- Get Number of legal aid desks ----
            $status = "active";
            $desk = Court::where('status','=',$status)->count();

            //--- Get Links ----
             $links = SiteLink::orderBy('created_at', 'desc')->get();

            //--- Get FAQ ----
            $how = "How";
            $what = "What";
            $howStatement = Faq::where('title','=',$how)->get();
        
            $whatStatement = Faq::where('title','=',$what)->get();
            
            
            return view('index', [
                'news' => $news,
                'howStatement' => $howStatement,
                'whatStatement' => $whatStatement,
                'management' => $management,
                'trendings' => $trendings,
                'links' => $links,
                'providedAid' =>$providedAid,
                'desk' => $desk,
                'anounsment' => $anounsment,
                'publication' => $publication,
                'newsCurrent' => $newsCurrent,
                'videoCurrent' => $videoCurrent,
                'pageSlugs' => $pages->toArray()
            ]);
        } else {
            $pageContent = Page::where('slug', '=', $slug)
                                ->where('status', '=', 'active')
                                ->first();

            if (is_object($pageContent)) {
                return view('page', ['pageSlugs' => $pages->toArray(),
                                      'pageTitle' => $pageContent->page_name,
                                      'pageContent' => $pageContent->content
                                     ]);
            } else {
                return 'page not found';
            }
        }
    }

    public function fetch_data(Request $request) {
        $iso = $request->input('regions');

        //--- Get News ----
        $publish = "1";
        $category = "News";
        $news = News::where('publish', '=', $publish)->where('category', '=', $category)->orderBy('created_at', 'desc')->limit(3)->get();
        //--- Get Links ----
        $links = SiteLink::orderBy('created_at', 'desc')->get();

        //--- Get Announcements ----
        $publish = "1";
        $status = "Anouncement";
        $anounsment = News::where('publish', '=', $publish)->where('status', '=', $status)->orderBy('created_at', 'desc')->limit(3)->get();
        

        //--- API to connect to LAP Database ----
        $login = $this->login();
        $res = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$login['token']
        ])->get('https://legalaid.sheria.go.tz/api/registered_laps_region/'.$iso);


         $region = $res->json();
            // return response()->json([
            //     'region' => $region[0]['institution']['name']
            // ]);
     

        return view('website.lasp.index',[
            'region' => $region,
            'news' => $news,
            'links' => $links,
            'anounsment' => $anounsment,
        ]);
    }

    //--- API login to connect to LAP Database ----
    private function login() {
        $res = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post('https://legalaid.sheria.go.tz/api/login', [
            'application_id' => '5490876352',
            'password' => '1DF00E523BA703BC96B021355D2ED2597AF0F5DDC6B13A984423D9E806953C19'
        ]);

        if ($res->successful()) {
            return $res;
        }

        $res->throw();
    }


}
