<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $auditInclude = [
        'title',
        'content',
    ];

    protected $table = 'comments';
    public $fillable = [
        'news_id',
        'comment',
        'username',
    ];

    
    public function news(){
        return $this->belongsTo('App\News','news_id');
    }
}