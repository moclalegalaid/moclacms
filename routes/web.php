<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ***** AUTHENTICATION ROUTE STARTS ******//

Route::get('/', 'PageController@index')->name('root-page');
Route::get('/page/{slug}', 'PageController@index')->name('page');

Route::get('/test', function () {
    return view('test');
});

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin-home');
Route::get('/admin/system-info', 'AdminController@showSysInfo')->name('admin-sysinfo');

Route::resource('admin/page', 'AdminPageController', [
    'names' => [
        'index'   => 'admin-page-index',
        'store'   => 'admin-page-store',
        'create'  => 'admin-page-create',
        'show'    => 'admin-page-show',
        'edit'    => 'admin-page-edit',
        'update'  => 'admin-page-update',
        'destroy' => 'admin-page-destroy'
    ]
]);

//Password reset
Route::match(['get', 'post'],'/reset-password', 'Auth\ResetPasswordController@ResetPassword');
Route::get('/user/reset/{token}', 'AuthController@reset');

// ****** ENDS *********//

//****** BACK END PROCESSES **********//

// --------- User Routes Start-----------//
Route::prefix('users')->group(function () {
    Route::prefix('permissions')->group(function () {
        Route::get('/', 'PermissionController@getIndex')->middleware('permission:view permissions');
    });

    Route::prefix('roles')->group(function () {
        Route::get('/', 'RoleController@getIndex')->middleware('permission:view roles');
        Route::match(['get','post'], '/add', 'RoleController@addRole')->middleware('permission:add roles');
        Route::match(['get','post'], '/edit/{id}', 'RoleController@editRole')->middleware('permission:edit roles');
        Route::get('/delete/{id}', 'RoleController@deleteRole')->middleware('permission:delete roles');
    });

    Route::prefix('courts')->group(function () {
        Route::get('/', 'CourtController@getIndex')->middleware('permission:view courts');
        Route::match(['get','post'], '/add', 'CourtController@addCourt')->middleware('permission:add courts');
        Route::match(['get','post'], '/edit/{id}', 'CourtController@editCourt')->middleware('permission:edit courts');
        Route::match(['get','post'], '/activate/{id}', 'CourtController@activateCourt')->middleware('permission:edit courts');
        Route::get('/delete/{id}', 'CourtController@deleteCourt')->middleware('permission:delete courts');
    });

    Route::prefix('managements')->group(function () {
        Route::get('/', 'UserController@getIndex')->middleware('permission:view users');
        Route::match(['get', 'post'], '/add', 'UserController@addUser')->middleware('permission:add users');
		Route::match(['get', 'post'], '/addExtUser', 'UserController@addExtUser')->middleware('permission:add users');
        Route::match(['get', 'post'], '/edit/{id}', 'UserController@editUser')->middleware('permission:edit users');
        Route::get('/delete/{id}', 'UserController@deleteUser')->middleware('permission:delete users');
        Route::get('/courts/{id}', 'UserController@getCourt');
    });

    Route::prefix('profile')->group(function () {
        Route::match(['get', 'post'], '/{id}', 'UserController@updateProfile')->middleware('permission:edit profile');
    });

    Route::match(['get', 'post'], '/change-password/{id}', 'UserController@changePassword');
});

// ----------- User Route Ends----------------// 

// ----------- Dashboard Route Starts----------------// 

Route::prefix('admin')->group(function () {
    Route::get('/aid-today', 'AdminController@getAdminAidToday');
    Route::get('/aid-thismonth', 'AdminController@getAdminAidThisMonth');
    Route::get('/aid-thisyear', 'AdminController@getAdminAidThisYear');
    Route::get('/aid-desk', 'AdminController@getAidDesk');
    
});

Route::prefix('user')->group(function () {
    Route::get('/aid-today', 'AdminController@getUserAidToday');
    Route::get('/aid-thismonth', 'AdminController@getUserAidThisMonth');
    Route::get('/aid-thisyear', 'AdminController@getUserAidThisYear');
    Route::get('/aid-desk', 'AdminController@getAidDesk');
    
});

// ----------- Dashboard Route ends----------------// 



// ---------- App Setting Route starts ------------- //

Route::group(['prefix' => 'home-page'], function() {
    Route::get('/', 'AppSettingController@getHomeIndex')->middleware('permission:view home');
    Route::match(['get', 'post'], '/background', 'AppSettingController@saveBackground')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/bgedit/{id}', 'AppSettingController@editBg')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/bgdelete/{id}', 'AppSettingController@deleteBg')->middleware('permission:delete home');
    
    
    Route::match(['get', 'post'], '/mission', 'AppSettingController@saveMission')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/msedit/{id}', 'AppSettingController@editMs')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/msdelete/{id}', 'AppSettingController@deleteMs')->middleware('permission:delete home');
    
    
    Route::match(['get', 'post'], '/vission', 'AppSettingController@saveVission')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/vsedit/{id}', 'AppSettingController@editMs')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/vsdelete/{id}', 'AppSettingController@deleteMs')->middleware('permission:delete home');
    

    Route::match(['get', 'post'], '/admin', 'AppSettingController@saveAdmin')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/adminedit/{id}', 'AppSettingController@editAdmin')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/admindelete/{id}', 'AppSettingController@deleteAdmin')->middleware('permission:delete home');
    

    Route::match(['get', 'post'], '/how', 'AppSettingController@saveHow')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/edithow/{id}', 'AppSettingController@editHow')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/deletehow/{id}', 'AppSettingController@deleteHow')->middleware('permission:delete home');
    
    
    Route::match(['get', 'post'], '/what', 'AppSettingController@saveWhat')->middleware('permission:view home');
    
    Route::match(['get', 'post'], '/editwhat/{id}', 'AppSettingController@editWhat')->middleware('permission:edit home');
    Route::match(['get', 'post'], '/deletewhat/{id}', 'AppSettingController@deleteWhat')->middleware('permission:delete home');

    Route::match(['get', 'post'], '/leaders', 'AppSettingController@addLeader')->middleware('permission:add home');
    Route::match(['get', 'post'], '/delete-mgt/{id}', 'AppSettingController@deleteMg')->middleware('permission:delete home');
    Route::match(['get', 'post'], '/publish-mgt/{id}', 'AppSettingController@publishMg')->middleware('permission:delete home');

    
});


Route::group(['prefix' => 'structure'], function() {
    Route::get('/', 'AppSettingController@getStructureIndex')->middleware('permission:view home');
    Route::match(['get', 'post'], '/add-str', 'AppSettingController@addStr')->middleware('permission:view home');

    Route::match(['get', 'post'], '/delete-str/{id}', 'AppSettingController@deleteStr')->middleware('permission:delete home');
    Route::match(['get', 'post'], '/publish-str/{id}', 'AppSettingController@publishStr')->middleware('permission:delete home');
    
});



Route::group(['prefix' => 'service'], function() {
    Route::get('/', 'AppSettingController@getServiceIndex')->middleware('permission:view home');
    Route::match(['get', 'post'], '/add-srv', 'AppSettingController@addSrv')->middleware('permission:view home');

    Route::match(['get', 'post'], '/delete-srv/{id}', 'AppSettingController@deleteSrv')->middleware('permission:delete home');
    Route::match(['get', 'post'], '/publish-srv/{id}', 'AppSettingController@publishSrv')->middleware('permission:delete home');
    
});


// ------------ App Setting Routes End ----------------- //

// ---------- Web Content Route starts ------------- //

Route::group(['prefix' => 'news'], function() {
    Route::get('/', 'WebContentController@getNewsIndex')->middleware('permission:view news');
    Route::match(['get', 'post'], '/add', 'WebContentController@addNews')->middleware('permission:view news');
    Route::match(['get', 'post'], '/submit', 'WebContentController@submitNews')->middleware('permission:add news');
    Route::match(['get', 'post'], '/edit/{id}', 'WebContentController@editNews')->middleware('permission:edit news');
    Route::match(['get', 'post'], '/delete/{id}', 'WebContentController@deleteNews')->middleware('permission:delete news');
    Route::match(['get', 'post'], '/publish/{id}', 'WebContentController@publishNews')->middleware('permission:delete news');
});


Route::group(['prefix' => 'media'], function() {
    Route::get('/', 'WebContentController@getMediaIndex')->middleware('permission:view media');
    Route::match(['get', 'post'], '/add', 'WebContentController@addMedia')->middleware('permission:view news');
    Route::match(['get', 'post'], '/submit', 'WebContentController@submitMedia')->middleware('permission:add media');
    Route::match(['get', 'post'], '/edit/{id}', 'WebContentController@editMedia')->middleware('permission:edit media');
    Route::match(['get', 'post'], '/delete/{id}', 'WebContentController@deleteMedia')->middleware('permission:delete media');
    Route::match(['get', 'post'], '/publish/{id}', 'WebContentController@publishMedia')->middleware('permission:delete media');

    
});

Route::group(['prefix' => 'adverts'], function() {
    Route::get('/', 'WebContentController@getPostIndex')->middleware('permission:view posts');
    Route::match(['get', 'post'], '/add', 'WebContentController@addPost')->middleware('permission:view posts');
    Route::match(['get', 'post'], '/submit', 'WebContentController@submitPost')->middleware('permission:add posts');
    Route::match(['get', 'post'], '/edit/{id}', 'WebContentController@editPost')->middleware('permission:edit posts');
    Route::match(['get', 'post'], '/delete/{id}', 'WebContentController@deletePost')->middleware('permission:delete posts');
    Route::match(['get', 'post'], '/publish/{id}', 'WebContentController@publishPost')->middleware('permission:delete posts');
});

Route::group(['prefix' => 'documents'], function() {
    Route::get('/', 'WebContentController@getDocIndex')->middleware('permission:view documents');
    Route::match(['get', 'post'], '/add', 'WebContentController@addDoc')->middleware('permission:view documents');
    Route::match(['get', 'post'], '/submit', 'WebContentController@submitDoc')->middleware('permission:add documents');
    Route::match(['get', 'post'], '/edit/{id}', 'WebContentController@editDoc')->middleware('permission:edit documents');
    Route::match(['get', 'post'], '/delete/{id}', 'WebContentController@deleteDoc')->middleware('permission:delete documents');
    Route::match(['get', 'post'], '/publish/{id}', 'WebContentController@publishDoc')->middleware('permission:delete documents');
});

Route::group(['prefix' => 'links'], function() {
    Route::get('/', 'WebContentController@getSiteIndex')->middleware('permission:view links');
    Route::match(['get', 'post'], '/add', 'WebContentController@addSite')->middleware('permission:view links');
    Route::match(['get', 'post'], '/submit', 'WebContentController@submitSite')->middleware('permission:add links');
    Route::match(['get', 'post'], '/delete/{id}', 'WebContentController@deleteSite')->middleware('permission:delete links');
});

Route::group(['prefix' => 'photo-gallery'], function() {
    Route::get('/', 'WebContentController@getGalleryIndex')->middleware('permission:view photo gallery');
});

// ------------ Web Content Routes End ----------------- //


// ------------ Leagal Aid Desk Routes Starts ----------------- //
Route::prefix('desk')->group(function () {
Route::group(['prefix' => 'aid-type'], function() {
    Route::get('/', 'LegalAidController@getAidTypeIndex');
    Route::match(['get', 'post'], '/add', 'LegalAidController@addAidType');
    Route::match(['get', 'post'], '/delete/{id}', 'LegalAidController@deleteAidType');
});


Route::group(['prefix' => 'register-aid'], function() {
    Route::get('/', 'LegalAidController@getAidRegisterIndex')->middleware('permission:view desks');
    Route::match(['get', 'post'], '/add', 'LegalAidController@registerAid')->middleware('permission:view desks');
    Route::match(['get', 'post'], '/delete/{id}', 'LegalAidController@deleteAidType')->middleware('permission:delete desks');
});

Route::group(['prefix' => 'desk'], function() {
    Route::get('/', 'LegalAidController@getAidDeskIndex')->middleware('permission:view desks');
    Route::match(['get', 'post'], '/add', 'LegalAidController@registerAidDesk')->middleware('permission:view desks');
    Route::match(['get', 'post'], '/delete/{id}', 'LegalAidController@deleteAidDesk')->middleware('permission:delete desks');
});

});

// ------------ Leagal Aid Desk Routes Ends ----------------- //


// ------------ Leagal Aid Report Routes Starts ----------------- //
Route::prefix('report')->group(function () {
    Route::group(['prefix' => 'legal-aid'], function() {
        Route::get('/', 'ReportController@getAidReportIndex');
        Route::post('/specific', 'ReportController@getAidReportSpecific');
        Route::post('/allStations', 'ReportController@getAidReportAll');
    });
    
    
    
    
    });
    
    // ------------ Leagal Aid Desk Routes Ends ----------------- //


//****** FRONT END PROCESSES **********//

// ---------- Website Content Route starts ------------- //

Route::group(['prefix' => 'cms'], function() {
    // ---------- Home Route starts ------------- //
    Route::get('/background', 'WebSiteContentController@getBackground');
    Route::get('/vission', 'WebSiteContentController@getVission');
    Route::get('/mission', 'WebSiteContentController@getMission');

    // ---------- Administration Route starts ------------- //
    Route::get('/board', 'WebSiteContentController@getBoard');
    Route::get('/registrar', 'WebSiteContentController@getRegistrar');
    Route::get('/a-registrar', 'WebSiteContentController@getAregistrar');


    // ---------- Legal Instruments Route starts ------------- //
    Route::get('/constitution', 'WebSiteContentController@getConstitution');
    Route::get('/policy', 'WebSiteContentController@getPolicy');
    Route::get('/act', 'WebSiteContentController@getAct');
    Route::get('/regulation', 'WebSiteContentController@getRegulation');
    Route::get('/circular', 'WebSiteContentController@getCircular');
    Route::get('/form', 'WebSiteContentController@getForm');
    Route::get('/program', 'WebSiteContentController@getProgram');
    Route::get('/guideline', 'WebSiteContentController@getGuideline');
    Route::get('/report', 'WebSiteContentController@getReport');
    Route::get('/workplan', 'WebSiteContentController@getWorkplan');

    // ---------- Services Route starts ------------- //
    Route::get('/reg-organisation', 'WebSiteContentController@getRegOrganisation');
    Route::get('/reg-paralegal', 'WebSiteContentController@getRegParalegal');
    Route::get('/dev-training', 'WebSiteContentController@getDevTraining');
    Route::get('/sup-training', 'WebSiteContentController@getSupTraining');
    Route::get('/train-justice', 'WebSiteContentController@getTrainJustice');
    Route::get('/train-provider', 'WebSiteContentController@getTrainProvider');
    Route::get('/service-lpa', 'WebSiteContentController@getServiceLpa');
    Route::get('/service-court', 'WebSiteContentController@getServiceCourt');
    Route::get('/service-detention', 'WebSiteContentController@getServiceDetention');

    // ----------Abandoned Services Route starts ------------- //
    Route::get('/registration', 'WebSiteContentController@getRegistration');
    Route::get('/m&e', 'WebSiteContentController@getMonitoring');
    Route::get('/service-provider', 'WebSiteContentController@getProvider');
    Route::get('/paralegal', 'WebSiteContentController@getParalegal');
    Route::get('/training', 'WebSiteContentController@getTraining');

    // ---------- Media Center Route starts ------------- //
    Route::get('/press', 'WebSiteContentController@getPress');
    Route::get('/speech', 'WebSiteContentController@getSpeech');
    Route::get('/video', 'WebSiteContentController@getVideo');
    Route::get('/photo', 'WebSiteContentController@getPhoto');
    Route::get('/news', 'WebSiteContentController@getNews');
    Route::get('/audio', 'WebSiteContentController@getAudio');
    Route::get('/announcement', 'WebSiteContentController@getAnnouncement');


    
    Route::get('/tender', 'WebSiteContentController@getTender');
    Route::get('/other', 'WebSiteContentController@getOther');
    Route::get('/blog', 'WebSiteContentController@getBlog');

    Route::get('/project', 'WebSiteContentController@getProject');

    Route::get('/news-view/{id}', 'WebSiteContentController@getNewsView');
    Route::get('/blog-view/{id}', 'WebSiteContentController@getBlogView');
    Route::get('/mgt-view/{id}', 'WebSiteContentController@getMgtView');

    
});

// ---------- Post Comment Route starts ------------- //

Route::group(['prefix' => 'post-comment'], function() {
    Route::match(['get', 'post'], '/post/{id}', 'WebContentController@postComment');
    
});

Route::get('/regions', 'PageController@fetch_data');

// ------------ Post Comment Routes End ----------------- //

// ------------ WebSite Content Routes End ----------------- //
