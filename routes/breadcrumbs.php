<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', url('admin'));
});

// News Posting
Breadcrumbs::register('news', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('News');
});


// Background
Breadcrumbs::register('web-news', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Backgound');
});

// User
Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('users');
});

