-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2020 at 11:13 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jsds1`
--

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(2, 91, 'App\\User'),
(2, 92, 'App\\User'),
(2, 93, 'App\\User'),
(2, 94, 'App\\User'),
(2, 120, 'App\\User'),
(2, 144, 'App\\User'),
(3, 3, 'App\\User'),
(3, 85, 'App\\User'),
(3, 86, 'App\\User'),
(3, 87, 'App\\User'),
(3, 88, 'App\\User'),
(3, 89, 'App\\User'),
(3, 90, 'App\\User'),
(3, 95, 'App\\User'),
(3, 96, 'App\\User'),
(3, 117, 'App\\User'),
(3, 118, 'App\\User'),
(3, 121, 'App\\User'),
(3, 132, 'App\\User'),
(3, 141, 'App\\User'),
(4, 2, 'App\\User'),
(4, 99, 'App\\User'),
(4, 100, 'App\\User'),
(4, 101, 'App\\User'),
(4, 102, 'App\\User'),
(4, 103, 'App\\User'),
(4, 104, 'App\\User'),
(4, 106, 'App\\User'),
(4, 112, 'App\\User'),
(4, 113, 'App\\User'),
(4, 114, 'App\\User'),
(4, 116, 'App\\User'),
(4, 119, 'App\\User'),
(4, 122, 'App\\User'),
(4, 123, 'App\\User'),
(4, 126, 'App\\User'),
(4, 127, 'App\\User'),
(4, 128, 'App\\User'),
(4, 129, 'App\\User'),
(4, 130, 'App\\User'),
(4, 134, 'App\\User'),
(4, 137, 'App\\User'),
(4, 139, 'App\\User'),
(4, 140, 'App\\User'),
(4, 146, 'App\\User'),
(4, 149, 'App\\User'),
(4, 151, 'App\\User'),
(4, 152, 'App\\User'),
(4, 153, 'App\\User'),
(4, 154, 'App\\User'),
(4, 155, 'App\\User'),
(4, 156, 'App\\User'),
(5, 138, 'App\\User'),
(5, 143, 'App\\User'),
(5, 145, 'App\\User'),
(18, 97, 'App\\User'),
(18, 108, 'App\\User'),
(18, 131, 'App\\User'),
(19, 105, 'App\\User'),
(21, 142, 'App\\User'),
(22, 147, 'App\\User'),
(23, 148, 'App\\User'),
(24, 159, 'App\\User'),
(25, 160, 'App\\User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
