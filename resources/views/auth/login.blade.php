<!DOCTYPE html>
<html>



<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'MoCLA') }}</title>
    
    <link rel="icon" type="image/png" href="{{ URL::asset('images/icons/scale.png') }}"/>
    <link href="{{ asset('css2/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/style.css') }}" rel="stylesheet">
    
    <style>
         @import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css););
        body{
            margin: 0;
            padding: 0;
        }
        body:before{
            content: '';
            position: fixed;
            width: 100vw;
            height: 100vh;
            background-image: url("images/flag1.jpg");
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
        .contact-form
        {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            width: 400px;
            padding: 80px 30px;
            box-sizing: border-box;
        }
        .avatar {
            position: absolute;
            width: 80px;
            height: 80px;
            border-radius: 0%;
            overflow: hidden;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
            top: calc(-70px/2);
        }
        
        
        
        .stream-panel
        {
            color: black;
        }
        
        
    </style>
</head>

<body class="gray-bg">
    
    <div class="contact-form fadeInDown">
        <img src="images/users/profile.png" class="avatar">
        <div class="heading">
					<center>
					<span style="font-size:15px;color:#0c0c0c;text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4;"><b>THE UNITED REPUBLIC OF TANZANIA</b></span><br/>
                    <span style="font-size:17px;color:#286eac;text-shadow: 0 1px 0 #999;"><b>MINISTRY OF CONSTITUTIONAL AND LEGAL AFFAIRS (MoCLA)</b></span><br/>
					<span style="font-size:15px;color:#286eac;text-shadow: 0 1px 0 #999;"><b>Legal Aid For Women</b></span><br/>
					</center>
				</div>
        <center>
            <p style="font-size:20px;color:black;"><strong>Sign in</strong> </p>
        </center>
        
            <div>
                <center>
                    @if($errors->any()) 
                        <div class="alert alert-danger alert-dismissable">
                            @foreach($errors->all() as $error)
                                <a href="#" class="alert-link">INFO! :</a> {{ $error }}
                            @endforeach
                        </div>
                    @endif
                </center>
            </div>
            <form method="POST" action="{{ route('login')}}" class="m-t" role="form" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input id="email" type="text" class="form-control text-input"
                               name="email" value="{{ old('email') }}" required placeholder="Username" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control text-input"
                               name="password" required placeholder="Password">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-success" href="{{ url('signup') }}"><i class="fa fa-sign-in"></i> Login</button>
                        <a data-toggle="modal" data-target="#forgetPasswd" class="pull-right">
                        <strong>Forgot password?</strong>
                    </a>
                    </div>

                    <center><p class="m-t" style="color:black;"> <small><span>Copyright &copy; <?php echo date('Y'); ?>. Ministry of Constitutional and Legal Affairs.</span><br><span>Legal Aid Services CMS version 1.0</span></small></p></center>
                    
                </form>
        
    </div>

    <div class="modal inmodal fade" id="forgetPasswd" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md">
            <form method="POST" action="{{ url('reset')}}" class="m-t" role="form" autocomplete="off" >
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <p style="font-size:20px;color:black;" class="modal-title">Reset Password</p>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p style="font-size:15px;color:black;">Enter your email address to receive recovery link<small> (Or contact system administrator for assistance)</small></p>
                        <input id="email" type="email" class="form-control text-input"
                               name="email" value="{{ old('email') }}" required placeholder="Enter email" autofocus>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Send</button>
                </div>
            </div>
         </form>
        </div>
    </div>

    
<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    
    
</body>


</html>




