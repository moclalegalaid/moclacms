<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>
    @section('title')
      MoCLA |
    @show
  </title>

  <!-- Main CSS -->
  <link rel="icon" type="image/png" href="{{ URL::asset('images/icons/scale.png') }}"/>
  <link href="{{ asset('css2/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

  @yield('page-specific-css')

  <link href="{{ asset('css2/animate.css') }}" rel="stylesheet">
  <link href="{{ asset('css2/style.css') }}" rel="stylesheet">
  <style> 
    @import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css););

    .modal
        {
            color: black;
        }

  </style>
</head>

<body>
<div id="wrapper">

  @include('layouts.side_navigation')

  <div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row border-bottom">
      @include('layouts.top_nav_bar.top_navigation')

      <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content animated">
            <div class="modal-header">
              <h4 class="modal-title">Logout ?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="fa fa-window-close"></i> No.!</button>
                  <a href="{{ url('logout') }}" class="btn btn-success md-close"
                     onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();" >
                    <i class="fa fa-sign-out"></i>Yes!
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    @if(View::hasSection('breadcrumbs'))
    <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-12">
        @yield('breadcrumbs')
      </div>
    </div>
    @endif

    <div class="row">
      <div class="col-lg-12">
        <div class="wrapper wrapper-content">
          @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <a href="#" class="alert-link">SUCCESS! :</a>. {{ session()->get('success') }}
            </div>
          @endif
          @if(session()->has('warning'))
            <div class="alert alert-warning alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <a href="#" class="alert-link">WARNING! :</a>. {{ session()->get('warning') }}
            </div>
          @endif
          @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <a href="#" class="alert-link">ERROR! :</a> {{ session()->get('error') }}
            </div>
          @endif

          @if(View::hasSection('content'))
            @yield('content')
          @endif

          @if(View::hasSection('raw-content'))
            @yield('raw-content')
          @endif
        </div>


      </div>
    </div>
    <div class="footer">
      @include('layouts.footer2')
    </div>
  </div>

</div>

@yield('modal')
<!-- Mainly Scripts -->
@include('layouts.assets.js2')

@yield('page-specific-js')

</body>
</html>
