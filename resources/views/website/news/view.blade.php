@extends('web_main')

@section('title')
{{strtolower($newsView->title)}}
@stop



@section('raw-content')
@if ($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @foreach ($errors->all() as $error)
            <a href="#" class="alert-link">ERROR! :</a> {{ $error }} <br/>
        @endforeach
    </div>
@endif


  <!-- ======= Breadcrumbs ======= -->
 <!-- Breadcrumb -->
 <div class="container">
		<div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
            <div class="f2-s-1 p-r-30 m-tb-6">
				<a href="{{ url('/') }}" class="breadcrumb-item f1-s-3 cl9">
					Home 
				</a>

				<a href="blog-list-01.html" class="breadcrumb-item f1-s-3 cl9">
					News 
				</a>
			</div>

			<div class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="search" placeholder="Search">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</div>
		</div>
	</div>

<!-- Content -->
<section class="bg0 p-b-110">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 col-lg-8 p-b-30">
                    <div class="p-r-10 p-r-0-sr991">
						<!-- Blog Detail -->
						<div class="p-b-70">
							<h3 class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
                                {{strtoupper($newsView->title)}}
							</h3>
							
							<div class="flex-wr-s-s p-b-40">
								<span class="f1-s-3 cl8 m-r-15">
									<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
										By {{$newsView->publisher}}
									</a>

									<span class="m-rl-3">-</span>

									<span>
                                    {{ date('F d Y', strtotime($newsView->date_published))}}
									</span>
								</span>

								<a href="#" class="f1-s-3 cl8 hov-cl10 trans-03 m-r-15">
									0 Comment
								</a>
							</div>

							<div class="wrap-pic-max-w p-b-30">
								<img src="{{ URL::to('storage/downloads/'.$newsView->file_name) }}" alt="IMG">
							</div>

							<p style="text-align:justify;" class="f1-s-11 cl6 p-b-25">
                                {{$newsView->content}}
							</p>

							

							<!-- Share -->
							<div class="flex-s-s">
								<span class="f1-s-12 cl5 p-t-1 m-r-15">
									Share:
								</span>
								
								<div class="flex-wr-s-s size-w-0">
									<a href="#" class="dis-block f1-s-13 cl0 bg-facebook borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-facebook-f m-r-7"></i>
										Facebook
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-twitter borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-twitter m-r-7"></i>
										Twitter
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-google borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-google-plus-g m-r-7"></i>
										Google+
									</a>
								</div>
							</div>
						</div>

						<!-- Leave a comment -->
						<div>
							<h4 class="f1-l-4 cl3 p-b-12">
								Leave a Comment
							</h4>

							<p class="f1-s-13 cl8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<form>
								<textarea class="bo-1-rad-3 bocl13 size-a-15 f1-s-13 cl5 plh6 p-rl-18 p-tb-14 m-b-20" name="msg" placeholder="Comment..."></textarea>

								<input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-20" type="text" name="name" placeholder="Name*">

								<input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-20" type="text" name="email" placeholder="Email*">

								<input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-20" type="text" name="website" placeholder="Website">

								<button class="size-a-17 bg2 borad-3 f1-s-12 cl0 hov-btn1 trans-03 p-rl-15 m-t-10">
									Post Comment
								</button>
							</form>
						</div>
					</div>
				</div>
				
				<!-- Sidebar -->
				<div class="col-md-5 col-lg-4 p-b-30">
					<div class="p-l-10 p-rl-0-sr991 p-t-5">
						<!-- Popular Posts -->
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Announcements
								</h3>
							</div>
							<br/>
							<a href="#" class="wrap-pic-w hov1 trans-03">
							@if($anounsment)
							<ul class="m-t--12">
								@foreach($anounsment as $key => $anounsments)
								<li class="how-bor1 p-rl-5 p-tb-10">
									<a style="text-align:justfy;color:black;" target="_blank" href="{{ asset('storage/downloads/'.$anounsments->file_name) }}">
									{{ $anounsments->title }}
									</a>
								</li>
								@endforeach
							</ul>
							<a href="#" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
									View all
									<i class="fs-12 m-l-5 fa fa-caret-right"></i>
							</a>
								
							@else
							<ul class="m-t--12">
								<li class="how-bor1 p-rl-5 p-tb-20">
									<a href="#" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
										No anounsment to display
									</a>
								</li>
							</ul>
							@endif
							</a>
							
						</div>
						<br/>
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Recent Post
								</h3>
							</div>

							<ul class="p-t-35">
              				@if($news)
							@foreach($news as $new)
								<li class="flex-wr-sb-s p-b-30">
									<a href="{{ url('cms/news-view', $new->id) }}" class="size-w-10 wrap-pic-w hov1 trans-03">
										<img src="{{ URL::to('storage/downloads/'.$new->file_name) }}" alt="IMG">
									</a>

									<div class="size-w-11">
										<h6 class="p-b-4">
											<a href="{{ url('cms/news-view', $new->id) }}" class="f1-s-5 cl3 hov-cl10 trans-03">
                      						{{strtoupper($new->title)}}
											</a>
										</h6>

										<span class="cl8 txt-center p-b-24">
											<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
											{{$new->publisher}}
											</a>

											<span class="f1-s-3 m-rl-3">
												-
											</span>

											<span class="f1-s-3">
                                              {{ date('F d Y', strtotime($new->date_published))}}
											</span>
										</span>
									</div>
								</li>
             				 @endforeach
              				@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


 
    


        
@stop