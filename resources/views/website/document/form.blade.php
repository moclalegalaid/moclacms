@extends('web_main')

@section('title')
    Forms				
@stop



@section('raw-content')
@if ($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @foreach ($errors->all() as $error)
            <a href="#" class="alert-link">ERROR! :</a> {{ $error }} <br/>
        @endforeach
    </div>
@endif


  <!-- ======= Breadcrumbs ======= -->
 <!-- Breadcrumb -->
 <div class="container">
		<div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 m-tb-6">
				<a href="{{ url('/') }}" class="breadcrumb-item f1-s-3 cl9">
					Home 
				</a>

				<span class="breadcrumb-item f1-s-3 cl9">
					Legal Instruments
				</span>

				<span class="breadcrumb-item f1-s-3 cl9">
                    Forms
				</span>
			</div>

			<div class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="search" placeholder="Search">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</div>
		</div>
	</div>

  <!-- Page heading -->
	<div class="container p-t-4 p-b-35">
		<h2 class="f1-l-1 cl2">
            Forms
		</h2>
	</div>

<!-- Content -->
<section class="bg0 p-b-110">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 col-lg-8 p-b-30">
					<div class="p-r-10 p-r-0-sr991">
						@if($form)
						<table id= "table-id" class="table table-hover dataTables-search">
							<thead>
							<tr>
									<th>File</th>
									<th>Title</th>
								</tr>
							</thead>
							<tbody>
                            @foreach($form as $tenders)
                            <tr>
                                    <td>
                                        <a target="_blank" href="{{ asset('storage/downloads/'.$tenders->file_name) }}">
                                            <span>
                                                <img style="width:50px; height:50px;" alt="image" class="img-square" src="{{ asset('images/file.png') }}" />  
                                            </span>
                                        </a>
                                    </td>
                                    <td>
                                        <a style="color:black;" target="_blank" href="{{ asset('storage/downloads/'.$tenders->file_name) }}">
                                            <span>
                                            {{strtoupper($tenders->title)}}
                                            </span>
                                        </a><br/>
                                        <span class="cl8 txt-center p-b-24">

											<span class="f1-s-3 m-rl-3">
												-
											</span>

											<span class="f1-s-3">
												{{ date('F d Y', strtotime($tenders->date_published))}}
											</span>
										</span>
                                    </td>
									
                                </tr>
                            @endforeach
						</tbody>
                        </table>
						@endif
					</div>
				</div>
				
				<!-- Sidebar -->
				<div class="col-md-5 col-lg-4 p-b-30">
					<div class="p-l-10 p-rl-0-sr991 p-t-5">
						<!-- Popular Posts -->
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Announcements
								</h3>
							</div>
							<br/>
							<a href="#" class="wrap-pic-w hov1 trans-03">
							@if($anounsment)
							<ul class="m-t--12">
								@foreach($anounsment as $key => $anounsments)
								<li class="how-bor1 p-rl-5 p-tb-10">
									<a style="text-align:justfy;color:black;" target="_blank" href="{{ asset('storage/downloads/'.$anounsments->file_name) }}">
									{{ $anounsments->title }}
									</a>
								</li>
								@endforeach
							</ul>
							<a href="#" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
									View all
									<i class="fs-12 m-l-5 fa fa-caret-right"></i>
							</a>
								
							@else
							<ul class="m-t--12">
								<li class="how-bor1 p-rl-5 p-tb-20">
									<a href="#" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
										No anounsment to display
									</a>
								</li>
							</ul>
							@endif
							</a>
							
						</div>
						<br/>
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Recent Post
								</h3>
							</div>

							<ul class="p-t-35">
              				@if($news)
							@foreach($news as $new)
								<li class="flex-wr-sb-s p-b-30">
									<a href="{{ url('cms/news-view', $new->id) }}" class="size-w-10 wrap-pic-w hov1 trans-03">
										<img src="{{ URL::to('storage/downloads/'.$new->file_name) }}" alt="IMG">
									</a>

									<div class="size-w-11">
										<h6 class="p-b-4">
											<a href="{{ url('cms/news-view', $new->id) }}" class="f1-s-5 cl3 hov-cl10 trans-03">
											{{substr(strtoupper($new->title), 0, 40)}}
											</a>
										</h6>

										<span class="cl8 txt-center p-b-24">
											<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
											{{$new->publisher}}
											</a>

											<span class="f1-s-3 m-rl-3">
												-
											</span>

											<span class="f1-s-3">
												{{ date('F d Y', strtotime($new->date_published))}}
											</span>
										</span>
									</div>
								</li>
             				 @endforeach
              				@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


 
    


        
@stop