<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
		@section('title')
			MoCLA |
		@show
</title>
<link rel="shortcut icon" href="images/icons/scale.png" type="image/x-icon">
<link rel="icon" href="images/icons/scale.png" type="image/x-icon">
<!-- bootstrap styles-->
<link href="{{asset('new-web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<!-- google font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
<!-- ionicons font -->
<link href="{{asset('new-web/css/ionicons.min.css')}}" rel="stylesheet" type="text/css">
<!-- animation styles -->
<link href="{{asset('new-web/css/animate.css')}}" rel="stylesheet" type="text/css">
<!-- custom styles -->
<link href="{{asset('new-web/css/custom-blue.css')}}" rel="stylesheet" type="text/css">
<!-- owl carousel styles-->
<link href="{{asset('new-web/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('new-web/css/owl.transitions.css')}}" rel="stylesheet" type="text/css">
<!-- magnific popup styles -->
<link href="{{asset('new-web/css/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<!-- fliating chat -->
<link href="{{asset('new-web/css/floating-chat.css')}}" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- preloader start -->
<div id="preloader">
  <div id="status"></div>
</div>
<!-- preloader end --> 

<!-- wrapper start -->
<div class="wrapper"> 
  <!-- header toolbar start -->
  <div class="header-toolbar">
    <div class="container">
      <div class="row">
        <div class="col-md-16 text-uppercase">
          <div class="row">
            <div class="col-sm-8 col-xs-16">
              <ul id="inline-popups" class="list-inline">
                <li class="hidden-xs"><a href="#"><i class="icon ion-android-call"></i> +255 26 2310021</a></li>
                <li class="hidden-xs text-lowercase"><a href="#"><i class="icon ion-email"></i> barua@sheria.go.tz</a></li>
                <li><a class="open-popup-link" href="#log-in" data-effect="mfp-zoom-in"><i class="icon ion-call"></i><i class="icon ion-locked"></i> log in</a></li>
                <li><a href="{{ url('/') }}" data-effect="mfp-zoom-in">English</a></li>
                <li><a  href="{{ url('/') }}" data-effect="mfp-zoom-in">Swahili</a></li>
              </ul>
            </div>
            <div class="col-xs-16 col-sm-8">
              <div class="row">
                <div id="weather" class="col-xs-16 col-sm-8 col-lg-9"></div>
                <div id="time-date" class="col-xs-16 col-sm-8 col-lg-7"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- header toolbar end --> 
  
  <!-- sticky header start -->
  <div class="sticky-header"> 
    <!-- header start -->
    <div style="background-image: url(new-web/images/general/flag.png);background-repeat: no-repeat;background-position: right;">
    <div class="container header headerBgImage">

      <div class="row" >
        <div class="col-sm-3 col-md-3 fadeInUpLeft animated pull-left"><img src="new-web/images/general/profile.png" alt="LOGO" id="logo"></div>
        <center>
          <div class="col-sm-10 col-md-10 animated">
            <span style="font-size:20px;color:#0c0c0c;text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4"><b>The United Republic of Tanzania</b></span><br/>
            <span style="font-size:22px;color:#286eac;text-shadow: 0 2px 0 rgba(29, 28, 28, 0.4)"><b>Ministry of Constitutional and Legal Affairs - Legal Aid Services</b></span>
          </div>
        </center>
        <div class="col-sm-2 col-md-2 fadeInUpLeft animated pull-right"><img src="new-web/images/general/scale.png" alt="LOGO" class="pull-right" id="logo"></div>
      </div>
      
    </div>
    </div>
    <!-- header end --> 
    <!-- nav and search start -->
    <div class="nav-search-outer"> 
      <!-- nav start -->
      
      <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
          <div class="row">
            <div class="col-sm-16"> <a href="javascript:;" class="toggle-search pull-right"><span class="ion-ios7-search"></span></a>
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav text-uppercase main-nav ">
                  <li class="dropdown active"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">home<span class="ion-ios7-arrow-down nav-icn"></span></a>
                  <!-- <li class="active"><a href="index.html">home</a></li> -->
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Background</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Mission Statement</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Vission Statement</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Administration<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="blog-masonry.html"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Directorate of Public Legal Services</a></li>
                      <li><a href="post-item-details.html"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Registrar of Legal Aid Services</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Documents<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Policy</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Acts</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Constitution</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Instruments</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Legal Aid Guidelines</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Services<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Legal Aid Services</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Media Center<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Press Release</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Speech</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Video Gallery</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Photo Gallery</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>News</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Audio</a></li>
                      <li><a href="javascript:void(0)"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Announsments</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Adverts<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="blog-masonry.html"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Tender</a></li>
                      <li><a href="post-item-details.html"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Others</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">blog<span class="ion-ios7-arrow-down nav-icn"></span></a>
                    <ul class="dropdown-menu text-capitalize" role="menu">
                      <li><a href="blog-masonry.html"><span class="ion-ios7-arrow-right nav-sub-icn"></span>Blog List</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- nav end --> 
        <!-- search start -->
        
        <div class="search-container ">
          <div class="container">
            <form action="" method="" role="search">
              <input id="search-bar" placeholder="Type & Hit Enter.." autocomplete="off">
            </form>
          </div>
        </div>
        <!-- search end --> 
      </nav>
      <!--nav end--> 
    </div>
    <!-- nav and search end--> 
  </div>
  <!-- sticky header end --> 
  
  <!-- main body data start -->
  <!-- === BEGIN CONTENT === -->
	@if(View::hasSection('content'))
	@yield('content')
	@endif

	@if(View::hasSection('raw-content'))
			@yield('raw-content')
	@endif
<!-- === END CONTENT === -->
  <!-- main body data end --> 
  
  <!-- Footer start -->
  <footer>
    <div class="top-sec">
      <div class="container ">
        <div class="row match-height-container">
          <div class="col-sm-6 subscribe-info wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="row">
              <div class="col-sm-16">
                <div class="f-title">Visit Us</div>
                <p>Lorem Ipsum has been the standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
              </div>
              <div class="col-sm-16">
                <div class="f-title">subscribe to news letter</div>
                <form class="form-inline">
                  <input type="email" class="form-control" id="input-email" placeholder="Type your e-mail adress">
                  <button type="submit" class="btn"> <span class="ion-paper-airplane text-danger"></span> </button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-sm-5 popular-tags  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="f-title">Useful Links</div>
            <ul class="tags list-unstyled pull-left">
              <li><a href="#">Business</a></li>
              <li><a href="#">Science</a></li>
              <li><a href="#">video conferece</a></li>
              <li><a href="#">conferece</a></li>
              <li><a href="#">Photo</a></li>
              <li><a href="#">education</a></li>
              <li><a href="#">Smart phones</a></li>
              <li><a href="#">Samsung mobile</a></li>
              <li><a href="#">AI</a></li>
              <li><a href="#">video conferece</a></li>
              <li><a href="#">conferece</a></li>
              <li><a href="#">education</a></li>
              <li><a href="#">Technology</a></li>
              <li><a href="#">computer</a></li>
            </ul>
          </div>
          <div class="col-sm-5 recent-posts  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="f-title">recent posts</div>
            <ul class="list-unstyled">
              <li> <a href="#">
                <div class="row">
                  <div class="col-sm-4"><img class="img-thumbnail pull-left" src="images/footer-recent/f-recent-1.jpg" width="70" height="70" alt=""/> </div>
                  <div class="col-sm-12">
                    <h4>The evolution of the apple ..</h4>
                    <div class="f-sub-info">
                      <div class="time"><span class="ion-android-data icon"></span>Dec 16 2014</div>
                      <div class="comments"><span class="ion-chatbubbles icon"></span>351</div>
                      <div class="stars"><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star-half"></span></div>
                    </div>
                  </div>
                </div>
                </a> </li>
              <li> <a href="#">
                <div class="row">
                  <div class="col-sm-4 "><img class="img-thumbnail pull-left" src="images/footer-recent/f-recent-2.jpg" width="70" height="70" alt=""/> </div>
                  <div class="col-sm-12">
                    <h4>The evolution of the apple ..</h4>
                    <div class="f-sub-info">
                      <div class="time"><span class="ion-android-data icon"></span>Dec 16 2014</div>
                      <div class="comments"><span class="ion-chatbubbles icon"></span>351</div>
                      <div class="stars"><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star-half"></span></div>
                    </div>
                  </div>
                </div>
                </a> </li>
              <li> <a href="#">
                <div class="row">
                  <div class="col-sm-4"><img class="img-thumbnail pull-left" src="images/footer-recent/f-recent-3.jpg" width="70" height="70" alt=""/> </div>
                  <div class="col-sm-12">
                    <h4>The evolution of the apple ..</h4>
                    <div class="f-sub-info">
                      <div class="time"><span class="ion-android-data icon"></span>Dec 16 2014</div>
                      <div class="comments"><span class="ion-chatbubbles icon"></span>351</div>
                      <div class="stars"><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star"></span><span class="ion-ios7-star-half"></span></div>
                    </div>
                  </div>
                </div>
                </a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="btm-sec">
      <div class="container">
        <div class="row">
          <div class="col-sm-16">
            <div class="row">
              <div class="col-sm-10 col-xs-16 f-nav wow fadeInDown animated" data-wow-delay="0.5s" data-wow-offset="10">
                <ul class="list-inline ">
                  <li> <a href="#"> Home </a> </li>
                  <li> <a href="#"> About </a> </li>
                  <li> <a href="#"> Contact </a> </li>
                </ul>
              </div>
              <div class="col-sm-6 col-xs-16 copyrights text-right wow fadeInDown animated" data-wow-delay="0.5s" data-wow-offset="10">
                    Copyright &copy; <?php echo date('Y'); ?> Ministry of Constitutional and Legal Affairs<a href="https://maxtech.com" target="_blank"> - maxtech</a>
              </div>
            </div>
          </div>
          <div class="col-sm-16 f-social  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="10">
            <ul class="list-inline">
              <li> <a href="#"><span class="ion-social-twitter"></span></a> </li>
              <li> <a href="#"><span class="ion-social-facebook"></span></a> </li>
              <li> <a href="#"><span class="ion-social-instagram"></span></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->

  <!-- Floating Chat -->
	<div class="l_c_h">
		<div class="c_h">
			<div class="left_c">
				<div class="left right_c left_icons">
					   <a href="#" class="mini" style="font-size:23px;">+</a>
				</div>
				<div class="left center_icons"><!--center_icons-->
					 Live help!
				</div><!--end center_icons-->        	
			</div>
			<div class="right right_c" style="width:35px;">
				<a href="#" class="logout" title="End chat" name="" style="display:none;"><img src="chat/logout.png"></a>        	
			</div>
			<div class="clear"></div>
		</div>
		<div class="chat_container" style="display: none;">
		
		<div class="chat_message" style="display: none;">
		<input type="hidden" class="my_user" value="">
				</div>
			<div class="chat_text_area" style="display:none;">
				<textarea name="messag_send" class="messag_send" id="messag_send" placeholder="Enter Your Message and press CTRL"></textarea>
			</div>
			<div class="chat_entry">
				   <form name="chat_form" class="chat_form has-validation-callback" id="chat_form" onsubmit="return false;">
					<div class="row">                    
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Name*</label>
								<input type="text" class="form-control" name="name" id="title" value="{{ old('kpiName') }}" placeholder="Enter your name" required>
							</div>
						</div>
				    </div>
					<div class="row">                    
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Email*</label>
								<input type="text" class="form-control" name="email" id="title" value="{{ old('kpiName') }}" placeholder="Enter your email" required>
							</div>
						</div>
				    </div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Message*</label>
								<textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter your message"  rows="2" cols="55"></textarea>
							</div>
						</div>
					</div>
				    <button type="submit" id="specific" class="btn btn-sm btn-primary"><i class="fa fa-sign-in"></i> Submit</button>
				</form>
			</div>
		</div>
	</div>

  <!-- Login Modal Popup -->
  <div id="log-in" class="white-popup mfp-with-anim mfp-hide">
    <form role="form">
      <h3>Staff Log In</h3>
      <hr>
      <div class="form-group">
        <input type="text" name="email" id="access_name" class="form-control" placeholder="Username" tabindex="3">
      </div>
      <div class="form-group">
        <input type="password" name="password" id="password" class="form-control " placeholder="Password" tabindex="4">
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-16">
          <input type="submit" value="Log In" class="btn btn-danger btn-block btn-lg" tabindex="7">
        </div>
      </div>
    </form>
  </div>
</div>
<!-- wrapper end --> 

<!-- jQuery --> 
<script src="{{asset('new-web/js/jquery.min.js')}}"></script>
<!--jQuery easing--> 
<script src="{{asset('new-web/js/jquery.easing.1.3.js')}}"></script>
<!-- bootstrab js --> 
<script src="{{asset('new-web/js/bootstrap.js')}}"></script> 
<!--style switcher-->  <!--wow animation-->
<script src="{{asset('new-web/js/style-switcher.js')}}"></script>
<script src="{{asset('new-web/js/wow.min.js')}}"></script>
<!-- time and date --> 
<script src="{{asset('new-web/js/moment.min.js')}}"></script> 
<!--news ticker-->
<script src="{{asset('new-web/js/jquery.ticker.js')}}"></script> 
<!-- owl carousel --> 
<script src="{{asset('new-web/js/owl.carousel.js')}}"></script> 
<!-- magnific popup --> 
<script src="{{asset('new-web/js/jquery.magnific-popup.js')}}"></script> 
<!-- weather --> 
<script src="{{asset('new-web/js/jquery.simpleWeather.min.js')}}"></script> 
<!-- calendar--> 
<script src="{{asset('new-web/js/jquery.pickmeup.js')}}"></script> 
<!-- go to top --> 
<script src="{{asset('new-web/js/jquery.scrollUp.js')}}"></script> 
<!-- scroll bar --> 
<script src="{{asset('new-web/js/jquery.nicescroll.js')}}"></script> 
<script src="{{asset('new-web/js/jquery.nicescroll.plus.js')}}"></script> 
<!--masonry--> 
<script src="{{asset('new-web/js/masonry.pkgd.js')}}"></script> 
<!--media queries to js--> 
<script src="{{asset('new-web/js/enquire.js')}}"></script> 
<!--custom functions-->
<script src="{{asset('new-web/js/custom-fun.js')}}"></script>
<!--floating chat-->
<script src="{{asset('new-web/js/floating-chat.js')}}"></script>
</body>
</html>