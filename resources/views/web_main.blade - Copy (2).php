<!DOCTYPE html>
<html lang="en">
<head>
	<title>
		@section('title')
			MoCLA |
		@show
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ URL::asset('images/icons/scale.png') }}"/>
<!--===============================================================================================-->
    <link href="{{asset('web/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/fonts/iconic/css/material-design-iconic-font.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/vendor/animate/animate.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/vendor/animsition/css/animsition.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/css/util.min.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/css/main.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
    <link href="{{asset('web/css/floating-chat.css?v=').time()}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
	<link href="{{asset('web/css/accordion.css?v=').time()}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
	<link href="{{asset('web/css/search.css?v=').time()}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
	<link href="{{asset('web/css/datatable.css?v=').time()}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
<link href="{{asset('web/css/navmenu.css')}}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
<style>
	@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css););
</style>
</head>
<body class="animsition">
<!-- === BEGIN HEADER === -->
	<!-- Header -->
	<header>
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<div class="topbar" style="height:45px;margin-bottom: 10px;background-color:transparent;">
				<div class="content-topbar container" style="background-color:transparent;">
					<div class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							
						</span>

						<a href="#" class="left-topbar-item" style="color: #666;">
							<i class="fas fa-phone"></i> +255 26 2310021
						</a>

						<a href="#" class="left-topbar-item" style="color: #666;">
							<i class="fas fa-envelope"></i> registrar@sheria.go.tz
						</a>

						
					</div>

					<div class="right-topbar" style="color: #666;">
						<a href="#">
							<span class="fas fa-language"></span>
						</a>
						<a href="#">
							<span class="#" style="color: #666;"><small>SW</small></span>
						</a>
						<a href="#">
							<span class="#" style="color: #666;"><small>EN</small></span>
						</a>
						<a href="#">
							<span style="color: #666;" class="fab fa-facebook-f"></span>
						</a>

						<a href="#">
							<span style="color: #666;" class="fab fa-twitter"></span>
						</a>

						<a href="#">
							<span style="color: #666;" class="fab fa-youtube"></span>
						</a>
					</div>
				</div>
			</div>

			<!-- Header Mobile -->
			<div class="wrap-header-mobile">
				<!-- Logo moblie -->		
				<div class="logo-mobile">
					<a href="#"><img src="{{ URL::asset('images/icons/profile.png') }}" alt="IMG-LOGO"></a>
				</div>

				<!-- Button show menu -->
				<div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>

			<!-- Menu Mobile -->
			<div class="menu-mobile">
				<ul class="topbar-mobile">
					<li class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							<a href="#" class="left-topbar-item">
								<i class="fas fa-phone"></i> +255 26 2310021
							</a>
	
							<a href="#" class="left-topbar-item">
								<i class="fas fa-envelope"></i> registrar@sheria.go.tz
							</a>
						</span>
					</li>

					<li class="left-topbar">
						
					</li>

					<li class="right-topbar">
						<a href="#">
							<span class="fas fa-language"></span>
						</a>
						<a href="#">
							<span class="#"><small>SW</small></span>
						</a>
						<a href="#">
							<span class="#"><small>EN</small></span>
						</a>
						<a href="#">
							<span class="fab fa-facebook-f"></span>
						</a>

						<a href="#">
							<span class="fab fa-twitter"></span>
						</a>

						<a href="#">
							<span class="fab fa-youtube"></span>
						</a>
					</li>
				</ul>

				<ul class="main-menu-m">
					<li>
						<a href="#">Home</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/background')}}">Background</a></li>
							<li><a href="{{url('cms/vission')}}">Vision</a></li>
							<li><a href="{{url('cms/mission')}}">Mission</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="#">Administration</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/board')}}">Legal Aid Board </a></li>
							<li><a href="{{url('cms/registrar')}}">Directorate of Public Legal Services </a></li>
							<li><a href="{{url('cms/directorate')}}">Registrar of Legal Services </a></li>
							<li><a href="{{url('cms/project')}}">Projects </a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="#">Documents</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/constitution')}}">Constitution </a></li>
							<li><a href="{{url('cms/policy')}}">Policy </a></li>
							<li><a href="{{url('cms/act')}}">Acts </a></li>
							<li><a href="{{url('cms/guideline')}}">Guidelines </a></li>
							<li><a href="{{url('cms/report')}}">Reports </a></li>
							<li><a href="{{url('cms/workplan')}}">Work Plans </a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>


					<li>
						<a href="#">Services</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/registration')}}">Registration Servicess</a></li>
							<li><a href="{{url('cms/m&e')}}">M&E Services</a></li>
							<li><a href="{{url('cms/service-provider')}}">Legal Aid Service Provides</a></li>
							<li><a href="{{url('cms/paralegal')}}">Paralegals</a></li>
							<li><a href="{{url('cms/training')}}">Trainings</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="#">Media Center</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/press')}}">Press Release</a></li>
							<li><a href="{{url('cms/speech')}}">Speech</a></li>
							<li><a href="{{url('cms/video')}}">Video Gallery</a></li>
							<li><a href="{{url('cms/photo')}}">Photo Gallery</a></li>
							<li><a href="{{url('cms/news')}}">News</a></li>
							<li><a href="{{url('cms/audio')}}">Audio</a></li>
							<li><a href="{{url('cms/announcement')}}">Announcements</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="#">Announcements</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/tender')}}">Tender</a></li>
							<li><a href="{{url('cms/other')}}">Others</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="#">Blog</a>
						<ul class="sub-menu-m">
							<li><a href="{{url('cms/blog')}}">Blog List</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

				
				</ul>
			</div>
			
			<!--  -->
			<div style="background-image: url('{{asset('images/flag.png')}}');background-repeat: no-repeat;background-position: right;">
			<div class="wrap-logo container">
				<!-- Logo desktop -->		
				<div class="logo">
					<a href="#"><img src="{{ URL::asset('images/icons/profile.png') }}" alt="LOGO" id="logo"></a>
				</div>	
				<div class="heading">
					<center>
					<span style="font-size:19px;color:#0c0c0c;text-shadow: 0 1px 0 rgba(255, 255, 255, 0.4;"><b>THE UNITED REPUBLIC OF TANZANIA</b></span><br/>
                    <span style="font-size:20px;color:#286eac;text-shadow: 0 1px 0 #999;"><b>MINISTRY OF CONSTITUTIONAL AND LEGAL AFFAIRS (MoCLA)</b></span><br/>
					<span style="font-size:20px;color:#286eac;text-shadow: 0 1px 0 #999;"><b>Legal Aid Services Portal</b></span>
					</center>
				</div>
				<!-- Banner -->
				<div class="banner-header">
					<a href="#"><img src="{{ URL::asset('images/scale1.png') }}" alt="IMG" id="logo"></a>
				</div>
			</div>	
			</div>
			
			<!--  -->
			<div class="wrap-main-nav">
				<div class="main-nav sticky">
					<!-- Menu desktop -->
					<nav class="menu-desktop">
						<a class="logo-stick" href="#">
							<img src="{{ URL::asset('images/icons/profile.png') }}" alt="LOGO" id="logo" alt="LOGO">
						</a>

						<ul class='core-menu'>
							<li><a href="{{url('/')}}">Home<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/background')}}">Background</a></li>
									<li><a href="{{url('cms/vission')}}">Vision</a></li>
									<li><a href="{{url('cms/mission')}}">Mission</a></li>
								</ul>
							</li>
							<li><a href='#'>Administration<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/board')}}">Legal Aid Board </a></li>
									<li><a href="{{url('cms/registrar')}}">Registrar of Legal Aid Services </a></li>
									<li><a href="{{url('cms/a-registrar')}}">Assistant Registrar of Legal Aid Services </a></li>
								</ul>
							</li>
							<li><a href='#'>Legal Instruments<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/constitution')}}">Constitution </a></li>
									<li><a href="{{url('cms/policy')}}">Policy </a></li>
									<li><a href="{{url('cms/act')}}">Acts </a></li>
									<li><a href="{{url('cms/regulation')}}">Regulations </a></li>
									<li><a href="{{url('cms/circular')}}">Circulars </a></li>
									<li><a href="{{url('cms/form')}}">Forms </a></li>
									<li><a href="{{url('cms/program')}}">Programs </a></li>
									<li><a href="{{url('cms/guideline')}}">Guidelines </a></li>
									<li><a href="{{url('cms/workplan')}}">Work Plans </a></li>
								</ul>
							</li>
							<li><a href='#'>Services<span class='toggle'></span></a>
								<ul class='dropdown'>
									<li><a href='#'>Registration<span class='toggle2'></span></a></a>
										<ul class='dropdown2'>
											<li><a href="{{url('cms/reg-organisation')}}">Registration of Organisation</a></li>
											<li><a href="{{url('cms/reg-paralegal')}}">Registration of Individual Paralegal</a></li>
										</ul>
									</li>
									<li><a href='#'>Acreditation<span class='toggle2'></span></a></a>
										<ul class='dropdown2'>
											<li><a href="{{url('cms/dev-training')}}">Development of training Cariculum & Manuals</a></li>
											<li><a href="{{url('cms/sup-training')}}">Supervision of Training of Paralegals</a></li>
										</ul>
									</li>
									<li><a href='#'>Capacity Building<span class='toggle2'></span></a></a>
										<ul class='dropdown2'>
											<li><a href="{{url('cms/train-justice')}}">Training to Justice Actors</a></li>
											<li><a href="{{url('cms/train-provider')}}">Training to Legal Aid Providers</a></li>
										</ul>
									</li>
									<li><a href='#'>Legal Aid Services<span class='toggle2'></span></a></a>
										<ul class='dropdown2'>
											<li><a href="{{url('cms/service-lap')}}">Legal Aid Service by LAPs</a></li>
											<li><a href="{{url('cms/service-court')}}">Legal Aid Service by Order of Court</a></li>
											<li><a href="{{url('cms/service-detention')}}">Legal Aid Service in Detention Services</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li><a href='#'>Media Center<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/press')}}">Press Release</a></li>
									<li><a href="{{url('cms/speech')}}">Speech</a></li>
									<li><a href="{{url('cms/video')}}">Video Gallery</a></li>
									<li><a href="{{url('cms/photo')}}">Photo Gallery</a></li>
									<li><a href="{{url('cms/news')}}">News</a></li>
									<li><a href="{{url('cms/audio')}}">Audio</a></li>
									<li><a href="{{url('cms/announcement')}}">Announcements</a></li>
								</ul>
							</li>
							<li><a href='#'>Annoucements<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/tender')}}">Tender</a></li>
									<li><a href="{{url('cms/other')}}">Others</a></li>
								</ul>
							</li>
							<li><a href='#'>More Pages<span class='toggle'></a>
								<ul class='dropdown'>
									<li><a href="{{url('cms/tender')}}">Tender</a></li>
									<li><a href="{{url('cms/other')}}">Others</a></li>
								</ul>
							</li>
						</ul>
					
					</nav>
				</div>
			</div>	
		</div>
	</header>
<!-- === END HEADER === -->

<!-- === BEGIN CONTENT === -->
	@if(View::hasSection('content'))
	@yield('content')
	@endif

	@if(View::hasSection('raw-content'))
			@yield('raw-content')
	@endif
<!-- === END CONTENT === -->
	

<!-- === BEGIN FOOTER === -->
	<!-- Footer -->
	<footer>
		<div class="bg2 p-t-40 p-b-25">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
						<h5 class="f1-m-7 cl0">
								Visit Us
							</h5>
						</div>

						<div>
							<p class="f1-s-1 cl11 p-b-16">
								Ministry of Constitutional and Legal Affairs (MoCLA)<br/>

								Government City,Mtumba<br/>

								P.O Box. 315,<br/>

								DODOMA.<br/>

								Registry: barua@sheria.go.tz<br/>

								Phone: +255 26 2310021<br/>

								Fax: 255 26 2321679
							</p>

							<p class="f1-s-1 cl11 p-b-16">
								Any questions? Barua pepe: km@sheria.go.tz
							</p>

							<div class="p-t-15">
								<a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
									<span class="fab fa-facebook-f"></span>
								</a>

								<a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
									<span class="fab fa-twitter"></span>
								</a>

								<a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
									<span class="fab fa-pinterest-p"></span>
								</a>

								<a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
									<span class="fab fa-vimeo-v"></span>
								</a>

								<a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
									<span class="fab fa-youtube"></span>
								</a>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Recent Posts
							</h5>
						</div>

						<ul>
							@if($news)
							@foreach($news as $new)	
							<li class="flex-wr-sb-s p-b-20">
								<a href="{{ url('cms/news-view', $new->id) }}" class="size-w-4 wrap-pic-w hov1 trans-03">
									<img src="{{ URL::to('storage/downloads/'.$new->file_name) }}" alt="IMG">
								</a>

								<div class="size-w-5">
									<h6 class="p-b-5">
										<a href="{{ url('cms/news-view', $new->id) }}" class="f1-s-5 cl11 hov-cl10 trans-03">
											{{substr(strtoupper($new->title), 0, 40)}}
										</a>
									</h6>

									<span class="f1-s-3 cl6">
										{{ date('F d Y', strtotime($new->date_published))}}
									</span>
								</div>
							</li>
							@endforeach
							@endif
						</ul>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Useful Links
							</h5>
						</div>

						<ul class="m-t--12">
							@foreach($links as $link)
								<li class="how-bor1 p-rl-5 p-tb-10">
									<a href="#" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
										{{$link->name}}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="bg11">
			<div class="container size-h-4 flex-c-c p-tb-15">
				<span class="f1-s-1 cl0 txt-center">
					Copyright &copy; <?php echo date('Y'); ?> Ministry of Constitutional and Legal Affairs
					
				</span>
			</div>
		</div>
	</footer>

	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>

    <!-- Floating Chat -->
	<div class="l_c_h">
		<div class="c_h">
			<div class="left_c">
				<div class="left right_c left_icons">
					   <a href="#" class="mini" style="font-size:23px;">+</a>
				</div>
				<div class="left center_icons"><!--center_icons-->
					 Live help!
				</div><!--end center_icons-->        	
			</div>
			<div class="right right_c" style="width:35px;">
				<a href="#" class="logout" title="End chat" name="" style="display:none;"><img src="chat/logout.png"></a>        	
			</div>
			<div class="clear"></div>
		</div>
		<div class="chat_container" style="display: none;">
		
		<div class="chat_message" style="display: none;">
		<input type="hidden" class="my_user" value="">
				</div>
			<div class="chat_text_area" style="display:none;">
				<textarea name="messag_send" class="messag_send" id="messag_send" placeholder="Enter Your Message and press CTRL"></textarea>
			</div>
			<div class="chat_entry">
				   <form name="chat_form" class="chat_form has-validation-callback" id="chat_form" onsubmit="return false;">
					<div class="row">                    
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Name*</label>
								<input type="text" class="form-control" name="name" id="title" value="{{ old('kpiName') }}" placeholder="Enter your name" required>
							</div>
						</div>
				    </div>
					<div class="row">                    
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Email*</label>
								<input type="text" class="form-control" name="email" id="title" value="{{ old('kpiName') }}" placeholder="Enter your email" required>
							</div>
						</div>
				    </div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-bold">Message*</label>
								<textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter your message"  rows="2" cols="55"></textarea>
							</div>
						</div>
					</div>
				    <button type="submit" id="specific" class="btn btn-sm btn-primary"><i class="fa fa-sign-in"></i> Submit</button>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal Video 01-->
	<div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document" data-dismiss="modal">
			<div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

			<div class="wrap-video-mo-01">
				<div class="video-mo-01">
					<iframe src="https://www.youtube.com/embed/wJnBTPUQS5A?rel=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

<!-- === END FOOTER === -->

<!--===============================================================================================-->
    <script src="{{asset('web/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('web/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('web/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('web/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('web/js/main.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('web/js/floating-chat.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('web/js/datatable.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('web/js/navbarmenu.js')}}"></script>
<!--===============================================================================================-->

	<script>
		$(function() {
		$(".expand").on( "click", function() {
		$(this).next().slideToggle(200);
		$expand = $(this).find(">:first-child");
		
		if($expand.text() == "+") {
		$expand.text("-");
		} else {
		$expand.text("+");
		}
		});
		});   

</script>

</body>
</html>