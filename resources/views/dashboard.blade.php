@extends('main')

@section('title')
    
    Admin Dashboard
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">
    <style>
        .card{
        border: 1px solid #C0C0C0;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        border-radius: 5px;
        }

        img{
          border-radius: 5px;
          width: 70px;
          top: 3px;
        }
        .col-md-3{
            padding-top: 5px;
        }
        
        .head{
            padding-top: 10px;
            color:  #3a383b;
        }

        .card-content{
          margin: 10px; 
          color:  #3a383b;
        }

        p{
          font-family: segoe ui;
          line-height: 1.4em;
          font-size: 4.1em;
          text-align: center;
        }

        #mainbox{
          font-family: calibri;
          box-sizing: border-box;
          justify-content: center;
          display: flex;
        flex-wrap: wrap;
        }
    </style>
 
@stop



@section('raw-content')
@if ($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @foreach ($errors->all() as $error)
            <a href="#" class="alert-link">ERROR! :</a> {{ $error }} <br/>
        @endforeach
    </div>
@endif


<div class="wrapper wrapper-content">
    <div class="row">
                <div class="col-md-3">
                    <div class="ibox float-e-margins">
                        <a href="{{url('user/aid-today')}}">
                        <div class="card">
                        <div class="card-content">
                            <span class="pull-right"><img src="{{ URL::to('/images/users/filed.png')}}" /></span>
                            <h1 class="no-margins"><strong>{{ $legalAidTodayCount }}</strong></h1>
                            <p style="font-size: 17px;text-align:left;">Legal Aid provided</p><small>(Today)</small>
                        </div>
                       </div>
                       </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="ibox float-e-margins">
                    <a href="{{url('user/aid-thismonth')}}">
                        <div class="card">
                        <div class="card-content">
                            <span class="pull-right"><img src="{{ URL::to('/images/users/filed.png')}}" /></span>
                            <h1 class="no-margins"><strong>{{ $legalAidThisMonthCount }}</strong></h1>
                            <p style="font-size: 17px;text-align:left;">Legal Aid provided</p><small>(This Month )</small>
                        </div>
                       </div>
                    </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="ibox float-e-margins">
                    <a href="{{url('user/aid-thisyear')}}">
                        <div class="card">
                        <div class="card-content">
                            <span class="pull-right"><img src="{{ URL::to('/images/users/filed.png')}}" /></span>
                            <h1 class="no-margins"><strong>{{ $legalAidThisYearCount }}</strong></h1>
                            <p style="font-size: 17px;text-align:left;">Legal Aid provided</p><small>(This Year )</small>
                        </div>
                       </div>
                    </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="ibox float-e-margins">
                    <a href="{{url('admin/aid-desk')}}">
                        <div class="card">
                        <div class="card-content">
                            <span class="pull-right"><img src="{{ URL::to('/images/users/filed.png')}}" /></span>
                            <h1 class="no-margins"><strong>{{ $legalAidDeskCount }}</strong></h1>
                            <p style="font-size: 17px;text-align:left;">Number of Legal Aid Desks</p><small>(Total )</small>
                        </div>
                       </div>
                    </a>
                    </div>
                </div>
    </div>
    
</div>

        
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function(){

            
            $('#courtLevel').on('change', function() {
                var url = "{{ url('civil-admission/courts') }}" + "/" + this.value;
                $.get(url).done(function( data ) {
                    var select = "";
                    var options = "";
                    select+= "<select class='chose-select form-control'>";
                    options+= "<option value=''>" +"</option>";
                    for(var datum in data){
                        options+= "<option value='" + datum + "'>" + data[datum] +"</option>";
                    }
                    select+= "</select";
                    $("#courtId").empty().append(options);
                });
            });
            
            
            $(".chosen-select-nature").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select-charges').select2({
                placeholder: "--Select Charge--",
                width: "100%"
            });

            $('.courtLevelId').select2({
                placeholder: '--Select Court Level--'
            });
            
            $('.courtId').select2({
                placeholder: '--Select Court--'
            });
            
            
           

            $('.fee-select').chosen({ width:'100%'});

            
            
            
            

            $("#selectbox").on("change", function() {
              var sOptionVal = $(this).val();
              if (/modal/i.test(sOptionVal)) {
                var $selectedOption = $(sOptionVal);
                $selectedOption.modal('show');
              }
            });
            
            
            $('table thead th').each(function(i) {
                calculateColumn(i);
            });
            
        });
        
        function calculateColumn(index) {
            var total = 0;
            $('table tr').each(function() {
                var value = parseInt($('td', this).eq(index).text());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            $('table tfoot td').eq(index).text(total);
        }
    </script>
@stop

