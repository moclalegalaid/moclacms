@extends('web_main')

@section('title')
    Legal Aid Services Portal
@stop
 <!-- === BEGIN CONTENT === -->
@section('raw-content')
@if ($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        @foreach ($errors->all() as $error)
            <a href="#" class="alert-link">ERROR! :</a> {{ $error }} <br/>
        @endforeach
    </div>
@endif

<!-- Headline -->
<div class="container">
		<div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 size-w-0 m-tb-6 flex-wr-s-c">
				<span class="text-uppercase cl2 p-r-8">
					Trending Now:
				</span>

				<span class="dis-inline-block cl6 slide100-txt pos-relative size-w-0" data-in="fadeInDown" data-out="fadeOutDown">
				  @if(count($trendings) > 0)
					@foreach( $trendings as $trending )
					<span style="color:#4682b4;" class="dis-inline-block slide100-txt-item animated visible-false">
						{{strtoupper($trending->title)}}
					</span>
					@endforeach
				  @else
				  <span style="color:#4682b4;" class="dis-inline-block slide100-txt-item animated visible-false">
						No Updates!
					</span>
				  @endif
				</span>
			</div>

			<div class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="search" placeholder="Search">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</div>
		</div>
	</div>

	<!-- Feature post -->
	<section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
			<div class="container">
				<div class="row">
					<div class="col-sm-2 col-lg-2 p-b-10">
						<!-- Left side image -->
						
						<div class="flex-wr-sb-s m-b-10">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Publications
								</h3>
							</div>
							<a href="#" class="wrap-pic-w hov1 trans-03">
							@if($publication)
							<ul class="m-t--12">
								@foreach($publication as $key => $publications)
								<li class="how-bor1 p-rl-5 p-tb-10">
									<a style="text-align:justfy;" target="_blank" href="{{ asset('storage/downloads/'.$publications->file_name) }}">
									{{ $publications->title }}
									</a>
								</li>
								@endforeach
							</ul>
							<a href="#" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
									View all
									<i class="fs-12 m-l-5 fa fa-caret-right"></i>
							</a>
								
							@else
							<ul class="m-t--12">
								<li class="how-bor1 p-rl-5 p-tb-20">
									<a href="#" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
										No document to display
									</a>
								</li>
							</ul>
							@endif
							</a>

						</div>
					</div>

					<div class="col-sm-8 col-lg-8 p-b-20">
						<!-- Carousel -->	
						<div class="col-md-12">

							<div id="carouselExample" class="carousel slide" data-ride="carousel">
			
								<div class="row">
			
									<div class="col-md-12">
			
										<div class="carousel-inner">
                                            @foreach( $news as $value )
											<div class="carousel-item {{ $loop->first ? 'active' : '' }}">
			
												<div class="row">
			
													<div class="col-md-9">
			
														<img class="d-block w-100" src="{{ URL::to('storage/downloads/'.$value->file_name) }}" alt="First slide">
														
													</div>
													<!-- End of Col-md-4 -->
			
													<div class="col-md-3">
			
														<p style="text-align: justify;color:black;">
                                                            {{strtoupper($value->title)}}
														</p>
			
														<a href="{{ url('cms/news-view', $value->id) }}" class="btn btn-sm btn-primary">Read More...</a>
			
													</div>
													<!-- End of Col-md-8 -->
			
												</div>
												<!-- End of Row -->
			
											</div>
											<!-- End of Carousel Item -->
                                            @endforeach
										</div>

										<!-- End of Carousel Inner -->
									
									</div>
									<!-- End of Col-md-12 -->
			
									<div class="col-md-12">
			
										<ol class="carousel-indicators">
                                            @foreach( $news as $value )
											<li data-target="#carouselExample" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                            @endforeach
										</ol>
										<!-- End of Carousel Indicators -->
			
									</div>
									<!-- End of Col-md-12 -->
			
								
								</div>
								<!-- End of Row -->
			
							</div>
							<!-- End of carouselExample-->
			
						</div>
					</div>

					<div class="col-sm-2 col-lg-2 p-b-20">
						<!-- Right side image -->
						<div class="flex-wr-sb-s m-b-30">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Management
								</h3>
							</div>
							<a href="#" class="wrap-pic-w hov1 trans-03">
							@if($management)
							<div id="carouselExample" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner">
									@foreach( $management as $value ) 
											<div class="carousel-item {{ $loop->first ? 'active' : '' }}">
												<a href="{{ url('cms/mgt-view', $value->id) }}">
			
												<div class="row">
			
													<div class="col-md-12">
			
														<img id="leader-image" src="{{ URL::to('storage/downloads/'.$value->file) }}">
			
													</div>
													<!-- End of Col-md-4 -->
			
												</div>

												<div class="row">
			
													<div class="col-md-12">
														<h3 style="color:black;font-size:13px;text-align:center;">{{strtoupper($value->name)}}</h3>
													</div>
			
												</div>
												<div class="row">
			
													<div class="col-md-12">
														<h3 style="color:black;font-size:13px;text-align:center;">{{strtoupper($value->title)}}</h3>
													</div>
			
												</div>

												<!-- End of Row -->
											  </a>
											</div>
											<!-- End of Carousel Item -->
											
											@endforeach
									</div>
								</div>
									
							@else
								<img src="images/bio_1627910885-Prof Kabudi2.jpg" alt="IMG" id="leader-image">
							@endif
							</a>

							<div class="p-t-10" id="avator">
								<h5 class="p-b-5">
									<a href="#" class="f1-s-5 cl3 hov-cl10 trans-03">
									@if($management->publish = 1)
										
									@else
										hshshshshs
									@endif
									</a>
								</h5>

								<span class="cl8">
									<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
									@if($management)
									
									@else
										hfhdjdhdjdhkdjdj
									@endif
									</a>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

  <!-- ======= Hero Section end ======= -->

  <section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-lg-12 p-b-10">
						<!-- Left side Bar -->
						
						<div class="flex-wr-sb-s m-b-10">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Need legal aid? Find legal aid providers near you
								</h3>
							</div>
							<section class="search-sec">
								<div class="container">
									<form action="{{ url('regions') }}" method="get" novalidate="novalidate">
										<div class="row">
											<div class="col-lg-12">
												<div class="row">
													<div class="col-lg-4 col-md-4 col-sm-12 p-0">
														<select class="form-control search-slt" id="exampleFormControlSelect1">
															<option>Select Legal Aid</option>
															<option>All</option>
														</select> 
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 p-0">
														<select name="regions" class="form-control search-slt" id="exampleFormControlSelect1">
															<option value="">Select Region</option>
															<option value="01">Arusha</option>
															<option value="02">Dar es Salaam</option>
															<option value="03">Dodoma</option>
															<option value="04">Iringa</option>
															<option value="05">Kagera</option>
															<option value="08">Kigoma</option>
															<option value="09">Kilimanjaro</option>
															<option value="12">Lindi</option>
															<option value="13">Mara</option>
															<option value="14">Mbeya</option>
															<option value="16">Morogoro</option>
															<option value="17">Mtwara</option>
															<option value="18">Mwanza</option>
															<option value="19">Pwani</option>
															<option value="20">Rukwa</option>
															<option value="21">Ruvuma</option>
															<option value="22">Shinyanga</option>
															<option value="23">Singida</option>
															<option value="24">Tabora</option>
															<option value="25">Tanga</option>
															<option value="26">Manyara</option>
															<option value="27">Geita</option>
															<option value="28">Katavi</option>
															<option value="29">Njombe</option>
															<option value="30">Simiyu</option>
															<option value="31">Songwe</option>
														</select>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-12 p-0">
														<button type="submit" class="btn btn-primary wrn-btn">Search</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</section>

						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

  <!-- Main Content -->
	<section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
			<div class="container">
				<div class="row">
					<div class="col-sm-9 col-lg-9 p-b-10">
						<!-- Left side Bar -->
						
						<div class="flex-wr-sb-s m-b-10">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									News
								</h3>
							</div>
								<!-- - -->
								<div class="tab-pane fade show active" id="tab3-1" role="tabpanel">
									<div class="row">
										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<!-- Item post -->
											@if($newsCurrent)
											@foreach($newsCurrent as $newsCurrents)	
											<div class="m-b-30">
												<a href="{{ url('cms/news-view', $newsCurrents->id) }}" class="wrap-pic-w hov1 trans-03">
													<img src="{{ URL::to('storage/downloads/'.$newsCurrents->file_name) }}" alt="IMG">
												</a>

												<div class="p-t-20">
													<h5 class="p-b-5">
														<a href="{{ url('cms/news-view', $newsCurrents->id) }}" class="f1-m-3 cl2 hov-cl10 trans-03">
															{{strtoupper($newsCurrents->title)}}
														</a>
													</h5>

													<span class="cl8">
														<a href="{{ url('cms/news-view', $newsCurrents->id) }}" class="f1-s-4 cl8 hov-cl10 trans-03">
															{{$newsCurrents->publisher}}
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															{{$newsCurrents->date_published}}
														</span>
													</span>
												</div>
											</div>
											@endforeach
											@endif
										</div>

										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<!-- Item post -->
											@if($news)
											@foreach($news as $new)	
											<div class="flex-wr-sb-s m-b-30">
												<a href="{{ url('cms/news-view', $new->id) }}" class="size-w-1 wrap-pic-w hov1 trans-03">
													<img src="{{ URL::to('storage/downloads/'.$new->file_name) }}" alt="IMG">
												</a>

												<div class="size-w-2">
													<h5 class="p-b-5">
														<a href="{{ url('cms/news-view', $new->id) }}" class="f1-s-5 cl2 hov-cl10 trans-03">
															{{substr(strtoupper($new->title), 0, 40)}}
														</a>
													</h5>

													<span class="cl8">
														<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
															{{$new->publisher}}
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															{{$new->date_published}}
														</span>
													</span>
												</div>
											</div>
											@endforeach
											@endif

										</div>
									</div>
								</div>

						</div>
					</div>

					

					<div class="col-sm-3 col-lg-3 p-b-20">
						<!-- Right side image -->
						<div class="flex-wr-sb-s m-b-30">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Announcements
								</h3>
							</div>
							<a href="#" class="wrap-pic-w hov1 trans-03">
							@if($anounsment)
							<ul class="m-t--12">
								@foreach($anounsment as $key => $anounsments)
								<li class="how-bor1 p-rl-5 p-tb-10">
									<a style="text-align:justfy;color:black;" target="_blank" href="{{ asset('storage/downloads/'.$anounsments->file_name) }}">
									{{ $anounsments->title }}
									</a>
								</li>
								@endforeach
							</ul>
							<a href="#" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03">
									View all
									<i class="fs-12 m-l-5 fa fa-caret-right"></i>
							</a>
								
							@else
							<ul class="m-t--12">
								<li class="how-bor1 p-rl-5 p-tb-20">
									<a href="#" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
										No anounsment to display
									</a>
								</li>
							</ul>
							@endif
							</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	 <!-- Main Content -->
	 <section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-lg-6 p-b-10">
						<!-- Left side Bar -->
						
						<div class="flex-wr-sb-s m-b-10">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									How do I
								</h3>
							</div>

							<div class="row">				
							<div class="col-md-12">
							<div class="outer">
							@if($howStatement)
								@foreach($howStatement as $howStatements)
								<details>
									<summary>{{$howStatements->question}}</summary>
									<div class="faq-content">
										<p>{{$howStatements->answer}}</p>
									</div>
								</details>
								@endforeach
							@endif
							</div>
							</div>
							</div>
						<!--- END ROW -->	

						</div>
					</div>

					

					<div class="col-sm-6 col-lg-6 p-b-20">
						<!-- Right side bar -->
						<div class="flex-wr-sb-s m-b-30">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									What is
								</h3>
							</div>

							<div class="row">				
							<div class="col-md-12">
							<div class="outer">
							@if($whatStatement)
							  @foreach($whatStatement as $whatStatements)
								<details>
									<summary>{{$whatStatements->question}}</summary>
									<div class="faq-content">
										<p>{{$whatStatements->answer}}</p>
									</div>
								</details>
								@endforeach
							@endif
							</div>
							</div>
							</div>
						<!--- END ROW -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	 <!-- Main Content -->
	 <section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-lg-9 p-b-10">
						<!-- Left side Bar -->
						
						<div class="flex-wr-sb-s m-b-10">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Recent Video
								</h3>
							</div>


							<div>
							@if($videoCurrent)
								@foreach($videoCurrent as $videoCurrents)	

								<div class="wrap-pic-w pos-relative">
										<a title="Video" href="#viewer{{$videoCurrents->id}}" data-toggle="modal" data-id="{{$videoCurrents->id}}" data-target="#viewer{{$videoCurrents->id}}">
											<iframe width="500px;" height="300px" src="https://www.youtube.com/embed/{{$videoCurrents->video_id}}">
											</iframe>
                                        </a>

								</div>

								<div class="p-tb-16 p-rl-25 bg3">
									<h5 class="p-b-5">
										<a href="#" class="f1-m-3 cl0 hov-cl10 trans-03">
											{{strtoupper($videoCurrents->title)}} 
										</a>
									</h5>

									<span class="cl15">
										<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											{{$videoCurrents->publisher}}
										</a>

										<span class="f1-s-3 m-rl-3">
											-
										</span>

										<span class="f1-s-3">
											{{$videoCurrents->date_published}}
										</span>
									</span>
								</div>
							</div>
							<!-- Modal Video 01-->
							<div class="modal inmodal fade" id="viewer{{$videoCurrents->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <img src="{{ asset('storage/downloads/'.$videoCurrents->file_name) }}" width="100%" height="100%" />
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                    </div>
                                </div>
							@endforeach
							@endif

						</div>
					</div>

					

					<div class="col-sm-3 col-lg-3 p-b-20">
						<!-- Right side image -->
						<div class="flex-wr-sb-s m-b-30">
							<div class="how2 how2-cl4 flex-s-c m-b-5">
								<h3 class="f1-m-2 cl3 tab01-title">
									Statistics Dashboard
								</h3>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

        
@stop
