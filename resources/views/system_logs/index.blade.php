@extends('main')

@section('title')
    @parent
    System Logs & Audits
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>System Logs & Audits</h2></span>
    {{ Breadcrumbs::render('system-logs') }}
@stop

@section('content-title')
    <div class="row">
        <div class="col-xs-12">
            <h1><i class="fa fa-cog"></i> System Audit & Logs</h1>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Event</th>
                                <th>Action</th>
                                <th>URL</th>
                                <th>Date & Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($audits)
                                @foreach($audits as $key => $audit)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}</td>
                                    <td class="text-info">
                                        @if($audit->user)
                                            [ {{ $audit->user->name }} -
                                            {{ $audit->user->username }} ]
                                        @endif
                                    </td>
                                    <td>[ {{ $audit->event }} ] </td>
                                    <td class="text-muted">{{ $audit->auditable_type }}</td>
                                    <td class="text-success">{{ $audit->url }}</td>
                                    <td class="text-warning">[ {{ date('d-m-Y H:i:s', strtotime($audit->created_at)) }} ]</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
