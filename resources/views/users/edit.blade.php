@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Users</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View Users" href="{{ url('users/managements') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        View Users</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::open(['url'=>url('users/managements/edit', $user->id), 'class'=>'wizard-big', 'id'=>'userForm', 'role'=>'form']) !!}
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="font-bold">First Salutation:</label>
                                    {!! Form::select('salutation', $salutation, $user->salutation, ['class'=>'salute-select form-control',
                                    'id'=>'roles', 'placeholder'=>'', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="font-bold">Second Salutation:</label>
                                    {!! Form::select('second_salutation', $salutation, $user->second_salutation, ['class'=>'salute-select form-control',
                                    'id'=>'roles', 'placeholder'=>'']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">First Name:</label>
                                    <input id="fname" type="text" class="form-control" name="fname"
                                           value="{{ $user->fname }}" placeholder="Enter First Name" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Last Name:</label>
                                    <input id="lname" type="text" class="form-control" name="lname"
                                           value="{{ $user->lname }}" placeholder="Enter Last Name" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Role:</label>
                                    {!! Form::select('roles[]', $roles, $user->getRoleNames(), ['class'=>'roles-select form-control',
                                    'id'=>'roles', 'multiple', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Court Level</label>
                                    {!! Form::select('courtLevel', $courtLevels, $user->court_level_id, ['class'=>'form-control',
                                    'id'=>'courtLevel', 'placeholder'=>'Select Court Level', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Court</label>
                                    {!! Form::select('courtId', [], null, ['class'=>'chosen-select form-control',
                                    'id'=>'courtId', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Status</label><br/>
                                    <div class="i-checks">
                                        <input type="radio" name="status" id="activated" value="activated"
                                               {{ $user->status == 'activated' ? 'checked' : '' }} required/>
                                        Activate &nbsp;&nbsp;
                                        <input type="radio" name="status" id="deactivate" value="deactivate"
                                               {{ $user->status == 'deactivated' ? 'checked' : '' }} required/>
                                        Deactivate
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="font-bold">Password</label>--}}
                                    {{--<input id="password" type="password" class="form-control" name="password"--}}
                                           {{--placeholder="Password" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="font-bold">Retype Password</label>--}}
                                    {{--<input id="password-confirm" type="password" class="form-control"--}}
                                           {{--name="password_confirmation" placeholder="Confirm Password" />--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="font-bold">Is user a Judge, Magistrate, or Registrar?</label>
                                    <div class="i-checks">
                                        <input type="radio" name="isJudge" id="isUser" value="notjudge"
                                               {{ $user->is_user == 'notjudge' ? 'checked' : '' }} required />
                                        No &nbsp;&nbsp;
                                        <input type="radio" name="isJudge" id="isJudge" value="judge"
                                               {{ $user->is_user == 'judge' ? 'checked' : '' }} required />
                                        Yes
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="addUserButton">Update User</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    {{--<script src="{{ asset('assets/js/inspinia.js') }}"></script>--}}
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#courtLevel').on('change', function() {
                var url = "{{ url('users/managements/courts') }}" + "/" + this.value;
                $.get(url).done(function( data ) {
                    var select = "";
                    var options = "";
                    select+= "<select class='chose-select form-control'>";
                    options+= "<option value=''>" +"</option>";
                    for(var datum in data){
                        options+= "<option value='" + datum + "'>" + data[datum] +"</option>";
                    }
                    $("#courtId").empty().append(options);
                });
            });

            $('.chosen-select').select2({width: "100%"});
            $('.roles-select').select2({width: "100%" , placeholder:'Select Role(s)'});
            $('.salute-select').select2({width: "100%" , placeholder:'Select 1st Salutation'});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@stop
