@extends('main')

@section('title')
    @parent
    Roles
@stop

@section('breadcrumbs')
    <span><h2>User Roles</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="Add Role" href="{{ url('users/roles') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        View Roles</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        {!! Form::open(['url'=> url('users/roles/edit', $role->id), 'role'=>'form', 'id'=>'ExpressionValidator']) !!}
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" value="{{ $role->name }}" placeholder="Role name" required>
                </div>
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>App Setting</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Home Page</h5>
                        {!! Form::checkbox('permissions[]', 'view home', in_array('view home', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add home', in_array('add home', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit home', in_array('edit home', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete home', in_array('delete home', $permissions)) !!} Delete
                        <hr />
                        <h5>Organisation Structure</h5>
                        {!! Form::checkbox('permissions[]', 'view structure', in_array('view structure', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add structure', in_array('add structure', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit structure', in_array('edit structure', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete structure', in_array('delete structure', $permissions)) !!} Delete
                        <hr />
                        
                        <h5>Services Provided</h5>
                        {!! Form::checkbox('permissions[]', 'view services', in_array('view services', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add services', in_array('add services', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit services', in_array('edit services', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete services', in_array('delete services', $permissions)) !!} Delete
                        <hr />
                        <h5>Contact Information</h5>
                        {!! Form::checkbox('permissions[]', 'view contact', in_array('view contact', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add contact', in_array('add contact', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit contact', in_array('edit contact', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete contact', in_array('delete contact', $permissions)) !!} Delete
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Web Content</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>News</h5>
                        {!! Form::checkbox('permissions[]', 'view news', in_array('view news', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add news', in_array('add news', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit news', in_array('edit news', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete news', in_array('delete news', $permissions)) !!} Delete
                        <hr />
                        <h5>Posts</h5>
                        {!! Form::checkbox('permissions[]', 'view posts', in_array('view posts', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add posts', in_array('add posts', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit posts', in_array('edit posts', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete posts', in_array('delete posts', $permissions)) !!} Delete
                        <hr />
                        <h5>Documents & Publications</h5>
                        {!! Form::checkbox('permissions[]', 'view documents', in_array('view documents', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add documents', in_array('add documents', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit documents', in_array('edit documents', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete documents', in_array('delete documents', $permissions)) !!} Delete
                        <hr />
                        <h5>Photo Gallery</h5>
                        {!! Form::checkbox('permissions[]', 'view photo gallery', in_array('view photo gallery', $permissions)) !!} Viewphoto gallery
                        {!! Form::checkbox('permissions[]', 'add photo gallery', in_array('add photo gallery', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit photo gallery', in_array('edit photo gallery', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete photo gallery', in_array('delete photo gallery', $permissions)) !!} Delete
                        <hr />
                        <h5>Video Gallery</h5>
                        {!! Form::checkbox('permissions[]', 'view media', in_array('view media', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add media', in_array('add media', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit media', in_array('edit media', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete media', in_array('delete media', $permissions)) !!} Delete
                        <hr />
                        <h5>Links</h5>
                        {!! Form::checkbox('permissions[]', 'view links', in_array('view links', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add links', in_array('add links', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit links', in_array('edit links', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete links', in_array('delete links', $permissions)) !!} Delete
                        
                    </div>
                </div>
                
                
     
                
            </div>

            <div class="col-lg-4">
                
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>User Management</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>User permissions</h5>
                        {!! Form::checkbox('permissions[]', 'view permissions', in_array('view permissions', $permissions)) !!} View
                        <hr />
                        <h5>User roles</h5>
                        {!! Form::checkbox('permissions[]', 'view roles', in_array('view roles', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add roles', in_array('add roles', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit roles', in_array('edit roles', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete roles', in_array('delete roles', $permissions)) !!} Delete
                        <hr />
                        <h5>Users</h5>
                        {!! Form::checkbox('permissions[]', 'view users', in_array('view users', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add users', in_array('add users', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit users', in_array('edit users', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete users', in_array('delete users', $permissions)) !!} Delete
                        <hr />
                        <h5>Courts</h5>
                        {!! Form::checkbox('permissions[]', 'view courts', in_array('view courts', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add courts', in_array('add courts', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit courts', in_array('edit courts', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete courts', in_array('delete courts', $permissions)) !!} Delete
                        <hr />
                        <h5>Profile</h5>
                        {!! Form::checkbox('permissions[]', 'view profile', in_array('view profile', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'edit profile', in_array('edit profile', $permissions)) !!} Edit
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>LASP</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Paralegal</h5>
                        {!! Form::checkbox('permissions[]', 'view paralegal', in_array('view paralegal', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add paralegal', in_array('add paralegal', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit paralegal', in_array('edit paralegal', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete paralegal', in_array('delete paralegal', $permissions)) !!} Delete
                        <hr />
                        <h5>Service Provider</h5>
                        {!! Form::checkbox('permissions[]', 'view provider', in_array('view provider', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add provider', in_array('add provider', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit provider', in_array('edit provider', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete provider', in_array('delete provider', $permissions)) !!} Delete
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Week</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Participants</h5>
                        {!! Form::checkbox('permissions[]', 'view participant', in_array('view participant', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add participant', in_array('add participant', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit participant', in_array('edit participant', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete participant', in_array('delete participant', $permissions)) !!} Delete
                        <hr />
                        <h5>Recorded Events</h5>
                        {!! Form::checkbox('permissions[]', 'view event', in_array('view event', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add event', in_array('add event', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit event', in_array('edit event', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete event', in_array('delete event', $permissions)) !!} Delete
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Agent</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Customer Query</h5>
                        {!! Form::checkbox('permissions[]', 'view query', in_array('view query', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add query', in_array('add query', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit query', in_array('edit query', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete query', in_array('delete query', $permissions)) !!} Delete
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Feedback</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Customer Comments</h5>
                        {!! Form::checkbox('permissions[]', 'view comment', in_array('view comment', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add comment', in_array('add comment', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit comment', in_array('edit comment', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete comment', in_array('delete comment', $permissions)) !!} Delete
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Desk</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Register Legal Aid</h5>
                        {!! Form::checkbox('permissions[]', 'view desks', in_array('view desks', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add desks', in_array('add desks', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit desks', in_array('edit desks', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete desks', in_array('delete desks', $permissions)) !!} Delete
                        <hr />
                        <h5>Comments & Feedbacks</h5>
                        {!! Form::checkbox('permissions[]', 'view comments', in_array('view comments', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add comments', in_array('add comments', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit comments', in_array('edit comments', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete comments', in_array('delete comments', $permissions)) !!} Delete
                        <hr />
                        <h5>Legal Aid Type</h5>
                        {!! Form::checkbox('permissions[]', 'view aid type', in_array('view aid type', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add aid type', in_array('add aid type', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit aid type', in_array('edit aid type', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete aid type', in_array('delete aid type', $permissions)) !!} Delete
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Locations</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Regions</h5>
                        {!! Form::checkbox('permissions[]', 'view regions', in_array('view regions', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add regions', in_array('add regions', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit regions', in_array('edit regions', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete regions', in_array('delete regions', $permissions)) !!} Delete
                        <hr />
                        <h5>Districts</h5>
                        {!! Form::checkbox('permissions[]', 'view districts', in_array('view districts', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add districts', in_array('add districts', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit districts', in_array('edit districts', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete districts', in_array('delete districts', $permissions)) !!} Delete
                        <hr />
                        <h5>Wards</h5>
                        {!! Form::checkbox('permissions[]', 'view wards', in_array('view wards', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add wards', in_array('add wards', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit wards', in_array('edit wards', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete wards', in_array('delete wards', $permissions)) !!} Delete
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Reports</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Legal Weeks Report</h5>
                        {!! Form::checkbox('permissions[]', 'view weeks', in_array('view weeks', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add weeks', in_array('add weeks', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit weeks', in_array('edit weeks', $permissions)) !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete weeks', in_array('delete weeks', $permissions)) !!} Delete
                        <hr />
                        <h5>Legal Aid Provided</h5>
                        {!! Form::checkbox('permissions[]', 'view legal aids', in_array('view legal aids', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add legal aids', in_array('add legal aids', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit legal aids', in_array('edit legal aids', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete legal aids', in_array('delete legal aids', $permissions)) !!} Delete
                        <hr />
                        <h5>Registered Claims</h5>
                        {!! Form::checkbox('permissions[]', 'view claims', in_array('view claims', $permissions)) !!} View
                        {!! Form::checkbox('permissions[]', 'add claims', in_array('add claims', $permissions)) !!} Add
                        {!! Form::checkbox('permissions[]', 'edit claims', in_array('edit claims', $permissions)) !!} Editmedia
                        {!! Form::checkbox('permissions[]', 'delete claims', in_array('delete claims', $permissions)) !!} Delete
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>System Logs</h5>
                    </div>
                    <div class="ibox-content">
                        {!! Form::checkbox('permissions[]', 'view system logs', in_array('view system logs', $permissions)) !!} View
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <!-- Custom and plugin javascript -->
    {{--<script src="{{ asset('assets/js/inspinia.js') }}"></script>--}}
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
