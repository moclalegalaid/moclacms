@extends('main')

@section('title')
    @parent
    Roles
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>User Roles</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="Add Role" href="{{ url('users/roles/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add Role</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
    @if(isset($roles) && count($roles) > 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-search">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-lg-2">Role Name</th>
                                    <th>Permissions Assigned</th>
                                    <th class="col-lg-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $role)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ ucwords(str_replace("-"," ",$role->name)) }}</td>
                                        <td>
                                            @foreach($role->permissions as $permission)
                                                <span class="btn btn-info btn-xs" style="margin-bottom: 2px">{{ ucwords($permission->name) }}</span>
                                            @endforeach
                                        </td>
                                        <td>
                                            <a title="Edit" href="{{ url('users/roles/edit', $role->id) }}" class="btn btn-info btn-xs">
                                                <span class="fa fa-pencil"></span>
                                                <span>Edit</span>
                                            </a>
                                            <a title="Delete" href="{{ url('users/roles/delete', $role->id) }}" class="btn btn-danger btn-xs">
                                                <span class="fa fa-trash"></span>
                                                <span>Delete</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-search">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th class="col-lg-2">Role Name</th>
                                        <th>Permissions Assigned</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $key => $role)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ ucwords(str_replace("-"," ",$role->name)) }}</td>
                                            <td>
                                                @foreach($role->permissions as $permission)
                                                    <span class="btn btn-info btn-xs" style="margin-bottom: 2px">{{ ucwords($permission->name) }}</span>
                                                @endforeach
                                            </td>
                                            <td>
                                                @hasanyrole('super-admin')
                                                <a title="Edit" href="{{ url('users/roles/edit', $role->id) }}" class="btn btn-info btn-xs">
                                                    <span class="fa fa-pencil"></span>
                                                    <span>Edit</span>
                                                </a>
                                                @endhasanyrole
                                                @hasanyrole('super-admin')
                                                <a title="Delete" href="{{ url('users/roles/delete', $role->id) }}" class="btn btn-danger btn-xs">
                                                    <span class="fa fa-trash"></span>
                                                    <span>Delete</span>
                                                </a>
                                                @endhasanyrole
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endif
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
