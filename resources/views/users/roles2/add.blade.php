@extends('main')

@section('title')
    @parent
    Roles
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>User Roles</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        {!! Form::open(['url'=> url('users/roles/add'), 'role'=>'form', 'id'=>'ExpressionValidator']) !!}
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Role name" required>
                </div>
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>App Setting</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Home Page</h5>
                        {!! Form::checkbox('permissions[]', 'view home') !!} View
                        {!! Form::checkbox('permissions[]', 'add home') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit home') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete home') !!} Delete
                        <hr />
                        <h5>Organisation Structure</h5>
                        {!! Form::checkbox('permissions[]', 'view structure') !!} View
                        {!! Form::checkbox('permissions[]', 'add structure') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit structure') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete structure') !!} Delete
                        <hr />
                        <h5>Services Provided</h5>
                        {!! Form::checkbox('permissions[]', 'view services') !!} View
                        {!! Form::checkbox('permissions[]', 'add services') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit services') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete services') !!} Delete
                        <hr />
                        <h5>Contact Information</h5>
                        {!! Form::checkbox('permissions[]', 'view contact') !!} View
                        {!! Form::checkbox('permissions[]', 'add contact') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit contact') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete contact') !!} Delete
                        
                    </div>
                </div>
                
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Web Content</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>News</h5>
                        {!! Form::checkbox('permissions[]', 'view news') !!} View
                        {!! Form::checkbox('permissions[]', 'add news') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit news') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete news') !!} Delete
                        <hr />
                        <h5>Posts</h5>
                        {!! Form::checkbox('permissions[]', 'view posts') !!} View
                        {!! Form::checkbox('permissions[]', 'add posts') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit posts') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete posts') !!} Delete
                        <hr />
                        <h5>Documents & Publications</h5>
                        {!! Form::checkbox('permissions[]', 'view documents') !!} View
                        {!! Form::checkbox('permissions[]', 'add documents') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit documents') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete documents') !!} Delete
                        <hr />
                        <h5>Photo Gallery</h5>
                        {!! Form::checkbox('permissions[]', 'view photo gallery') !!} View
                        {!! Form::checkbox('permissions[]', 'add photo gallery') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit photo gallery') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete photo gallery') !!} Delete
                        <hr />
                        <h5>Video Gallery</h5>
                        {!! Form::checkbox('permissions[]', 'view media') !!} View
                        {!! Form::checkbox('permissions[]', 'add media') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit media') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete media') !!} Delete
                        <hr />
                        <h5>Links</h5>
                        {!! Form::checkbox('permissions[]', 'view links') !!} View
                        {!! Form::checkbox('permissions[]', 'add links') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit links') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete links') !!} Delete
                        
                    </div>
                </div>
            
                
                
            </div>

            <div class="col-lg-4">
                
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>User Management</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>User permissions</h5>
                        {!! Form::checkbox('permissions[]', 'view permissions') !!} View
                        <hr />
                        <h5>User roles</h5>
                        {!! Form::checkbox('permissions[]', 'view roles') !!} View
                        {!! Form::checkbox('permissions[]', 'add roles') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit roles') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete roles') !!} Delete
                        <hr />
                        <h5>Users</h5>
                        {!! Form::checkbox('permissions[]', 'view users') !!} View
                        {!! Form::checkbox('permissions[]', 'add users') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit users') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete users') !!} Delete
                        <hr />
                        
                        <h5>Courts</h5>
                        {!! Form::checkbox('permissions[]', 'view courts') !!} View
                        {!! Form::checkbox('permissions[]', 'add courts') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit courts') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete courts') !!} Delete
                        <hr />
                        
                        <h5>Profile</h5>
                        {!! Form::checkbox('permissions[]', 'view profile') !!} View
                        {!! Form::checkbox('permissions[]', 'edit profile') !!} Edit
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>LASP</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Paralegal</h5>
                        {!! Form::checkbox('permissions[]', 'view paralegal') !!} View
                        {!! Form::checkbox('permissions[]', 'add paralegal') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit paralegal') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete paralegal') !!} Delete
                        <hr />
                        <h5>Service Provider</h5>
                        {!! Form::checkbox('permissions[]', 'view provider') !!} View
                        {!! Form::checkbox('permissions[]', 'add provider') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit provider') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete provider') !!} Delete
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Week</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Participants</h5>
                        {!! Form::checkbox('permissions[]', 'view participant') !!} View
                        {!! Form::checkbox('permissions[]', 'add participant') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit participant') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete participant') !!} Delete
                        <hr />
                        <h5>Recorded Events</h5>
                        {!! Form::checkbox('permissions[]', 'view event') !!} View
                        {!! Form::checkbox('permissions[]', 'add event') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit event') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete event') !!} Delete
                       
                    </div>
                </div>
            
            </div>

            <div class="col-lg-4">
                
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Agent</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Customer Query</h5>
                        {!! Form::checkbox('permissions[]', 'view query') !!} View
                        {!! Form::checkbox('permissions[]', 'add query') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit query') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete query') !!} Delete
                       
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Feedback</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Customer Comments</h5>
                        {!! Form::checkbox('permissions[]', 'view comment') !!} View
                        {!! Form::checkbox('permissions[]', 'add comment') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit comment') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete comment') !!} Delete
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Legal Aid Desk</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Register Legal Aid</h5>
                        {!! Form::checkbox('permissions[]', 'view desks') !!} View
                        {!! Form::checkbox('permissions[]', 'add desks') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit desks') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete desks') !!} Delete
                        <hr />
                        <h5>Comments & Feedbacks</h5>
                        {!! Form::checkbox('permissions[]', 'view comments') !!} View
                        {!! Form::checkbox('permissions[]', 'add comments') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit comments') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete comments') !!} Delete
                        <hr />
                        <h5>Legal Aid Type</h5>
                        {!! Form::checkbox('permissions[]', 'view aid type') !!} View
                        {!! Form::checkbox('permissions[]', 'add aid type') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit aid type') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete aid type') !!} Delete
                    </div>
                </div>
            
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Locations</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Regions</h5>
                        {!! Form::checkbox('permissions[]', 'view regions') !!} View
                        {!! Form::checkbox('permissions[]', 'add regions') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit regions') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete regions') !!} Delete
                        <hr />
                        <h5>Districts</h5>
                        {!! Form::checkbox('permissions[]', 'view districts') !!} View
                        {!! Form::checkbox('permissions[]', 'add districts') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit districts') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete districts') !!} Delete
                        <hr />
                        <h5>Wards</h5>
                        {!! Form::checkbox('permissions[]', 'view wards') !!} View
                        {!! Form::checkbox('permissions[]', 'add wards') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit wards') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete wards') !!} Delete
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>Reports</h5>
                    </div>
                    <div class="ibox-content">
                        <h5>Legal Weeks Report</h5>
                        {!! Form::checkbox('permissions[]', 'view legal weeks') !!} View
                        {!! Form::checkbox('permissions[]', 'add legal weeks') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit legal weeks') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete legal weeks') !!} Delete
                        <hr />
                        <h5>Legal Aid Provided</h5>
                        {!! Form::checkbox('permissions[]', 'view legal aids') !!} View
                        {!! Form::checkbox('permissions[]', 'add legal aids') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit legal aids') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete legal aids') !!} Delete
                        <hr />
                        <h5>Registered Claims</h5>
                        {!! Form::checkbox('permissions[]', 'view claims') !!} View
                        {!! Form::checkbox('permissions[]', 'add claims') !!} Add
                        {!! Form::checkbox('permissions[]', 'edit claims') !!} Edit
                        {!! Form::checkbox('permissions[]', 'delete claims') !!} Delete
                    </div>
                </div>
            
            </div>

            <div class="col-lg-4">
                
            <div class="ibox">
                    <div class="ibox-title" style="background-color:#20639b; color:  white;">
                        <h5>System Logs</h5>
                    </div>
                    <div class="ibox-content">
                        {!! Form::checkbox('permissions[]', 'view system logs') !!} View systems logs
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

        });
    </script>
@stop
