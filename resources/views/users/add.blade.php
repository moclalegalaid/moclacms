@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Users</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View Users" href="{{ url('users/managements') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        View Users</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::open(['url'=>url('users/managements/add'), 'class'=>'wizard-big', 'id'=>'userForm', 'role'=>'form']) !!}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">First Salutation:</label>
                                    {!! Form::select('salutation', $salutation, null, ['class'=>'salute-select form-control',
                                    'id'=>'roles', 'placeholder'=>'', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Second Salutation:</label>
                                    {!! Form::select('second_salutation', $salutation, null, ['class'=>'salute-select form-control',
                                    'id'=>'roles', 'placeholder'=>'']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Role:</label>
                                    {!! Form::select('roles[]', $roles, null, ['class'=>'roles-select form-control',
                                    'id'=>'roles', 'multiple', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">First Name:</label>
                                    <input id="fname" type="text" class="form-control" name="fname"
                                           value="{{ old('fname') }}" placeholder="Enter First Name" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Middle Name:</label>
                                    <input id="oname" type="text" class="form-control" name="oname"
                                           value="{{ old('oname') }}" placeholder="Enter Middle Name" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Last Name:</label>
                                    <input id="lname" type="text" class="form-control" name="lname"
                                           value="{{ old('lname') }}" placeholder="Enter Last Name" required />
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Email</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" placeholder="Email" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Password</label>
                                    <input id="password" type="password" class="form-control" name="password"
                                           placeholder="Password" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Retype Password</label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" placeholder="Confirm Password" required />
                                </div>
                            </div>
                          </div>
                          <div class="row">
                          <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Desk Officer?</label><br/>
                                    <div class="i-checks">
                                        <input type="radio" name="desk" id="desk" value="desk" />
                                        Yes &nbsp;&nbsp;
                                        <input type="radio" name="desk" id="notDeskOfficer" value="normal" />
                                        No
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row" id="deskOfficer" style="display: none">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Court Station:</label>
                                    {!! Form::select('court', $courts, null, ['class'=>'court-select form-control',
                                    'id'=>'courts', 'placeholder'=>'', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Workplace</label>
                                    <input id="workplace" type="text" class="form-control" name="workplace"
                                           value="{{ old('workplace') }}" placeholder="Organisation / workplace" required />
                                </div>
                            </div>
                        </div>
                        <br/>
                        <button type="submit" class="btn btn-success" id="addUserButton">Add User</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.chosen-select').select2({width: "100%"});
            $('.roles-select').select2({width: "100%" , placeholder:'Select Role(s)'});
            $('.salute-select').select2({width: "100%" , placeholder:'Select 1st Salutation'});
            $('.court-select').select2({width: "100%" , placeholder:'Select Court Station'});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('#desk').on('ifChecked', function () {
                $('#deskOfficer :input').attr('disabled', false);
                $("#deskOfficer").slideDown();
            });
            $('#notDeskOfficer').on('ifChecked', function () {
                $("#deskOfficer").slideUp();
                $('#deskOfficer :input').attr('disabled', true);
                
            });

        });
    </script>
@stop
