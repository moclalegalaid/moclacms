@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Courts</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
                    <a title="Add User" href="{{ url('users/courts') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add Court</a>
                         @hasanyrole('super-admin')
                        @endhasanyrole
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    <div class="row">
    <div class="col-lg-14">
        <div class="tabs-container">
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">

                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-14">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="table-responsive">
                                                <table class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($court)
                                                        @foreach($court as $key => $courts)
                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    {{ ucwords($courts->name) }}
                                                                </td>
                                                                <td>
                                                                    {{ ucwords($courts->status) }}
                                                                </td>
                                                                <td>
                                                                    <a title="Edit" href="{{ url('users/courts/edit', $courts->id) }}" class="btn btn-info btn-xs">
                                                                        <span class="fa fa-pencil"></span>
                                                                        <span>Edit</span>
                                                                    </a>
                                                                    @if($courts->status == "inactive")
                                                                    <a title="Activate" href="{{ url('users/courts/activate', $courts->id) }}" class="btn btn-primary btn-xs">
                                                                        <span class="fa fa-trash"></span>
                                                                        <span>Activate</span>
                                                                    </a>
                                                                    @endif

                                                                    <a title="Delete" href="{{ url('users/courts/delete', $courts->id) }}" class="btn btn-danger btn-xs">
                                                                        <span class="fa fa-trash"></span>
                                                                        <span>Delete</span>
                                                                    </a>
                                                                   
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div>
        
    </div>
</div>
        

    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
