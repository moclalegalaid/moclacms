@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>External Users</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View Users" href="{{ url('users/managements') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        View Users</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::open(['url'=>url('users/managements/addExtUser'), 'class'=>'wizard-big', 'id'=>'userForm', 'role'=>'form']) !!}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">First Name:</label>
                                    <input id="fname" type="text" class="form-control" name="fname"
                                           value="{{ old('fname') }}" placeholder="Enter First Name" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Middle Name:</label>
                                    <input id="oname" type="text" class="form-control" name="mname"
                                           value="{{ old('mname') }}" placeholder="Enter Middle Name" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Last Name:</label>
                                    <input id="lname" type="text" class="form-control" name="lname"
                                           value="{{ old('lname') }}" placeholder="Enter Last Name" required />
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Role:</label>
                                    {!! Form::select('roles[]', $roles, null, ['class'=>'roles-select form-control',
                                    'id'=>'roles', 'multiple', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Roll No</label>
                                    <input id="roll" type="text" class="form-control" name="rollNumber"
                                           value="{{ old('rollNumber') }}" placeholder="Enter Roll Number" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Nationality</label>
                                    {!! Form::select('countries', $countries, null, ['class'=>'chosen-select form-control',
                                    'id'=>'countries', 'placeholder'=>'Select Country', 'required']) !!}
                                </div>
                            </div>
                        </div>


                        <div class="row">
							<div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">TIN Number:</label>
                                    <input id="tin" type="text" class="form-control" name="tin"
                                           value="{{ old('tin') }}" placeholder="Enter TIN" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Date of Birth:</label>
                                    <input id="dateBorth" type="text" class="form-control" name="dateBorth" value="{{ old('dateBorth') }}" placeholder="Date of Birth" required readonly/>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Identity Number:</label>
                                    <table>
										<tr>
											<td>
												{!! Form::select('idType', $idType, null, ['class'=>'form-control',
												'id'=>'idType', 'placeholder'=>'ID Type', 'required']) !!}
											</td>
												&nbsp;&nbsp;
											<td>
												<input id="idNumber" type="text" class="form-control" name="idNumber"
												value="{{ old('idNumber') }}" placeholder="Enter ID Number" required />
											</td>
										</tr>
									</table>
                                </div>
                            </div>
							
							<div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Law Firm Name:</label>
                                    {!! Form::select('firmName', $firmName, null, ['class'=>'firm-select form-control',
                                    'id'=>'firmName', 'placeholder'=>'Select Law Firm', 'required']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Email</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" placeholder="Enter Email" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Physical Address</label>
                                    <input id="address" type="text" class="form-control" name="address"
                                           value="{{ old('address') }}" placeholder="Enter Physical Address" />
                                </div>
                            </div>
							
                        </div>

                        <div class="row">
							<div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Mobile Number</label>
                                    <input id="mobile" type="text" class="form-control" name="mobile"
                                           placeholder="Mobile Number" required />
                                </div>
                            </div>
							<div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Password</label>
                                    <input id="password" type="password" class="form-control" name="password"
                                           placeholder="Password" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Retype Password</label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" placeholder="Confirm Password" required />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="font-bold">Status</label><br/>
                                    <div class="i-checks">
                                        <input type="radio" name="status" id="activated" value="activated" checked />
                                        Activate &nbsp;&nbsp;
                                        <input type="radio" name="status" id="deactivate" value="deactivate" />
                                        Deactivate
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="addUserButton">Register User</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#courtLevel').on('change', function() {
                var url = "{{ url('users/managements/courts') }}" + "/" + this.value;
                $.get(url).done(function( data ) {
                    var select = "";
                    var options = "";
                    select+= "<select class='chose-select form-control'>";
                    options+= "<option value=''>" +"</option>";
                    for(var datum in data){
                        options+= "<option value='" + datum + "'>" + data[datum] +"</option>";
                    }
                    $("#courtId").empty().append(options);
                });
            });

            $('.chosen-select').select2({width: "100%"});
            $('.roles-select').select2({width: "100%" , placeholder:'Select Role(s)'});
			$('.firm-select').select2({width: "100%" , placeholder:'Select Law Firm'});
            $('.salute-select').select2({width: "100%" , placeholder:'Select 1st Salutation'});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
            $('#dateBorth').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'yyyy-mm-dd'
            });
        });
    </script>
@stop
