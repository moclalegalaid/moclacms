@extends('main')

@section('title')
    @parent
    Permissions
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>User Permissions</h2></span>
    {{ Breadcrumbs::render('user-permissions') }}
@stop

@section('content-title')
    <div class="row">
        <div class="col-xs-12">
            <h1><i class="fa fa-users"></i> User Permissions</h1>
        </div>
    </div>
@stop

@section('raw-content')
    @if(isset($permissions) && count($permissions) > 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-search">
                                <thead>
                                <tr>
                                    <th class="col-lg-1">#</th>
                                    <th class="col-lg-4">Permission Name</th>
                                    <th>Roles Assigned</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($permissions)
                                    @foreach($permissions as $key => $permission)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ ucwords($permission->name) }}</td>
                                            <td>
                                                @foreach($permission->roles as $role)
                                                    @if($role->name <> 'super-admin')
                                                    <span class="btn btn-info btn-xs" style="margin-bottom: 2px">
                                                        {{ $role->name }}
                                                    </span>
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        There are no permissions
    @endif
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
