@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>External Users</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
						<a title="Add User" href="{{ url('users/managements/addExtUser') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        Add User</a>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
       
    <div class="row">
    <div class="col-lg-14">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"> Active Users <span class="label label-primary">{{ $extUsersCount }}</span></a></li>
                <li class=""><a data-toggle="tab" href="#tab-2"> Reset Codes <span class="label label-success">{{ $resetCodeCount }}</span></a></li>
            </ul>
            <div class="tab-content">
                
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <strong>External Users Registered</strong>

                        <div class="wrapper wrapper-content animated fadeInRight">
                                    <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="table-responsive">
                                                <table class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Username</th>
                                                        <th>Law Firm</th>
                                                        <th>Roll Number</th>
                                                        <th>Mobile</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($extUsers)
                                                        @foreach($extUsers as $key => $user)
                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    {{ $user->fname }}
                                                                    {{ $user->mname }}
                                                                    {{ ucwords($user->lname) }}
                                                                </td>
                                                                <td>{{ $user->username }}</td>
                                                                <td>
                                                                    {{ $user->username }}
                                                                </td>
                                                                <td>{{ $user->roll_number }}</td>
                                                                <td>
                                                                    {{ $user->mobile }}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-{{ ($user->verified == 1)? 'success':'primary' }} btn-xs">
                                                                        <span>Active</span>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    @hasanyrole('super-admin|admin')
                                                                    <a href="#" class="btn btn-{{ ($user->verified == 1)? 'success':'warning' }} btn-xs">
                                                                        <span class="fa fa-repeat"></span>
                                                                        <span>Deactivate</span>
                                                                    </a>
                                                                    @endhasanyrole
                                                                    @hasanyrole('super-admin')
                                                                    <a href="#" class="btn btn-success btn-xs">
                                                                        <span class="fa fa-trash"></span>
                                                                        <span>Activate</span>
                                                                    </a>
                                                                    @endhasanyrole
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <strong>All Users</strong>

                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-14">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="table-responsive">
                                                <table class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Mobile Number</th>
                                                        <th>Reset Code</th>
                                                        <th>Role</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($resetCode)
                                                        @foreach($resetCode as $key => $user)
                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    {{ $user->salutation }}
                                                                    {{ $user->second_salutation }}
                                                                    {{ ucwords($user->name) }}
                                                                </td>
                                                                <td>{{ $user->mobile }}</td>
                                                                <td>{{ $user->ver_code }}</td>
                                                                <td>{!! Form::listbuttons($user->getRoleNames(), "btn-info btn-xs") !!}</td>
                                                                
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div>
        
    </div>
</div> 
        

    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
