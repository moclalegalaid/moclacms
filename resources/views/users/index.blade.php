@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Users2</h2></span>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="Add User" href="{{ url('users/managements/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add Internal User</a>
						<a title="Add User" href="{{ url('users/managements/addExtUser') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add External User</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="table-search" class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Court</th>
                                    <th>Roles</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $.fn.dataTable.ext.errMode = 'throw';
            $("#table-search").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('users.getuser') }}",
                    "dataType": "json"
                },
                columns:[
                    { 'data': 'id', 'name': 'id'},
                    { 'data': 'name', 'name': 'name'},
                    { 'data': 'username', 'name': 'username'},
                    { 'data': '', 'name': ''},
                    { 'data': 'court.name', 'name': 'court.name'},
                    { 'data': 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Users'},
                    {extend: 'pdf', title: 'Users'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });
    </script>
@stop
