@extends('main2')

@section('title')
    @parent
    User Profile
@stop

@section('page-specific-css')

@stop

@section('breadcrumbs')
    <span><h2>User Profile</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row m-b-lg m-t-lg">
            <div class="col-md-8">
                <div class="profile-image">
                    <img alt="image" class="" src="{{ asset('images/users/profile.png') }}" />
                    {{--<img src="" class="img-circle circle-border m-b-md" alt="profile">--}}
                </div>
                <div class="profile-info">
                    <div class="">
                        <div>
                            <h2 class="no-margins">
                                {{ Auth::user()->fname }} {{ Auth::user()->lname }}
                            </h2>
                            <h4>{{ $user->getRoleNames() }}<span class="small font-bold">
                                <span><i class="fa fa-circle text-navy"></i> Online</span>
                            </span>
                            </h4>


                            <small>
                                All credentials given to a username <strong>{{ Auth::user()->username }}</strong> with your name
                                <strong>{{ Auth::user()->name }}</strong> are only meant to be used by a single person. Any actions taken by users
                                in the systems are highly audited and monitored. Kindly share with no one your credentials.
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Change Personal Details</h3>
                        {!! Form::open(['url'=>url('users/profile', $user->id), 'class'=>'registration-form', '
                            id'=>'registrationForm', 'role'=>'form']) !!}
                        <div class="form-bottom">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">First Name:</label>
                                        <input id="fname" type="text" class="form-control" name="fname"
                                               value="{{ $user->fname }}" placeholder="Enter First Name" readonly required />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">Second Name:</label>
                                        <input id="oname" type="text" class="form-control" name="oname"
                                               value="{{ $user->oname }}" placeholder="Enter Second Name" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">Last Name:</label>
                                        <input id="lname" type="text" class="form-control" name="lname"
                                               value="{{ $user->lname }}" placeholder="Enter Last Name" readonly required />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">Address:</label>
                                        <input id="address" type="text" class="form-control" name="address"
                                               value="{{ $user->address }}" placeholder="Enter Address" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">Mobile:</label>
                                        <input id="mobile" type="text" class="form-control" name="mobile"
                                               value="{{ $user->mobile }}" placeholder="Enter Mobile" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="font-bold">Email:</label>
                                        <input id="email" type="text" class="form-control" name="email"
                                               value="{{ $user->email }}" placeholder="Enter Email" />
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                Save Changes <i class="fa fa-send"></i>
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="col-lg-4 m-b-lg">
                <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                    <div class="vertical-timeline-block">
                        <div class="vertical-timeline-icon navy-bg">
                            <i class="fa fa-briefcase"></i>
                        </div>

                        <div class="vertical-timeline-content">
                            <h2>Meeting</h2>
                            <p></p>
                            <a href="#" class="btn btn-sm btn-primary"> More info</a>
                            <span class="vertical-date">
                                Today <br>
                                <small>Dec 24</small>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        <div class="col-lg-8">
            <div class="ibox">
                <div class="ibox-content">
                    <h3>Change Password</h3>
                    {!! Form::open(['url'=>url('users/change-password', $user->id), 'class'=>'registration-form', '
                    id'=>'registrationForm', 'role'=>'form']) !!}
                    <div class="form-bottom">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="font-bold">Password:</label>
                                    <input id="password" type="password" class="form-control" name="password"
                                           value="{{ old('password') }}" placeholder="Enter Password" required />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="font-bold">Confirm Password:</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                           value="{{ old('password-confirm') }}" placeholder="Confirm Password" required />
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Save Changes <i class="fa fa-send"></i>
                        </button>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-lg-4 m-b-lg">
            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon blue-bg">
                        <i class="fa fa-file-text"></i>
                    </div>

                    <div class="vertical-timeline-content">
                        <h2>Attachments</h2>
                        <p></p>
                        <a href="#" class="btn btn-sm btn-success"> Download </a>
                        <span class="vertical-date">
                            Today <br>
                            <small>Dec 24</small>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <!-- Custom and plugin javascript -->
    {{--<script src="{{ asset('assets/js/inspinia.js') }}"></script>--}}
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
@stop



