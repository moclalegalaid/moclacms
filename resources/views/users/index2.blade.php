@extends('main')

@section('title')
    @parent
    Users
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Users</h2></span>
    {{ Breadcrumbs::render('users') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
                    <a title="Add User" href="{{ url('users/managements/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add User</a>
                         @hasanyrole('super-admin')
                        @endhasanyrole
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    <div class="row">
    <div class="col-lg-14">
        <div class="tabs-container">
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">

                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="row">
                                <div class="col-lg-14">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="table-responsive">
                                                <table class="table display nowrap table-striped table-bordered table-hover dataTables-search" >
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Roles</th>
                                                        <th>Organisation</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($users)
                                                        @foreach($users as $key => $user)
                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    {{ $user->salutation }}
                                                                    {{ $user->second_salutation }}
                                                                    {{ ucwords($user->name) }}
                                                                </td>
                                                                <td>{{ $user->email }}</td>
                                                                
                                                                <td>{{$user->getRoleNames()}}</td>
                                                                <td>{{$user->workplace}}</td>
                                                                <td>
                                                                    <a title="Edit" href="{{ url('users/managements/edit', $user->id) }}" class="btn btn-info btn-xs">
                                                                        <span class="fa fa-pencil"></span>
                                                                        <span>Edit</span>
                                                                    </a>
                                                                    @hasanyrole('super-admin')
                                                                    <a href="{{ url('users/status', $user->id) }}" class="btn btn-{{ ($user->verified == 1)? 'success':'warning' }} btn-xs">
                                                                        <span class="fa fa-repeat"></span>
                                                                        <span>{{ ucwords($user->status ) }}</span>
                                                                    </a>
                                                                    @endhasanyrole
                                                                    @hasanyrole('super-admin')
                                                                    <a title="Delete" href="{{ url('users/managements/delete', $user->id) }}" class="btn btn-danger btn-xs">
                                                                        <span class="fa fa-trash"></span>
                                                                        <span>Delete</span>
                                                                    </a>
                                                                    @endhasanyrole
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div>
        
    </div>
</div>
        

    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-search').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });
    </script>
@stop
