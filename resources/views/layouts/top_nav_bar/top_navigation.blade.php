<!-- ########## START: HEAD PANEL ########## -->
<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-success " href="{{ url('#') }}">
            <i class="fa fa-bars"></i>
            MoCLA - Legal Aid For Women 
            @if(Auth::user()->court)
              [ Legal Aid Desk at {{ Auth::user()->court->name }} ]
            @endif
        </a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">Loged in as, <strong>{{ Auth::user()->name }}</strong></span>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>  <span class="label label-primary"></span>
            </a>
        </li>
        <li>
            <a data-toggle="modal" data-target="#myModal2">
                <i class="fa fa-sign-out"></i> Log out
            </a>
        </li>
    </ul>

</nav>
<!-- ########## END: HEAD PANEL ########## -->