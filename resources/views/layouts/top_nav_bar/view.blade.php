@extends('main')

@section('title')
    @parent
    Civil Registry
@stop

@section('page-specific-css')
    <!-- FooTable -->
    <link href="{{ asset('css2/plugins/footable/footable.core.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    
@stop

@section('breadcrumbs')
<span><h2>Criminal Registry | <small>Case View</small></h2></span>
    {{ Breadcrumbs::render('criminal-registers') }}
@stop

@section('content')
    <div class="row">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View Civil Cases" href="{{ url('criminal-cases') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        View Criminal Cases
                    </a>
                    <a title="File New Case" href="{{ url('criminal-cases/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>
                        Add New Case</a>
                </div>
            </div>
    </div>
@stop

@section('raw-content')

    <div class="row">
        
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#details"><i class="fa fa-info-circle"></i>  Case Details<small> (Click to open/close pane)</small></a>
                                            </h5>
                                        </div>
                                        <div id="details" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                
                                            <div class="col-lg-12">
                                                <div class="wrapper wrapper-content animated fadeInUp">
                                                    <div class="ibox">
                                                        <div class="ibox-content">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="m-b-md">
                                                                        <a href="{{ url('criminal-cases/edit', $case->id) }}" class="btn btn-white btn-xs pull-right">Edit Case Details</a>
                                                                        <h2>{{ $case->caseType->name }} No. {{ $case->case_no }} of {{ $case->case_year }}</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12"> 
                                                                <div>
                                                                    <table class="table table-bordered" style="font-size:13px;">
                                                                    <tbody>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Status</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               @if($case->getLastProceeding())
                                                    <span class="label label-{{ ($case->getLastProceeding()->proceeding_outcome == 'pending')?'danger':'success' }}">
                                                            {{ $case->getLastProceeding()->proceeding_outcome }}
                                                </span>
                                                @else
                                                    <span class="label">filed</span>
                                                @endif
                                                                            </div>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Reference Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_ref_number }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Code</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                DC-MBY-CIV-MSC-12-2019
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_no }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Year</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_year }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Filing Date</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->filing_date  }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Submission Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               {{ $case->submission_type  }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                 {{ $case->caseType->name }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong> Court Fee Exemption</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>{{ $case->fee_exemption }}</div>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <div><strong>Involved Dept</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->involved_debt }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Subject Matter Value/Claim Amount</strong> </div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->currency }} - {{ $case->subject_matter }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Case Urgency</strong> </div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->urgency }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Records Management Assistant</strong></div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->user_id }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                                </div>
                                                        </div>
                                                            @if($case->instance_level == 'Appeal/Application')
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="m-b-md">
                                                                        <h2>Details for the Existing/Originationg Case</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12"> 
                                                                <div>
                                                                    <table class="table table-bordered" style="font-size:13px;">
                                                                    <tbody>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Original Court</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_court_id }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_caseType_id }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_case_number }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Year</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_case_year }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                                </div>
                                                        </div>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#claim"><i class="fa fa-gavel"></i> Case Charges / Offence<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="claim" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                              Charges 
                                            </legend>
                                                
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Case Charge / Offence</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($case)
                                                    @foreach($case->caseCharges as $key => $charge)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>
                                                        @if($charge->charge)
                                                            {{ $charge->charge->name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a title="Edit" href="{{ url('criminal-charges/edit', [$case->id, $charge->id]) }}" class="btn btn-info btn-xs">
                                                            <span class="fa fa-edit"></span>
                                                            <span>Edit</span>
                                                        </a>
                                                        @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                        <a title="Edit" href="{{ url('criminal-charges/delete', $charge->id) }}" class="btn btn-danger btn-xs">
                                                            <span class="fa fa-trash"></span>
                                                            <span>Delete</span>
                                                        </a>
                                                        @endhasanyrole
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="3">
                                                <a title="Add Case Charge" href="{{ url('criminal-charges/add', $case->id) }}"
                                                   class="btn btn-primary btn-xs pull-left">
                                                    <i class="fa fa-plus"></i>
                                                    Add New
                                                </a>
                                            </td>
                                        </tr>
                                        </tfoot>
                                        </table>
                                     </fieldset>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#parties"><i class="fa fa-gavel"></i> Case Parties<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="parties" class="panel-collapse collapse">
                                            <div class="panel-body">
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            @if($case)
                                            @foreach($case->caseComplainants as $key => $complainant)
                                                {{ $complainant->appeared_as }}(s)
                                            @endforeach
                                            @endif
                                            </legend>
                                                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>More Details</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($case)
                                        @foreach($case->caseComplainants as $key => $complainant)
                                        <tr>
                                            <td>
                                                <span>
                                                 <img alt="Avatar" class="img-circle" src="{{ asset('images/users/profile_small1.jpg') }}" /> 
                                                </span>
                                               
                                            </td>
                                            <td>
                                               {{ $complainant->name }}
                                            </td>
                                            <td>
                                            <p class="small">
                                                Age: {{ $complainant->date_of_birth }} years; Gender: {{ $complainant->gender }}; Mobile Number : {{ $complainant->mobile }}; Physical Address : {{ $complainant->address }}; PostCode : {{ $complainant->post_code }}; email address : {{ $complainant->email }}.
                                            </p>
                                            </td>
                                            <td>
                                                <a title="Edit" href="{{ url('criminal-complainants/edit', [$case->id, $complainant->id]) }}" class="btn btn-info btn-xs">
                                                <span class="fa fa-edit"></span>
                                                <span>Edit</span>
                                                </a>
                                                @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                <a title="Delete" href="{{ url('criminal-complainants/delete', $complainant->id) }}" class="btn btn-danger btn-xs">
                                                    <span class="fa fa-trash"></span>
                                                    <span>Delete</span>
                                                </a>
                                                @endhasanyrole
                                            </td>

                                        </tr>
                                         @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <a title="Add New" href="{{ url('criminal-complainants/add', $case->id) }}"
                                                       class="btn btn-primary btn-xs pull-left">
                                                        <i class="fa fa-plus"></i>
                                                        Add More
                                                    </a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                            </fieldset>
                                                
                                             <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                             @if($case)
                                            @foreach($case->caseRespondents as $key => $respondent)
                                                {{ $respondent->appeared_as }}(s)
                                            @endforeach
                                            @endif
                                            </legend>
                                                
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Avatar</th>
                                                        <th>Name</th>
                                                        <th>More Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($case)
                                                    @foreach($case->caseRespondents as $key => $respondent)
                                                    <tr>
                                                        <td>
                                                           <span>
                                                             <img alt="Avatar" class="img-circle" src="{{ asset('images/users/profile_small1.jpg') }}" /> 
                                                            </span>
                                                        </td>
                                                        <td>
                                                           {{ $respondent->name }}
                                                        </td>
                                                        <td>
                                                        <p class="small">
                                                           Age: {{ $respondent->date_of_birth }} years; Gender: {{ $respondent->gender }}; Mobile Number : {{ $respondent->mobile }}; Physical Address : {{ $respondent->address }}; PostCode : {{ $respondent->post_code }}; email address : {{ $respondent->email }}.
                                                        </p>
                                                        </td>
                                                        <td>
                                                           <a title="Edit" href="{{ url('criminal-respondents/edit', [$case->id, $respondent->id] ) }}" class="btn btn-info btn-xs">
                                                            <span class="fa fa-edit"></span>
                                                            <span>Edit</span>
                                                            </a>
                                                            @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                            <a title="Delete" href="{{ url('criminal-respondents/delete', $respondent->id) }}" class="btn btn-danger btn-xs">
                                                                <span class="fa fa-trash"></span>
                                                                <span>Delete</span>
                                                            </a>
                                                            @endhasanyrole
                                                        </td>

                                                    </tr>
                                                     @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="4">
                                                            <a title="Add New" href="{{ url('criminal-respondents/add', $case->id) }}"
                                                               class="btn btn-primary btn-xs pull-left">
                                                                <i class="fa fa-plus"></i>
                                                                Add New
                                                            </a>
                                                        </td>
                                                    </tr>
                                                  </tfoot>
                                                </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#nature"><i class="fa fa-gears"></i> Nature of Case<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="nature" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                     <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th data-toggle="true">#</th>
                                                        <th>Case Nature</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($case)
                                                        @foreach($case->caseNatures as $key => $nature)
                                                            <tr>
                                                                <td>{{ ++$key }}</td>
                                                                <td>
                                                                    @if($nature->natureCase)
                                                                        {{ $nature->natureCase->nature }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <a title="Edit" href="{{ url('criminal-nature-cases/edit', [$case->id, $nature->id]) }}" class="btn btn-info btn-xs">
                                                                        <span class="fa fa-edit"></span>
                                                                        <span>Edit</span>
                                                                    </a>
                                                                    @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                                    <a title="Edit" href="{{ url('criminal-nature-cases/delete', $nature->id) }}" class="btn btn-danger btn-xs">
                                                                        <span class="fa fa-trash"></span>
                                                                        <span>Delete</span>
                                                                    </a>
                                                                    @endhasanyrole
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="3">
                                                            <a title="Add Case Nature" href="{{ url('criminal-nature-cases/add', $case->id) }}"
                                                               class="btn btn-primary btn-xs pull-left">
                                                                <i class="fa fa-plus"></i>
                                                                Add More
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                           
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#legal"><i class="fa fa-gavel"></i> Legal Representative<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="legal" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            @if($case)
                                            @foreach($case->caseAdvocates as $key => $advocate)
                                                {{ $advocate->qualification }}(s)
                                            @endforeach
                                            @endif
                                            </legend>
                                        @if($case)
                                            @foreach($case->caseAdvocates as $key => $advocate)
                                                @if($advocate->case_appearence == 'Litigant')
                                                This case has no Advocate
                                                @else        
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>Representing</th>
                                            <th>More Details</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <tr>
                                            <td>
                                                <span>
                                                 <img alt="Avatar" class="img-circle" src="{{ asset('images/users/profile_small3.jpg') }}" /> 
                                                </span>
                                               
                                            </td>
                                            <td>
                                               {{ $advocate->advocate->name }}
                                            </td>
                                            <td>
                                               {{ $advocate->representing }}
                                            </td>
                                            <td>
                                            <p class="small">
                                                Roll Number: {{ $advocate->advocate->rollNumber }}; Mobile : {{ $advocate->advocate->mobile }} email address : {{ $advocate->advocate->email }}.
                                            </p>
                                            </td>
                                            <td>
                                                <a title="Edit" href="{{ url('criminal-advocates/edit', [$case->id, $advocate->id]) }}" class="btn btn-info btn-xs">
                                                    <span class="fa fa-edit"></span>
                                                    <span><strong>Edit</strong></span>
                                                </a>
                                                @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                <a title="Delete" href="{{ url('criminal-advocates/delete', $advocate->id) }}" class="btn btn-danger btn-xs">
                                                    <span class="fa fa-trash"></span>
                                                    <span><strong>Delete</strong></span>
                                                </a>
                                                @endhasanyrole
                                            </td>

                                        </tr>
                                         
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <a title="Add New" href="{{ url('criminal-advocates/add', $case->id) }}"
                                                       class="btn btn-primary btn-xs pull-left">
                                                        <i class="fa fa-plus"></i>
                                                        Add New
                                                    </a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
					  @endif
					 @endforeach
                                        @endif

                                            </fieldset>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#victim"><i class="fa fa-puzzle-piece"></i> Case Victims<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="victim" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                              Case Victim(s)
                                            </legend>
                                                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>More Details</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($case)
                                        @foreach($case->caseVictims as $key => $victim)
                                        <tr>
                                            <td>
                                                <span>
                                                 <img alt="Avatar" class="img-circle" src="{{ asset('images/users/profile_small1.jpg') }}" /> 
                                                </span>
                                               
                                            </td>
                                            <td>
                                               {{ $victim->victim_name }}
                                            </td>
                                            <td>
                                            <p class="small">
                                                Age: {{ $victim->victim_age }}, Gender: {{ $victim->victim_gender }}, Mobile : {{ $victim->victim_mobile }}, Physical  Address : {{ $victim->victim_address }}.
                                            </p>
                                            </td>
                                            <td>
                                                <a title="Edit" href="{{ url('case-victims/edit', [$case->id, $victim->id]) }}" class="btn btn-info btn-xs">
                                                <span class="fa fa-edit"></span>
                                                <span><strong>Edit</strong></span>
                                                </a>
                                                @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                <a title="Delete" href="{{ url('case-victims/delete', $victim->id) }}" class="btn btn-danger btn-xs">
                                                    <span class="fa fa-trash"></span>
                                                    <span><strong>Delete</strong></span>
                                                </a>
                                                @endhasanyrole
                                            </td>

                                        </tr>
                                         @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <a title="Add Victim" href="{{ url('case-victims/add', $case->id) }}"
                                                       class="btn btn-primary btn-xs pull-left">
                                                        <i class="fa fa-plus"></i>
                                                        Add New
                                                    </a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#assignment"><i class="fa fa-sign-in"></i> Case Assignment<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="assignment" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Judge/Magistrate
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Avatar</th>
                                                    <th>Name</th>
                                                    <th>Situation</th>
                                                    <th>Status</th>
                                                    <th>More Details</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @if($case)
                                               @foreach($case->caseAssignments as $key => $assignment)
                                                <tr>
                                                    <td>
                                                        <span>
                                                         <img alt="Avatar" class="img-circle" src="{{ asset('images/users/profile_small1.jpg') }}" /> 
                                                        </span>

                                                    </td>
                                                    <td>
                                                       @if($assignment->user)
                                                          Hon.  {{ $assignment->user->name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                       @if($assignment->assignment_status)
                                                            <span class="label label-{{ ($assignment->assignment_status == 'assigned')? 'primary':'info' }}"
                                                                  style="font-weight: bold; font-size: 12px;">
                                                                    {{ $assignment->assignment_status }}
                                                        </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                       <a href="{{ url('assignment-status/criminal/change', [$case->id, $assignment->user_id]) }}" class="btn btn-{{ ($assignment->status =='active')?'success':'warning' }} btn-xs">
                                                            <strong>{{ $assignment->status  }}</strong>
                                                        </a>
                                                    </td>
                                                    <td>
                                                    <p class="small">
                                                        Assigned Date: {{ $assignment->date_of_assignment }}; Assignment Status : {{ $assignment->assignment_status }} Assignment Mode : {{ $assignment->assignment_mode }}.
                                                    </p>
                                                    </td>
                                                    <td>
                                                        <a title="Edit" href="{{ url('criminal-assignments/edit', [$case->id, $assignment->id]) }}" class="btn btn-primary btn-xs">
                                                            <span class="fa fa-edit"></span>
                                                            <span><strong>Edit</strong></span>
                                                        </a>
                                                        @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                        <a title="Delete" href="{{ url('criminal-assignments/delete', $assignment->id) }}" class="btn btn-danger btn-xs">
                                                            <span class="fa fa-trash"></span>
                                                            <span><strong>Delete</strong></span>
                                                        </a>
                                                        @endhasanyrole
                                                    </td>

                                                </tr>
                                                 @endforeach
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5">
                                                        <a title="Add Assignment" href="{{ url('criminal-assignments/add', $case->id) }}"
                                                           class="btn btn-primary btn-xs pull-left">
                                                            <i class="fa fa-plus"></i>
                                                            Add New
                                                        </a>
                                                    </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                      </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#proceedings"><i class="fa fa-refresh"></i> Case Proceedings<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="proceedings" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Case History - Proceedings
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Coram Date</th>
                                                    <th>Next Stage</th>
                                                    <th>Date of Next Stage</th>
                                                    <th>More Details</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @if($case)
                                               @foreach($case->caseProceedings as $key => $proceeding)    
                                                <tr>
                                                    <td>
                                                        {{++$key}}
                                                    </td>
                                                    <td>
                                                       {{ $proceeding->coram_date }}
                                                    </td>
                                                    <td>
                                                        @if($proceeding->getDeterminationOrNextStage())
                                                            {{ $proceeding->getDeterminationOrNextStage()->name }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                       {{ $proceeding->date_of_stage_or_determination }}
                                                    </td>
                                                    <td>
                                                        <p class="small">
                                                        <b>Before :</b> @if($proceeding->judge)
                                                        Hon. {{ $proceeding->judge->name }}
                                                        @endif, <b>Records Management Assistant :</b>{{ $proceeding->rma_attended }}, <b>Parties Attended :</b> {{ $proceeding->parties_attended }}, <b>Proceeding Outcome :</b> {{ $proceeding->proceeding_outcome }}, <b>Outcome Descriptions :</b> {{ $proceeding->proceeding_outcome_description }}<br/><b>This proceeding was updated on :</b>  {{ $proceeding->date_created }}, <b>By RMA : {{ $proceeding->user }}</b>.
                                                    </p>
                                                    </td>
                                                    <td>
                                                        <a title="Edit" href="{{ url('criminal-proceedings/edit', [$case->id, $proceeding->id]) }}" class="btn btn-info btn-xs">
                                                            <span class="fa fa-edit"></span>
                                                            <span><strong>Edit</strong></span>
                                                        </a>
                                                        @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                        <a title="Delete" href="{{ url('criminal-proceedings/delete', $proceeding->id) }}" class="btn btn-danger btn-xs">
                                                            <span class="fa fa-trash"></span>
                                                            <span><strong>Delete</strong></span>
                                                        </a>
                                                        @endhasanyrole
                                                    </td>

                                                </tr>
                                                @endforeach
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5">
                                                        <a title="Add New Proceeding" href="{{ url('criminal-proceedings/add', $case->id) }}"
                                                           class="btn btn-primary btn-xs pull-left">
                                                            <i class="fa fa-plus"></i>
                                                            Add New
                                                        </a>
                                                    </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#documents"><i class="fa fa-file-pdf-o"></i> Case Documents<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="documents" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Documents & Attachements
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Download/View</th>
                                                    <th>Document Name</th>
                                                    <th>File Name</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @if($case)
                                               @foreach($case->caseDocuments as $key => $document)
                                                <tr>
                                                    <td>
                                                        <span>
                                                         <a href="#document{{$document->id}}"  title="{{ $document->name }}" data-toggle="modal" data-id="{{ $document->id }}" data-name="{{ $document->name }}" data-file="{{ $document->file }}" data-target="#document{{$document->id}}" id="caseDocumentView">
                                                        <span>
                                                          <img alt="image" class="img-square" src="{{ asset('images/users/pdf-small.png') }}" />  
                                                        </span>
                                                        </a>
                                                        </span>

                                                    </td>
                                                    <td>
                                                       {{ ucwords($document->name) }}
                                                    </td>
                                                    <td>
                                                       {{ ucwords($document->file) }}
                                                    </td>
                                                    
                                                    <td>
                                                        <a title="Edit" href="{{ url('criminal-documents/edit', [$case->id, $document->id]) }}" class="btn btn-info btn-xs">
                                                            <span class="fa fa-edit"></span>
                                                            <span>Edit</span>
                                                        </a>
                                                        @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                        <a title="Delete" href="{{ url('criminal-documents/delete', $document->id) }}" class="btn btn-danger btn-xs">
                                                            <span class="fa fa-trash"></span>
                                                            <span>Delete</span>
                                                        </a>
                                                        @endhasanyrole
                                                    </td>

                                                </tr>
                                                    <div class="modal inmodal fade" id="document{{$document->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h2 class="modal-title">{{ $document->name }}-{{ $document->file }} </h2>
                                                            </div>
                                                            <div class="modal-body">
                                                              <embed src="{{ asset('storage/downloads/'.$document->file) }}#toolbar=0" type="application/pdf" width="100%" height="500px" />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                 @endforeach
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5">
                                                        <a title="Add Case Document" href="{{ url('criminal-documents/add', $case->id) }}"
                                                           class="btn btn-primary btn-xs pull-left">
                                                            <i class="fa fa-plus"></i>
                                                            Add New
                                                        </a>
                                                    </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#bill"><i class="fa fa-university"></i> Bill & Payment Details<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="bill" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Fee and Other Payments
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Bill Description</th>
                                                    <th>Bill Amount</th>
                                                    <th>Control Number</th>
                                                    <th>Paid Amount</th>
                                                    <th>Court User</th>
                                                    <th>Date Paid</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @foreach($case->bills as $key => $bill)
                                                <tr>
                                                    <td>
                                                        {{ ++$key }}
                                                    </td>
                                                    <td>
                                                       {{ $bill->BillDescription }}
                                                    </td>
                                                    
                                                    <td>
                                                        {{ $bill->CurrencyCode }}-{{ $bill->BilledAmount }}
                                                    </td>
                                                    
                                                    <td>
                                                        {{ $bill->BillControlNumber }}
                                                    </td>
                                                    <td>
                                                         {{ $bill->CurrencyCode }}-{{ $bill->PaidAmt }}
                                                    </td>
                                                    <td>
                                                         {{ $bill->CustomerPayerName }}
                                                    </td>
                                                    <td>
                                                        {{ $bill->TrxDtTm }}
                                                    </td>
                                                    <td>
                                                        <a title="Print Recipt" 
                                                           href="#receiptPreview{{$bill->id}}" data-toggle="modal" data-id="{{$bill->id}}"  data-target="#receiptPreview{{$bill->id}}" class="btn btn-primary btn-xs printBill">
                                                            <span class="fa fa-print"></span>
                                                            <span><strong>Print</strong></span>
                                                        </a>
                                                    </td>

                                                </tr>
                                                    
                                                      <div class="modal inmodal fade" id="receiptPreview{{$bill->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <span>Jamhuri ya Muungano wa Tanzania</span><br/>
                                                        <span>United Republic of Tanzania</span><br/>
                                                        <span><strong>Judiciary Fund</strong></span><br/>
                                                        <span>Exchequer Receipt</span>
                                                        <br/>
                                                        <span><strong>Stakabadhi ya Malipo ya Serikali</strong></span>
                                                    </div>
                                                    <cente>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <span>Receipt Number </span>:
                                                    <span><strong>{{$bill->PayRefId}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Received From </span>:
                                                    <span><strong>{{$bill->CustomerPayerName}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Amount </span>:
                                                    <span><strong>{{$bill->PaidAmt}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Amount in Words </span>:
                                                    <span><strong>{{$bill->PaidAmt}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>In Respect of </span>:
                                                    <span><strong>{{$bill->GfsCode}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Bill Reference </span>:
                                                    <span><strong>{{$bill->CaseTypeId}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Payment Control Number </span>:
                                                    <span><strong>{{$bill->BillControlNumber}} </strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Payment Date </span>:<span><strong>{{$bill->TrxDtTm}} </strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Issued By </span>:<span><strong>{{Auth::user()->name}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Date Issued </span>:<span><strong>{{date('Y-m-d')}}</strong></span>
                                                </div>
                                                <div class="row">
                                                    <span>Signature </span>:<span><strong>_____________________</strong></span>
                                                </div>
                                            </div>
                                                    </cente>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#execution"><i class="fa fa-power-off"></i> Inforcement of Criminal Order<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="execution" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Execution Proceedings
                                            </legend>
                                                <table class="table table-stripped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date of Enforcement</th>
                                                    <th>Type of Order Enforcement</th>
                                                    <th>Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($case)
                                                @foreach($case->caseEnforcements as $key => $enforcement)
                                                    <tr class="gradeX">
                                                            <td>{{ ++$key }}</td>
                                                        <td>{{ $enforcement->date_of_enforcement }}</td>
                                                        <td>{{ ucwords($enforcement->enforcement_name) }}</td>
                                                        <td>
                                                            <a title="Edit" href="{{ url('criminal-enforcements/edit', [$enforcement->cases_id, $enforcement->id]) }}" class="btn btn-primary btn-xs">
                                                                <span class="fa fa-edit"></span>
                                                                <span><strong>Edit</strong></span>
                                                            </a>
                                                            @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                            <a title="Delete" href="{{ url('criminal-enforcements/delete', $enforcement->id) }}" class="btn btn-danger btn-xs">
                                                                <span class="fa fa-trash"></span>
                                                                <span><strong>Delete</strong></span>
                                                            </a>
                                                            @endhasanyrole
                                                        </td>
                                                        </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="4">
                                                        <a title="Add Enforcement" href="{{ url('criminal-enforcements/add', $case->id) }}"
                                                           class="btn btn-primary btn-xs pull-left">
                                                            <i class="fa fa-plus"></i>
                                                            Add New
                                                        </a>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#reopen"><i class="fa fa-undo"></i> Case Re-Open<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="reopen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Case Re-Open (Re-Admission)
                                            </legend>
                                                
                                        <table class=" table table-stripped" >
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date of Re-Open</th>
                                                <th>Reason for Re-Open</th>
                                                <th class="col-lg-2"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($case)
                                                @foreach($case->caseReadmissions as $key => $readmission)
                                                    <tr class="gradeX">
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $readmission->re_admission_date }}</td>
                                                        <td>{{ ucwords($readmission->reason) }}</td>
                                                        <td>
                                                            <a title="Edit" href="{{ url('criminal-readmissions/edit', [$readmission->cases_id, $readmission->id]) }}" class="btn btn-primary btn-xs">
                                                                <span class="fa fa-edit"></span>
                                                                <span><strong>Edit</strong></span>
                                                            </a>
                                                            @hasanyrole('super-admin|registrar|magistrate-in-charge')
                                                            <a title="Edit" href="{{ url('criminal-readmissions/delete', $readmission->id) }}" class="btn btn-danger btn-xs">
                                                                <span class="fa fa-trash"></span>
                                                                <span><strong>Delete</strong></span>
                                                            </a>
                                                            @endhasanyrole
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
 
@stop

@section('modal')
    {{--@include('advocates.criminal.delete')--}}
    {{--@include('assignments.criminal.delete')--}}
    {{--@include('complainants.criminal.delete')--}}
    {{--@include('proceedings.criminal.delete')--}}
    {{--@include('respondents.criminal.delete')--}}
@stop

@section('page-specific-js')
    <!-- FooTable -->

    <script src="{{ asset('assets/js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('assets/js/plugins/footable/footable.all.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@stop

