<!-- ########## START: HEAD PANEL ########## -->
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="#" class="navbar-brand"><strong>JSDS</strong></a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li @if(\Request::is('dashboard')) class="active" @endif>
                        <a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                    </li>
					<li>
                        <a href="{{ url('civil-admission/bills') }}"><i class="fa fa-bank"></i> <span class="nav-label">Bills & Payments</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-th-list"></i> <span class="nav-label">Tanzania Postcode Lists</span></a>
                    </li>
                    
                    @if(
                        Auth::user()->can('view civil cases') ||
                        Auth::user()->can('view criminal cases') ||
                        Auth::user()->can('view case proceedings')
                    )
                    
                    
                        
                   
                    @endif
                    
                    @if(
                    Auth::user()->can('view civil admission') ||
                    Auth::user()->can('view admission approval') ||
                    Auth::user()->can('view admission register')
                    )
                    
                    
                    @endif

                    @if(
                     Auth::user()->can('view summary statistical reports') ||
                     Auth::user()->can('view case register report') ||
                     Auth::user()->can('view case history report') ||
                     Auth::user()->can('view cause list report') ||
                     Auth::user()->can('view case assignment report') ||
                     Auth::user()->can('view case report by age') ||
                     Auth::user()->can('view performance and analysis report') ||
                     Auth::user()->can('view returning report') ||
                     Auth::user()->can('view pending judgement report') ||
                     Auth::user()->can('view summons report')
                    )
                    
                    <li @if(\Request::is('reports') || \Request::is('reports/*') ) class='active' @endif class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart-o"></i> Case Reports <span class="caret"></span></a>
                        
                        <ul role="menu" class="dropdown-menu">
                            @can('view summary statistical reports')
                            <li @if(\Request::is('reports') || \Request::is('reports/*') )
                                class='active' @endif>
                                <a href="{{ url('reports') }}">Statistical Reports</a>
                            </li>
                        @endcan
                        @can('view case register report')
                            <li @if(\Request::is('reports/case-register') || \Request::is('reports/case-register/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-register') }}">Case Register Report</a>
                            </li>
                        @endcan
                        @can('view case history report')
                            <li @if(\Request::is('reports/case-history') || \Request::is('reports/case-history/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-history') }}">Case History Report</a>
                            </li>
                        @endcan
                        @can('view cause list report')
                            <li @if(\Request::is('reports/cause-list') || \Request::is('reports/cause-list/*'))
                                class='active' @endif>
                                <a href="{{ url('civil-admission/causeListReport') }}">Cause List Report</a>
                            </li>
                        @endcan
                        @can('view case assignment report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-assignment') }}">Case Assignment Report</a>
                            </li>
                        @endcan
						@can('view case report by age')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Case Report By Age</a>
                            </li>
                        @endcan
						@can('view case performance report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Performance Report</a>
                            </li>
                        @endcan
						@can('view case return report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Return Report</a>
                            </li>
                        @endcan
						@can('view case pendiing judgment report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Pending Judgment</a>
                            </li>
                        @endcan
						@can('view case summons report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Summons Report</a>
                            </li>
                        @endcan
                        </ul>
                    </li>
                     @endif

                </ul>
                
                
                <ul class="nav navbar-top-links navbar-right">
                    
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-primary">{{$admissionNotificationCount}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    @foreach($admissionNotification as $key => $admissionNotifications)
                                    <div>
                                        <a data-toggle="modal" data-target="#notificationContent">
                                        <small class="pull-right">{{$admissionNotifications->created_at->diffForHumans()}}</small><i class='fa fa-bell faa-ring animated fa-1x'></i>
                                         <small>
                                             {{$admissionNotifications->head}} :
                                             {{substr($admissionNotifications->content, 0, 10)}}....
                                        </small>
                                        </a>
                                    </div><hr/>
                                    
                                    @endforeach
                                </div>
                            </li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="{{ url('notifications/readAll') }}">
                                        <i class="fa fa-bell"></i> <strong>Read All Notifications</strong>
                                    </a>
                                </div>
                                
                                
                                
                            </li>
                        </ul>
                    </li>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <li class="dropdown">
                        <span>
                        <img alt="image" class="img-circle" src="{{ asset('images/users/profile_small.gif') }}" />
                        </span>
                        
                    </li>
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> <strong class="font-bold">{{ Auth::user()->username }}</strong> <span class="caret"></span></a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ url('users/profile', Auth::user()->id) }}">My Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('#') }}"><i class="fa fa-info"></i> User Guide</a></li>
                            <li>
                                <a data-toggle="modal" data-target="#myModal2">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </nav>
<!-- ########## END: HEAD PANEL ########## -->