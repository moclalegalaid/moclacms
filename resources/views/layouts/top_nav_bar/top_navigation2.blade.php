<!-- ########## START: HEAD PANEL ########## -->
        <nav class="navbar navbar-static-top " role="navigation" >
<!--
            <div class=" row">
                <span style="padding:20px;">
                    <img alt="image" style="height:100px;width:100px;" src="{{ asset('images/users/logo.png') }}" /> <span style="font-size:30px;"><strong>The Judiciary of Tanzania | <small>e filing</small></strong></span>
            </span>
            </div>
-->
            <div class="navbar-collapse collapse" id="navbar" style="background-color: #e42824;">
                <ul class="nav navbar-nav">
                    <li @if(\Request::is('dashboard')) class="active" @endif>
                    <img alt="image" style="height:50px;width:50px;border-radius: 50%;" src="{{ asset('images/users/jotLogo.png') }}" /> 
                    </li>
                    <li @if(\Request::is('dashboard')) class="active" @endif>
                        <a href="{{ url('/') }}" style="background-color: #e42824;"><span class="nav-label" style="color:#393B35;font-family: segoe ui;font-size: 1.2em;">The Judiciary of Tanzania | e-Filing</span></a>
                    </li>
                    <li @if(\Request::is('dashboard')) class="active" @endif>
                        <a href="{{ url('/') }}" style="background-color: #e42824;"><i style="color:white;" class="fa fa-home"></i> <span class="nav-label" style="color:#FFFFFF;">Home</span></a>
                    </li>
					<li>
                        <a href="{{ url('civil-admission/bills') }}"style="background-color: #e42824;"><i style="color:white;" class="fa fa-bank"></i> <span class="nav-label" style="color:#FFFFFF;">Bills</span></a>
                    </li>
                    <li>
                        <a href="https://www.tcra.go.tz/images/documents/postcode/Combined-Postcode-List_Final.pdf"  target="_blank" style="background-color: #e42824;"><i style="color:white;" class="fa fa-list"></i> <span class="nav-label" style="color:#FFFFFF;">Postcode List</span></a>
                    </li>
                    
                    @if(
                        Auth::user()->can('view civil cases') ||
                        Auth::user()->can('view criminal cases') ||
                        Auth::user()->can('view case proceedings')
                    )
                    
                    <li @if(\Request::is('case-proceedings') || \Request::is('case-proceedings/*')
                         || \Request::is('criminal-proceedings') || \Request::is('criminal-proceedings/*')
                         || \Request::is('civil-proceedings') || \Request::is('civil-proceedings/*')
                         || \Request::is('case-assignments') || \Request::is('case-assignments/*')
                         || \Request::is('criminal-assignments') || \Request::is('criminal-assignments/*')
                         || \Request::is('civil-assignments') || \Request::is('civil-assignments/*')
                         || \Request::is('case-advocates') || \Request::is('case-advocates/*')
                         || \Request::is('criminal-advocates') || \Request::is('criminal-advocates/*')
                         || \Request::is('civil-advocates') || \Request::is('civil-advocates/*')
                         || \Request::is('case-documents') || \Request::is('case-documents/*')
                         || \Request::is('criminal-documents') || \Request::is('criminal-documents/*')
                         || \Request::is('civil-documents') || \Request::is('civil-documents/*')
                         || \Request::is('criminal-cases') || \Request::is('criminal-cases/*')
                         || \Request::is('criminal-complainants') || \Request::is('criminal-complainants/*')
                         || \Request::is('criminal-respondents') || \Request::is('criminal-respondents/*')
                         || \Request::is('case-victims') || \Request::is('case-victims/*')
                         || \Request::is('criminal-nature-cases') || \Request::is('criminal-nature-cases/*')
                         || \Request::is('civil-nature-cases') || \Request::is('civil-nature-cases/*')
                         || \Request::is('civil-cases') || \Request::is('civil-cases/*')
                         || \Request::is('civil-complainants') || \Request::is('civil-complainants/*')
                         || \Request::is('civil-respondents') || \Request::is('civil-respondents/*')
                         || \Request::is('case-fees') || \Request::is('case-fees/*')
                         || \Request::is('civil-nature-cases') || \Request::is('civil-nature-cases/*')
                         || \Request::is('criminal-nature-cases') || \Request::is('civil-nature-cases/*'))
                        class='active'  @endif class="dropdown">
                        
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-book"></i> Case Management <span class="caret"></span></a>
                        
                        <ul role="menu" class="dropdown-menu">
                            @can('view civil cases')
                            <li @if(\Request::is('civil-cases') || \Request::is('civil-cases/*')
                                 || \Request::is('civil-complainants') || \Request::is('civil-complainants/*')
                                 || \Request::is('civil-respondents') || \Request::is('civil-respondents/*')
                                 || \Request::is('case-fees') || \Request::is('case-fees/*')
                                 || \Request::is('civil-nature-cases') || \Request::is('civil-nature-cases/*')
                                 || \Request::is('criminal-nature-cases') || \Request::is('civil-nature-cases/*'))
                                class='active'  @endif><a href="{{ url('civil-cases') }}">Civil Register</a>
                            </li>
                            @endcan
                            @can('view criminal cases')
                            <li @if(\Request::is('criminal-cases') || \Request::is('criminal-cases/*')
                                 || \Request::is('criminal-complainants') || \Request::is('criminal-complainants/*')
                                 || \Request::is('criminal-respondents') || \Request::is('criminal-respondents/*')
                                 || \Request::is('case-victims') || \Request::is('case-victims/*')
                                 || \Request::is('criminal-nature-cases') || \Request::is('criminal-nature-cases/*')
                                 || \Request::is('civil-nature-cases') || \Request::is('civil-nature-cases/*'))
                                class='active'  @endif><a href="{{ url('criminal-cases') }}">Criminal Register</a>
                            </li>
                            @endcan
                            @can('view case proceedings')
                            <li @if(\Request::is('case-proceedings') || \Request::is('case-proceedings/*')
                                 || \Request::is('criminal-proceedings') || \Request::is('criminal-proceedings/*')
                                 || \Request::is('civil-proceedings') || \Request::is('civil-proceedings/*') )
                                class='active'  @endif><a href="{{ url('case-proceedings') }}">Case Proceedings</a>
                            </li>
                            @endcan
                            @can('view case assignments')
                            <li @if(\Request::is('case-assignments') || \Request::is('case-assignments/*')
                                 || \Request::is('criminal-assignments') || \Request::is('criminal-assignments/*')
                                 || \Request::is('civil-assignments') || \Request::is('civil-assignments/*') )
                                class='active'  @endif><a href="{{ url('case-assignments') }}">Case Assignments</a>
                            </li>
                            @endcan
                            @can('view case advocates')
                            <li @if(\Request::is('case-advocates') || \Request::is('case-advocates/*')
                                 || \Request::is('criminal-advocates') || \Request::is('criminal-advocates/*')
                                 || \Request::is('civil-advocates') || \Request::is('civil-advocates/*') )
                                class='active'  @endif><a href="{{ url('case-advocates') }}">Case Advocates</a>
                            </li>
                            @endcan
                            @can('view case documents')
                            <li @if(\Request::is('case-documents') || \Request::is('case-documents/*')
                                 || \Request::is('criminal-documents') || \Request::is('criminal-documents/*')
                                 || \Request::is('civil-documents') || \Request::is('civil-documents/*') )
                                class='active'  @endif><a href="{{ url('case-documents') }}">Case Documents</a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endif
                    
                    @if(
                    Auth::user()->can('view civil admission') ||
                    Auth::user()->can('view admission approval') ||
                    Auth::user()->can('view admission register')
                    )
                    
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file"></i> eCase Filing <span class="caret"></span></a>
                        <ul role="menu" class="dropdown-menu">
                            @can('view civil admission')
                            <li><a href="{{ url('civil-admission') }}"
                               @if(\Request::is('civil-admission') || \Request::is('civil-admission/*')) @endif>Case Filing</a>
                            </li>
                            @endcan
                            @can('view admission approval')
                            <li><a href="{{ url('admission-approval') }}"
                               @if(\Request::is('admission-approval') || \Request::is('admission-approval/*'))  @endif>Case Admission</a>
                            </li>
                            @endcan
                            @can('view admission register')
                            <li><a href="{{ url('admission-register') }}"
                               @if(\Request::is('admission-register') || \Request::is('admission-register/*'))  @endif>Case Registration</a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endif

                    @if(
                     Auth::user()->can('view summary statistical reports') ||
                     Auth::user()->can('view case register report') ||
                     Auth::user()->can('view case history report') ||
                     Auth::user()->can('view cause list report') ||
                     Auth::user()->can('view case assignment report') ||
                     Auth::user()->can('view case report by age') ||
                     Auth::user()->can('view performance and analysis report') ||
                     Auth::user()->can('view returning report') ||
                     Auth::user()->can('view pending judgement report') ||
                     Auth::user()->can('view summons report')
                    )
                    
                    <li @if(\Request::is('reports') || \Request::is('reports/*') ) class='active' @endif class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: #e42824;"><i style="color:white;" class="fa fa-bar-chart-o"></i> <span class="nav-label" style="color:#FFFFFF;">Reports <span class="caret"></span></a>
                        
                        <ul role="menu" class="dropdown-menu">
                            @can('view summary statistical reports')
                            <li @if(\Request::is('reports') || \Request::is('reports/*') )
                                class='active' @endif>
                                <a href="{{ url('reports') }}">Statistical Reports</a>
                            </li>
                        @endcan
                        @can('view case register report')
                            <li @if(\Request::is('reports/case-register') || \Request::is('reports/case-register/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-register') }}">Case Register Report</a>
                            </li>
                        @endcan
                        @can('view case history report')
                            <li @if(\Request::is('reports/case-history') || \Request::is('reports/case-history/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-history') }}">Case History Report</a>
                            </li>
                        @endcan
                        @can('view cause list report')
                            <li @if(\Request::is('reports/cause-list') || \Request::is('reports/cause-list/*'))
                                class='active' @endif>
                                <a href="{{ url('ext-reports/cause-list') }}">Cause List Report</a>
                            </li>
                        @endcan
                        @can('view case assignment report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('reports/case-assignment') }}">Case Assignment Report</a>
                            </li>
                        @endcan
						@can('view case report by age')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Case Report By Age</a>
                            </li>
                        @endcan
						@can('view case performance report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Performance Report</a>
                            </li>
                        @endcan
						@can('view case return report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Return Report</a>
                            </li>
                        @endcan
						@can('view case pendiing judgment report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Pending Judgment</a>
                            </li>
                        @endcan
						@can('view case summons report')
                            <li @if(\Request::is('reports/case-assignment') || \Request::is('reports/case-assignment/*'))
                                class='active' @endif>
                                <a href="{{ url('') }}">Summons Report</a>
                            </li>
                        @endcan
                        </ul>
                    </li>
                     @endif

                </ul>
                
                
                <ul class="nav navbar-top-links navbar-right">
                    
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-default">{{$admissionNotificationCount}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    @foreach($admissionNotification as $key => $admissionNotifications)
                                    <div>
                                        <a data-toggle="modal" data-target="#notificationContent">
                                        <small class="pull-right">{{$admissionNotifications->created_at->diffForHumans()}}</small><i class='fa fa-bell faa-ring animated fa-1x'></i>
                                         <small>
                                             {{$admissionNotifications->head}} :
                                             {{substr($admissionNotifications->content, 0, 10)}}....
                                        </small>
                                        </a>
                                    </div><hr/>
                                    
                                    @endforeach
                                </div>
                            </li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="{{ url('notifications/readAll') }}">
                                        <i class="fa fa-bell"></i> <strong>Read All Notifications</strong>
                                    </a>
                                </div>
                                
                                
                                
                            </li>
                        </ul>
                    </li>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <li class="dropdown">
                        
                    </li>
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: #e42824;"> <strong class="font-bold" style="color:#FFFFFF;">{{ Auth::user()->username }}</strong> <span class="caret" style="color:#FFFFFF;"></span></a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ url('ext-users/profile', Auth::user()->id) }}">My Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('#') }}"><i class="fa fa-info"></i> User Guide</a></li>
                            <li>
                                <a data-toggle="modal" data-target="#myModal2">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </nav>
<!-- ########## END: HEAD PANEL ########## -->