<ul class="dropdown-menu dropdown-message">
    @if(Auth::user()->notifications->count())
        <li class="dropdown-header notif-header"><i class="icon-bell-2"></i>
            New notifications
        </li>
        @foreach(Auth::user()->unreadNotifications as $notification)
            <li class="unread">
                <a href="{{ url('/') }}" class="clearfix">
                    <img src="{{ asset('images/users/user-100.jpg') }}" class="xs-avatar ava-dropdown" alt="Avatar">
                    <strong>{{ Auth::user()->name }}</strong><i class="pull-right msg-time">5 minutes ago</i><br />
                    <p>{{ $notification->data['data'] }}...</p>
                </a>
            </li>
        @endforeach
        @foreach(Auth::user()->readNotifications as $notification)
            <li class="read">
                <a href="{{ url('/') }}" class="clearfix">
                    <img src="{{ asset('images/users/user-100.jpg') }}" class="xs-avatar ava-dropdown" alt="Avatar">
                    <strong>{{ Auth::user()->name }}</strong><i class="pull-right msg-time">5 minutes ago</i><br />
                    <p>{{ $notification->data['data'] }}...</p>
                </a>
            </li>
        @endforeach
        <li class="dropdown-footer">
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <a href="{{ url('notifications/delete', Auth::user()->id) }}" class="btn btn-sm btn-danger"><i class="icon-trash-3"></i> Clear All</a>
                </div>
                <div class="btn-group">
                    <a href="{{ url('notifications/mark-as-read', Auth::user()->id) }}" class="btn btn-sm btn-success">Mark as Read <i class="icon-right-open-2"></i></a>
                </div>
            </div>
        </li>
    @else
        <li class="dropdown-header notif-header"><i class="icon-bell-2"></i> No new notifications</li>
    @endif
</ul>