<div class="topbar">
    <div class="topbar-left">
        <div class="logo">
            <h1>&nbsp;JSDS</h1>
        </div>
        <button class="button-menu-mobile open-left">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-collapse2">
                <ul class="nav navbar-nav hidden-xs">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-th"></i>
                        </a>
                        <div class="dropdown-menu grid-dropdown">
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <li class="language_bar dropdown hidden-xs">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">
                            @if(Auth::user()->court)
                                {{ Auth::user()->court->name }}
                            @endif
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right top-navbar">
                    <li class="dropdown iconify hide-phone">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i>
                            @if(Auth::user()->unreadNotifications->count())
                                <span class="label label-danger absolute">
                                {{ Auth::user()->unreadNotifications->count() }}
                            </span>
                            @endif
                        </a>
                        @include('layouts.top_nav_bar.notifications')
                    </li>
                    <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a></li>
                    <li class="dropdown topbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="rounded-image topbar-profile-image"><img src="{{ asset('images/users/user-35.jpg') }}"></span>
                            {{ Auth::user()->name }}
                            <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('users/profile', Auth::user()->id) }}">My Profile</a></li>
                            <li><a href="{{ url('users/change-password', Auth::user()->id) }}">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('#') }}"><i class="icon-help-2"></i> Help</a></li>
                            <li>
                                <a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>