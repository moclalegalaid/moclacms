 <!-- ########## START: LEFT PANEL CURRENTLY USED########## -->
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle img-lg" src="{{ asset('images/users/user.png') }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">[{{ Auth::user()->username }}] <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ url('users/profile', Auth::user()->id) }}">My Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('#') }}"><i class="fa fa-info"></i> Help</a></li>
                        <li>
                            <a data-toggle="modal" data-target="#myModal2">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    MOCLA
                </div>
            </li>

            <li @if(\Request::is('dashboard')) class="active" @endif>
                <a href="{{ url('admin') }}"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
            </li>

            
            <!--  ***** APP SETTINGS STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view home') ||
                    Auth::user()->can('view structure') ||
                    Auth::user()->can('view services') 
                )
                <li @if(\Request::is('home-page') || \Request::is('home-page/*') ||
                        \Request::is('structure') || \Request::is('structure/*') ||
                        \Request::is('service') || \Request::is('service/*') 
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-cog"></i> <span class="nav-label">App Settings</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view home')
                        <li @if(\Request::is('home-page') || \Request::is('home-page/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('home-page') }}">Home Page</a>
                        </li>
                    @endcan
                    @can('view structure')
                        <li @if(\Request::is('structure') || \Request::is('structure/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('structure') }}">Org. Structure</a>
                        </li>
                    @endcan
                    @can('view services')
                        <li @if(\Request::is('service') || \Request::is('service/*'))
                            class='active'  @endif>
                            <a href="{{ url('service') }}">Services Provided</a>
                        </li>
                    @endcan
                    
                </ul>
            </li>
            @endif
            
            <!--  ***** APP SETTINGS ENDS ******   -->
            
          
            <!--  ***** WEB CONTENTS STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view news') ||
                    Auth::user()->can('view posts') ||
                    Auth::user()->can('view documents') ||
                    Auth::user()->can('view photo gallery') ||
                    Auth::user()->can('view media') ||
                    Auth::user()->can('view links')
                )
                <li @if(\Request::is('news') || \Request::is('news/*') ||
                        \Request::is('adverts') || \Request::is('adverts/*') ||
                        \Request::is('documents') || \Request::is('documents/*') ||
                        \Request::is('photo-gallery') || \Request::is('photo-gallery/*') ||
                        \Request::is('media') || \Request::is('media/*') ||
                        \Request::is('links') || \Request::is('links/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-clipboard"></i> <span class="nav-label">Web Content</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view news')
                        <li @if(\Request::is('news') || \Request::is('news/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('news') }}">News</a>
                        </li>
                    @endcan
                    @can('view posts')
                        <li @if(\Request::is('adverts') || \Request::is('adverts/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('adverts') }}">Adverts</a>
                        </li>
                    @endcan
                    @can('view documents')
                        <li @if(\Request::is('documents') || \Request::is('documents/*'))
                            class='active'  @endif>
                            <a href="{{ url('documents') }}">Documents & Publications</a>
                        </li>
                    @endcan
                    @can('view photo gallery')
                        <li @if(\Request::is('photo-gallery') || \Request::is('photo-gallery/*'))
                            class='active'  @endif>
                            <a href="{{ url('photo-gallery') }}">Photo Gallery</a>
                        </li>
                    @endcan
                    @can('view media')
                        <li @if(\Request::is('media') || \Request::is('media/*'))
                            class='active'  @endif>
                            <a href="{{ url('media') }}">Video Gallery</a>
                        </li>
                    @endcan
                    @can('view links')
                        <li @if(\Request::is('links') || \Request::is('links/*') )
                            class='active'  @endif>
                            <a href="{{ url('links') }}">Site Links</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** WEB CONTENTS ENDS ******   -->



            <!--  ***** LASP STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view paralegal') ||
                    Auth::user()->can('view provider')
                )
                <li @if(\Request::is('paralegal') || \Request::is('paralegal/*') ||
                        \Request::is('provider') || \Request::is('provider/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-clipboard"></i> <span class="nav-label">LASP</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view paralegal')
                        <li @if(\Request::is('paralegal') || \Request::is('paralegal/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('paralegal') }}">Paralegal</a>
                        </li>
                    @endcan
                    @can('view provider')
                        <li @if(\Request::is('provider') || \Request::is('provider/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('provider') }}">Service Provider</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** LASP ENDS ******   -->


            <!--  *****  LEGAL AID WEEK STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view participant') ||
                    Auth::user()->can('view event')
                )
                <li @if(\Request::is('participant') || \Request::is('participant/*') ||
                        \Request::is('event') || \Request::is('event/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-clipboard"></i> <span class="nav-label">Legal Aid Week</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view participant')
                        <li @if(\Request::is('participant') || \Request::is('participant/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('participant') }}">Participants</a>
                        </li>
                    @endcan
                    @can('view event')
                        <li @if(\Request::is('event') || \Request::is('event/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('event') }}">Recorded Events</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** LEGAL AID WEEK ENDS ******   -->

            <!--  *****  LEGAL AID AGENT STARTS ******   -->
            
            @if(   
                    Auth::user()->can('view query')
                )
                <li @if(\Request::is('query') || \Request::is('query/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-clipboard"></i> <span class="nav-label">Legal Aid Agent</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view query')
                        <li @if(\Request::is('query') || \Request::is('query/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('query') }}">Customer Query</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** LEGAL AID AGENT ENDS ******   -->


            <!--  *****  FEEDBACK STARTS ******   -->
            
            @if(   
                    Auth::user()->can('view comment')
                )
                <li @if(\Request::is('comment') || \Request::is('comment/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-clipboard"></i> <span class="nav-label">Feedback</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view comment')
                        <li @if(\Request::is('comment') || \Request::is('comment/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('comment') }}">User Comments</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** FEEDBACK ENDS ******   -->

           
         
            
            <!--  ***** LEGAL AID MANAGEMENT STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view desks') ||
                    Auth::user()->can('view comments') ||
                    Auth::user()->can('view aid type')  ||
                    Auth::user()->can('view desks')
                )
                <li @if(\Request::is('desk/register-aid') || \Request::is('desk/register-aid/*') ||
                        \Request::is('desk/comments') || \Request::is('desk/comments/*') ||
                        \Request::is('desk/desk') || \Request::is('desk/desk/*') ||
                        \Request::is('desk/aid-type') || \Request::is('desk/aid-type/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-cog"></i> <span class="nav-label">Legal Aid Desk</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view desks')
                        <li @if(\Request::is('desk/register-aid') || \Request::is('desk/register-aid/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('desk/register-aid') }}">Register Legal Aid</a>
                        </li>
                    @endcan
                    @can('view comments')
                        <li @if(\Request::is('desk/comments') || \Request::is('desk/comments/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('desk/comments') }}">Comments & Feedbacks</a>
                        </li>
                    @endcan
                    @can('view aid type')
                        <li @if(\Request::is('desk/aid-type') || \Request::is('desk/aid-type/*'))
                            class='active'  @endif>
                            <a href="{{ url('desk/aid-type') }}">Legal Aid Type</a>
                        </li>
                    @endcan
                    @can('view desks')
                        <li @if(\Request::is('desk/desk') || \Request::is('desk/desk/*'))
                            class='active' @endif>
                            <a href="{{ url('desk/desk') }}">Aid Desk Stations</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** LEGAL AID MANAGEMENT ENDS ******   -->
            
            
            <!--  ***** LOCATION STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view regions') ||
                    Auth::user()->can('view districts') ||
                    Auth::user()->can('view wards')
                )
                <li @if(\Request::is('regions') || \Request::is('regions/*') ||
                        \Request::is('districts') || \Request::is('districts/*') ||
                        \Request::is('wards') || \Request::is('wards/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-cog"></i> <span class="nav-label">Locations</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view regions')
                        <li @if(\Request::is('regions') || \Request::is('regions/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('regions') }}">Regions</a>
                        </li>
                    @endcan
                    @can('view districts')
                        <li @if(\Request::is('districts') || \Request::is('districts/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('districts') }}">Districts</a>
                        </li>
                    @endcan
                    @can('view wards')
                        <li @if(\Request::is('wards') || \Request::is('wards/*'))
                            class='active'  @endif>
                            <a href="{{ url('wards') }}">Wards</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif
            
            <!--  ***** LOCATION ENDS ******   -->
            
            
            <!--  ***** REPORTS STARTS ******   -->
            
            @if(    
                    Auth::user()->can('view legal weeks') ||
                    Auth::user()->can('view legal aids') ||
                    Auth::user()->can('view claims')
                )
                <li @if(\Request::is('report/legal-weeks') || \Request::is('report/legal-weeks/*') ||
                        \Request::is('report/legal-aid') || \Request::is('report/legal-aid/*') ||
                        \Request::is('report/claims') || \Request::is('report/claims/*')
                       )
                    class="active" @endif>
                <a href="javascript:void(0);"><i class="fa fa-cog"></i> <span class="nav-label">Reports</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view legal weeks')
                        <li @if(\Request::is('report/legal-weeks') || \Request::is('report/legal-weeks/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('report/legal-weeks') }}">Legal Weeks Report</a>
                        </li>
                    @endcan
                    @can('view legal aids')
                        <li @if(\Request::is('report/legal-aid') || \Request::is('report/legal-aid/*')
                             )
                            class='active'  @endif>
                            <a href="{{ url('report/legal-aid') }}">Legal Aid Provided</a>
                        </li>
                    @endcan
                    @can('view claims')
                        <li @if(\Request::is('report/claims') || \Request::is('report/claims/*'))
                            class='active'  @endif>
                            <a href="{{ url('report/claims') }}">Registered Claims</a>
                        </li>
                    @endcan
                </ul>
            </li>
            @endif


               <!--  ***** USER MANAGEMENT STARTS ******   -->
            
               @if(
                     Auth::user()->can('view permissions') ||
                     Auth::user()->can('view roles') ||
                     Auth::user()->can('view users') ||
            
                     Auth::user()->can('view courts') ||
            
                     Auth::user()->can('view profile')
                )
                <li @if(\Request::is('users/managements') || \Request::is('users/managements/*') ||
                        \Request::is('users/permissions') || \Request::is('users/roles') ||
                        \Request::is('users/roles/*') || \Request::is('users/permissions')||
                        \Request::is('users/courts/*') || \Request::is('users/courts')||
                        \Request::is('users/profile/*')
                        )
                    class='active' @endif>
                    <a href="javascript:void(0);"><i class="fa fa-users"></i> <span class="nav-label">User Management</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        @can('view permissions')
                            <li @if(\Request::is('users/permissions'))
                                class='active' @endif>
                                <a href="{{ url('users/permissions') }}">User Permissions</a>
                            </li>
                        @endcan
                        @can('view roles')
                            <li @if(\Request::is('users/roles') || \Request::is('users/roles/*'))
                                class='active' @endif>
                                <a href="{{ url('users/roles') }}">User Roles</a>
                            </li>
                        @endcan
                        @can('view users')
                            <li @if(\Request::is('users/managements') || \Request::is('users/managements/*'))
                                class='active' @endif>
                                <a href="{{ url('users/managements') }}">Manage Users</a>
                            </li>
                        @endcan
                        
                        
                        @can('view profile')
                            <li @if(\Request::is('users/profile') || \Request::is('users/profile/*'))
                                class='active' @endif>
                                <a href="{{ url('users/profile', Auth::user()->id) }}">My Profile</a>
                            </li>
                        @endcan

                        @can('view courts')
                            <li @if(\Request::is('users/courts') || \Request::is('users/courts/*'))
                                class='active' @endif>
                                <a href="{{ url('users/courts') }}">Courts</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endif
            
            
            
            <!--  ***** REPORTS ENDS ******   -->
            

            @can('view system logs')
            <li @if(\Request::is('system/audit-logs')) class="active" @endif>
                <a href="{{ url('system/audit-logs') }}"><i class="fa fa-cogs"></i> <span class="nav-label">System Logs & Audits</span></a>
            </li>
            @endcan
            
             <!--  ***** USER MANAGEMENT ENDS ******   -->
            
        </ul>

    </div>
</nav>
<!-- ########## END: LEFT PANEL ########## -->
