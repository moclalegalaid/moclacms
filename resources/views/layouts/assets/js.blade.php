<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('libs/jquery/jquery-1.11.1.min.js') }}" ></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}" ></script>
<script src="{{ asset('libs/jqueryui/jquery-ui-1.10.4.custom.min.js') }}" ></script>
<script src="{{ asset('libs/jquery-ui-touch/jquery.ui.touch-punch.min.js') }}" ></script>
<script src="{{ asset('libs/jquery-detectmobile/detect.js') }}" ></script>
<script src="{{ asset('libs/jquery-animate-numbers/jquery.animateNumbers.js') }}" ></script>
<script src="{{ asset('libs/ios7-switch/ios7.switch.js') }}" ></script>
<script src="{{ asset('libs/fastclick/fastclick.js') }}" ></script>
<script src="{{ asset('libs/jquery-blockui/jquery.blockUI.js') }}" ></script>
<script src="{{ asset('libs/bootstrap-bootbox/bootbox.min.js') }}" ></script>
<script src="{{ asset('libs/jquery-slimscroll/jquery.slimscroll.js') }}" ></script>
<script src="{{ asset('libs/jquery-sparkline/jquery-sparkline.js') }}" ></script>
<script src="{{ asset('libs/nifty-modal/js/classie.js') }}" ></script>
<script src="{{ asset('libs/nifty-modal/js/modalEffects.js') }}" ></script>
<script src="{{ asset('libs/sortable/sortable.min.js') }}" ></script>
<script src="{{ asset('libs/bootstrap-fileinput/bootstrap.file-input.js') }}" ></script>
<script src="{{ asset('libs/bootstrap-select/bootstrap-select.min.js') }}" ></script>
{{--<script src="{{ asset('libs/bootstrap-select2/select2.min.js') }}" ></script>--}}
<script src="{{ asset('libs/magnific-popup/jquery.magnific-popup.min.js') }}" ></script>
<script src="{{ asset('libs/pace/pace.min.js') }}" ></script>
<script src="{{ asset('libs/jquery-notifyjs/notify.min.js') }}"></script>
<script src="{{ asset('libs/jquery-notifyjs/styles/metro/notify-metro.js') }}"></script>
<script src="{{ asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" ></script>
<script src="{{ asset('libs/jquery-icheck/icheck.min.js') }}" ></script>

<script src="{{ asset('libs/prettify/prettify.js') }}" ></script>
<script src="{{ asset('js/init.js') }}" ></script>


