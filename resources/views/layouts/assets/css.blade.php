<link rel="stylesheet" href="{{ asset('libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/fontello/css/fontello.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/animate-css/animate.min.css') }}" />
<link rel="stylesheet" href="{{ asset('libs/nifty-modal/css/component.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/magnific-popup/magnific-popup.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/ios7-switch/ios7-switch.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/pace/pace.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/sortable/sortable-theme-bootstrap.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/bootstrap-datepicker/css/datepicker.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/jquery-icheck/skins/all.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/jquery-notifyjs/styles/metro/notify-metro.css') }}" >
<link rel="stylesheet" href="{{ asset('libs/prettify/github.css') }}" >

<link rel="stylesheet" href="{{ asset('libs/bootstrap-select/bootstrap-select.min.css') }}" >