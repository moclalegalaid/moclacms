@extends('main')

@section('title')
    @parent
    Adoption Cases
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    
    <style>
        
        
        .head{
            padding-top: 10px;
            color:  #3a383b;
        }
        
        .well{
           padding-left: 10px;
           padding-right: 10px;
           padding-top: 2px;
           padding-bottom: 2px;
           border-bottom-color: #e42824;
        }
        
        #table-search thead td {
          background-color: #e42824;
          color: white;
          font-size: 14px;
          font-weight: 600;
          width: 30px;
        }
        
        #table-search {
          color: black;
          font-size: 14px;
        }
    
        #font {
          color: black;
        }
    </style>
    
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight" id="font">
	
    <div class="row" id="font">
        <div class="col-lg-12">
            
         <div class="well">
         <div class="form-inline pull-right head">
           
            <button  title="Go back" onclick="goBack()" class="btn btn-default btn-sm"><i class="fa fa-level-up"></i> Go Back </button>
        </div>
         <h3 style="padding:0px;"><img src="{{ URL::to('/images/users/filed1.png')}}" style="width:40px;height:40px;" /> Adoption Case View</h3>
     </div>
        </div>
    </div>
    
    
    @if($case)
    <div class="row">
        
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#details"><i class="fa fa-info-circle"></i>  Case Details<small> (Click to open/close pane)</small></a>
                                            </h5>
                                        </div>
                                        <div id="details" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                
                                            <div class="col-lg-12">
                                                <div class="wrapper wrapper-content animated fadeInUp">
                                                    <div class="ibox">
                                                        <div class="ibox-content">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="m-b-md">
                                                                        
                                                                        <h2>{{ $case->caseType->name }} No. {{ $case->case_no }} of {{ $case->case_year }}</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12"> 
                                                                <div>
                                                                    <table class="table table-bordered" style="font-size:13px;">
                                                                    <tbody>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Status</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               @if($case->getLastProceeding())
                                                                                <span class="label label-{{ ($case->getLastProceeding()->proceeding_outcome == 'pending')?'danger':'success' }}">
                                                                                        {{ $case->getLastProceeding()->proceeding_outcome }}
                                                                            </span>
                                                                            @else
                                                                                <span class="label">filed</span>
                                                                            @endif
                                                                            </div>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Reference Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_ref_number }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Code</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                DC-MBY-CIV-MSC-12-2019
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_no }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Year</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->case_year }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Filing Date</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->filing_date  }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Submission Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               {{ $case->submission_type  }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                 {{ $case->caseType->name }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong> Court Fee Exemption</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>{{ $case->fee_exemption }}</div>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <div><strong>Involved Dept</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->involved_debt }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Subject Matter Value/Claim Amount</strong> </div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->currency }} - {{ $case->subject_matter }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Case Urgency</strong> </div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->urgency }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div><strong>Records Management Assistant</strong></div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $case->user_id }}</div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                                </div>
                                                        </div>
                                                            @if($case->instance_level == 'Appeal/Application')
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="m-b-md">
                                                                        <h2>Details for the Existing/Originationg Case</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12"> 
                                                                <div>
                                                                    <table class="table table-bordered" style="font-size:13px;">
                                                                    <tbody>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Original Court</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_court_id }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_caseType_id }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Number</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_case_number }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Case Year</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $case->origin_case_year }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                                </div>
                                                        </div>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#claim"><i class="fa fa-gavel"></i> Claim/Releaf<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="claim" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                {{ $case->charge_or_claim }}
                                            </div>
                                        </div>
                                        </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#parties"><i class="fa fa-gavel"></i> Case Parties<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="parties" class="panel-collapse collapse">
                                            <div class="panel-body">
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            @if($case)
                                            @foreach($case->caseComplainants as $key => $complainant)
                                                {{ $complainant->appeared_as }}(s)
                                            @endforeach
                                            @endif
                                            </legend>
                                                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>More Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($case)
                                        @foreach($case->caseComplainants as $key => $complainant)
                                        <tr>
                                            <td>
                                                {{++$key}}
                                               
                                            </td>
                                            <td>
                                               {{ $complainant->name }}
                                            </td>
                                            <td>
                                            <p class="small">
                                                Age: {{ $complainant->date_of_birth }} years; Gender: {{ $complainant->gender }}; Mobile Number : {{ $complainant->mobile }}; Physical Address : {{ $complainant->address }}; PostCode : {{ $complainant->post_code }}; email address : {{ $complainant->email }}.
                                            </p>
                                            </td>
                                            

                                        </tr>
                                         @endforeach
                                        @endif
                                        </tbody>
                                        
                                    </table>
                                            </fieldset>
                                                
                                             <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                             @if($case)
                                            @foreach($case->caseRespondents as $key => $respondent)
                                                {{ $respondent->appeared_as }}(s)
                                            @endforeach
                                            @endif
                                            </legend>
                                                
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Name</th>
                                                        <th>More Details</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($case)
                                                    @foreach($case->caseRespondents as $key => $respondent)
                                                    <tr>
                                                        <td>
                                                           {{++$key}}
                                                        </td>
                                                        <td>
                                                           {{ $respondent->name }}
                                                        </td>
                                                        <td>
                                                        <p class="small">
                                                           Age: {{ $respondent->date_of_birth }} years; Gender: {{ $respondent->gender }}; Mobile Number : {{ $respondent->mobile }}; Physical Address : {{ $respondent->address }}; PostCode : {{ $respondent->post_code }}; email address : {{ $respondent->email }}.
                                                        </p>
                                                        </td>
                                                        

                                                    </tr>
                                                     @endforeach
                                                    @endif
                                                    </tbody>
                                                    
                                                </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#assignment"><i class="fa fa-sign-in"></i> Case Assignment<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="assignment" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Judge/Magistrate
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>More Details</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @if($case)
                                               @foreach($case->caseAssignments as $key => $assignment)
                                                <tr>
                                                    <td>
                                                        {{++$key}}

                                                    </td>
                                                    <td>
                                                       @if($assignment->user)
                                                            {{ $assignment->user->name }}
                                                        @endif
                                                    </td>
                                                    
                                                    
                                                    <td>
                                                    <p class="small">
                                                        Assigned Date: {{ $assignment->date_of_assignment }}; Assignment Status : {{ $assignment->assignment_status }} Assignment Mode : {{ $assignment->assignment_mode }}.
                                                    </p>
                                                    </td>
                                                    

                                                </tr>
                                                 @endforeach
                                                @endif
                                                </tbody>
                                                
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                      </div>
                                    
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#documents"><i class="fa fa-file-pdf-o"></i> Case Documents<small> (Click to open/close pane)</small></a>
                                            </h4>
                                        </div>
                                        <div id="documents" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                
                                            <fieldset class="fieldset">
                                            <legend style="font-size: 15px;">
                                            Documents & Attachements
                                            </legend>
                                                
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Document Name</th>
                                                    <th>File Name</th>
                                                    <th>Download/View</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                               @if($case)
                                               @foreach($case->caseDocuments as $key => $document)
                                                <tr>
                                                    <td>
                                                        {{++$key}}
                                                    </td>
                                                    
                                                    <td>
                                                       {{ ucwords($document->name) }}
                                                    </td>
                                                    <td>
                                                       {{ ucwords($document->file) }}
                                                    </td>
                                                    <td>
                                                        <span>
                                                         <a href="#document{{$document->id}}"  title="{{ $document->name }}" data-toggle="modal" data-id="{{ $document->id }}" data-name="{{ $document->name }}" data-file="{{ $document->file }}" data-target="#document{{$document->id}}" id="caseDocumentView">
                                                        <span>
                                                          <img alt="image" style="height:30px;width:30px;" class="img-square" src="{{ asset('images/users/attachment.png') }}" />  
                                                        </span>
                                                        </a>
                                                        </span>

                                                    </td>
                                                    
                                                </tr>
                                                    <div class="modal inmodal fade" id="document{{$document->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h2 class="modal-title">{{ $document->name }}-{{ $document->file }} </h2>
                                                            </div>
                                                            <div class="modal-body">
                                                              <embed src="{{ asset('storage/downloads/'.$document->file) }}#toolbar=0" type="application/pdf" width="100%" height="500px" />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                 @endforeach
                                                @endif
                                                </tbody>
                                                
                                            </table>
                                            </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
    @else
        <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-search" >
                            <tbody>
                                <tr class="gradeX">
                                    <td>No case with ID {{$caseId}} in your scope</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                     
                </div>
        </div>
    @endif

</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        
        function goBack() {
          window.history.back();
        }
        
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Civil Register'},
                    {extend: 'pdf', title: 'Civil Register'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            $('.owners-select').select2({width: "100%" , placeholder:'----Select SO Owner(s)----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});
            
            
             $('#courtLevel').on('change', function() {
                var url = "{{ url('civil-admission/courts') }}" + "/" + this.value;
                $.get(url).done(function( data ) {
                    var select = "";
                    var options = "";
                    select+= "<select class='chose-select form-control'>";
                    options+= "<option value=''>" +"</option>";
                    for(var datum in data){
                        options+= "<option value='" + datum + "'>" + data[datum] +"</option>";
                    }
                    select+= "</select";
                    $("#courtId").empty().append(options);
                });
            });
            
            
           

        });
        
         
        
        
    </script>
@stop