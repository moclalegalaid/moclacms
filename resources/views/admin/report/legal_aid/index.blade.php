@extends('main')

@section('title')
    Legal Aid Report
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    
    <style>
        
        
        .head{
            padding-top: 10px;
            color:  #3a383b;
        }
        
        .well{
           padding-left: 10px;
           padding-right: 10px;
           padding-top: 2px;
           padding-bottom: 2px;
           border-bottom-color: #4682b4;
        }
        
        #table-search thead td {
          background-color: #e42824;
          color: white;
          font-size: 14px;
          font-weight: 600;
          width: 30px;
        }
        
        #table-search {
          color: black;
          font-size: 14px;
        }
    
        #font {
          color: black;
        }
    </style>
    
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight" id="font">
	
    <div class="row" id="font">
        <div class="col-lg-12">
            
         <div class="well">
         <div class="form-inline pull-right head">
           
            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#allCourt">
                <i class="fa fa-file"></i> View All Stations Report
            </button>
             
             <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#specificCourt">
                <i class="fa fa-file"></i> View Specific Desk Station
            </button>
        </div>
         <h3 style="padding:0px;"><img src="{{ URL::to('/images/users/filed1.png')}}" style="width:40px;height:40px;" /> Legal Aid Provided Report</h3>
     </div>
        </div>
    </div>
    
    
    <div class="row">
       
        @if(isset($details))
        
         @if(count($aids) > 0)
            <div class=" pull-right head">
                        <a title="Pending Tasks" href="{{ url('report/legal-aid') }}" class="btn btn-default btn-sm"><i class="fa fa-level-up"></i> Back to Search </a>
                
            </div>
            <h3>Legal Aid Provided @if($station) at {{ $station }} @else at all Desk stations @endif from {{ date('F d Y', strtotime($dateFrom))}} to {{ date('F d Y', strtotime($dateTo))}} </h3>
           <div class="ibox float-e-margins">
           <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-search" >
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Desk Station</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Provided Aid</th>
                        <th>Registered By</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($aids)
                        @foreach($aids as $key => $new)
                            <tr class="gradeX">
                                <td>{{ ++$key }}</td>
                                <td>{{ $new->court_name }}</td>
                                <td>{{$new->register_date }} </td>
                                <td>{{ $new->litigant_name }}</td>
                                <td>{{ $new->aid_name }}</td>
                                <td>{{ $new->registered_by }}</td>
                                <td>
                                    <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                    </a>
                                </td>
                                
                            </tr>
                            <!-- View modal -->
                                
                            <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Desk Station:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->court_name}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Name:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_name}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Address:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_address}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Phone:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_phone}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Email:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_email}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Concern:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_concern}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Provided:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->aid_name}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Decription:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->description}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Registry Information:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                               This legal aid was registered by {{$new->registered_by}} on {{ $new->register_date}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
           </div>
           </div>
            
        @else
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-search" >
                            <tbody>
                                
                                <tr class="gradeX">
                                    <td>No Report data found</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                     
                </div>
               <div class=" pull-left head">
                        <a title="Pending Tasks" href="{{ url('report/legal-aid') }}" class="btn btn-default btn-sm"><i class="fa fa-level-up"></i> Back to Search </a>

                </div>
            </div>
           
            @endif
        @endif
    </div>
    
    

    <div class="modal inmodal fade document" id="allCourt" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('report/legal-aid/allStations') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Search Form</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group" id="dateRange">
                                                    <label class="font-bold">Enter Registration Date Range</label><br/>
                                                    <div class="input-daterange input-group" id="datepicker_one">
                                                        <span class="input-group-addon">From</span>
                                                        <input type="date" class="input-sm form-control" id="startDateOne" name="startDate" />
                                                        <span class="input-group-addon">to</span>
                                                        <input type="date" class="input-sm form-control" id="endDateOne" name="endDate" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row not box">
                                           
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="allCourt" class="btn btn-success"><i class="fa fa-sign-in"></i> Search</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
    
    
    <div class="modal inmodal fade document" id="specificCourt" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('report/legal-aid/specific') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Search Form</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="font-bold">Legal Aid Desk *</label>
                                                    {!! Form::select('station', $legalAidDesk, null, ['class'=>'legalAidDesk form-control', 'placeholder'=>'--Select Station Level--',
													'id'=>'courtLevel', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group" id="dateRange">
                                                    <label class="font-bold">Enter Registration Date Range</label>
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <span class="input-group-addon">From</span>
                                                        <input type="date" class="input-sm form-control" id="startDate" name="startDate" />
                                                        <span class="input-group-addon">to</span>
                                                        <input type="date" class="input-sm form-control" id="endDate" name="endDate" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Search</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Legal Aid'},
                    {extend: 'pdf', title: 'Legal Aid'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
           
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            $('.owners-select').select2({width: "100%" , placeholder:'----Select SO Owner(s)----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});
            
            
             
            
            
            $('#dateRange .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoClose: true,
            selectMonths: true,
            format: 'yyyy-mm-dd',
            clear: false,
            onSet: function(ele) {
                if(ele.select) {
                    this.close();
                }
            },
            closeOnSelect: true,
            onClose: function() {
                document.activeElement.blur();
            }
            });
            
            $('#dateRange .input-daterange').datepicker_one({
                keyboardNavigation: false,
                forceParse: false,
                autoClose: true,
                selectMonths: true,
                format: 'yyyy-mm-dd',
                clear: false,
                onSet: function(ele) {
                    if(ele.select) {
                        this.close();
                    }
                },
                closeOnSelect: true,
                onClose: function() {
                    document.activeElement.blur();
                }
            });

        });
        
         
        
        
    </script>
@stop