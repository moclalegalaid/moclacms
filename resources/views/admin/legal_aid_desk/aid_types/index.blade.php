@extends('main')

@section('title')
    Aid Types
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        #newPost {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">Aid Types</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop

@section('content')
    <div class="row" id="font">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#newPost">
                        <i class="fa fa-file"></i> Add Type
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif
    


<div class="row" id="font">
    
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Aid Types</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped table-hover dataTables-search">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Legal aid type</th>
                                <th>Created by</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($aidtype)
                               @foreach($aidtype as $key => $aidtypes)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    <td>{{ $aidtypes->name }}</td>
                                    <td>{{ $aidtypes->creator_name }}</td>
                                    <td>
                                        <a title="Delete" href="#delete{{$aidtypes->id}}" data-toggle="modal" data-id="{{$aidtypes->id}}" data-target="#delete{{$aidtypes->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="delete{{$aidtypes->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('aid-type/delete',$aidtypes->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Aid Type ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
</div>  

    <div class="modal inmodal fade document" id="newPost" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('desk/aid-type/add') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Add Type</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                            <fieldset>      
                                <div class="row">          
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Name*</label>
                                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="Enter Aid Type" required>
                                        </div>
                                    </div>
                                </div>     
                            </fieldset>    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Submit</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});

        });
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
        
    </script>
@stop