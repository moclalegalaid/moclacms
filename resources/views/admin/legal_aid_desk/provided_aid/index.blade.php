@extends('main')

@section('title')
    Register Aid
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        #newPost {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">Legal Aid Register</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop

@section('content')
    <div class="row" id="font">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#newPost">
                        <i class="fa fa-file"></i> Register Aid
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif
    


<div class="row" id="font">
    
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Provided Aid</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        @if($role == "Desk Officer")
                        <table id="tables" class="table table-striped table-hover dataTables-search">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Desk Station</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Provided Aid</th>
                                <th>Registered By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($aidprovidedstation)
                               @foreach($aidprovidedstation as $key => $new)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    
                                    <td>{{ $new->court_name }}</td>
                                    <td>{{$new->register_date }} </td>
                                    <td>{{ $new->litigant_name }}</td>
                                    <td>{{ $new->aid_name }}</td>
                                    <td>{{ $new->registered_by }}</td>
                                    <td>
                                        <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                        </a>
                                        <a title="Edit" href="#edit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#edit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                <!-- Viewer modal -->
                                
                                
                                <!-- View modal -->
                                
                                <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Desk Station:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->court_name}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Name:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_name}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Address:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_address}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Phone:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_phone}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Email:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_email}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Concern:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_concern}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Provided:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->aid_name}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Decription:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->description}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Registry Information:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                               This legal aid was registered by {{$new->registered_by}} on {{ $new->register_date}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="edit{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/edit',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2><b>Edit Post </b></h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Title*</label>
                                                        <input type="text" class="form-control" name="title" id="title" value="{{ $new->title }}" placeholder="Enter Title" required>
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Category*</label>
                                                        
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55">{{$new->content}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group"><br/>
                                                <label for="image" class="font-bold">New Image Upload</label>
                                                <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                                <img src="#" id="img" width="200px" alt="New Image Goes here"/>
                                            </div>
                                            </div>
                                          </div>

                                                </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update Post</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="delete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/delete',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Legal Aid ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                        @else
                        <table id="tables" class="table table-striped table-hover dataTables-search">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Desk Station</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Provided Aid</th>
                                <th>Registered By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($aidprovided)
                               @foreach($aidprovided as $key => $new)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    
                                    <td>{{ $new->court_name }}</td>
                                    <td>{{$new->register_date }} </td>
                                    <td>{{ $new->litigant_name }}</td>
                                    <td>{{ $new->aid_name }}</td>
                                    <td>{{ $new->registered_by }}</td>
                                    <td>
                                        <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                        </a>
                                        <a title="Edit" href="#edit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#edit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                <!-- Viewer modal -->
                                
                                
                                <!-- View modal -->
                                
                                <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Desk Station:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->court_name}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Name:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_name}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Address:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_address}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Phone:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_phone}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Email:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_email}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Concern:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_concern}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Provided:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->aid_name}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Decription:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->description}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Registry Information:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                               This legal aid was registered by {{$new->registered_by}} on {{ $new->register_date}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="edit{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/edit',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2><b>Edit Post </b></h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Title*</label>
                                                        <input type="text" class="form-control" name="title" id="title" value="{{ $new->title }}" placeholder="Enter Title" required>
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Category*</label>
                                                        
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55">{{$new->content}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group"><br/>
                                                <label for="image" class="font-bold">New Image Upload</label>
                                                <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                                <img src="#" id="img" width="200px" alt="New Image Goes here"/>
                                            </div>
                                            </div>
                                          </div>

                                                </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update Post</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="delete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/delete',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Legal Aid ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
    
</div>  

    <div class="modal inmodal fade document" id="newPost" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('desk/register-aid/add') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Register Legal Aid</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                            <fieldset>
                                        
                                        
                            <div class="row">
                                            
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Name*</label>
                                            <input type="text" class="form-control" name="litigant_name" id="title" value="{{ old('litigant_name') }}" placeholder="Enter Customer Name" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Address*</label>
                                            <input type="text" class="form-control" name="litigant_address" id="title" value="{{ old('litigant_address') }}" placeholder="Enter Customer Address" required>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="row">
                                            
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Age*</label>
                                            <input type="text" class="form-control" name="age" id="title" value="{{ old('age') }}" placeholder="Enter Litigant Age" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Sex*</label>
                                            <select name="sex" class="form-control is-valid" id="exampleSelectGender" onchange="showDiv(this)" required>
                                                <option value="">--Choose one--</option>    
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="row">

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Phone*</label>
                                            <input type="text" class="form-control" name="litigant_phone" id="title" value="{{ old('litigant_phone') }}" placeholder="Enter Customer Phone" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Email*</label>
                                            <input type="text" class="form-control" name="litigant_email" id="title" value="{{ old('litigant_email') }}" placeholder="Enter Customer Email" required>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Litigant Concern*</label>
                                            <textarea name="litigant_concern" id="ta-1" class="form-control col-xs-12" value="{{ old('litigant_concern') }}" placeholder="Enter Content"  rows="2" cols="55"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">Case in court?</label><br/>
                                            <select name="case" class="form-control is-valid" id="exampleSelectGender" onchange="showDiv(this)" required>
                                                <option value="">--Choose one--</option>    
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="hidden_div" style="display:none;">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">Case Reference Number*</label>
                                            <input type="text" class="form-control" name="ref_number" id="title" value="{{ old('ref_number') }}" placeholder="Enter  Case Referrence Number" required>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">Legal Aid Provided*</label>
                                            {!! Form::select('aid_type', $aidtype, null, ['class'=>'owners-select form-control',
                                            'id'=>'owners',  'placeholder'=>'--Select Aid Type--', 'required']) !!}
                                        </div>
                                    </div>
                                    
                                </div>

                              <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Legal Aid Provided Description*</label>
                                            <textarea name="description" id="ta-1" class="form-control col-xs-12" value="{{ old('description') }}" placeholder="Enter Description"  rows="2" cols="55"></textarea>
                                        </div>
                                    </div>
                                </div>
                                        
                            </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Submit</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});

        });


        // Show div based on drop down select
        function showDiv(select){
            if(select.value=="Yes"){
                document.getElementById('hidden_div').style.display = "block";
            } else{
                document.getElementById('hidden_div').style.display = "none";
            }
        } 
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
        
    </script>
@stop