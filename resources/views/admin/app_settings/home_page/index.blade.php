@extends('main')

@section('title')
    Home Page
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        .modal {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">Home Page</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop



@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif

    

<div class="row" id="font">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#backgound">
                <i class="fa fa-file" ></i> Background
                </a>

                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#mission">
                    <i class="fa fa-file"></i> Mission
                </a>

                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#vission">
                    <i class="fa fa-eye"></i> Vission
                </a>

                 <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#how">
                    <i class="fa fa-question-circle"></i> How do I
                </a>

                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#what">
                    <i class="fa fa-question-circle"></i> What Is
                </a>

                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#leaders">
                    <i class="fa fa-users"></i> Management
                </a>

                <a style="color:#293846;" class="btn btn-default btn-lg btn-outline" data-toggle="modal" data-target="#admin">
                    <i class="fa fa-users"></i> Administration
                </a>

            </div>
        </div>
    </div>
   
</div>
    
<div class="row" id="font">
    <div class="col-lg-4">
        @if(count($background) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Background</th>
                                
                            </tr>
                            </thead>
                            @foreach($background as $key => $new)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $new->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#bgedit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#bgedit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#bgdelete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#bgdelete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="bgedit{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/bgedit',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Background </h2></span>
                                            </div>


                                            <cente>

                                            <div class="modal-body">
                                                <fieldset>


                                                <div class="row">

                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label class="font-bold">Content*</label>
                                                                <textarea name="background" class="form-control col-xs-12" value="{{ $new->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $new->content }}</textarea>
                                                                
                                                            </div>
                                                        </div>

                                                </div> 
                                                </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="bgdelete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/bgdelete',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Background ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
    
    
    <div class="col-lg-4">
        @if(count($vission) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Vission</th>
                                
                            </tr>
                            </thead>
                            @foreach($vission as $key => $vis)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $vis->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#vsedit{{$vis->id}}" data-toggle="modal" data-id="{{$vis->id}}" data-target="#vsedit{{$vis->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#vsdelete{{$vis->id}}" data-toggle="modal" data-id="{{$vis->id}}" data-target="#vsdelete{{$vis->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="vsedit{{$vis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/vsedit',$vis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Vission Statement </h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $vis->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $vis->content }}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="vsdelete{{$vis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/vsdelete',$vis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Vission Statement ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
    
    
    <div class="col-lg-4">
        @if(count($mission) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Mission</th>
                                
                            </tr>
                            </thead>
                            @foreach($mission as $key => $mis)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $mis->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#msedit{{$mis->id}}" data-toggle="modal" data-id="{{$mis->id}}" data-target="#msedit{{$mis->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#msdelete{{$mis->id}}" data-toggle="modal" data-id="{{$mis->id}}" data-target="#msdelete{{$mis->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="msedit{{$mis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/msedit',$mis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Mission Statement</h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $mis->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $mis->content }}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update Post</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="msdelete{{$mis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/msdelete',$mis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Mission Statement?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
   
</div>

<div class="row" id="font">
    <div class="col-lg-4">
        @if(count($board) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Legal Aid Board</th>
                                
                            </tr>
                            </thead>
                            @foreach($board as $key => $new)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $new->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#boardgedit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#boardgedit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#boardgdelete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#boardgdelete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="boardgedit{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/adminedit',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Board </h2></span>
                                            </div>


                                            <cente>

                                            <div class="modal-body">
                                                <fieldset>


                                                <div class="row">

                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label class="font-bold">Content*</label>
                                                                <textarea name="background" class="form-control col-xs-12" value="{{ $new->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $new->content }}</textarea>
                                                                
                                                            </div>
                                                        </div>

                                                </div> 
                                                </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="bgdelete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/admindelete',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Board ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
    
    
    <div class="col-lg-4">
        @if(count($reg) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Registrar</th>
                                
                            </tr>
                            </thead>
                            @foreach($reg as $key => $vis)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $vis->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#regedit{{$vis->id}}" data-toggle="modal" data-id="{{$vis->id}}" data-target="#regedit{{$vis->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#regdelete{{$vis->id}}" data-toggle="modal" data-id="{{$vis->id}}" data-target="#regdelete{{$vis->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="regedit{{$vis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/adminedit',$vis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Registrar Statement </h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $vis->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $vis->content }}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="regdelete{{$vis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/admindelete',$vis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Registrar Statement ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
    
    
    <div class="col-lg-4">
        @if(count($areg) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Assistant Registrar</th>
                                
                            </tr>
                            </thead>
                            @foreach($areg as $key => $mis)
                            <tbody>
                                <tr class="gradeX">
                                    <td>{{ $mis->content }}
                                    </td>
                                 </tr>
                                 <tr>
                                    
                                    <td>
                                        <a title="Edit" href="#aregedit{{$mis->id}}" data-toggle="modal" data-id="{{$mis->id}}" data-target="#aregedit{{$mis->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#aregdelete{{$mis->id}}" data-toggle="modal" data-id="{{$mis->id}}" data-target="#aregdelete{{$mis->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="aregedit{{$mis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/adminedit',$mis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit Assistant Registrar Statement</h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $mis->content }}" placeholder="Enter Background"  rows="2" cols="55">{{ $mis->content }}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update Post</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="aregdelete{{$mis->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/admindelete',$mis->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Assistant Registrar Statement?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
        </div>
        
        @endif
    </div>
   
</div>


<div class="row" id="font">
    <div class="col-lg-6">
        @if(count($howStatement) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>How Do I</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            
                            </tr>
                        </tbody>
                        </table>
                        <div class="bs-example">
                            <div class="panel-group" id="accordion">
                            @foreach($howStatement as $index => $faq)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $index }}" ><i class="fa fa-question-circle"></i> {{ $faq->question }}</a>
                                        </h4>
                                    </div>
                                    <div id="collapse-{{ $index }}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>{{ $faq->answer }}</p>
                                            <p>
                                               <a title="Edit" href="#edithow{{$faq->id}}" data-toggle="modal" data-id="{{$faq->id}}" data-target="#edithow{{$faq->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                                </a>
                                                <a title="Delete" href="#deletehow{{$faq->id}}" data-toggle="modal" data-id="{{$faq->id}}" data-target="#deletehow{{$faq->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                                </a> 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                  
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="edithow{{$faq->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/edithow',$faq->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit How do I Statement </h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $faq->question }}" placeholder="Enter Background"  rows="2" cols="55">{{ $faq->question }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $faq->answer }}" placeholder="Enter Background"  rows="2" cols="55">{{ $faq->answer}}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="deletehow{{$faq->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/deletehow',$faq->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete How do I Statement ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                                
                        
                            @endforeach
                            </div>
                            </div>
                    </div>
        </div>
        
        @endif
    </div>
    
    
    
    <div class="col-lg-6">
        @if(count($whatStatement) > 0)
        <div class="ibox float-e-margins">
            
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>What Is</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            
                            </tr>
                        </tbody>
                        </table>
                        <div class="bs-example">
                            <div class="panel-group" id="accordion">
                            @foreach($whatStatement as $key => $faq)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#what-{{ $key }}" ><i class="fa fa-question-circle"></i> {{ $faq->question }}</a>
                                        </h4>
                                    </div>
                                    <div id="what-{{ $key }}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>{{ $faq->answer }}</p>
                                            <p>
                                               <a title="Edit" href="#editwhat{{$faq->id}}" data-toggle="modal" data-id="{{$faq->id}}" data-target="#editwhat{{$faq->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                                </a>
                                                <a title="Delete" href="#deletewhat{{$faq->id}}" data-toggle="modal" data-id="{{$faq->id}}" data-target="#deletewhat{{$faq->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                                </a> 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                  
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="editwhat{{$faq->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/editwhat',$faq->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2>Edit What Is Statement </h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $faq->question }}" placeholder="Enter Background"  rows="2" cols="55">{{ $faq->question }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="background" class="form-control col-xs-12" value="{{ $faq->answer }}" placeholder="Enter Background"  rows="2" cols="55">{{ $faq->answer}}</textarea>
                                                    </div>
                                                </div>

                                        </div> 
                                        </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="deletewhat{{$faq->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/deletewhat',$faq->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete What Is Statement ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                                
                        
                            @endforeach
                            </div>
                            </div>
                    </div>
        </div>
        
        @endif
    </div>
    
    
   
</div>



<div class="row" id="font">
    
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Management Team</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Picture</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Publish Status</th>
                                <th>Date Posted</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($leaders)
                               @foreach($leaders as $key => $new)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    <td>
                                        <a title="View Image" href="#viewer{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#viewer{{$new->id}}"><img src="{{ asset('storage/downloads/'.$new->file) }}" width="60px" height="60px" />
                                        </a>   
                                    </td>
                                    <td>{{ $new->name }}</td>
                                    <td>{{ $new->title }}</td>
                                    <td>
                                        @if($new->publish == "1")
                                        Publised
                                        @else
                                        Not Published
                                        @endif
                                    </td>
                                    <td>{{ $new->date_posted }}</td>
                                    <td>
                                        <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                        </a>
                                        <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                        @if($new->publish == "0")
                                        <a title="Publish" href="#publish{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#publish{{$new->id}}" class="btn btn-primary btn-xs"><span class="fa fa-cloud-upload"></span><span></span>
                                        @endif
                                        </a>
                                    
                                    </td>
                                </tr>
                                <!-- Viewer modal -->
                                
                                <div class="modal inmodal fade" id="viewer{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <img src="{{ asset('storage/downloads/'.$new->file) }}" width="100%" height="100%" />
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                    </div>
                                </div>
                                
                                <!-- View modal -->
                                
                                <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-12">
                                                <img src="{{ asset('storage/downloads/'.$new->file) }}" width="100%" height="100%" />
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Full Name:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->name}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Title:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->title}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Profile:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->profile}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Info:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                @if($new->editor) 
                                                   Last updated by {{$new->editor}} on {{$new->date_edited}}
                                                 @else
                                                   Posted by {{$new->reporter}} on {{$new->date_posted}} 
                                                @endif
                                                <br/>
                                                @if($new->publish == "1")
                                                Published by {{$new->publisher}} on {{$new->date_published}}
                                                @else
                                                Not Published <small> ( Can not be seen on website )</small>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                               <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="delete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/delete-mgt',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                <!-- Publish modal -->
                                
                                   <div class="modal inmodal fade" id="publish{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('home-page/publish-mgt',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Publish Picture ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Publish</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
</div>  
    

<div class="modal inmodal fade document" id="backgound" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/background') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Background</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="background" class="form-control col-xs-12" value="{{ old('background') }}" placeholder="Paste or type  background here"  rows="2" cols="55"></textarea>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>



<div class="modal inmodal fade document" id="mission" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/mission') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Mission</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="background" class="form-control col-xs-12" value="{{ old('background') }}" placeholder="Paste or type  mission statement here"  rows="2" cols="55"></textarea>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>

    

<div class="modal inmodal fade document" id="vission" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/vission') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Vission</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="background" class="form-control col-xs-12" value="{{ old('background') }}" placeholder="Paste or type  vission here"  rows="2" cols="55"></textarea>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>

    

<div class="modal inmodal fade document" id="how" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/how') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">How do I (Frequently asked questions)</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="question" class="form-control col-xs-12" value="{{ old('question') }}" placeholder="Paste or type  question here"  rows="2" cols="55"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="answer" class="form-control col-xs-12" value="{{ old('answer') }}" placeholder="Paste or type  answer here"  rows="2" cols="55"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>


    
<div class="modal inmodal fade document" id="what" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/what') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">What Is (Frequently asked questions)</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                    <fieldset>
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="question" class="form-control col-xs-12" value="{{ old('question') }}" placeholder="Paste or type  question here"  rows="2" cols="55"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="answer" class="form-control col-xs-12" value="{{ old('answer') }}" placeholder="Paste or type  answer here"  rows="2" cols="55"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>


<div class="modal inmodal fade document" id="leaders" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/leaders') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Management</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">

                            <fieldset>  
                                      <div class="row">
                                                            
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="font-bold">Full Name*</label>
                                                            <input type="text" class="form-control" name="name" id="title" value="{{ old('name') }}" placeholder="Enter Full Name" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                    <div class="form-group">
                                                            <label class="font-bold">Title/Designation*</label>
                                                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Enter Title" required>
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                            <div class="row">

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="font-bold">Group*</label>
                                                            {!! Form::select('group', $group, null, ['class'=>'owners-select form-control',
                                                            'id'=>'owners',  'placeholder'=>'--Select Goup--', 'required']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="form-group">
                                                            <label class="font-bold">Seniority*</label>
                                                            {!! Form::select('seniority', $seniority, null, ['class'=>'owners-select form-control',
                                                            'id'=>'owners',  'placeholder'=>'--Select Seniority--', 'required']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label class="font-bold">Profile/Description*</label>
                                                            <textarea name="profile" class="form-control col-xs-12" value="{{ old('profile') }}" placeholder="Enter Profile"  rows="2" cols="55"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group"><br/>
                                                    <label for="image" class="font-bold">Image Upload</label>
                                                        
                                                    <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                                    <img src="#" id="img" width="200px" alt="Image Goes here" />
                                                </div>
                                                </div>
                                            </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade document" id="admin" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('home-page/admin') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Administration</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">

                            <fieldset>  
                                       <div class="row">

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label class="font-bold">Group*</label>
                                                            {!! Form::select('group', $group, null, ['class'=>'owners-select form-control',
                                                            'id'=>'owners',  'placeholder'=>'--Select Goup--', 'required']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label class="font-bold">Content*</label>
                                                            <textarea name="content" class="form-control col-xs-12" value="{{ old('profile') }}" placeholder="Enter Profile"  rows="2" cols="55"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Save</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>


</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});

        });
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
        
    </script>
@stop