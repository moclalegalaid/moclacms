@extends('main')

@section('title')
    Home Page
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">


    <!-- include libraries summernote -->

    <link href="{{ asset('css2/summernote/summernote-lite.min.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        .modal {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">Services</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop


@section('content')
    <div class="row" id="font">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#newPost">
                        <i class="fa fa-file"></i> Add Service
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif

    <div class="row" id="font">
    
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Services</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                <table id="tables" class="table table-striped">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Service</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($service)
                       @foreach($service as $key => $new)
                        <tr class="gradeX">
                            <td>{{ ++$key }}
                            </td>
                            
                            <td>{{ $new->status }}</td>
                            <td>{{ $new->content }}</td>
                            <td>
                                <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                </a>
                            
                            </td>
                        </tr>

                        
                       
                        
                        
                       <!-- Delete modal -->
                        
                           <div class="modal inmodal fade" id="delete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                            <form method="POST" id="edit" action="{{ url('service/delete-srv',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <span><h2>Delete Service ?</h2></span>
                                    </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                            <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                        </div>
                                         {{ csrf_field() }}
                                    </div>
                                </div>
                             </form>
                            </div>
                        
                        @endforeach
                @endif
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>  

<div class="modal inmodal fade document" id="newPost" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
 <form method="POST" id="form" action="{{ url('service/add-srv') }}" class="wizard-big" enctype="multipart/form-data" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 class="modal-title">Add Service</h2>
    </div>
    
    <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                            <fieldset>
                            <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Service Type*</label>
                                            <select class="form-control" id="options" name="type">
                                                <option value="" disabled selected>Select type</option>
                                                <option value="Registration">Registration</option>
                                                <option value="Accreditation">Accreditation</option>
                                                <option value="Capacity Building">Capacity Building</option>
                                                <option value="Legal Aid Services">Legal Aid Services</option>
                                            </select>
                                        </div>
                                    </div>
                                    

                                </div>   
                                
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Service Sub Type*</label>
                                            <select class="form-control" id="choices" name="subtype">
                                                <option value="" disabled selected>Please select service</option>
                                            </select>
                                        </div>
                                    </div>
                                    

                                </div>  
                                        
                            
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="font-bold">Content*</label>
                                        <textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group"><br/>
                                    <label for="image" class="font-bold">File  Upload (If any)</label>
                                        
                                    <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                    
                                </div>
                                </div>
                              </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
   
    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
        <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Submit</button>
    </div>
    {{ csrf_field() }}

    </form>
</div>
</div>
</div>   


</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Summernote javascript -->
    <script src="{{ asset('js/summernote/summernote-lite.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});



            $('#summernote').summernote({
                placeholder: 'Servise Description',
                tabsize: 1,
                height: 120,
                toolbar: [
                ['style', ['style']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });

        });
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }


        // Map your choices to your option value
        var lookup = {
        'Registration': ['Registration of Organisation', 'Registration of Individual Paralegal'],
        'Accreditation': ['Development of Training Carriculum & Manuals', 'Supervision of Training of Paralegals'],
        'Capacity Building': ['Training to Justice Actors', 'Training to Legal Aid Providers'],
        'Legal Aid Services': ['Legal Aid Services by LAPs', 'Legal Aid Service by Oder of Court', 'Legal Aid Service in Detention Services'],
        };

        // When an option is changed, search the above for matching choices
        $('#options').on('change', function() {
        // Set selected option as variable
        var selectValue = $(this).val();

        // Empty the target field
        $('#choices').empty();
        
        // For each chocie in the selected option
        for (i = 0; i < lookup[selectValue].length; i++) {
            // Output choice in the target field
            $('#choices').append("<option value='" + lookup[selectValue][i] + "'>" + lookup[selectValue][i] + "</option>");
        }
        });
        
    </script>
@stop