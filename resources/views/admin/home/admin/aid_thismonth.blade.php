@extends('main')

@section('title')
    Provided Aid
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        #newPost {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">Legal Aid Provided in {{ $thisMonth }}</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop


@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif
    


<div class="row" id="font">
    
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Provided Aid</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped table-hover dataTables-search">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Desk Station</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Provided Aid</th>
                                <th>Registered By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($legalAidThisMonth)
                               @foreach($legalAidThisMonth as $key => $new)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    
                                    <td>{{ $new->court_name }}</td>
                                    <td>{{$new->register_date }} </td>
                                    <td>{{ $new->litigant_name }}</td>
                                    <td>{{ $new->aid_name }}</td>
                                    <td>{{ $new->registered_by }}</td>
                                    <td>
                                        <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                        </a>
                                        <a title="Edit" href="#edit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#edit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                    
                                    </td>
                                </tr>
                                <!-- Viewer modal -->
                                
                                
                                <!-- View modal -->
                                
                                <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Desk Station:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->court_name}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Name:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_name}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Address:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_address}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Phone:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_phone}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Email:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_email}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Litigant Concern:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->litigant_concern}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Provided:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->aid_name}}
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Legal Aid Decription:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->description}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Registry Information:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                               This legal aid was registered by {{$new->registered_by}} on {{ $new->register_date}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                               

                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
</div>  

    
</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});

        });


        // Show div based on drop down select
        function showDiv(select){
            if(select.value=="Yes"){
                document.getElementById('hidden_div').style.display = "block";
            } else{
                document.getElementById('hidden_div').style.display = "none";
            }
        } 
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
        
    </script>
@stop