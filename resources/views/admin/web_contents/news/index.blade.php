@extends('main')

@section('title')
    Post News
@stop

@section('page-specific-css')

	<link href="{{ asset('css2/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/nouslider/jquery.nouislider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/dualListbox/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/steps/jquery.steps.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/radiocheck.css') }}" rel="stylesheet">

    <style>
        
        #tables th {
          background-color: slategrey;
          color: white;
        }
        #font {
          color: black;
        }
        #newPost {
          color: black;
        }
    </style>


@stop
@section('breadcrumbs')
    <span><h2 id="font">News</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop

@section('content')
    <div class="row" id="font">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#newPost">
                        <i class="fa fa-file"></i> New Post
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
<div class="wrapper wrapper-content animated fadeInRight">
	@if($errors->any()) 
		<div class="alert alert-danger alert-dismissable">
			<button class="button" class="close" data-dismiss="alert" aria-hiden="true">x</button>
			@foreach($errors->all() as $error)
				<a href="#" class="alert-link">ERROR! :</a> {{ $error }}
			@endforeach
		</div>
	@endif
    


<div class="row" id="font">
    
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>News</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="tables" class="table table-striped table-hover dataTables-search">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Media</th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Publish Status</th>
                                <th>Date Posted</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($news)
                               @foreach($news as $key => $new)
                                <tr class="gradeX">
                                    <td>{{ ++$key }}
                                    </td>
                                    <td>
                                        @if($new->status == "video")
                                        <a title="Video" href="#viewer{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#viewer{{$new->id}}">
                                        <iframe width="100px" height="60px" src="https://www.youtube.com/embed/{{$new->video_id}}">
                                        </iframe>
                                        </a>  
                                        @elseif($new->status == "audio")
                                        <a title="Audio" href="#viewer{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#viewer{{$new->id}}"><img src="{{ asset('storage/downloads/'.$new->file_name) }}" width="100px" height="60px" />
                                        </a>  
                                        @else
                                        <a title="View Image" href="#viewer{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#viewer{{$new->id}}"><img src="{{ asset('storage/downloads/'.$new->file_name) }}" width="100px" height="60px" />
                                        </a> 
                                        @endif  
                                    </td>
                                    <td>{{ $new->title }}</td>
                                    <td>{{ substr($new->content, 0, 50) }} .....</td>
                                    <td>
                                        @if($new->publish == "1")
                                        Publised
                                        @else
                                        Not Published
                                        @endif
                                    </td>
                                    <td>{{ $new->date_posted }}</td>
                                    <td>
                                        <a title="View" href="#view{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#view{{$new->id}}" class="btn btn-info btn-xs"><span class="fa fa-eye-slash"></span><span></span>
                                        </a>
                                        <a title="Edit" href="#edit{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#edit{{$new->id}}" class="btn btn-success btn-xs"><span class="fa fa-edit"></span>
                                        </a>
                                        <a title="Delete" href="#delete{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#delete{{$new->id}}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span><span></span>
                                        </a>
                                        @if($new->publish == "0")
                                        <a title="Publish" href="#publish{{$new->id}}" data-toggle="modal" data-id="{{$new->id}}" data-target="#publish{{$new->id}}" class="btn btn-primary btn-xs"><span class="fa fa-cloud-upload"></span><span></span>
                                        @endif
                                        </a>
                                    
                                    </td>
                                </tr>
                                <!-- Viewer modal -->
                                
                                <div class="modal inmodal fade" id="viewer{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <img src="{{ asset('storage/downloads/'.$new->file_name) }}" width="100%" height="100%" />
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                    </div>
                                </div>
                                
                                <!-- View modal -->
                                
                                <div class="modal inmodal fade" id="view{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                    <div class="modal-body">

                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>File:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                <img src="{{ asset('storage/downloads/'.$new->file_name) }}" width="100%" height="100%" />
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Category:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->status}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Title:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->title}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Content:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$new->content}}
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-bottom: 20px">
                                            <div class="col-sm-2">
                                                <h5>Information:</h5>
                                            </div>
                                            <div class="col-sm-10">
                                                @if($new->editor) 
                                                   Last updated by {{$new->editor}} on {{$new->date_edited}}
                                                 @else
                                                   Posted by {{$new->reporter}} on {{$new->date_posted}} 
                                                @endif
                                                <br/>
                                                @if($new->publish == "1")
                                                Published by {{$new->publisher}} on {{$new->date_published}}
                                                @else
                                                Not Published <small> ( Can not be seen on website )</small>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer"><button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                <!-- Edit modal -->
                                
                                   <div class="modal inmodal fade" id="edit{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/edit',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <span><h2><b>Edit Post </b></h2></span>
                                            </div>


                                            <cente>

                                    <div class="modal-body">
                                        <fieldset>
                                        
                                        
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Title*</label>
                                                        <input type="text" class="form-control" name="title" id="title" value="{{ $new->title }}" placeholder="Enter Title" required>
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Category*</label>
                                                        {!! Form::select('category', $category, null, ['class'=>'owners-select form-control',
                                                        'id'=>'owners',  'placeholder'=>'--Select Category--', 'required']) !!}
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="font-bold">Content*</label>
                                                        <textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55">{{$new->content}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group"><br/>
                                                <label for="image" class="font-bold">New Image Upload</label>
                                                <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                                <img src="#" id="img" width="200px" alt="New Image Goes here"/>
                                            </div>
                                            </div>
                                          </div>

                                                </fieldset>

                                                </div>
                                                        </cente>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-sign-in"></i> Update Post</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                 
                                <!-- Delete modal -->
                                
                                   <div class="modal inmodal fade" id="delete{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/delete',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Delete Post ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Delete</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>

                                <!-- Publish modal -->
                                
                                   <div class="modal inmodal fade" id="publish{{$new->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                    <form method="POST" id="edit" action="{{ url('news/publish',$new->id) }}" class="wizard-big" enctype="multipart/form-data" >
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span><h2>Publish Post ?</h2></span>
                                            </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="specific" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Publish</button>
                                                </div>
                                                 {{ csrf_field() }}
                                            </div>
                                        </div>
                                     </form>
                                    </div>
                            @endforeach
                        @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    
</div>  

    <div class="modal inmodal fade document" id="newPost" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
         <form method="POST" id="form" action="{{ url('news/submit') }}" class="wizard-big" enctype="multipart/form-data" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title">Post News</h2>
            </div>
            
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                            <fieldset>
                                        
                                        
                            <div class="row">
                                            
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Title*</label>
                                            <input type="text" class="form-control" name="title" id="title" value="{{ old('kpiName') }}" placeholder="Enter Title" required>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Category*</label>
                                            {!! Form::select('category', $category, null, ['class'=>'owners-select form-control',
                                            'id'=>'owners',  'placeholder'=>'--Select Category--', 'required']) !!}
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Content*</label>
                                            <textarea name="content" id="ta-1" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55"></textarea>
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group"><br/>
                                    <label for="image" class="font-bold">Image Upload</label>
<!--
                                      <input id="upload" type="file" class="form-control" name="cat_image">
                                      <img src="#" id="category-img-tag" width="200px" />   for preview purpose 
-->
                                        
                                    <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                    <img src="#" id="img" width="200px" alt="Image Goes here" />
                                </div>
                                </div>
                              </div>
                                        
                                    </fieldset>

                                    
                            </div>
                        </div>
                </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" id="specific" class="btn btn-danger"><i class="fa fa-sign-in"></i> Submit</button>
            </div>
            {{ csrf_field() }}

            </form>
        </div>
    </div>
</div>
</div>
@stop
@section('page-specific-js')

    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/steps/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/validate/jquery.validate.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/flot/jquery.flot.time.js') }}"></script>


    <script src="{{ asset('assets/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.dataTables-search').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'KPI Results Framework'},
                    {extend: 'pdf', title: 'KPI Results Framework'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            
            
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('A Case Management System', 'Welcome to JSDS+');

            }, 1300);
            


            $(".select2_demo_2").select2({
                placeholder: "Select Nature of Case",
                width: '100%'
            });

            $('.chosen-select').select2({width: "100%"});
            //$('.owners-select').select2({width: "100%" , placeholder:'----Select Category----'});

            $('#filingDate1').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoClose: true,
                format: 'dd-mm-yyyy'
            });
            
            
            $('.cancelBill').on('click', function (event) {
                event.preventDefault();
                var id =  $(this).data('id');
                var postUrl = "{{ url('bill-cancel') }}" + "/cancel/" + id;
                $('#billingForm').attr('action', postUrl);
			});

        });
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
        
    </script>
@stop