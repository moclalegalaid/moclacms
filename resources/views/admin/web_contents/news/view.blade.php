@extends('main')

@section('title')
    @parent
    KPI List
@stop

@section('page-specific-css')
    <!-- FooTable -->
    <link href="{{ asset('css2/plugins/footable/footable.core.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css2/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">

    <style>
        
        #targets th {
          background-color: slategrey;
          color: white;
        }
    </style>
    
@stop

@section('breadcrumbs')
<span><h2>Key Performance Indicator View</h2></span>
    {{ Breadcrumbs::render('civil-registers') }}
@stop

@section('content')
    <div class="row">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View KPI List" href="{{ url('kpi-list') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        KPI List</a>
                </div>
            </div>
    </div>
@stop

@section('raw-content')

    <div class="row">
        
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a style="font-size:14px;" data-toggle="collapse" data-parent="#accordion" href="#details"><i class="fa fa-info-circle"></i>  Key Performance Indicator Details<small> (Click to open/close pane)</small></a>
                                            </h5>
                                        </div>
                                        <div id="details" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                
                                            <div class="col-lg-12">
                                                <div class="wrapper wrapper-content animated fadeInUp">
                                                    <div class="ibox">
                                                        <div class="ibox-content">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="m-b-md">
                                                                        <h2 style="color:#1448A0;"><b>{{ $indicators->owner }}- 
                                                                            @if($indicators->kpi_type == "Top Line")TL
                                                                            @else
                                                                                 KPI 
                                                                            @endif
                                                                            {{ $indicators->kpi_number }}: {{ $indicators->name }}</b></h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12"> 
                                                                <div>
                                                                    <table class="table table-bordered" style="font-size:13px;color:black">
                                                                    <tbody>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>KPI Definition</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                              {{ $indicators->definition }} 
                                                                             <a href="{{ url('kpi-dictionary') }}">
                                                                                Read more...</a>
                                                                            </div>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Indicator Type</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $indicators->kpi_type }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Category</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $indicators->category }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Base Line</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                {{ $indicators->base_line }} {{ $indicators->base_line_symbol }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Base Line Description</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div> {{ $indicators->base_line_desc }} 
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Indicator Target</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" data-toggle="modal" data-id="{{$indicators->id}}"  data-target="#" class="btn btn-white btn-xs pull-right">Last Update: {{ $indicators->target_last_update }}</a>
                                                                            <div>
                                                                                <table id="targets" class="table table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Year</th>
                                                                                        <th>JSP Target</th>
                                                                                        <th>CCP Target</th>
                                                                                        <th>Description</th>
                                                                                        <th>Achievement</th>
                                                                                        <th>% Achievement</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                     <tbody>
                                                                                        @foreach($indicators->kpiTargets as $targets)
                                                                                         <tr>
                                                                                            <td>{{ $targets->target_time }}</td>
                                                                                             <td>{{ $targets->target_value }}</td>
                                                                                             <td>
                                                                                                  @if(!$targets->ccp_target_value)
                                                                                                    N/A
                                                                                                    @else
                                                                                                     {{ $targets->ccp_target_value }}
                                                                                                    @endif
                                                                                             </td>
                                                                                             <td>
                                                                                                 @if(!$targets->target_description)
                                                                                                    N/A
                                                                                                    @else
                                                                                                     {{ $targets->target_description }}
                                                                                                    @endif
                                                                                             </td>
                                                                                             <td>
                                                                                                 @if(!$targets->achievement)
                                                                                                    No data
                                                                                                    @else
                                                                                                     {{ $targets->achievement }}
                                                                                                    @endif
                                                                                             </td>
                                                                                             <td>
                                                                                                 @if(!$targets->percentage_achievement)
                                                                                                    No data
                                                                                                    @else
                                                                                                     {{ $targets->percentage_achievement }}
                                                                                                    @endif
                                                                                                 
                                                                                            </td>
                                                                                         </tr>
                                                                                        @endforeach
                                                                                    </tbody>
                                                                                        
                                                                                </table>

                                                                            </div>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Current Status</strong>
                                                                            </div>
                                                                        </td>
                                                                            <td>
                                                                            <a href="#" data-toggle="modal" data-id="{{$indicators->id}}"  data-target="#" class="btn btn-white btn-xs pull-right">Last Update: {{ $indicators->status_last_update }}</a>
                                                                            <div>
                                                                                <table id="targets" class="table table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Actual Achievement</th>
                                                                                        <th>% Achievement</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                     <tbody>
                                                                                         <tr>
                                                                                            <td>{{ $indicators->actual_achievement }}</td>
                                                                                             <td>{{ $indicators->percentage_achievement }}</td>
                                                                                         </tr>
                                                                                    </tbody>
                                                                                        
                                                                                </table>

                                                                            </div>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Unit of Measure</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               {{ $indicators->unit_measure }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Data Collection Frequency</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                               {{ $indicators->frequency }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>SO Owner</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                 {{ $indicators->owner }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                        
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Supported by</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                 {{ $indicators->owner }}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                        
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Attachements</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                
                                                                                <table id="targets" class="table table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Date/Year</th>
                                                                                        <th>Description</th>
                                                                                        <th>Attachement</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                     <tbody>
                                                                                        @foreach($indicators->kpiStatus as $status)
                                                                                         <tr>
                                                                                            <td>{{ $status->created_year }}</td>
                                                                                             <td>{{ $status->evidence_desc }}</td>
                                                                                             <td> @if(!$status->evidence_id)
                                                                                                    N/A
                                                                                                    @else
                                                                                                 <a href="#document{{$status->evidence_id}}"  data-toggle="modal" data-id="{{ $status->evidence_id }}" data-name="{{ $status->evidence_id }}" data-target="#document{{$status->evidence_id}}" id="documentView">
                                                <span>
                                                  <img alt="image" style="height:50px;width:50px;" class="img-square" src="{{ asset('images/users/attachment.png') }}" />  
                                                </span>
                                                </a>
                                                                                                    @endif
                                                                                             </td>   
                                                                                         </tr>
                                                                                         
                                        <div class="modal inmodal fade" id="document{{$status->evidence_id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                  <img src="{{ asset('storage/downloads/'.$status->evidence_file) }}" width="100%" height="500px" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                                                                        @endforeach
                                                                                    </tbody>
                                                                                        
                                                                                </table>
                                                                                 
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                        
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <strong>Narrative Reports</strong>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div>
                                                                                 
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    
                                                                    </tbody>
                                                                </table>
                                                                    
                                                                    
                                                 <div class="modal inmodal fade" id="updateTarget{{$indicators->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                                    <form method="POST" id="formSms" action="{{ url('court-bills/sendBill') }}" class="wizard-big" enctype="multipart/form-data" >
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <span><strong>Update KPI Target</strong></span>
                                                            </div>
                                                            
                                                        
                                                            <cente>
                                                    
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <label class="font-bold">Target Value</label>
                                                            <input type="text" class="form-control" name="value" id="value" value="{{ old('value') }}" placeholder="Enter Target Value" required>
                                                        </div>
                                                        <div class="row">
                                                            <label class="font-bold">Time (Year)</label>
                                                            <input type="text" class="form-control" name="year" id="value" value="{{ old('year') }}" placeholder="Enter Target Year" required>
                                                        </div>
                                                       
                                                    </div>
                                                            </cente>
                                                    <div class="modal-footer">
                                                        
                                                        
                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Update</button>
                                                    </div>
                                                     {{ csrf_field() }}
                                                </div>
                                            </div>
                                         </form>
                                        </div>
                                                                    
                                                                </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
 
@stop

@section('modal')
    {{--@include('advocates.criminal.delete')--}}
    {{--@include('assignments.criminal.delete')--}}
    {{--@include('complainants.criminal.delete')--}}
    {{--@include('proceedings.criminal.delete')--}}
    {{--@include('respondents.criminal.delete')--}}
@stop

@section('page-specific-js')
    <!-- FooTable -->

    <script src="{{ asset('assets/js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('assets/js/plugins/footable/footable.all.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
@stop
