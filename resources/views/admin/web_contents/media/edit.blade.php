@extends('main')

@section('title')
    @parent
    KPI List
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Key Performance Indicators Edit</h2></span>
    {{ Breadcrumbs::render('case-registration') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View KPI List" href="{{ url('kpi-list') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        KPI List</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::open(['url'=>url('kpi-list/edit',$indicators->id), 'class'=>'wizard-big', 'id'=>'userForm', 'role'=>'form']) !!}
                        
                            <div class="row">
                                            
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Name*</label>
                                            <input type="text" class="form-control" name="kpiName" id="kpiName" value="{{ $indicators->name }}" placeholder="Enter KPI Name" required>
                                        </div>
                                    </div>


                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">S-O Owner(s)*</label>
                                            {!! Form::select('owners', $owners, $indicators->owner, ['class'=>'owners-select form-control',
                                            'id'=>'owners',  'placeholder'=>'--Select Owner--', 'required']) !!}
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">Unit of Measure*</label>
                                            {!! Form::select('measure', $measure, $indicators->unit_measure, ['class'=>'form-control',
                                            'id'=>'measure', 'placeholder'=>'--Select Unit of Measure--', 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Definition*</label>
                                            <textarea id="definition" class="form-control" name="definition" rows="4"
                                                placeholder="Enter KPI Definition" required="">{{ $indicators->definition }}
                                            </textarea>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Base Line*</label>
                                            <input type="text" class="form-control" name="baseLine" id="baseLine" value="{{ $indicators->base_line }}" placeholder="Enter KPI Base Line" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Base Line Symbol*</label>
                                            <input type="text" class="form-control" name="baseLineSymbol" id="baseLineSymbol" value="{{ $indicators->base_line_symbol }}" placeholder="Enter KPI Base Line Symbol" >
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Base Line Description*</label>
                                            <input type="text" class="form-control" name="baseLineDesc" id="baseLineDesc" value="{{ $indicators->base_line_desc }}" placeholder="Enter KPI Base Line Description" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                  <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Name (WB-CCP)</label>
                                            <input type="text" class="form-control" name="wbName" id="wbName" value="{{$indicators->wb_kpi_name }}" placeholder="Enter CCP KPI Name" >
                                        </div>
                                    </div>

                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Type*</label>
                                            {!! Form::select('type', $type, $indicators->kpi_type, ['class'=>'form-control',
                                            'id'=>'type', 'placeholder'=>'--Select KPI Type--', 'required']) !!}
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Category*</label>
                                            {!! Form::select('category', $category, $indicators->category, ['class'=>'form-control',
                                            'id'=>'type', 'placeholder'=>'--Select KPI Category--', 'required']) !!}
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Data Collection Frequency*</label>
                                            {!! Form::select('frequency', $frequency, $indicators->frequency, ['class'=>'form-control',
                                            'id'=>'frequency', 'placeholder'=>'--Select Collection Frequency--', 'required']) !!}
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">Strategic Objective (WB-CCP)</label>
                                            {!! Form::select('strategic_obective', $strategic_object, $indicators->strategic_objective, ['class'=>'form-control',
                                            'id'=>'frequency', 'placeholder'=>'--Select Strategic Objective--']) !!}
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Class*</label>
                                            {!! Form::select('kpi_class', $kpi_class, $indicators->kpi_class, ['class'=>'form-control',
                                            'id'=>'frequency', 'placeholder'=>'--Select KPI Class--', 'required']) !!}
                                        </div>
                                    </div>

                                </div>
                        
                                <div class="row">

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Result Part (WB-CCP)</label>
                                            {!! Form::select('result_part', $result_part, $indicators->result_part, ['class'=>'form-control',
                                            'id'=>'frequency', 'placeholder'=>'--Select Result Part--',]) !!}
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Result Type (WB-CCP)</label>
                                            {!! Form::select('result_type', $result_type, $indicators->result_type, ['class'=>'form-control',
                                            'id'=>'frequency', 'placeholder'=>'--Select Result Type--', ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="font-bold">KPI Number*</label>
                                            <input type="text" class="form-control" name="kpiNumber" id="kpiNumber" value="{{ $indicators->kpi_number }}" placeholder="Enter KPI Number" required>
                                        </div>
                                    </div>
                                    
                                </div>
                        
                        <button type="submit" class="btn btn-primary" id="addUserButton">Save Changes</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.chosen-select').select2({width: "100%"});
            $('.owners-select').select2({width: "100%" , placeholder:'--Select Owner(s)--'});
            $('.salute-select').select2({width: "100%" , placeholder:'Select 1st Salutation'});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@stop
