@extends('main')

@section('title')
    Post News
@stop

@section('page-specific-css')
    <link href="{{ asset('css2/plugins/iCheck/custom.css')  }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css2/plugins/select2/select2.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    <span><h2>Post News</h2></span>
    {{ Breadcrumbs::render('news') }}
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-sm-12">
                    <a title="View KPI List" href="{{ url('news') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye-slash"></i>
                        Back</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('raw-content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <form method="POST" id="form" action="{{ url('news/submit') }}" class="wizard-big" enctype="multipart/form-data" id="userForm" >
                        
                            <div class="row">
                                            
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Title*</label>
                                            <input type="text" class="form-control" name="title" id="title" value="{{ old('kpiName') }}" placeholder="Enter Title" required>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Category*</label>
                                            {!! Form::select('category', $category, null, ['class'=>'owners-select form-control',
                                            'id'=>'owners',  'placeholder'=>'--Select Category--', 'required']) !!}
                                        </div>
                                    </div>
                                    

                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="font-bold">Content*</label>
                                            <textarea name="content" class="form-control col-xs-12" value="{{ old('content') }}" placeholder="Enter Content"  rows="2" cols="55"></textarea>
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group"><br/>
                                    <label for="image" class="font-bold">Image Upload</label>
<!--
                                      <input id="upload" type="file" class="form-control" name="cat_image">
                                      <img src="#" id="category-img-tag" width="200px" />   for preview purpose 
-->
                                        
                                    <input name="file" type='file' id="upload" onChange="readURL(this);"/>
                                    <img src="#" id="img" width="200px" alt="Image Goes here" />
                                </div>
                                </div>
                              </div>
                        
                        <button type="submit" class="btn btn-primary" id="addUserButton">Post News</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-specific-js')
    <script src="{{ asset('assets/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.chosen-select').select2({width: "100%"});
            $('.owners-select').select2({width: "100%" , placeholder:'--Select Owner(s)--'});
            $('.salute-select').select2({width: "100%" , placeholder:'Select 1st Salutation'});

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
        
//        function readURL(input) {
//            if (input.files && input.files[0]) {
//                var reader = new FileReader();
//
//                reader.onload = function (e) {
//                    $('#category-img-tag').attr('src', e.target.result);
//                }
//
//                reader.readAsDataURL(input.files[0]);
//            }
//        }
//
//        $("#cat_image").change(function(){
//            readURL(this);
//        });
        
        
        function readURL(input){
         var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }else{
             $('#img').attr('src', '/assets/images/nopreview.jpeg');
        }
        }
    </script>
@stop
